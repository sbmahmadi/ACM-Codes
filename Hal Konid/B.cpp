/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0);
#define trace(x) //cerr << #x << ": " << x << endl;
#define traceV(x,t) //cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl;
#define _ << " :: " <<
#define allof(x) (x).begin(),(x).end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

vector<int> h;
vector<ii> par;

int findpar(int u) {
	if(par[u].first == u)
		return u;
	return findpar(par[u].first);
}

int f(int u, int v) {
	if(findpar(u) != findpar(v))
		return -2;
	vi v1, v2;
	int uu = u;
	trace("FIND");
	while(par[uu].first != uu) {
		v1.push_back(par[uu].second);
		uu = par[uu].first;
	}
	int vv = v;
	while(par[vv].first != vv) {
		v2.push_back(par[vv].second);
		vv = par[vv].first;
	}
	traceV(v1, int);
	traceV(v2, int);
	int k;
	while(v1.size() && v2.size() && v1.back() == v2.back())
		v1.pop_back(),
		v2.pop_back();
	int mx;
	if(v1.empty())
		mx = v2.back();
	else if(v2.empty())
		mx = v1.back();
	else
		mx = max(v1.back(), v2.back());
	return mx;
}


void Union(int u, int v, int ind) {
	int pu = findpar(u);
	int pv = findpar(v);
	if(pu == pv)
		return;
	if(h[pu] < h[pv])
		par[pu] = {pv, ind};
	else if(h[pv] < h[pu])
		par[pv] = {pu, ind};
	else {
		par[pu] = {pv, ind};
		h[pv]++;
	}
}

int main()
{
	fastio();
	int t;
	cin >> t;
	while(t--) {
		int n,m;
		cin >> n >> m;
		h = vector<int>(n,1);
		par = vector<ii>(n);
		For(i,0,n)
			par[i] = {i,-1};
		For(i,0,m) {
			int c,u,v;
			cin >> c >> u >> v; u--; v--;
			if(c == 1) {
				trace(1)
				Union(u,v,i);
				trace(2)
			}
			else{
				trace(3)
				cout << f(u,v) + 1 << endl;
				trace(4)
			}
		}
	}
}