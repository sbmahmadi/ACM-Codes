/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0)
#define trace(x) cerr << #x << ": " << x << endl
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl
#define _ << " :: " <<
#define allof(x) (x).begin(),(x).end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

struct Edge
{
	int v;
	int c;
	int w;
};

bool djikstra(vector<vector<Edge>>& graph, int wis, int k) {
	set<ii> pq;
	pq.insert({0,0});
	vector<int> dist = vector<int>(graph.size(),1e9); dist[0] = 0;
	vector<bool> visit = vector<bool>(graph.size(),0);
	while(!pq.empty()) {
		ii me = *pq.begin(); pq.erase(pq.begin());
		int u = me.second;
		int cost = me.first;
		if(visit[u])
			continue;
		visit[u] = true;
		For(i,0,graph[u].size()) {
			Edge e = graph[u][i];
			if(dist[e.v] > dist[u] + e.c && e.w <= wis) {
				dist[e.v] = dist[u] + e.c;
				pq.insert({dist[e.v], e.v});
			}
		}
	}
	return dist[graph.size() - 1] < k;
}

int solve(vector<vector<Edge>>& graph, int k) {
	int beg = 0;
	int end = 1e9;
	bool found = false;
	int mid;
	For(i,0,50) {
		mid = (beg + end) / 2;
		
		if(djikstra(graph, mid, k)) {
			found = true;
			end = mid;
		}
		else {
			beg = mid + 1;
		}
	}
	if(!found) {
		return -1;
	}
	return mid;
}

int main()
{
	fastio();
	int t;
	cin >> t;
	while(t--) {
		int n, m, k;
		cin >> n >> m >> k;
		vector<vector<Edge>> graph(n, vector<Edge>());
		For(i,0,m) {
			int u,v,c,w;
			cin >> u >> v >> c >> w; u--; v--;
			graph[u].push_back({v,c,w});
			graph[v].push_back({u,c,w});
		}
		cout << solve(graph,k) << endl;
	}
}

