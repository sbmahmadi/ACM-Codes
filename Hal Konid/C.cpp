/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0);
#define trace(x) //cerr << #x << ": " << x << endl;
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl;
#define _ << " :: " <<
#define allof(x) (x).begin(),(x).end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll h[105],w[105],d[105],k[105];
long double dist[105];
vector<vector<pair<int, long double>>> graph;

long double bf() {
	int n = graph.size();
	For(i,0,n) dist[i] = -1; dist[0] = 0;
	For(K,0,n) {
		For(i,0,n) {
			For(j,0,graph[i].size()) {
				pair<int,long double> p = graph[i][j];
				if(dist[p.first] == -1 || dist[p.first] > dist[i] + p.second)
					dist[p.first] = dist[i] + p.second;
			}
		}
	}
	return dist[graph.size()-1];
}

 long double getdist(int i, int j, int l, int u) {
	if(k[i] == k[j]) {
		if(d[i] >= d[j] + h[j])
			return d[i] - d[j] - h[j];
		else
			return d[j] - d[i] - h[i];
	}
	if(w[i] + w[j] >= u) {
		if(d[i] >= d[j] + h[j])
			return d[i] - d[j] - h[j];
		else if(d[j] >= d[i] + h[i])
			return d[j] - d[i] - h[i];
		else return 0;
	}

	if((d[i] >= d[j] && d[i] <= d[j] + h[j]) || (d[j] >= d[i] && d[j] <= d[i] + h[i])) {
		return u - w[i] - w[j];
	}

	long double hd;
	if(d[i] > d[j])
		hd = d[i] - d[j] - h[j];
	else
		hd = d[j] - d[i] - h[i];
	long double wd = u - w[i] - w[j];
	return sqrt(hd * hd + wd * wd);
}

void makegraph(int l, int u) {
	For(i,0,graph.size()) {
		For(j,0,graph.size()) {
			if(i == j)
				continue;
			long double dst = getdist(i,j,l,u);
			if(dst < 0)
				cout << 1/0 << endl;
			graph[i].push_back({j,dst});
		}
	}
}

int main()
{
	//fastio();
	cout << fixed << setprecision(6);
	int t;
	ifstream fin("street.in");
	fin >> t;
	while(t--) {
		int n,l,u;
		fin >> n >> l >> u;
		//h = w = d = k = vi(n+2);
		graph = vector<vector<pair<int, long double>>>(n+2);
		h[0] = h[n+1] = 0;
		w[0] = w[n+1] = u;
		d[0] = 0; d[n+1] = l;
		k[0] = k[n+1] = 0;

		For(i,1,n+1)
			fin >> h[i] >> w[i] >> d[i] >> k[i];

		makegraph(l,u);
		cout << bf() << endl;
	}
}