#include<iostream>
#include<math.h>
#include<vector>

using namespace std;

vector<int> bigMultiplication(const vector<int> &a, int b)
{
	int z = 0;

	vector<int> result;

	for (int i = 0; i < a.size(); i++)
	{
		int temp = a[i] * b + z;
		result.push_back(temp % 10);
		z = temp / 10;
	}
	while (z)
	{
		result.push_back(z % 10);
		z /= 10;
	}

	return result;
};


int main()
{
	int n, t;
	cin >> n >> t;
	if (n == 1 && t == 10)
	{
		cout << -1 << endl;
	}
	else 
	{
		vector<int> num = { 1 };
		while (num.size() < n)
		{
			num = bigMultiplication(num, t);
		}
		for (auto it = num.rbegin(); it != num.rend(); it++)
		{
			cout << *it;
		}
		cout << endl;
	}
}