#include <iostream>
#include <vector>
#include <queue>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <iomanip>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define allof(x) x.begin(),x.end()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int main()
{
	int n,m;
	cin>> n >> m;
	vector<int> a(n);
	For(i,0,n)
	{
		cin>> a[i];
	}
	sort(a.begin(), a.end());
	For(i,0,m)
	{
		int b;
		cin>>b;
		cout<<upper_bound(a.begin(), a.end(),b) - a.begin()<<" ";
	}
	cout<<endl;
}