#include <iostream>
#include <vector>
#include <queue>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

struct Node
{
	vector<int>adj;
	vector<int> distance;
	bool visited;
	Node()
	{
		visited = false;
	}
};

queue<int> q;

void BFS(vector<Node> &graph)
{
	while (!q.empty())
	{
		int tp = q.front();
		q.pop();
		graph[tp].visited = true;
		For(i, 0, graph[tp].adj.size())
		{

		}
	}
}

int main()
{
	int n, m;
	cin >> n >> m;
	vector<Node> graph(n);
	For(i, 0, n - 1)
	{
		int u, v;
		cin >> u >> v;
		graph[u - 1].adj.push_back(v - 1);
		graph[v - 1].adj.push_back(u - 1);
	}
	For(i, 0, m)
	{
		int a;
		cin >> a;
		q.push(a - 1);
	}
}