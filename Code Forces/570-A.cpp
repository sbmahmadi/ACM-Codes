#include<iostream>
#include<vector>
#include<queue>
#include<math.h>
#include<algorithm>
#include<map>
#include<set>
#include<unordered_set>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define ll long long int

using namespace std;

int citiesWon[110] = {};

int main()
{
	int n, m;
	cin >> n >> m;
	vector<vector<int>> elec;
	elec.assign(m, vector<int>());
	For(i, 0, m)
	{
		For(j, 0, n)
		{
			int a;
			cin >> a;
			elec[i].push_back(a);
		}
	}
	For(i, 0, m)
	{
		citiesWon[distance(elec[i].begin(), max_element(elec[i].begin(), elec[i].end()))]++;
	}
	cout << distance(citiesWon, max_element(citiesWon, citiesWon + n)) + 1 << endl;

}