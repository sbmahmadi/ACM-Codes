#include <iostream>
#include <vector>
#include <queue>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;


int main()
{
	map<char, char> mp;
	int n, m;
	cin >> n >> m;
	string s;
	cin >> s;
	For(i, 0, 26)
		mp['a' + i] = 'a' + i;
	
	For(i, 0, m)
	{
		char a, b; cin >> a >> b;
		for (auto it = mp.begin(); it != mp.end(); it++)
		{
			if (it->second == a)
				it->second = b;
			else if (it->second == b)
				it->second = a;
		}
	}
	For(i, 0, n)
	{
		cout << mp[s[i]];
	}
	cout << endl;
}