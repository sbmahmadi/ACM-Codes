/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0)
#define trace(x) //cerr << #x << ": " << x << endl
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl
#define _ << " :: " <<
#define allof(x) (x).begin(),(x).end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef long double ld;
typedef pair<int, int> ii;
typedef pair<double, double> dd;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<double> vd;
typedef vector<string> vs;

vi caps, full;

int main()
{
	int w,l;
	cin >> w >> l;
	caps = vi(w+1,0); caps[w] = 2e9;
	full = vi(w+1,0); full[0] = 2e9;
	For(i,1,w)
		cin >> caps[i];
	int from = 0;
	int to = 1;
	while(from != w) {
		if(to == from)
			to++;
		else if(to - from > l)
			from++;
		else if(full[to] == caps[to])
			to++;
		else if(full[from] == 0)
			from++;
		else {
			int k = min(full[from], caps[to] - full[to]);
			full[from] -= k;
			full[to] += k;
		}
	}
	cout << full[w] << endl;
}
