#include<iostream>
#include<vector>

#define For(i,a,b) for(int i = a;i<b;i++)

using namespace std;

int cnt;
vector<vector<int>> grid;

int dx[4] = { 0,0,1,-1 };
int dy[4] = { 1,-1,0,0 };
int n, m, k;
void DFS(int x, int y, int c)
{
	grid[x][y] = c;
	For(i, 0, 4)
	{
		if (grid[x + dx[i]][y + dy[i]] == -2) cnt++;
	}
	For(i, 0, 4)
	{
		int newx = x + dx[i];
		int newy = y + dy[i];
		if (newx >= 0 && newy >= 0 && newx < n && newy < m && grid[newx][newy] == -1)
		{
			DFS(x + dx[i], y + dy[i], c);
		}
	}
}

int main()
{
	cerr<<"marg"<<endl;
	cin >> n >> m >> k;
	grid.assign(n,vector<int>());
	vector<int> res;
	For(i, 0, n)
	{
		grid[i].assign(m,0);
		For(j, 0, m)
		{
			char cc;
			cin >> cc;
			if (cc == '.') grid[i][j] = -1;
			else if (cc == '*') grid[i][j] = -2;
		}
	}
	int c = 0;
	For(i, 0, k)
	{
		int x, y;
		cin >> x >> y;
		x--; y--;
		if (grid[x][y] >=0 )
		{
			cout << res[grid[x][y]] << endl;
		}
		else
		{
			cnt = 0;
			DFS(x, y, c);
			c++;
			res.push_back(cnt);
			cout << cnt << endl;
		}
	}
}