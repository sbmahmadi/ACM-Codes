#include<iostream>
#include<vector>
#include<algorithm>

using namespace std;

struct Node
{
	vector<int> people;
	vector<int> adj;
	bool visited;
	Node() :visited(false){}
};

bool found;

void DFS(int start, int dest, vector<Node> &graph, vector<int> &res)
{
	graph[start].visited = true;
	if (start == dest)
	{
		found = true;
		for (int j = 0; j < graph[start].people.size(); j++)
		{
			res.push_back(graph[start].people[j]);
		}
		graph[start].visited = false;
		return;
	}
	else
	{
		for (int i = 0; i < graph[start].adj.size(); i++)
		{
			if(!graph[graph[start].adj[i]].visited) 
				DFS(graph[start].adj[i], dest, graph, res);
			if (found)
			{
				for (int j = 0; j < graph[start].people.size(); j++)
				{
					res.push_back(graph[start].people[j]);
				}
				graph[start].visited = false;
				return;
			}
		}
	}
	graph[start].visited = false;
}

int main()
{
	int n, m, q;
	cin >> n >> m >> q;
	vector<Node> graph(n);
	for (int i = 0; i < n - 1; i++)
	{
		int u, v;
		cin >> u >> v;
		graph[u - 1].adj.push_back(v - 1);
		graph[v - 1].adj.push_back(u - 1);
	}
	for (int i = 0; i < m; i++)
	{
		int c;
		cin >> c;
		graph[c - 1].people.push_back(i + 1);
	}
	for (int i = 0; i < q; i++)
	{
		found = false;
		int u, v, k;
		cin >> u >> v >> k;
		
		vector<int> result;
		
		DFS(u-1, v-1, graph, result);
		sort(result.begin(), result.end());
		
		k = min(k, (int)result.size());
		cout << k;
		for (int i = 0; i < k; i++)
		{
			cout <<" "<<result[i];
		}
		cout << endl;
	}

}