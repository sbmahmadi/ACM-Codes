/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <list>
#include <vector>
#include <queue>
#include <deque>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <math.h>
#include <algorithm>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

int main()
{
	int n,k;
	cin >> n >> k;
	vi b(n);
	ll total = 0;
	For(i,0,n)
		cin >> b[i], total += b[i];
	ll res = b[0] * b[n-1];
	For(i,1,n)
		res += b[i]*b[i-1];
	//trace(res)
	//trace(total)
	For(i,0,k)
	{
		int a;
		cin >> a;
		a--;
		//trace(b[a] _ b[(a-1+n)%n] _ b[(a+1)%n])
		total -= b[a]; 
		res += b[a] * (total - b[(a-1+n)%n] - b[(a+1)%n]);
		b[a] = 0;
	}
	cout << res << endl;
	
}