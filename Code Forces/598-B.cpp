#include<iostream>
#include<string>

using namespace std;

int main()
{
	string s;
	cin >> s;
	int m;
	cin >> m;
	for (int i = 0; i < m; i++)
	{
		int l, r, k;
		cin >> l >> r >> k;
		int size = r - l + 1;
		k %= size;
		string temps = "";
		for (int j = 0; j < size; j++)
		{
			temps += ' ';
		}
		for (int j = 0; j < size; j++)
		{
			temps[(k + j) % size] = s[l - 1 + j];
		}
		for (int j = 0; j < size; j++)
		{
			s[l - 1 + j] = temps[j];
		}
	}
	cout << s << endl;
}