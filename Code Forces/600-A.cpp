#include <iostream>
#include <vector>
#include <queue>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <iomanip>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define allof(x) x.begin(),x.end()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

bool isnum(string s)
{
	if(s.size() == 0) return false;
	if(s.size() == 1 && s[0] == '0') return true;
	if(s[0] == '0') return false;
	For(i,0,s.size())
	{
		if(s[i] > '9' || s[i] < '0') return false;
	}
	return true;
}

int main()
{
	string s;
	cin >> s;
	vector<string> a;
	vector<string> b;
	For(i,0,s.size())
	{
		string temp = "";
		while(i < s.size() && s[i] != ',' && s[i] != ';')
		{
			temp += s[i];
			i++;
		}
		if(isnum(temp)) a.push_back(temp);
		else b.push_back(temp);
	}

	if(s.back() == ',' || s.back() == ';')
	b.push_back("");

	if(a.size() == 0) cout<<"-"<<endl;
	else 
	{
		cout<<char(34)<<a[0];
		For(i,1,a.size())
		{
			cout<<","<<a[i];
		}
		cout<<char(34)<<endl;
	}
	if(b.size() == 0) cout<<"-"<<endl;
	else 
	{
		cout<<char(34)<<b[0];
		For(i,1,b.size())
		{
			cout<<","<<b[i];
		}
		cout<<char(34)<<endl;
	}
}