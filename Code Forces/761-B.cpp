/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

int n,l;

string f(vi& a, vi& b){
	For(i,0,n)
	{
		bool ok = true;
		For(j,0,n){
			if(a[j] != b[j+i])
				ok = false;
		}
		if(ok)
			return "YES";
	}
	return "NO";
}

int main()
{
	cin >> n >> l;
	vi a(n),b(n*2);
	
	int sum = 0;
	For(i,0,n)
		cin >> a[i];
	roF(i,n-1,1) 
		a[i] -= a[i-1], sum += a[i];
	a[0] = l - sum;

	sum = 0;
	For(i,0,n)
		cin >> b[i];
	roF(i,n-1,1)
		b[i] -= b[i-1], sum += b[i];
	b[0] = l - sum;
	
	For(i, n, n*2)
		b[i] = b[i-n];
	
	cout << f(a,b) << endl;
}
