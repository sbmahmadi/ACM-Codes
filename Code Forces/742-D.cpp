/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

vi W,B,visit, BT, WT;
vector<vi> graph, comps;

int bt; int wt;

int n,m,w;
int nt;

int dp[1005][1005];

int knapsack(int ind, int weight)
{
	if(weight > w)
		return INT_MIN;
	if(ind == nt)
		return 0;
	int& mem = dp[ind][weight];
	if(mem >= 0)
		return mem;
	int v1 = BT[ind] + knapsack(ind+1, weight + WT[ind]);
	int v2 = knapsack(ind + 1, weight);
	For(i,0,comps[ind].size())
	{
		int v = comps[ind][i];
		v2 = max(v2, B[v] + knapsack(ind+1, weight + W[v]));
	}
	return mem = max(v2,v1);
}

void dfs(int ind, vi& cmp)
{
	visit[ind] = true;
	cmp.push_back(ind);
	bt += B[ind];
	wt += W[ind];
	For(i,0,graph[ind].size())
		if(!visit[graph[ind][i]])
			dfs(graph[ind][i],cmp);
}

int main()
{
	memset(dp,-1,sizeof dp);
	cin >> n >> m >> w;
	W.assign(n,0);
	B.assign(n,0);
	For(i,0,n)
		cin >> W[i];
	For(i,0,n)
		cin >> B[i];

	graph.assign(n,vector<int>());
	For(i,0,m)
	{
		int x,y;
		cin >> x >> y; x--; y--;
		graph[x].push_back(y);
		graph[y].push_back(x);
	}
	int cnt = 0;
	visit.assign(n,0);
	For(i,0,n)
	{
		if(!visit[i])
		{
			vi cmp;
			bt = wt = 0;
			dfs(i,cmp);
			comps.push_back(cmp);
			BT.push_back(bt);
			WT.push_back(wt);
		}
	}
	nt = comps.size();
	cout << knapsack(0,0) << endl;
}