/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

int main()
{
	ios_base::sync_with_stdio(0);
	cin.tie(0);
	int n,x;
	cin >> n >> x;
	vi v(n);
	map<int,int> mp;
	
	For(i,0,n)
	{
		int a;
		cin >> a;
		mp[a]++;
	}
	ll sum = 0;
	if(x == 0)
	{
		for( auto& it : mp)
		{
			sum += (ll)it.second * (ll)(it.second -1ll)/2ll;  
		}
		cout << sum << endl;
	}
	else
	{
		for(auto& it : mp)
		{
			int v1 = it.first ^ x;
			if(v1 < it.first)
				continue;
			if(mp.count(v1))
				sum += (ll)mp[v1] * (ll)it.second;
		}
		cout << sum << endl;
	}
	
}