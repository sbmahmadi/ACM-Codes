/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

int main()
{
	set<int> a31 = {1,3,5,7,8,10,12};
	int m,d;
	cin >> m >> d;
	if(m == 2)
	{
		if(d == 1)
			cout << 4 << endl;
		else 
			cout << 5 << endl;
	}
	else if(a31.count(m))
	{
		if(d < 6)
			cout << 5 << endl;
		else
			cout << 6 << endl;
	}
	else
	{
		if(d == 7)
			cout << 6 << endl;
		else
			cout << 5 << endl;
	}
}