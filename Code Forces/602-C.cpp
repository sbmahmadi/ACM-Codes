#include <iostream>
#include <vector>
#include <queue>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <iomanip>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define allof(x) x.begin(),x.end()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;


vector<vi> Rails;
vector<vi> Roads;

bool visited[405];
int dist[405];
int n, m;

void BFS(vector<vi> &graph)
{
	queue<int> q;
	q.push(0);
	dist[0] = 0;
	visited[0] = true;
	while (!q.empty())
	{
		int t = q.front();
		q.pop();
		For(i, 0, graph[t].size())
		{
			if (!visited[graph[t][i]])
			{
				visited[graph[t][i]] = true;
				q.push(graph[t][i]);
				dist[graph[t][i]] = dist[t] + 1;
			}
		}
	}
	if (!visited[n - 1])
	{
		cout << -1 << endl;
	}
	else cout << dist[n - 1] << endl;
}

int main()
{	
	cin >> n >> m;
	Rails.assign(n, vi());
	Roads.assign(n, vi());
	
	For(i, 0, n)
	{
		visited[i] = 0;
		For(j, 0, n)
		{
			if (i != j)
			{
				Roads[i].push_back(j);
			}
		}
	}
	
	For(i, 0, m)
	{
		int u, v;
		cin >> u >> v;
		Rails[u - 1].push_back(v - 1);
		Rails[v - 1].push_back(u - 1);
		Roads[u - 1].erase(find(Roads[u - 1].begin(), Roads[u - 1].end(), v - 1));
		Roads[v - 1].erase(find(Roads[v - 1].begin(), Roads[v - 1].end(), u - 1));
	}

	For(i, 0, Rails[0].size())
	{
		if (Rails[0][i] == n - 1)
		{
			BFS(Roads);
			return 0;
		}
	}
	BFS(Rails);
}