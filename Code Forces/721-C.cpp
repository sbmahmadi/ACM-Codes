/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <list>
#include <vector>
#include <queue>
#include <deque>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <math.h>
#include <algorithm>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

vector<vector<ii>> graph;
int par[5005];
int n;

unordered_map<int,int> dp[5005];

int knapsack(int ind, int rem)
{
	if(rem < 0)
		return INT_MIN;
	if(graph[ind].size() == 0 && ind != n-1)
		return INT_MIN;
	if(ind == n-1)
		return 1;

	if(dp[ind][rem] != 0)
		return dp[ind][rem];
	int res = 0;
	For(i,0,graph[ind].size())
	{
		if(knapsack(graph[ind][i].first, rem - graph[ind][i].second) > res)
		{
			res = knapsack(graph[ind][i].first, rem - graph[ind][i].second);
			par[ind] = graph[ind][i].first;
	//		trace(ind _ par[ind])
		}
	}
	//trace(ind _ rem _ res + 1)
	return dp[ind][rem] = res + 1;
}

void printparents(int ind)
{
	cout << " " << ind + 1;
	if(ind == n-1)
		return;
	printparents(par[ind]);
}

int main()
{
	int m,t;
	cin >> n >> m >> t;
	graph = vector<vector<ii>>(n);
	For(i,0,m)
	{
		int u,v,w;
		cin >> u >> v >> w;
		graph[u-1].push_back({v-1,w});
	}
	cout << knapsack(0,t) << endl;
	cout << 1 ;
	printparents(par[0]);
	cout << endl;
}
