#include <iostream>
#include <vector>
#include <queue>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <iomanip>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define allof(x) x.begin(),x.end()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int main()
{
	set<int> xx, yy;
	int n;
	cin >> n;
	For(i, 0, n)
	{
		int x, y;
		cin >> x >> y;
		xx.insert(x);
		yy.insert(y);
	}
	if (xx.size() == 1 || yy.size() == 1)
		cout << -1 << endl;
	else
		cout << (*(++xx.begin()) - *xx.begin()) * (*(++yy.begin()) - *yy.begin()) << endl;
}