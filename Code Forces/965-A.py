from math import ceil

lst = input().split(" ")
k = int(lst[0])
n = int(lst[1])
s = int(lst[2])
p = int(lst[3])

print(ceil(k*(ceil(n/s))/p))