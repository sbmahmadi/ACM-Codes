/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0);
#define trace(x) cerr << #x << ": " << x << endl;
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl;
#define _ << " :: " <<
#define allof(x) (x).begin(),(x).end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef long double ld;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<double> vd;

vector<int> dirs = {0,1,2,3};

int dx[] = {1,0,0,-1};
int dy[] = {0,-1,1,0};

int sx,sy, ex,ey;
string inst;

int n,m;

bool sim(vector<vector<char>>& grid) {
	int mx = sx;
	int my = sy;

	For(i,0,inst.size()) {
		int mi = inst[i] - '0';
		mi = dirs[mi];
		
		mx = mx + dx[mi];
		my = my + dy[mi];

		if(mx < 0 || mx >= n || my < 0 || my >= m)
			return 0;
		if(grid[mx][my] == '#')
			return 0;
		if(mx == ex && my == ey)
			return 1;
	}
	return 0;
}

int main()
{
	cin >> n >> m;
	vector<vector<char>> grid(n,vector<char>(m));
	
	For(i,0,n)
		For(j,0,m){
			cin >> grid[i][j];
			if(grid[i][j] == 'S')
				sx = i, sy = j;
			if(grid[i][j] == 'E')
				ex = i, ey = j;
		}
	
	cin >> inst;
	int res = 0;
	do {
		res += sim(grid);
	} while(next_permutation(allof(dirs)));
	cout << res << endl;
}