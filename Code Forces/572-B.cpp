#include<iostream>
#include<vector>
#include<queue>
#include<math.h>
#include<algorithm>
#include<map>
#include<set>
#include<unordered_set>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define ll long long int

using namespace std;

int main()
{
	int n, s;
	cin >> n >> s;
	char c;
	int p, q;
	map<int, int> buy, sell;
	
	For(i, 0, n)
	{
		cin >> c >> p >> q;
		if (c == 'B')
		{
			buy[p] += q;
		}
		else
		{
			sell[p] += q;
		}
	}
	int i = 0;
	
	map<int,int>::iterator it; 
	if (s > sell.size()) it = --sell.end();
	else it = next(sell.begin(), s - 1);

	for (; it != --sell.begin(); it--)
	{
		printf("%c %d %d\n", 'S', it->first, it->second);
	}
	i = 0;
	for (auto rit = buy.rbegin(); rit != buy.rend() && i < s; i++, rit++)
	{
		printf("%c %d %d\n", 'B', rit->first, rit->second);
	}
}