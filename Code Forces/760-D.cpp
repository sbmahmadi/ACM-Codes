/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

vi t;
vi dp;

int f(int i){
	if(i <= 0)
		return 0;
	int& res = dp[i];
	if(res != -1)
		return res;
	int i1 = (--upper_bound(allof(t),t[i]-1)) - t.begin();
	int i2 = (--upper_bound(allof(t),t[i]-90)) - t.begin();
	int i3 = (--upper_bound(allof(t),t[i]-1440)) - t.begin();
	res = min({20 + f(i1), 50 + f(i2), 120 + f(i3)});
	//trace(t[i] _ res)
	return res;
}

int main()
{
	int n;
	cin >> n;
	t.assign(n+1,0);
	dp.assign(n+1, -1);
	dp[0] = t[0] = 0;
	For(i,1,n+1){
		cin >> t[i];
	}
	For(i,1,n+1){
		cout << f(i) - f(i-1) << endl;
	}
}