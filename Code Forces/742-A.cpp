/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

int main()
{
	int n;
	cin >> n;
	if(n == 0)
		cout << 1 << endl;
	else
	{
		switch ((n-1)%5)
		{
			case 0 : cout << 8 << endl; break;
			case 1 : cout << 4 << endl; break;
			case 2 : cout << 2 << endl; break;
			case 3 : cout << 6 << endl; break;
		}
	}	
}