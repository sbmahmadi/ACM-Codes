/*
ID: behdad.1
LANG: C++11
PROB:
*/

#include <iostream>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <iomanip>
#include <fstream>
#include <limits.h>
#include <time.h>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int bulbs[1000];

int main()
{
	/*ifstream fin("file.in");
	ofstream fout("file.out");*/
	int n, m;
	cin >> n >> m;
	For(i, 0, n)
	{
		int x;
		cin >> x;
		For(j, 0, x)
		{
			int k;
			cin >> k;
			bulbs[k - 1]++;
		}
	}
	bool t = true;
	For(i, 0, m)
	{
		if (bulbs[i] == 0) t = false;
	}
	if (t) cout << "YES" << endl;
	else cout << "NO" << endl;
}
