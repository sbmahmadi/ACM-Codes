#include<iostream>

#define trace(x) //cout<<#x<<": "<<x<<endl;

using namespace std;

long long pw(int a, int b)
{
	long long temp = 1;
	for (int i = 0; i < b; i++)
	{
		temp *= a;
	}
	return temp;
}

int main()
{
	int n, bx;
	cin >> n >> bx;
	long long res1 = 0;
	long long p1 = pw(bx, n - 1);
	trace(bx);
	trace(p1);
	for (int i = 0; i < n; i++)
	{
		int c;
		cin >> c;
		res1 += p1* c;
		trace(p1);
		p1 /= bx;
	}

	int m, by;
	cin >> m >> by;
	long long res2 = 0;
	long long p2 = pw(by, m - 1);
	for (int i = 0; i < m; i++)
	{
		int c;
		cin >> c;
		res2 += p2* c;
		p2 /= by;
	}
	trace(res1);
	trace(res2);
	if (res1 < res2) cout << "<" << endl;
	else if (res1 == res2) cout << "=" << endl;
	else cout << ">" << endl;
}