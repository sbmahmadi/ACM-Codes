#include<iostream>
#include<algorithm>
#include<vector>

using namespace std;

int main()
{
	int n, x, x2;
	cin >> n >> x >> x2;
	int k, b;
	vector<pair<int, int>> all;
	for (int i = 0; i < n; i++)
	{
		int k, b;
		cin >> k >> b;
		for (int j = 0; j < all.size(); j++)
		{
			if (1.0*(b - all[j].second) / (all[j].first - k) > x && 1.0*(b - all[j].second) / (all[j].first - k) < x2)
			{
				cout << "Yes" << endl;
				return 0;
			}
		}
	}
	cout << "No" << endl;
	return 0;
}