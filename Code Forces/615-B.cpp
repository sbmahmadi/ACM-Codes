/*
ID: behdad.1
LANG: C++11
PROB:
*/

#include <iostream>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <iomanip>
#include <fstream>
#include <limits.h>
#include <time.h>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

bool visited[100050];
bool dfsVisited[100050];
int dist[100050];

int maxVal = -1;

void DFS(int n,int len, vector<vi> &graph)
{
	visited[n] = true;
	dfsVisited[n] = true;
	maxVal = max(maxVal, int(len*graph[n].size()));
	For(i, 0, graph[n].size())
	{
		int m = graph[n][i];
		if (m < n) continue;
		if (!dfsVisited[m])
		{
			DFS(m, len + 1, graph);
		}
	}
	dfsVisited[n] = false;
}

int main()
{
	/*ifstream fin("file.in");
	ofstream fout("file.out");*/
	int n, m;
	cin >> n >> m;
	vector<vi> graph(n);
	For(i, 0, m)
	{
		int u, v;
		cin >> u >> v;
		graph[u - 1].push_back(v-1);
		graph[v - 1].push_back(u-1);
	}
	memset(dist, -1, sizeof dist);
	For(i, 0, n)
	{
		if (!visited[i])
		{
			DFS(i,1, graph);
		}
	}
	cout << maxVal << endl;
}

