/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0);
#define trace(x) cerr << #x << ": " << x << endl;
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl;
#define _ << " :: " <<
#define allof(x) (x).begin(),(x).end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

void dfs(int ind, int cl, vi& visit, vector<vi>& t, vi& c){
	visit[ind] = true;
	For(i,0,t[ind].size())
		if(!visit[t[ind][i]] && c[t[ind][i]] == cl)
			dfs(t[ind][i], cl, visit, t, c);
}

bool tr(int ind, vector<vi>& t, vi& c){
	vi visit(t.size(),0);
	visit[ind] = true;
	For(i,0,t[ind].size()){
		dfs(t[ind][i], c[t[ind][i]], visit, t, c);
	}
	For(i,0,t.size())
		if(!visit[i])
			return 0;
	return 1;
}

int main()
{
	int n;
	cin >> n;
	vector<vi> t(n);
	For(i,0,n-1){
		int u,v;
		cin >> u >> v; u--; v--;
		t[u].push_back(v);
		t[v].push_back(u);
	}
	vi c(n);
	For(i,0,n)
		cin >> c[i];

	vi visit(n,0);
	For(i,0,n)
		For(j,0,t[i].size()){
			if(c[i] != c[t[i][j]])
			{
				if(tr(i, t, c))
					cout << "YES" << endl << i+1 << endl;
				else if(tr(t[i][j], t, c))
					cout << "YES" << endl << t[i][j]+1 << endl;
				else cout << "NO" << endl;
				return 0;
			}
		}
	cout << "YES" << endl << 1 << endl;
}
