#include <iostream>
#include <vector>
#include <queue>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <iomanip>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define allof(x) x.begin(),x.end()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int main()
{
	vector<ii> nums;
	int n;
	cin >> n;
	int k;
	cin >> k;
	nums.push_back(make_pair(k, 1));
	For(i, 0, n-1)
	{
		cin >> k;
		if (k == nums.back().first)
			nums.back().second++;
		else nums.push_back(make_pair(k, 1));
	}
	int mx = 0;

	if(nums.size() < 3)
	{
		int k = nums[0].second;
		if(nums.size() == 2)
			k+= nums[1].second;
		cout<<k<<endl;
		return 0;
	}

	For(i, 0, nums.size() - 1)
	{
		int a1 = nums[i].first;
		int a2 = nums[i + 1].first;
		int res = nums[i].second + nums[i + 1].second;
		int it = i+2;
		while (it < nums.size() && (nums[it].first == a1 || nums[it].first == a2))
		{
			res += nums[it].second;
			it++;
		}
		mx = max(mx, res);
		if(res > n/2) break;
	}
	cout << mx << endl;
}