/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

int main()
{
	int n,m;
	cin >> n >> m;
	vector<string> v(n);
	For(i,0,n)
		cin >> v[i];
	vector<vi> a(n,vi(3,1e8));
	For(i,0,n)
	{
		string& s = v[i];
		For(j,0,m)
		{
			if(s[j] <= 'z' && s[j] >= 'a' && a[i][0] > min(j, m - j))
				a[i][0] = min(j, m-j);
			if(s[j] <= '9' && s[j] >= '0' && a[i][1] > min(j, m - j))
				a[i][1] = min(j, m-j);
			if((s[j] == '#' || s[j] == '*' || s[j] == '&') && a[i][2] > min(j, m - j))
				a[i][2] = min(j, m-j);
		}
	}
	
	int mn = 1e8;
	For(i,0,n)
		For(j,0,n)
		{
			if(i == j)
				continue;
			For(k,0,n)
			{
				if(k == i || k == j)
					continue;
				mn = min(mn,a[i][0] + a[j][1] + a[k][2]);
			}
		}
	cout << mn << endl;
}
