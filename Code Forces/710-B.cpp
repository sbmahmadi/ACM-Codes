/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <list>
#include <vector>
#include <queue>
#include <deque>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <math.h>
#include <algorithm>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

int main()
{
	int n;
	cin >> n;
	vector<int> p(n);
	ll sumr = 0;
	For(i,0,n)
	{
		cin >> p[i];
		sumr += p[i];
	}
	sort(allof(p));
	sumr -= (n)*p[0];
	ll suml = 0; ll i = 0; ll best = sumr; ll bestx = p[0];
	for(i = 1; i < n; i++)
	{
		sumr -= (n-i) * (p[i] - p[i-1]);
		suml += i * (p[i] - p[i-1]);
		if(sumr + suml < best)
		{
			best = sumr + suml;
			bestx = p[i];
		}
	}
	cout << bestx << endl;
}