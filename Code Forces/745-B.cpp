/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

int n,m;

ii first(vector<vi>& grid)
{
	For(i,0,n)
	{
		For(j,0,m)
		{
			if(grid[i][j] == 1)
				return {i,j};
		}
	}
}


ii last(vector<vi>& grid)
{
	roF(i,n-1,0)
	{
		roF(j,m-1,0)
		{
			if(grid[i][j] == 1)
				return {i,j};
		}
	}	
}

bool eq(vector<vi>& grid, vector<vi>& grid2)
{
	For(i,0,n)
		For(j,0,m)
			if(grid[i][j] != grid2[i][j])
				return false;
	return true;
}

int main()
{
	cin >> n >> m;
	vector<vi> grid(n, vi(m));
	vector<vi> grid2(n, vi(m, 0));
	For(i,0,n)
	{
		For(j,0,m)
		{
			char t;
			cin >> t;
			grid[i][j] = (t == '.' ? 0 : 1);
		}
	}
	ii f = first(grid);
	ii l = last(grid);
	For(i,f.first,l.first+1)
		For(j,f.second, l.second+1)
			grid2[i][j] = 1;

	cout << (eq(grid,grid2) ? "YES" : "NO") << endl;
}