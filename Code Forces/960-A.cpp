/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0)
#define trace(x) cerr << #x << ": " << x << endl
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl
#define _ << " :: " <<
#define allof(x) (x).begin(),(x).end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef long double ld;
typedef pair<int, int> ii;
typedef pair<double, double> dd;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<double> vd;
typedef vector<string> vs;

int main()
{
	string s;
	cin >> s;
	int a = 0,b= 0,c= 0;
	for(char ch : s){
		if(ch == 'a') {
			if(b || c) {
				cout << "NO" << endl;
				return 0;
			}
			a++;
		}
		if(ch == 'b') {
			if(!a || c){
				cout << "NO" << endl;
				return 0;
			}
			b++;
		}
		if(ch == 'c') {
			if(!a || !b) {
				cout << "NO" << endl;
				return 0;
			}
			c++;
		}
	}	

	if(c == a || c == b) {
		cout << "YES" << endl;
		return 0;
	}
	cout << "NO" << endl;
}
