#include<iostream>
#include<vector>
#include<math.h>

using namespace std;

typedef long long ll;

int main()
{
	vector<ll> pows;
	pows.push_back(1);
	int t;
	cin >> t;
	int i = 0;
	while (t--)
	{
		i++;
		ll n;
		cin >> n;
		ll ans = (n * (n + 1)) / 2;
		int i = 0;
		while (pows[i] <= n)
		{
			ans -= pows[i] * 2;
			if (i == pows.size() - 1)
			{
				pows.push_back(pows.back() * 2);
			}
			i++;
		}
		cout << ans << endl;
	}
}