#include <iostream>
#include <vector>
#include <algorithm>

#define For(i,a,b) for(int i = a; i<b;i++)

using namespace std;

int main()
{
	int n;
	cin >> n;
	vector<int> v(n);
	For(i,0,n)
	{
		cin >> v[i];
	}
	int res = 1;
	int seq = 1;
	For(i,1,n)
	{
		if(v[i]<v[i-1])
			seq = 1;
		else
		{
			seq++;
			res = max(res,seq);
		}
	}
	cout<<res<<endl;
}