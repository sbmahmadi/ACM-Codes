#include<iostream>
#include<vector>
#include<queue>
#include<math.h>
#include<algorithm>
#include<map>
#include<set>
#include<unordered_set>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define ll long long int

using namespace std;



int main()
{
	int n1, n2, k, m;
	cin >> n1 >> n2 >> k >> m;
	
	int arn1[100100];
	int arn2[100100];

	For(i, 0, n1)
	{
		cin >> arn1[i];
	}
	For(i, 0, n2)
	{
		cin >> arn2[i];
	}
	cout << (arn1[k - 1] < arn2[n2-m] ? "YES" : "NO") << endl;
}