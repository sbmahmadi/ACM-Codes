#include <iostream>
#include <vector>
#include <queue>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <iomanip>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define allof(x) x.begin(),x.end()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int alph[30];

int main()
{
	string s;
	cin>>s;
	For(i,0,s.size())
	{
		alph[s[i]-'a']++;
	}
	int beg = 0;
	int end = 25;

	while(1)
	{
		if(alph[beg]%2 == 0)
		{
			beg++;
			continue;
		}
		if(alph[end]%2 == 0)
		{
			end--;
			continue;
		}
		if(beg >= end) break;
		alph[end]--;
		alph[beg]++;
	}
	For(i,0,30)
	{
		For(j,0,alph[i]/2)
		{
			cout<<char('a' + i);
		}
	}
	if(s.size()%2 == 1)
	{
		cout<<char('a' + end);
	}
	roF(i,25,0)
	{
		For(j,0,alph[i]/2)
		{
			cout<<char('a' + i);
		}
	}
	cout<<endl;
}