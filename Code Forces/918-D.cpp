/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0);
#define trace(x) cerr << #x << ": " << x << endl;
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl;
#define _ << " :: " <<
#define allof(x) (x).begin(),(x).end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef long double ld;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<double> vd;
typedef vector<vector<ii>> Graph;

int dp[105][105][30][2];

int f(int u1, int u2, int ch, bool turn, Graph& graph) {
	int& res = dp[u1][u2][ch][turn];
	if(res != -1)
		return res;
	if(turn == 1) {
		res = 0;
		For(i,0,graph[u1].size()) {
			int v = graph[u1][i].first;
			int nch = graph[u1][i].second;
			if(nch >= ch) {
				res |= f(v,u2,nch,turn^1,graph);
			}
		}
		return res;
	} else {
		res = 1;
		For(i,0,graph[u2].size()) {
			int v = graph[u2][i].first;
			int nch = graph[u2][i].second;
			if(nch >= ch) {
				res &= f(u1,v,nch,turn^1,graph);
			}
		}
		return res;
	}
}

int main()
{
	memset(dp, -1, sizeof dp);
	int n,m;
	cin >> n>> m;
	Graph graph(n);
	For(i,0,m) {
		int u,v;
		char c;
		cin >> u>> v >> c; u--; v--; c-= 'a' - 1;
		graph[u].push_back({v,c});
	}
	For(i,0,n) {
		For(j,0,n) {
			cout << (f(i,j,0,1,graph) ? 'A' : 'B');
		}
		cout << endl;
	}
}