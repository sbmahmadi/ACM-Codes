#include<iostream>
#include<string>
#include<vector>

using namespace std;

class Node
{
public:
	vector<int> childs;
	char value;
	int depth;
};

void dfs(int v, int d, string &s, vector<Node> &tree)
{
	if (tree[v].depth == d) s += tree[v].value;
	else if (tree[v].depth < d)
	{
		for (int i = 0; i < tree[v].childs.size(); i++)
		{
			dfs(tree[v].childs[i], d, s, tree);
		}
	}
}

string isPal(string &s)
{
	int a[30] = {};
	bool flag = false;
	for (int i = 0; i < s.size(); i++)
	{
		a[s[i] - 'a']++;
	}
	for (int i = 0; i < 30; i++)
	{
		if (a[i] % 2 == 1)
		{
			if (flag) return "No";
			else flag = true;
		}
	}
	return "Yes";
}

int main()
{
	int n, m;
	cin >> n >> m;
	vector<Node>tree;
	tree.assign(n,Node());
	tree[0].depth = 1;
	
	for (int i = 1; i < n ; i++)
	{
		int a;
		cin >> a;
		tree[a-1].childs.push_back(i);
		tree[i].depth = tree[a-1].depth + 1;
	}
	string s;
	cin >> s;
	for (int i = 0; i < n; i++)
	{
		tree[i].value = s[i];
	}

	int v, d;
	for (int i = 0; i < m; i++)
	{
		string res = "";
		cin >> v >> d;
		dfs(v-1, d, res, tree);
		cout << isPal(res) << endl;
	}
}