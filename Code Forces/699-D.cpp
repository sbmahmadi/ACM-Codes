/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

int p[200005];
bool visit[200005];
bool visit2[200005];
bool visit3[200005];
int comp[200005];
int root = -1;
int cmpcnt = 0;
int res = 0;

int getcmp(int i)
{
	if (comp[i] >= 0)
		return comp[i];
	visit[i] = true;
	if(visit[p[i]])
		if(comp[p[i]] == -1)
			return comp[i] = cmpcnt++;
		else
			return comp[i] = comp[p[i]];
	else
		return comp[i] = getcmp(p[i]);
}

void dfs(int i)
{
	visit3[i] = true;
	if(!visit3[p[i]])
		dfs(p[i]);
	else
	{
		if(root == -1)
			root = i;

		if(p[i] != root)
			p[i] = root,
			res++;	
	}
}

int main()
{
	int n;
	cin >> n;
	
	For(i,1,n+1)
	{
		cin >> p[i];
		if(p[i] == i)
			root = i;
	}
	memset(comp,-1,sizeof comp);
	For(i,1,n+1)
		if(!visit2[getcmp(i)])
		{
			visit2[getcmp(i)] = true;
			dfs(i);
		}

	cout << res << endl;
	cout << p[1];
	For(i,2,n+1)
		cout << " " << p[i];
	cout << endl;
}