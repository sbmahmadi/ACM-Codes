#include <iostream>
#include <vector>
#include <queue>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <iomanip>
#include <fstream>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define allof(x) x.begin(),x.end()
#define _ << " " <<
using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

struct Node
{
	vector<int> adj;
	bool hasCat;
};

vector<Node> tree;

int n,maxCats;

int DFS(int s,int p,int seenCats)
{
	if(tree[s].hasCat) 
		seenCats++;
	else 
		seenCats = 0;

	if (seenCats > maxCats) 
		return 0;
	else if(tree[s].adj.size() == 1 && s!=0) 
		return 1;

	int res = 0;
	For(i,0,tree[s].adj.size())
	{
		int child = tree[s].adj[i];
		if(child == p) continue;
		res += DFS(child,s,seenCats);
	}
	return res;
}

int main()
{
	cin >> n >> maxCats;
	tree.assign(n,Node());
	For(i,0,n)
		cin >> tree[i].hasCat;
	
	For(i,0,n-1)
	{
		int a,b;
		cin >> a >> b;
		tree[a-1].adj.push_back(b-1);
		tree[b-1].adj.push_back(a-1);
	}
	cout << DFS(0,-1,0) << endl;
}
