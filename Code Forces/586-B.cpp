#include <iostream>
#include <vector>
#include <queue>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <iomanip>
#include <fstream>
#include <limits.h>
#include <time.h>
#include <deque>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) //cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int main()
{
	int n;
	cin >> n;
	vector <int> row1(n),row2(n),mid(n);
	
	row1[0] = 0;
	For(i,1,n)
	{
		cin >> row1[i];
		row1[i] += row1[i-1];
	}
	
	row2[n-1] = 0;
	For(i,0,n-1)
		cin >> row2[i];
	roF(i,n-2,0)
		row2[i] += row2[i+1];
		
	For(i,0,n)
		cin >> mid[i];

	int min1 = INT_MAX, min2 = INT_MAX;
	For(i,0,n)
	{
		int me = row1[i] + row2[i] + mid[i];
		if(me < min1)
		{
			min2 = min1;
			min1 = me;
		}
		else if(me < min2)
			min2 = me;
	}
	cout<<min1 + min2<<endl;
}
