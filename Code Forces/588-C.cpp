#include<iostream>
#include<vector>
#include<map>

using namespace std;

int main()
{
	int n;
	cin >> n;
	map<int, int> arr;
	for (int i = 0; i < n; i++)
	{
		int k;
		cin >> k;
		arr[k]++;
	}
	for (auto it = arr.begin(); it != arr.end(); it++)
	{
		if (it->second < 2) continue;
		if (it->second % 2 == 0)
		{
			arr[it->first + 1] = it->second / 2;
			arr[it->first] = 0;
		}
		else
		{
			arr[it->first + 1] = it->second / 2;
			arr[it->first] = 1;
		}
	}
	int sum = 0;
	for (auto it = arr.begin(); it != arr.end(); it++)
	{
		sum += it->second;
	}
	cout << sum << endl;
}