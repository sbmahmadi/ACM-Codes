/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <list>
#include <vector>
#include <queue>
#include <deque>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <math.h>
#include <algorithm>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

int main()
{
	int n,k;
	cin >> n >> k;
	vi v(101,0);
	For(i,0,n)
	{
		string s;
		cin >> s;
		v[s.size()]++;
	}
	string s;
	cin >> s;
	int pass = s.size();
	v[pass]--;
	int tmp = 0;
	int res1 = 0;
	int res2 = 0;
	For(i,1,pass)
	{
		tmp += v[i];
	}
	res1 = tmp + (tmp/k) * 5 + 1;
	tmp += v[pass];
	res2 = tmp + (tmp/k) * 5 + 1;
	cout << res1 << " " << res2 << endl; 
}