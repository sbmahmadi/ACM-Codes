/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(ll i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

ll FULLBIT;
ll n,m,k;
ll maxcost;
vi sat;
map<pair<ll,ll>,ll> rules;

int __builtin_popcount(unsigned int n)
{
	int res = 0;
	while (n)
	{
		if (n & 1)
			res++;
		n >>= 1;
	}
	return res;
}

int main()
{
	cin >> n >> m >> k;
	sat.resize(n);
	For(i,0,n)
		cin >> sat[i];	
	For(i,0,k)
	{
		ll a,b,c;
		cin >> a >> b >> c;
		rules[make_pair(a-1,b-1)] = c;
	}
	FULLBIT = (1<<n) - 1;
	vector<vector<ll>> dp(FULLBIT+1,vector<ll>(n,0));
	
	For(i,0,n)
		dp[1<<i][i] = sat[i];
	
	For(i,1,FULLBIT+1)
	{
		For(j,0,n)
		{
			if(!(i & 1<<j)) 
				continue;
			int newbit = i - (1<<j);
			For(p,0,n)
				if(newbit & 1<<p)
					dp[i][j] = max(dp[i][j],dp[newbit][p] + sat[j] + rules[make_pair(p,j)]);
		}
	}
	ll mx = 0;
	For(i,0,FULLBIT+1)
	{
		if(__builtin_popcount(i) == m)
			For(j,0,n)
				mx = max(mx,dp[i][j]);
	}
	cout << mx << endl;
}