#include <iostream>
#include <vector>
#include <queue>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <iomanip>
#include <fstream>
#include <limits.h>
#include <time.h>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<" --> "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()

using namespace std;

int d[1000000];

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int main()
{
	int res = 0;
	int n;
	cin >> n;
	For(i,0,n)
		cin >> d[i];

	For(i,0,n)
		if(d[i] == 1)
			res++;
	For(i,1,n-1)
		if(d[i] == 0 && d[i-1] == 1 && d[i+1] == 1)
			res++;
	cout<<res<<endl;
}
