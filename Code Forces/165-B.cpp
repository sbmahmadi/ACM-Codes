#include <iostream>
#include <vector>
#include <queue>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define allof(x) x.begin(),x.end()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

bool check(ll n,ll v, ll k)
{
	ll p = 1;
	ll sum = 0;
	while (v / p)
	{
		sum += v / p;
		p *= k;
	}
	if (sum >= n) return true;
	return false;
}

int main()
{
	ll n, k;
	cin >> n >> k;
	ll up = 1000000005;
	ll down = 1;
	ll mid = 0;
	while (down < up)
	{
		mid = (down + up) / 2;
		if (check(n,mid,k))
		{
			up = mid;
		}
		else down = mid + 1;
	}

	cout << up << endl;
}