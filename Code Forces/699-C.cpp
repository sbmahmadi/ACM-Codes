/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

int act[105];
int n;
int dp[105][5];
// rest 0   cont 1   sport 2
int f(int i, int last)
{
	if(i == n)
		return 0;
	int &res = dp[i][last];
	if(res >= 0)
		return res;

	if(act[i] == 3 && last == 2)
		return res = f(i+1,1);
	if(act[i] == 3 && last == 1)
		return res = f(i+1,2);
	if(act[i] == 3 && last == 0)
		return res = min(f(i+1,1),f(i+1,2));
	
	if(act[i] == 2 && last == 2)
		return res = 1 + f(i+1,0);
	if(act[i] == 2 && last < 2)
		return res = f(i+1,2);
	if(act[i] == 1 && last == 1)
		return res = 1 + f(i+1,0);
	if(act[i] == 1 && (last == 0 || last == 2))
		return res = f(i+1, 1);
	if(act[i] == 0)
		return res = 1 + f(i+1,0);
}

int main()
{
	cin >> n;
	memset(dp, -1, sizeof dp);
	For(i,0,n)
	{
		cin >> act[i];
	}
	cout << f(0,0) << endl;
}