#include<iostream>
#include<math.h>

using namespace std;

int countDiv(long long &n, long long a)
{
	long long sum = 0;
	while (n%a == 0)
	{
		n /= a;
		sum++;
	}
	return sum;
}

int main()
{	
	long long n;
	cin >> n;
	long long num = 1;
	long long temp = n;
	long long sq = sqrtl(temp);

	if (countDiv(n, 2) > 0) num *= 2;
	if (countDiv(n, 3) > 0) num *= 3;
	if (countDiv(n, 5) > 0) num *= 5;
	if (countDiv(n, 7) > 0) num *= 7;
	long long i = 5;
	while(n!=1)
	{
		if (countDiv(n, i) > 0) num *= i;
		if (countDiv(n, i + 2) > 0)num *= i + 2;
		i += 6;
		if (i > sq && num == 1)
		{
			num *= temp;
			break;
		}
	}
	cout << num << endl;
}