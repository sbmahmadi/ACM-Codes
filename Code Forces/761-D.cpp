/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

void dfs(int ind, int par, vi& t, vii& pos, int x, int y, int sz){
	For(i, 0, t[ind].size()){
		if(t[ind][i] == par)
			continue;
	}
}

int main()
{
	int n;
	cin >> n;
	vector<vi> t(n);
	For(i,0,n-1){
		int u,v;
		cin >> u >> v; u--; v--;
		t[u].push_back(v);
		t[v].push_back(u);
	}
	For(i,0,n)
		if(t[i].size() > 4){
			cout << "NO" << endl;
			return 0;
		}

	vi size(40,0);

	For(i,1,40);
		size[i] = size[i-1] + i;
	vii pos(n);
	pos[0] = {0,0};
	
	dfs(0,0, t, pos, 0,0, 39);
	
	cout << "YES" << endl;
	For(i,0,n){
		cout << pos[i].first << " " << pos[i].second << endl;
	}
}
