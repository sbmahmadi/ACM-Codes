/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <iomanip>
#include <fstream>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int cols[101];
int rows[101];

int main()
{
	/*ifstream fin("file.in");
	ofstream fout("file.out");*/
	int n;
	cin >> n;
	For(i,0,n)
	{
		For(j,0,n)
		{
			char c;
			cin >> c;
			if(c == 'C')
			{
				cols[i]++;
				rows[j]++;
			}
		}
	}
	ll res = 0;
	For(i,0,n)
	{
		if(cols[i] > 0)
			res += (cols[i] * (cols[i]-1)) / 2;
		if(rows[i] > 0)
			res += (rows[i] * (rows[i]-1)) / 2;			
	}
	cout << res << endl;
}
