/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0);
#define trace(x) cerr << #x << ": " << x << endl;
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl;
#define _ << " :: " <<
#define allof(x) (x).begin(),(x).end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

struct rec{
	int x1,y1,x2,y2;
};

bool valid(int ind, int cl, vector<vi>& graph, vi& c){
	For(i,0,graph[ind].size())
		if(c[graph[ind][i]] == cl)
			return false;
	return true;			
}

void dfs(int ind, vector<vi>& graph, vi& c){
	For(i,0,4){
		if(valid(ind, i+1, graph, c)){
			c[ind] = i+1;
			For(j,0,graph[ind].size()){
				if(c[graph[ind][j]] == -1)
					dfs(graph[ind][j], graph, c);
			}
			return;
		}
	}
}

void solve(vector<vi>& graph, vi& c){
	
	For(i,0,graph.size())
		if(c[i] == -1)
			dfs(i,graph,c);

}

int main()
{
	int n;
	cin >> n;
	map<int,vi> up,dw,rt,lt;
	vector<rec> recs;
	vector<vi> graph(n);
	vi c(n, -1);
	For(i,0,n){
		int x1,x2,y1,y2;
		cin >> x1 >> y1 >> x2 >> y2;
		recs.push_back({x1,y1,x2,y2});
		
		For(j,0,rt[x1].size()){
			int u = rt[x1][j];
			if(recs[u].y2 > y1 && recs[u].y1 < y2){
				graph[i].push_back(u), graph[u].push_back(i);
			}
		}

		For(j,0,lt[x2].size()){
			int u = lt[x2][j];
			if(recs[u].y2 > y1 && recs[u].y1 < y2)
				graph[i].push_back(u), graph[u].push_back(i);
		}

		For(j,0,dw[y2].size()){
			int u = dw[y2][j];
			if(recs[u].x2 > x1 && recs[u].x1 < x2)
				graph[i].push_back(u), graph[u].push_back(i);
		}

		For(j,0,up[y1].size()){
			int u = up[y1][j];
			if(recs[u].x2 > x1 && recs[u].x1 < x2)
				graph[i].push_back(u), graph[u].push_back(i);
		}

		rt[x2].push_back(i);
		dw[y1].push_back(i);
		up[y2].push_back(i);
		lt[x1].push_back(i);
	}

	solve(graph, c);
	cout << "YES" << endl;
	For(i,0,n)
		cout << c[i] << endl;
}