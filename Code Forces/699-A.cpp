/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

char c[200005];
vi r;
vi l;
int main()
{
	int n;
	cin >> n;
	int rc = 0,lc = 0;
	For(i,0,n)
	{
		cin >> c[i];
	}
	For(i,0,n)
	{
		int a; cin >> a;
		if(c[i] == 'R')
			r.push_back(a);
		else 
			l.push_back(a);
	}
	int l0 = 0, r0 = 0;
	int res = INT_MAX;
	while(r0 < r.size() && l0 < l.size())
	{
		//trace(r[r0] _ l[l0]);
		if(r[r0] <= l[l0])
		{
			res = min(res, (l[l0] - r[r0])/2);
			r0++;
		}
		else
			l0++;
	}
	if(res == INT_MAX)
		cout << -1 << endl;
	else
		cout << res << endl;
}