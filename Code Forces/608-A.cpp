/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <iomanip>
#include <fstream>
#include <limits.h>
#include <time.h>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) //cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int main()
{
	/*ifstream fin("file.in");
	ofstream fout("file.out");*/
	int n,s;
	cin >> n >> s;
	map<int,int> mp;
	mp[0] = 0;
	mp[s] = 0;
	For(i,0,n)
	{
		int f,t;
		cin >> f >> t;
		mp[f] = max(mp[f],t);
	}
	int sum = mp[s];
	for(auto it = ++mp.rbegin(); it!= mp.rend(); it++)
	{
		auto rit = it;
		rit--;
		sum += rit->first - it->first;
		trace(rit->first _ rit->second)
		trace(it->first _ it->second)
		trace(sum)
		if(it->second > sum)
			sum+= it->second - sum;
	}
	cout<<sum<<endl;
}
