#include <iostream>
#include <vector>
#include <queue>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define allof(x) x.begin(),x.end()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

struct st
{
	vi up, down, left, right;
};

int main()
{
	vector<ii> grid;
	int n;
	cin >> n;
	For(i, 0, n)
	{
		int a, b;
		cin >> a >> b;
		grid.push_back(make_pair(a, b));
	}
	int sum = 0;
	For(i, 0, n)
	{
		ii p = grid[i];
		bool l = false, r = false, u = false, d = false;
		For(j, 0, n)
		{
			if (grid[j].first == p.first && p.second < grid[j].second) u = true;
			if (grid[j].first == p.first && p.second > grid[j].second) d = true;
			if (grid[j].second == p.second && p.first < grid[j].first) r = true;
			if (grid[j].second == p.second && p.first > grid[j].first) l = true;
		}
		if (r && l && u && d)
			sum++;
	}
	cout << sum << endl;
}