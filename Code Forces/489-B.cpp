#include<iostream>
#include<vector>
#include<algorithm>

#define For(i,a,b) for(int i = a;i<b;i++)
#define allof(x) x.begin(),x.end()

using namespace std;

int main()
{
	int n, m;
	cin >> n;
	vector<int> boy(n);
	For(i, 0, n)
		cin >> boy[i];
	cin >> m;
	vector<int> girl(m);
	For(i, 0, m)
		cin >> girl[i];

	sort(allof(girl));
	sort(allof(boy));
	
	int sum = 0;
	for (auto it1 = girl.begin(), it2 = boy.begin(); it1 != girl.end() && it2 != boy.end();)
	{
		if (abs(*it1 - *it2) <= 1)
		{
			sum++;
			it1++;
			it2++;
		}
		else if (*it2 > *it1)
			it1++;
		else 
			it2++;
	}
	cout << sum << endl;
}