/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <iomanip>
#include <fstream>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int fyear[370];
int myear[370];

int main()
{
	/*ifstream fin("file.in");
	ofstream fout("file.out");*/
	int n;
	cin >> n;
	For(i,0,n)
	{
		char c;
		int a,b;
		cin >> c >> a >> b;
		For(j,a,b+1)
		{
			if(c == 'F')
				fyear[j]++;
			if(c == 'M')
				myear[j]++;
		}

	}
	int mx = 0;
	For(i,0,370)
	{
		int mn = min(myear[i] , fyear[i]);
		if(mn > mx)
			mx = mn;
	}
	cout << mx*2 << endl;
}
