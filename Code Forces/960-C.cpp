/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0)
#define trace(x) cerr << #x << ": " << x << endl
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl
#define _ << " :: " <<
#define allof(x) (x).begin(),(x).end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef long double ld;
typedef pair<int, int> ii;
typedef pair<double, double> dd;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<double> vd;
typedef vector<string> vs;

int main()
{
	int x,d;
	cin >> x >> d;
	int beg = 1;
	vector<ll> res;
	int rem = __builtin_popcount(x);
	int pow = 0;
	while(x > 0) {
		int lsb = x%2;
		x = x/2;
		if(lsb) {
			For(i,0,pow) {
				res.push_back(beg);
			}
			beg += d;
		}
		pow++;
	}
	For(i,0,rem) {
		res.push_back(beg);
		beg += d;
	}
	cout << res.size() << endl;
	For(i,0,res.size()) {
		cout << res[i] << " \n"[i == res.size() - 1];
	}
}
