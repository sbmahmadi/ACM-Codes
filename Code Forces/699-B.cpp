/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
//#define X real()
//#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

int X[1005];
int Y[1005];

char grid[1005][1005];

int main()
{
	int n,m;
	cin >> n >> m;
	int cx = 0,cy = 0;
	int cnt = 0;
	For(i,0,n)
	{
		For(j,0,m)
		{
			cin >> grid[i][j];
			if(grid[i][j] == '*')
				X[i]++, Y[j]++, cnt++;
		}
	}
	bool win = false;
	For(i,0,n)
	{
		For(j,0,m)
		{
			if(X[i] + Y[j] - (grid[i][j] == '*') == cnt)
			{
				win = true,
				cx = i,
				cy = j;
				//trace(cx _ cy)
			}
		}
	}

	if(win)
	{
		cout << "YES" << endl;
		cout << cx + 1 << " " << cy + 1 << endl;
	}
	else cout << "NO" << endl;
}