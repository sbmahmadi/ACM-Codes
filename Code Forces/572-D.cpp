#include <iostream>
#include <vector>
#include <queue>
#include <math.h>
#include <algorithm>
#include <map>
#include <set>
#include <unordered_set>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define ll long long int

using namespace std;

int main()
{
	cerr<<"HELLOOO"<<endl;

	int n, k;
	cin >> n >> k;
	vector<int> a;
	
	For(i, 0, n)
	{
		int t; cin >> t;
		a.push_back(t);
	}

	sort(a.begin(), a.end());
	k = min(k, n - k);
	
	int sum = 0, p = 0;
	vector<int> nums;
	
	For(i, 0, a.size()-1)
	{
		if (a[i] == a[i + 1])
		{
			i++;
			p++;
		}
		else nums.push_back(a[i]);
	}

	if (a[a.size() - 1] != a[a.size() - 2]) nums.push_back(a.back());

	int c = 0;
	for (int i = 0; i < k - p; i++)
	{
		sum += nums[c + 1] - nums[c];
		c += 2;
	}
	cout << sum << endl;
}