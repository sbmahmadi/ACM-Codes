#include <iostream>
#include <map>

#define For(i,a,b) for(int i = a;i<b;i++)

using namespace std;

int main()
{
	map<int,int> mp;
	int n;
	cin >> n;
	For(i,0,n)
	{
		int k;
		cin >> k;
		mp[k] = i;
	}
	long long sum = 0;
	for(auto it = mp.begin();it != mp.end(); it++)
	{
		if(it->first ==1 ) continue;
		sum+= abs(it->second - mp[it->first-1]);
	}
	cout << sum << endl;
}