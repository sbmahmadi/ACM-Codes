#include<iostream>
#include<vector>
#include<string>
#include<algorithm>

using namespace std;

int main()
{
	int n;
	cin >> n;
	vector<int> arr;
	int rem[3] = { -1,-1,-1 };
	int sum = 0;

	for (int i = 0; i < n; i++)
	{
		int a;
		cin >> a;
		arr.push_back(a);
		sum += a;
	}

	sum %= 3;

	sort(arr.begin(), arr.end());

	if (arr.front() != 0)
	{
		cout << -1 << endl;
	}
	else
	{
		int ind11 = -1, ind12 = -1, ind21 = -1, ind22 = -1;

		for (int i = 0; i < arr.size(); i++)
		{
			if (arr[i] % 3 == 1)
			{
				if (ind11 == -1)
					ind11 = i;
				else if (ind12 == -1)
					ind12 = i;
			}
			if (arr[i] % 3 == 2)
			{
				if (ind21 == -1)
					ind21 = i;
				else if (ind22 == -1)
					ind22 = i;
			}
		}

		if (sum == 1)
		{
			if (ind11 != -1)
			{
				arr.erase(arr.begin() + ind11);
			}
			else if (ind22 != -1)
			{
				arr.erase(arr.begin() + ind21);
				arr.erase(arr.begin() + ind22 - 1);
			}
		}

		else if (sum == 2)
		{
			if (ind21 != -1)
			{
				arr.erase(arr.begin() + ind21);
			}
			else if (ind12 != -1)
			{
				arr.erase(arr.begin() + ind11);
				arr.erase(arr.begin() + ind12 - 1);
			}
			
		}
		if (arr.back() == 0)
		{
			cout << 0;
		}
		else
			for (auto rit = arr.rbegin(); rit != arr.rend(); rit++)
			{
				cout << *rit;
			}
			cout << endl;
	}
}