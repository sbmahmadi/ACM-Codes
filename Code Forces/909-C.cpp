/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0);
#define trace(x) cerr << #x << ": " << x << endl;
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl;
#define _ << " :: " <<
#define allof(x) (x).begin(),(x).end()
#define X real()
#define Y imag()

const int MOD  = 1e9+7;

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<double> vd;

int dp[5000][5000];
int n;

vector<char> v;

int f(int ind, int lvl) {
	if(lvl < 0)
		return 0;
	if(ind == n-1)
		return 1;
	int& me = dp[ind][lvl];
	if(me > 0) {
		return me;
	}
	if(v[ind] == 'f') {
		return me = f(ind+1, lvl+1);
	}
	ll res = f(ind+1, lvl);
	res += f(ind, lvl-1);
	res %= MOD;
	return me = res;
}

int main()
{	
	fastio();
	cin >> n;
	v = vector<char>(n);
	For(i,0,n) {
		cin >> v[i];
	}

	For(i,0,n) {
		dp[n-1][i] = 1;
	}

	cout << f(0,0) << endl;
}