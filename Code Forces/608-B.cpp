/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <iomanip>
#include <fstream>
#include <limits.h>
#include <time.h>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int f(string a,string b,int i,int size)
{
	int k = 0;
	for(int j = 0;j<size;j++)
	{
		k += (a[j] - '0') ^ (b[j+i] - '0');
	}
	return k;
}

int main()
{
	/*ifstream fin("file.in");
	ofstream fout("file.out");*/
	string a,b;
	cin >> a >> b;
	int size = a.size();
	int res = 0;
	For(i,0,b.size() - size + 1)
	{
		res += f(a,b,i,size);
	}
	cout<<res;
}
