#include <iostream>
#include <vector>
#include <queue>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int board[10][10];
int black[8];
int white[8];

int main()
{
	memset(black, -1, sizeof black);
	memset(white, -1, sizeof white);
	For(i, 0, 8)
	{	
		string s;
		cin >> s;
		For(j, 0, 8)
		{
			if (s[j] == 'B' && i >= black[j]) black[j] = i;
			else if (s[j] == 'W' && i >= white[j]) white[j] = 7 - i;
		}
	}
	int maxB=-1, maxW=-1;
	For(i, 0, 8)
	{
		if (black[i] >= 0 && white[i] >= 0) continue;
		else if (black[i] >= 0 && black[i] > maxB)
		{
			maxB = black[i];
		}
		else if (white[i] >= 0 && white[i] > maxW)
		{
			maxW = white[i];
		}
	}
	if (maxB > maxW) cout << 'B' << endl;
	else cout << 'A' << endl;
}