/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0)
#define trace(x) //cerr << #x << ": " << x << endl
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl
#define _ << " :: " <<
#define allof(x) (x).begin(),(x).end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef long double ld;
typedef pair<int, int> ii;
typedef pair<double, double> dd;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<double> vd;
typedef vector<string> vs;

int n,k;

vector<vector<char>> grid;
vector<vi> val;

void f(int x, int y, int dx, int dy) {
	trace(x _ y);
	int nx = x;
	int ny = y;
	bool ok = true;
	For(i,0,k) {
		if(grid[nx][ny] == '#')
			ok = false;
		nx += dx;
		ny += dy;
	}
	trace(2 _ ok);
	if(ok) {
		For(i,0,k) {
			val[x][y]++;
			x += dx;
			y += dy;
		}
	}
}

int main()
{
	cin >> n >> k;
	grid = vector<vector<char>>(n, vector<char>(n));
	val = vector<vi>(n, vi(n,0));

	For(i,0,n)
		For(j,0,n)
			cin >> grid[i][j];

	int mx = 0;
	int mi = 0, mj = 0;
	For(i,0,n) {
		For(j,0,n) {
			if(grid[i][j] == '#')
				continue;
			if(j+k-1 < n)
				f(i,j,0,1);
			if(i+k-1 < n)
				f(i,j,1,0);
			if(val[i][j] > mx) {
				mx = val[i][j];
				mi = i;
				mj = j;
			}
		}
	}
	cout << ++mi << " " << ++mj << endl;
}