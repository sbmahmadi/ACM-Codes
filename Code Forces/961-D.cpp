/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0)
#define trace(x) //cerr << #x << ": " << x << endl
#define traceV(x,t) //cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl
#define _ << " :: " <<
#define allof(x) (x).begin(),(x).end()
#define X first
#define Y second

using namespace std;

typedef long long ll;
typedef long double ld;
typedef pair<ld, ld> ii;
typedef pair<double, double> dd;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<double> vd;
typedef vector<string> vs;

const ld eps = 1e-7;

vii ps;
int n;

ld ABS(ld a) {
	if(a < 0)
		return -a;
	return a;
}

bool online(ii a, ii b, ii c) {

	return ABS(a.X * (b.Y - c.Y) + b.X*(c.Y - a.Y) + c.X * (a.Y - b.Y)) < eps;
}

bool f(int a, int b) {
	
	vi visit(n,0);
	visit[a] = visit[b] = 1;
	int c = -1, d = -1;
	For(i,0,n) {
		if(i == a || i == b)
			continue;
		if(online(ps[a],ps[b],ps[i])) {
			trace("LINE: " _ a _ b _ i);
			visit[i] = true;
		} else {
			if(c == -1) 
				c = i;
			else 
				d = i;
		}
	}
	traceV(visit,int);
	trace(c _ d);
	if(d == -1) {
		trace("ME");
		return true;
	}
	visit[c] = visit[d] = true;
	For(i,0,n) {
		if(visit[i])
			continue;
		if(!online(ps[c],ps[d],ps[i]))
			return false;
	}
	trace("ME2");
	return true;
}

int main()
{
	cin >> n;
	ps = vii(n);
	For(i,0,n)
		cin >> ps[i].X >> ps[i].Y;
	sort(allof(ps));
	if(n < 5) {
		cout << "YES" << endl;
		return 0;
	}
	int third = -1;
	For(i,2,n) {
		if(!online(ps[0],ps[1],ps[i])) {
			third = i;
			break;
		}
	}
	trace(third);
	if(third == -1) {
		cout << "YES" << endl;
		return 0;
	}
	if(f(0,1) || f(0,third) || f(1,third)) {
		cout << "YES" << endl;
	} else 
		cout << "NO" << endl;
}