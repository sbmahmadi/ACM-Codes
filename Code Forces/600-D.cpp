#include <iostream>
#include <vector>
#include <queue>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <iomanip>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define allof(x) x.begin(),x.end()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

struct Node
{
	unordered_map<int> mp;
	int color;
	vector<int> childs;
};

vector<Node> tree;

int main()
{
	int n;
	cin >> n;
	tree.assign(n,Node());
	For(i,0,n)
	{
		cin>> tree[i].color;
	}
	For(i,0,n-1)
	{
		int a,b;
		cin >> a >> b;
		if(a>b) swap(a,b);
		tree[a-1].childs.push_back(b-1);
	}
	unordered_map<int> res = f(0);
}