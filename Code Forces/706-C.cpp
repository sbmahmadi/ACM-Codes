/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <list>
#include <vector>
#include <queue>
#include <deque>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <math.h>
#include <algorithm>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

string s[100050];
int c[100050];
ll dp[2][100050];
int n;

bool smaller(const string& s1,const string& s2)
{
	For(i,0,min(s1.size(),s2.size()))
	{
		if(s1[i] != s2[i])
		{
			return s1[i] < s2[i];
		}
	}
	return s1.size() <= s2.size();
}

ll f(int rev, int ind)
{
	if(ind == n)
		return 0;
	if(dp[rev][ind] != -1)
		return dp[rev][ind];

	string tmp = s[ind-1];
	if(rev == 1)
		reverse(allof(tmp));
	
	ll res = LLONG_MAX;
	
	string revs = s[ind];
	reverse(allof(revs));
	
	if(smaller(tmp,s[ind]))
	{
		ll t = f(0,ind+1);
		if(t != -2)
			res = min(res,f(0,ind+1));
	}
	if(smaller(tmp,revs))
	{
		ll t = f(1, ind + 1);
		if(t != -2)
			res = min(res, c[ind] + f(1, ind + 1));
	}
	if(res == LLONG_MAX)
		res = -2;
	return dp[rev][ind] = res;
}

ll solve()
{
	memset(dp,-1,sizeof dp);
	ll res = min(f(0,1), c[0] + f(1,1));
	if(f(0,1) == -2 && f(1,1) == -2)
		return -1;
	if(f(0,1) == -2)
		return c[0] + f(1,1);
	if(f(1,1) == -2)
		return f(0,1);
	return res;
}

int main()
{
	cin >> n;
	For(i,0,n)
		cin >> c[i];
	For(i,0,n)
		cin >> s[i];
	cout << solve() << endl;
}