#include <iostream>
#include <set>
#include <vector>
#include <math.h>
#include <iomanip>

#define For(i,a,b) for(int i = a; i < b; i++)

using namespace std;


typedef long double ld;
typedef pair<ld,ld> pdd;

set<pdd> st;

int n,r;

pdd push(int x) {
	ld mx = 0;
	for(auto it = st.rbegin(); it != st.rend(); it++) {
		pdd p = *it;
		if(abs(x - p.second) <= 2*r) {
			double dy = sqrt((2.0*r)*(2.0*r) - (x - p.second)*(x - p.second));
			mx = max(mx, dy + p.first);
		}
	}
	if(mx)
		return {mx,x};
	return {r,x};
}

int main() {
	cin >> n >> r;
	cout << fixed << setprecision(8);
	vector<ld> v;

	For(i,0,n) {
		int x;
		cin >> x;
		pdd p = push(x);
		v.push_back(p.first);
		st.insert(p);
	}
	For(i,0,n)
		cout << v[i] << " \n"[i == n-1];
}