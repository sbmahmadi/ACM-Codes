#include <iostream>
#include <queue>

#define For(i,a,b) for(int i = a; i < b; i++)
#define trace(x) //cerr << #x << ": " << x << endl;
#define _ << " :: " <<
using namespace std;

int main() {
	int n;
	cin >> n;
	long long res = 0;
	priority_queue<int> bl,rd;
	
	int lasr = -1;
	int lasb = -1;
	int lasg = -1;
	For(i,0,n) {
		int x; char c;
		cin >> x >> c;
		if(c == 'G') {
			if(lasg != -1) {
				res += x - lasg;
				trace("GREEN ADDED" _ (x-lasg));
				if(lasr > lasg){
					trace("GREEN" _ x _ lasg _ lasr _ lasb)
					res += x - lasr;
					trace("RED ADDED" _ (x-lasr));
					rd.push(x-lasr);
				}
				if(lasb > lasg){
					trace("BLUE ADDED" _ (x-lasb));
					res += x - lasb;
					bl.push(x-lasb);
				}
			} else {
				if(lasr > 0)
					res += x - lasr;
				if(lasb > 0)
					res += x - lasb;
			}
			
			lasg = x;

			if(bl.size()){
				res -= bl.top();
			}
			if(rd.size())
				res -= rd.top();
			bl = priority_queue<int>();
			rd = priority_queue<int>();
		}
		if(c == 'B') {
			int d = max(lasb, lasg);
			lasb = x;
			if(d == -1)
				continue;
			res += x - d;
			trace("BLUE ADDED : " _ (x-d));
			if(lasg != -1) {
				bl.push(x-d);
			}
		}

		if(c == 'R') {
			int d = max(lasr, lasg);
			lasr = x;
			if(d == -1)
				continue;
			res += x - d;
			trace("RED ADDED : " _ (x-d));
			if(lasg != -1) {
				rd.push(x-d);
			}
		}
	}
	cout << res << endl;
}