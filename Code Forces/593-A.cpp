#include<iostream>
#include<string>
#include<set>

using namespace std;
int grid[26][26];
int main()
{
	int n;
	cin >> n;
	for (int i = 0; i < n; i++)
	{
		string s;
		cin >> s;
		set<char> st;
		for(int j = 0; j < s.size(); j++)
		{
			st.insert(s[j]);
		}
		if (st.size() > 2) continue;
		if (st.size() == 2)
		{
			grid[*st.begin() - 'a'][*(++st.begin()) - 'a'] += s.size();
		}
		if (st.size() == 1)
		{
			grid[*st.begin() - 'a'][*st.begin() - 'a'] += s.size();
		}
	}
	int max = 0;
	for (int i = 0; i < 26; i++)
	{
		for (int j = i+1; j < 26; j++)
		{
			int val = grid[i][j] + grid[i][i] + grid[j][j];
			if (val > max) max = val;
		}
	}
	cout << max << endl;
}