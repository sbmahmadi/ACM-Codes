#include<iostream>
#include<vector>
#include<queue>
#include<math.h>
#include<algorithm>
#include<map>
#include<set>
#include<unordered_set>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define ll long long int

using namespace std;

int main()
{
	int n, m;
	cin >> n >> m;
	if (n == 1)
	{
		cout << 1;
	}
	else if (m > n/2)
	{
		cout << m - 1<<endl;
	}
	else
	{
		cout << m + 1 << endl;
	}
}