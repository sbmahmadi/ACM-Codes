#include<iostream>
#include<stack>
#include<string>

using namespace std;

int main()
{
	string s;
	/*stack<char>*/ int st = 0;
	cin >> s;
	int sum = 0;

	for (char c : s)
	{
		if (c == '(')
			st++;
		else if (c == ')')
		{
			if (st>0)
			{
				st--;
			}
			else
			{
				cout << -1 << endl;
				return 0;
			}
		}
		else if (c == '#')
		{
			if (st > 0)
			{
				cout << st << endl;
				st=0;
			}
			else if (st == 0)
				st++;
		}
	}
}