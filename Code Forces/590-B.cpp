#include <iostream>
#include <iomanip>
#include <math.h>

using namespace std;

struct point
{
	double x, y;
};

point src, dest, fWind, sWind;
double vmax, twind;

double BS(double begin, double end)
{
	for (int repeat = 0; repeat < 200; repeat++) 
	{
		double newT = (end + begin) * 0.5;
		point newSrc;
		
		newSrc.x = src.x + min(newT,twind) * fWind.x + max(0.0,newT - twind)*sWind.x;
		newSrc.y = src.y + min(newT,twind) * fWind.y + max(0.0,newT - twind)*sWind.y;

		double newdist = hypot(newSrc.x - dest.x, newSrc.y  - dest.y);
		if (newdist > vmax * newT) begin = newT;
		else end = newT;
	}
	return (begin + end) * 0.5;
}

int main()
{
	cout << fixed << setprecision(10);
	cin >> src.x >> src.y >> dest.x >> dest.y;
	cin >> vmax >> twind;
	cin >> fWind.x >> fWind.y >> sWind.x >> sWind.y;
	if (src.x == dest.x && src.y == dest.y)
	{
		cout << 0 << endl;
		return 0;
	}
	cout << BS(0, 1e12) << endl;
}