#include <iostream>
#include <vector>
#include <queue>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <iomanip>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define allof(x) x.begin(),x.end() 

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int main()
{
	string s;
	cin >> s;
	vector<int>a, b;
	For(i, 0, s.size() - 1)
	{
		if (s[i] == 'A' && s[i + 1] == 'B') a.push_back(i);
		if(s[i] == 'B' && s[i + 1] == 'A') b.push_back(i);
	}
	if (a.empty() || b.empty())
	{
		cout << "NO" << endl;
		return 0;
	}
	if (abs(a.front() - b.back()) > 1 || abs(b.front() - a.back()) > 1)
	{
		cout << "YES" << endl;
	}
	else 
		cout << "NO" << endl;
}