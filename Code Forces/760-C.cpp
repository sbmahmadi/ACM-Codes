/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

void dfs(int i, vi& visit, vi& p){
	visit[i] = true;
	if(!visit[p[i]])
		dfs(p[i],visit,p);
}

int main()
{
	int n;
	cin >> n;
	vi p(n);
	For(i,0,n){
		cin >> p[i]; p[i]--;
	}	
	int carry = 0;
	For(i,0,n){
		int a;
		cin >> a;
		carry += a;
	}
	carry %= 2; carry = 1 - carry;

	vi visit(n,0);
	int c = 0;
	For(i,0,n){
		if(!visit[i])
			dfs(i,visit,p),c++;
	}
	if(c == 1)
		c = 0;
	//trace(c _ carry)
	cout << c + carry << endl;
}
