/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

int t;

void dfs(int ind, vi& visited, vector<vi>& graph)
{
	visited[ind] = true;
	t++;
	For(i,0,graph[ind].size())
	{
		if(!visited[graph[ind][i]])
			dfs(graph[ind][i], visited, graph);
	}
}

int main()
{
	int n,m,k;
	cin >> n >> m >> k;
	set<int> st;
	For(i,0,k)
	{
		int h;
		cin >> h; h--;
		st.insert(h);
	}
	vector<vi> graph(n);
	For(i,0,m)
	{
		int u,v;
		cin >> u >> v; u--; v--;
		graph[u].push_back(v);
		graph[v].push_back(u);
	}
	vi visited(n,0);
	vii comps;
	ll res = 0;
	int mx = -1, mxind = -1;
	int i = 0;
	for(auto it = st.begin(); it != st.end(); i++, it++)
	{
		t = 0;
		dfs(*it, visited, graph);
		comps.push_back({*it,t});
		if(t > mx)
		{
			mx = t;
			mxind = *it;
		}
	}

	For(i,0,n)
	{
		if(!visited[i])
		{
			t = 0;
			dfs(i,visited,graph);
			comps.push_back({i,t});
		}
	}

	For(i,0,comps.size())
	{
		For(j,i,comps.size())
		{
			if(i==j)
				res += (comps[i].second * (comps[i].second - 1)) / 2;
			
			else if(st.count(comps[i].first) == 0 && (st.count(comps[j].first) == 0 || comps[j].first == mxind))
			{
				res += comps[i].second * comps[j].second;
			}
			else if(st.count(comps[j].first) == 0 && comps[i].first == mxind)
			{
				res += comps[i].second * comps[j].second;
			}
		}
	}
	cout << res - m << endl;
}
