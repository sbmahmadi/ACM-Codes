/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

vi sz;

ll lcm(ll a, ll b)
{
	return a*b / __gcd(a,b);
}

void dfs(int ind, vi& cr, vi& visit, vi& cycle)
{
	visit[ind] = true;

	if(!visit[cr[ind]])
		dfs(cr[ind],cr,visit,cycle);
	else
	{
		cycle[ind] = true;
		int s = 1;
		while(!cycle[cr[ind]])
		{
			cycle[cr[ind]] = true;
			s++;
			ind = cr[ind];
		}
		if(s%2 == 0)
			s/= 2;
		sz.push_back(s);
	}
}

int main()
{
	int n;
	cin >> n;
	vi cr(n);
	For(i,0,n)
		cin >> cr[i], cr[i]--;
	vi visit(n,0);
	vi cycle(n,0);
	For(i,0,n)
		{ 	visit.assign(n,0);
			dfs(i,cr,visit,cycle);
		}
	bool ok = true;
	For(i,0,n)
	{
		if(!cycle[i])
		{
			cout << -1 << endl;
			return 0;
		}
	}
	ll t = 1;
	For(i,0,sz.size())
	{
		t = lcm(t,sz[i]);
	}
	cout << t << endl;
}