#include <iostream>

using namespace std;

int main() {
	string s;
	cin >> s;
	int r = 0;
	for(char c : s) {
		if(c <= '9' && c >= '0' && ((c - '0')&1))
			r++;
		else if(c == 'a' || c == 'e' || c == 'i' || c =='o' || c == 'u')
			r++;
	}
	cout << r << endl;
}