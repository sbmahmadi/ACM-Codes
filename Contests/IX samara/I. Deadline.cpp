/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <list>
#include <vector>
#include <queue>
#include <deque>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <math.h>
#include <algorithm>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");


struct Task
{
	int d,c,i;
};

vector<Task> tasks;
vector<vi> graph;
vi stack;
vi in;
struct cmp
{
	bool operator()(const int a, const int b) const
	{
		return tasks[a].d < tasks[b].d;
	}	
};


bool toposort()
{
	int time = 0;
	set<int,cmp> st;
	For(i,0,graph.size())
		if(in[i] == 0)
			st.insert(i);
	if(st.size() == 0)
		return false;
	while(st.size())
	{
		int u = *st.begin(); st.erase(st.begin());
		if(tasks[u].d - time < tasks[u].c)
			return false;
		time = tasks[u].d;
		stack.push_back(u);
		For(i,0,graph[u].size())
		{
			in[graph[u][i]]--;
			if(in[graph[u][i]] == 0)
				st.insert(graph[u][i]);
		}
	}
	return true;
}

int main()
{
	int n;
	cin >> n;
	tasks.resize(n);
	graph.resize(n);
	in.assign(n,0);
	For(i,0,n)
	{
		int r;
		cin >> tasks[i].d >> tasks[i].c >> r;
		tasks[i].i = i;
		For(j,0,r)
		{
			int u;
			cin >> u; u--;
			graph[u].push_back(i);
		}
		in[i] = r;
	}

	if(!toposort())
		cout << "NO" << endl;
	else
	{
		cout << "YES" << endl;
		For(i,0,stack.size())
		{
			cout << stack[i] + 1;
			if(i != stack.size()-1)
				cout << " ";
		}
		cout << endl;
	}
}