/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <list>
#include <vector>
#include <queue>
#include <deque>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <math.h>
#include <algorithm>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

char grid[55][55];
int visit[55][55];
int lx,ly,n,m;
int dx[] = {1,-1,0,0};
int dy[] = {0,0,1,-1};

void dfs(int x, int y)
{
	visit[x][y] = true;
	For(i,0,4)
	{
		int nx = x + dx[i];
		int ny = y + dy[i];
		if(ny >= 0 && ny < m && nx >= 0 && nx < n && !visit[nx][ny] && grid[nx][ny] != '#')
			dfs(nx,ny);
	}
}

bool isConnected()
{
	For(i,0,n)
		For(j,0,m)
			if(grid[i][j] != '#' && !visit[i][j])
				return false;
	return true;
}

bool solve()
{
	For(i,0,n)
		For(j,0,m)
			if(grid[i][j] == '?')
			{
				memset(visit,0,sizeof visit);
				grid[i][j] = '#';
				dfs(lx,ly);
				if(isConnected())
					return false;
				grid[i][j] = '.';
			}
	return true;
}

int main()
{
	cin >> n >> m;
	For(i,0,n)
		For(j,0,m)
		{
			cin >> grid[i][j];
			if(grid[i][j] == '.')
				lx = i, ly = j;
		}
	For(i,0,n)
	{
		if(grid[i][0] == '.' ||  grid[i][m-1] == '.')
		{
			cout << "Impossible" << endl;
			return 0;
		}
		grid[i][0] = grid[i][m-1] = '#';
	}
	For(i,0,m)
	{
		if(grid[0][i] == '.' ||  grid[n-1][i] == '.')
		{
			cout << "Impossible" << endl;
			return 0;
		}
		grid[0][i] = grid[n-1][i] = '#';
	}
	memset(visit, 0, sizeof visit);
	dfs(lx,ly);
	if(!isConnected())
		cout << "Impossible" << endl;
	else
	{
		if(!solve())
			cout << "Ambiguous" << endl;
		else
		{
			For(i,0,n)
			{
				For(j,0,m)
					cout << grid[i][j];
				cout << endl;
				//if(i != n-1)
				//	cout << endl;
			}
		}
	}
}