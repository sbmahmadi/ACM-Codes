/*
ID: behdad.1
LANG: C++11
PROB:
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <list>
#include <vector>
#include <queue>
#include <deque>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <math.h>
#include <algorithm>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

struct s
{
	int p, x, i, par;
};

bool cmp1(const s s1, const s s2)
{
	return s1.p > s2.p;
}

bool cmp2(const s s1, const s s2)
{
	return s1.i < s2.i;
}

struct cmp3
{
	bool operator()(const s s1, const s s2) const
	{
		return s1.x < s2.x;
	}
};

int main()
{
	int n;
	cin >> n;
	vector<s> v(n);
	For(i, 0, n)
		cin >> v[i].x >> v[i].p,
		v[i].i = i+1;
	sort(allof(v), cmp1);
	set<s, cmp3> xs;
	xs.insert(v[0]);
	v[0].par = -1;
	vector<s>::iterator it = ++v.begin();
	for (; it != v.end(); it++)
	{
		auto it1 = xs.lower_bound(*it);
		auto it2 = it1; it2--;
		if (it1 == xs.end())
			it1 = it2;
		if (abs(it->x - it1->x) < abs(it->x - it2->x) || abs(it->x - it1->x) == abs(it->x - it2->x) && it1->p >= it2->p)
			it->par = it1->i;
		else if (abs(it->x - it1->x) > abs(it->x - it2->x) || abs(it->x - it1->x) == abs(it->x - it2->x) && it1->p < it2->p)
			it->par = it2->i;
		xs.insert(*it);
	}
	sort(allof(v), cmp2);
	Fori(it, v)
	{
		cout << it->par;
		if (it != --v.end())
			cout << " ";
	}
	cout << endl;

}