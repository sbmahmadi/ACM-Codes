#include <bits/stdc++.h>

using namespace std;

int main()
{
	int n, m, k;
	while(cin >> n >> m >> k && (n || m || k))
	{
		int rem = n - m  - k;
		if(m + rem <= k)
			cout << -1 << endl;
		else if(rem == 0)
			cout << 0 << endl;
		else
		{
			int cnt = 0;
			k += rem;
			while(m <= k)
			{
				m++;
				k--;
				cnt++;
			}
			cout << cnt << endl;
		}
	}
}