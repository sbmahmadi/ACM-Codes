#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define allof(x) x.begin(),x.end()

using namespace std;

typedef long long ll;

struct Point
{
	ll x, y;
	Point(ll x, ll y)
	{
		this->x = x;
		this->y = y;
	}
	Point()
	{

	}
	bool operator==(const Point p)
	{
		return x == p.x && y == p.y;
	}
	bool operator<(const Point p)
	{
		if(x == p.x)
			return y < p.y;
		return x < p.x;
	}
};

Point p0;

Point nextToTop(vector<Point> &S)
{
	Point p = S.back();
	S.pop_back();
	Point res = S.back();
	S.push_back(p);
	return res;
}

void swap(Point &p1, Point &p2)
{
	Point temp = p1;
	p1 = p2;
	p2 = temp;
}

ll distSq(Point p1, Point p2)
{
	return (p1.x - p2.x)*(p1.x - p2.x) +
		(p1.y - p2.y)*(p1.y - p2.y);
}

ll orientation(Point p, Point q, Point r)
{
	ll val = (q.y - p.y) * (r.x - q.x) -
		(q.x - p.x) * (r.y - q.y);

	if (val == 0) return 0;
	return (val > 0) ? 1 : 2;
}

int compare(const void *vp1, const void *vp2)
{
	Point *p1 = (Point *)vp1;
	Point *p2 = (Point *)vp2;

	ll o = orientation(p0, *p1, *p2);
	if (o == 0)
		return (distSq(p0, *p2) >= distSq(p0, *p1)) ? -1 : 1;

	return (o == 2) ? -1 : 1;
}

vector<Point> convexHull(vector<Point>& points, ll n)
{
	ll ymin = points[0].y, min = 0;
	for (ll i = 1; i < n; i++)
	{
		ll y = points[i].y;
		if ((y < ymin) || (ymin == y && points[i].x < points[min].x))
			ymin = points[i].y, min = i;
	}

	swap(points[0], points[min]);
	p0 = points[0];
	qsort(&points[1], n - 1, sizeof(Point), compare);
	ll m = 1;
	for (ll i = 1; i<n; i++)
	{
		while (i < n - 1 && orientation(p0, points[i], points[i + 1]) == 0)
			i++;


		points[m] = points[i];
		m++;
	}
	vector<Point> S;
	if (m < 3)
		return S;
	
	S.push_back(points[0]);
	S.push_back(points[1]);
	S.push_back(points[2]);
	for (int i = 3; i < m; i++)
	{
		while (orientation(nextToTop(S), S.back(), points[i]) != 2)
			S.pop_back();
		S.push_back(points[i]);
	}
	return S;
}

int main()
{
	int n;
	while(cin >> n)
	{
		if(n == 0)
			break;
		vector<Point> points;
		For(i,0,n)
		{
			int a,b;
			cin >> a >> b;
			Point p(a, b);
			points.push_back(p);
		}
		sort(allof(points));
		points.erase(unique(allof(points)),points.end());

		n = points.size();
		
		if(n < 2)
		{
			cout << 0 << " " << 0 << endl;
			continue;
		}
		ll a = 0, b = 0;
		vector<Point> ch = convexHull(points,n);

		if(ch.size()  == 0)
		{
			ll distx = abs(points[0].x - points[points.size()-1].x);
			ll disty = abs(points[0].y - points[points.size()-1].y);

			a += abs(disty - distx);
			b += min(distx, disty);
			cout << a*2 << " " << b*2 << endl;
			continue;
		}

		n = ch.size();

		For(i,0,n-1)
		{
			ll distx = abs(ch[i].x - ch[i+1].x);
			ll disty = abs(ch[i].y - ch[i+1].y);

			a += abs(disty - distx);
			b += min(distx, disty);
		}
		ll distx = abs(ch[0].x - ch[n-1].x);
		ll disty = abs(ch[0].y - ch[n-1].y);

		a += abs(disty - distx);
		b += min(distx, disty);

		cout << a << " " << b << endl;
	}

}