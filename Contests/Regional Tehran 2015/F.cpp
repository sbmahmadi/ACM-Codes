#include <bits/stdc++.h>

using namespace std;

struct Game
{
	int a;
	double l, p;
	bool operator<(const Game &g1)
	{
		double d1 = 1 - l;
		double d2 = 1 - g1.l;
		if(a == 0 && g1.a == 0)
			return l < g1.l;
		else if(g1.a == 0)
			return 1;
		else if(a == 0)
			return 0;
		return d1 / a < d2 / g1.a;
	}
};

vector<Game> v;

int main()
{
	int n, k, x0;
	cin >> n >> k >> x0;
	for(int i = 0; i < n; i++)
	{
		Game g;
		cin >> g.a >> g.l >> g.p;
		g.l /= 100.0;
		g.p /= 100.0;
		v.push_back(g);
	}
	sort(v.begin(), v.end());
	double res = 0;
	int sum = x0;
	for(int i = 0; i < k; i++)
	{
		sum += v[i].a;
		res += v[i].p * (1 - v[i].l) * sum;
	}
	cout << fixed << setprecision(2);
	cout << res << endl;
}