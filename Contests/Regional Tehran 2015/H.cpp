#include <bits/stdc++.h>

using namespace std;

void f(string &s, int minsize, set<string>& st)
{
	while(st.count(s))
	{
		s.pop_back();
	}
	if(s.size() >= minsize)
	{
		st.insert(s);
	}
}

int main()
{
	ios_base::sync_with_stdio(0);
	cin.tie(0);
	int k,m;
	while(cin >> k >> m && k)
	{
		set<string> st;
		while(k--)
		{
			int n;
			string s;
			cin >> n >> s;
			while(n--)
			{
				f(s,m,st);	
			}
		}
		cout << st.size() << endl;
	}
}