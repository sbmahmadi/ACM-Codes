#include <bits/stdc++.h>

#define fore(i,a,b) for(int i = a; i < b; i++)
#define allof(x) (x).begin(),(x).end()
#define L(s) (int)(s).size()
#define trace(x) cerr << #x << ": " << x << endl
#define tracev(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl;
#define _ << " " << 

using namespace std;

typedef vector<int> vi;
typedef pair<int,int> ii;

bool check(string s, int mid)
{
	fore(i, 0, L(s))
	{
		if(s[i] == '?')
		{
			mid--;
			if(mid > 0)
				s[i] = '(';
			else
				s[i] = ')';
		}
	}
	int open = 0, close = 0;
	fore(i, 0, L(s))
	{
		if(s[i] == '(') open++;
		else close++;
		if(close > open)
			return false;
	}
	return true;
}

int main()
{
	string s;
	cin >> s;
	if(L(s) & 1)
	{
		cout << "Impossible" << endl;
		return 0;
	}
	int open = 0, close = 0, unknown = 0;
	fore(i, 0, L(s))
	{
		if(s[i] == '(')
			open++;
		else if(s[i] == ')')
			close++;
		else
			unknown++;
	}
	int open_unknown = 0;
	fore(i, 0, L(s))
	{
		if(open_unknown + open < close + unknown - open_unknown)
		{
			if(s[i] == '?')
			{
				s[i] = '(';
				open_unknown++;
			}
		}
		else
		{
			if(s[i] == '?')
				s[i] = ')';
		}
	}
	open = 0, close = 0;
	fore(i, 0, L(s))
	{
		if(s[i] == '(')
			open++;
		else
			close++;
		if(close > open)
		{
			cout << "Impossible" << endl;
			return 0;
		}
	}
	open = 0, close = 0;
	for(int i = L(s); i >= 0; i--)
	{
		if(s[i] == '(')
			open++;
		else
			close++;
		if(close < open)
		{
			cout << "Impossible" << endl;
			return 0;
		}
	}
	cout << s << endl;
}