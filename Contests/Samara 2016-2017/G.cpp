#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define allof(x) (x).begin(),(x).end()
#define trace(x) cerr << #x << ": " << x << endl
#define tracev(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl;
#define _ << " " << 

using namespace std;

typedef vector<int> vi;
typedef pair<int,int> ii;

struct ax{
	int w,c,i;

	bool operator<(const ax a) const {
		if(w != a.w)
			return w < a.w;
		if(c != a.c)
			return c < a.c;
		return i < a.i;
	}
};

struct orc{
	int w,c,i,axe;

	bool operator<(const orc a) const {
		if(w <= a.w && c <= a.c)
			return 0;
		return w < a.w;
	}
};

bool cmp(const orc a, const orc b) {
	return a.i < b.i;
}

bool cmp1(const ax a, const ax b) {
	return a.w < b.w;
}

bool cmp2(const ax a, const ax b) {
	return a.c < b.c;
}


int main()
{	
	int n;
	cin >> n;
	vector<orc> orcs;
	vi ci(n),wi(n);
	For(i,0,n) {
		int a,b;
		cin >> a >> b;
		orcs.push_back({a,b,i});
	}
	int m;
	cin >> m;
	vector<ax> axs;
	For(i,0,m){
		int w,c;
		cin >> w >> c;
		axs.push_back({w,c,i+1});
	}

	sort(allof(axs));
	sort(allof(orcs));
	
	For(i,0,n){
		ax a({orcs[i].w, orcs[i].c, 0});
		auto it = lower_bound(allof(axs),a,cmp1);
		trace(it->w _ it->c);
		auto it1 = lower_bound(it, axs.end(), a, cmp2);
		if(it1 == axs.end()){
			cout << -1 << endl;
			return 0;
		}
		orcs[i].axe = it1->i;
	}

	sort(allof(orcs), cmp);
	For(i,0,n)
		cout << orcs[i].axe << " ";
	cout << endl;
}