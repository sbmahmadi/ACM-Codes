#include <bits/stdc++.h>

#define fore(i,a,b) for(int i = a; i < b; i++)
#define allof(x) (x).begin(),(x).end()
#define L(s) (int)(s).size()
#define trace(x) cerr << #x << ": " << x << endl
#define tracev(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl;
#define _ << " " << 

using namespace std;

typedef vector<int> vi;
typedef pair<int,int> ii;

int main()
{
	ios_base::sync_with_stdio(false);
	cin.tie(0);
	int n;
	cin >> n;
	vi v(n);
	fore(i, 0, n)
		cin >> v[i];
	vi vp;
	int prev = -1;
	fore(i, 0, L(v))
	{
		if(v[i] != prev)
			vp.push_back(v[i]);
		prev = v[i];
	}
	v = vp;
	int mini = *min_element(allof(v));
	long long sum = mini;
	if(L(v) == 1)
	{
		cout << v[0] << endl;
		return 0;
	}
	if(L(v) == 2)
	{
		cout << *max_element(allof(v)) << endl;
		return 0;
	}
	fore(i, 1, L(v) - 1)
	{
		if(v[i] > v[i - 1] && v[i + 1] < v[i])
			sum += v[i] - mini;
	}
	if(v[0] > v[1])
		sum += v[0] - mini;
	reverse(allof(v));
	if(v[0] > v[1])
		sum += v[0] - mini;
	cout << sum << endl;
}