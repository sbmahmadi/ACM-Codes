#include <bits/stdc++.h>

#define fore(i,a,b) for(long long i = a; i < b; i++)
#define allof(x) (x).begin(),(x).end()
#define L(s) (int)(s).size()
#define trace(x) cerr << #x << ": " << x << endl
#define tracev(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl;
#define _ << " " << 

using namespace std;

int main()
{
	long long p;
	cin >> p;
	vector <int> v(p, -1);
	fore(i, 0, p)
		v[(i * i) % p] = i;
	fore(i, 0, L(v))
		cout << v[i] << " ";
	//cout << endl;
}