#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define allof(x) (x).begin(),(x).end()
#define trace(x) cerr << #x << ": " << x << endl
#define tracev(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl;
#define _ << " " << 

using namespace std;

typedef vector<pair<int,int>> vii;

int main()
{	
	int n;
	cin >> n;
	vii vi;
	int minind = -1;
	int minl = INT_MIN;
	For(i,0,n){
		int u,v;
		cin >> u >> v;
		//trace(u _ v);
		vi.push_back(make_pair(u,v));
		//trace(vi[i].first _ vi[i].second);
		if(u > minl)
			minl = u, minind = i;
	}
	//trace(minl);
	int res = 0;
	For(i,0,n){
		if(vi[i].second >= minl)
			res++;
		//trace(vi[i].second);
	}
	cout << res << endl;
}