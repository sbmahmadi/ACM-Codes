#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define allof(x) (x).begin(),(x).end()
#define trace(x) cerr << #x << ": " << x << endl
#define tracev(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl;
#define _ << " " << 
#define ll long long
#define ii pair<int, int>
#define vii vector<ii>

using namespace std;

int main()
{
	ios_base::sync_with_stdio(false);
	cin.tie(0);
	int n;
	cin >> n;
	vii v;
	For(i, 0, n)
	{
		int a, b;
		cin >> a >> b;
		v.push_back(make_pair(b - a, a));
	}
	sort(allof(v));
	ll soldiers = 0;
	ll alive = 0;
	For(i, 0, n)
		v[i].first += v[i].second;
	For(i, 0, n)
	{
		if(alive < v[i].second)
		{
			soldiers += v[i].second - alive;
			alive += v[i].second - alive;
		}
		alive -= v[i].first;
	}
	cout << soldiers << endl;
}