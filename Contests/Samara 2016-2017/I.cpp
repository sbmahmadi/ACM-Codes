#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define allof(x) (x).begin(),(x).end()
#define trace(x) cerr << #x << ": " << x << endl
#define tracev(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl;
#define _ << " " << 

using namespace std;

typedef vector<int> vi;
typedef pair<int,int> ii;

int main()
{
	int n,m;
	cin >> n >> m;
	vector<vi> g(n);
	For(i,0,m){
		int u,v;
		cin >> u >> v; u--; v--;
		g[u].push_back(v);
		g[v].push_back(u);
	}
	int minind = -1;
	int dg = 1e8;
	For(i,0,n){
		if(g[i].size() < dg)
			dg = g[i].size(), minind = i;
	}
	For(i,0,n){
		if(i == minind || find(allof(g[minind]),i) != g[minind].end())
			cout << 0 << " ";
		else
			cout << 1 << " ";
	}
	cout << endl;
}