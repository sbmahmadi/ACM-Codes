#include <iostream>
#include <vector>
#include <queue>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <iomanip>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define allof(x) x.begin(),x.end()

#define ZERO 105

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

vector<ii> v[55];

int dist[500][500];

int dx[8] = {1, 1, 2, 2, -1, -1, -2, -2};
int dy[8] = {2, -2, 1, -1, 2, -2, -1, 1};

void BFS()
{
	v[0].push_back(make_pair(0,0));
	queue<ii> q;
	q.push(make_pair(0,0));

	dist[ZERO + 0][ZERO + 0] = 0;
	
	while(!q.empty())
	{
		ii pii = q.front();
		if (dist[ZERO + pii.first][ZERO + pii.second] == 51)
			break;
		q.pop();
		For(i,0,8)
		{
			int nx = pii.first + dx[i];
			int ny = pii.second + dy[i];
			if(dist[ZERO + nx][ZERO + ny] == -1)
			{
				q.push(make_pair(nx,ny));
				dist[ZERO + nx][ZERO + ny] = dist[ZERO + pii.first][ZERO + pii.second] + 1;
				//trace(dist[ZERO + nx][ZERO + ny]);
				v[dist[ZERO + nx][ZERO + ny]].push_back(make_pair(nx,ny));
			}
		}
	}
}

int main()
{
	ios_base::sync_with_stdio(0);
	cin.tie(0);

	memset(dist,-1,sizeof dist);
	int n;
	BFS();
	while( cin >> n && n)
	{
		set <ii> st;

		For(i,0,n)
		{
			int x,y,k;
			cin >> x >> y >> k;
			For(j,0,k+1)
			{
				For(p,0,v[j].size())
				{
					ii pii = v[j][p];
					pii.first += x;
					pii.second += y;
					st.insert(pii);
				}
			}
		}
		cout<<st.size()<<endl;
	}
}