#include<iostream>
#include<vector>
#include<queue>
#include<math.h>
#include<algorithm>
#include<map>
#include<unordered_map>
#include<set>
#include<unordered_set>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int main()
{
	int n, m;
	while (cin >> n >> m && n && m)
	{
		int res = 0;
		if (n == 0 || m == 0)
		{
			res = 0;
		}
		else if (n == 1 || m == 1)
		{
			res = n == 1 ? m : n;
		}
		else if (n == 2 || m == 2)
		{
			int k = n*m / 2;
			for (int i = 0; i < k; i++)
			{
				if (i % 4 < 2) res++;
			}
			res *= 2;
		}
		else
			res = ceil(n*m / 2.0);

		cout << res << " knights may be placed on a " << n << " row " << m << " column board." << endl;
	}
}