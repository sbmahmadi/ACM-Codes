#include<iostream>
#include<vector>
#include<queue>
#include<math.h>
#include<algorithm>
#include<map>
#include<unordered_map>
#include<set>
#include<unordered_set>
#include<string>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;


struct Node
{
	int distance = -1;
	vector<string> adj;
};

void init(map<string, Node> &names)
{
	for (auto it = names.begin(); it != names.end(); it++)
	{
		it->second.distance = -1;
	}
}

int BFS(map<string, Node> &names, string s)
{
	int mx = 0;
	queue<string> q;

	names[s].distance = 0;
	q.push(s);

	while (!q.empty())
	{
		string s1 = q.front();
		q.pop();
		For(i, 0, names[s1].adj.size())
		{
			string n = names[s1].adj[i];
			if (names[n].distance == -1)
			{
				names[n].distance = names[s1].distance + 1;
				q.push(n);
				mx = max(mx, names[n].distance);
			}
		}
	}
	return mx;
}

void readPaper(map<string, Node> &graph)
{
	string s, sx;
	vector<string> v;
	getline(cin, s);
	int a = 0;
	for (int i = 0; s[i] != ':'; i++)
	{
		sx += s[i];
		if (s[i] == ',')
		{
			a++;
			if (a % 2 == 0)
			{
				sx.pop_back();
				v.push_back(sx);
				sx = "";
				i++;
			}
		}
	}
	v.push_back(sx);

	For(i, 0, v.size())
	{
		For(j, 0, v.size())
		{
			if (i != j)
			{
				graph[v[i]].adj.push_back(v[j]);
			}
		}
	}
}


int main()
{
	int scen;
	cin >> scen;

	For(sc, 0, scen)
	{
		cout << "Scenario " << sc + 1 << endl;
		map<string, Node> graph;
		int p, n;
		cin >> p >> n;
		cin.ignore();
		For(i, 0, p)
		{
			readPaper(graph);
		}
		BFS(graph, "Erdos, P.");
		For(i, 0, n)
		{
			string s;
			getline(cin, s);
			cout << s << " ";
			if (graph[s].distance > 0)
			{
				cout << graph[s].distance << endl;
			}
			else
			{
				cout << "infinity" << endl;
			}
		}

	}
}