#include<iostream>
#include<vector>
#include<queue>
#include<math.h>
#include<algorithm>
#include<map>
#include<unordered_map>
#include<set>
#include<unordered_set>
#include<string>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;


int main()
{
	string s;
	int n;
	while (cin >> n >> s)
	{
		unordered_map<string, int> m;
		for (int i = 0; i < s.size() - n; i++)
		{
			m[s.substr(i, n)]++;
		}
		string ss;
		int max = 0;
		for (auto it = m.begin(); it != m.end(); it++)
		{
			if (it->second > max)
			{
				max = it->second;
				ss = it->first;
			}
		}
		cout << ss << endl;
	}
}