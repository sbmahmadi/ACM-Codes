#include<iostream>
#include<vector>
#include<queue>
#include<math.h>
#include<algorithm>
#include<map>
#include<unordered_map>
#include<set>
#include<unordered_set>
#include<string>
#include<string.h>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int dp[15];

int f(int n)
{
	if(dp[n] != -1) return dp[n];
	int sum = 0;
	For(i, 0, n)
	{
		For(j, 0, n)
		{
			if (i + j == n - 1)
			{
				sum += f(i) * f(j);
			}
		}
	}
	return dp[n] = sum;
}

int main()
{
	memset(dp, -1, sizeof dp);
	dp[1] = 1;
	dp[0] = 1;
	int n;
	bool flag = true;
	while (cin >> n)
	{
		if (flag)
		{
			flag = false;
			cout << f(n);
		}
		else
		{
			cout << endl << endl << f(n);
		}
	}
	cout << endl;
}