#include<iostream>
#include<vector>
#include<queue>
#include<math.h>
#include<algorithm>
#include<map>
#include<unordered_map>
#include<set>
#include<unordered_set>
#include<string>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int f(int cig, int pow)
{
	if (cig < pow) return 0;
	return cig/pow + f(cig / pow + cig%pow, pow);
}

int main()
{
	int n, m, s;
	while (cin >> n >> m)
	{	
		cout << n+f(n,m) << endl;
	}
}