#include<iostream>
#include<fstream>
#include<string>
#include<algorithm>
#include<queue>
#include<string.h>

#define For(i,a,b) for(int i = a; i<b; i++)
#define roF(i,b,a) for(int i = b; i >=a ; i--)

#define MaxSize 105

using namespace std;

int main()
{
	int n, m;
	while (cin >> n >> m)
	{
		int dp[MaxSize][MaxSize];
		vector<int> v[MaxSize][MaxSize];
		int grid[MaxSize][MaxSize];
		memset(dp, 0, sizeof dp);
		
		For(i, 0, n)
		{
			For(j, 0, m)
			{
				cin >> grid[i][j];
			}
		}
		For(i, 0, n)
		{
			dp[i][m - 1] = grid[i][m - 1];
			v[i][m - 1].push_back(i);
		}

		roF(j, m - 2, 0)
		{
			For(i, 0, n)
			{
				int mn = 1 << 25;
				For(k, -1, 2)
				{
					int x = (i + k + n) % n;
					if (mn > dp[x][j + 1] || (mn==dp[x][j+1] && v[i][j].back() > x))
					{
						mn = dp[x][j + 1];
						v[i][j] = v[x][j + 1];
					}
				}
				dp[i][j] = grid[i][j] + mn;
				v[i][j].push_back(i);
			}
		}
		int mn = 2<<25;
		int x;
		For(i, 0, n)
		{
			if (mn > dp[i][0])
			{
				mn = dp[i][0];
				x = i;
			}
		}

		for (auto rit = v[x][0].rbegin(); rit != v[x][0].rend(); rit++)
		{
			if (rit != v[x][0].rbegin()) cout << " ";
			cout << *rit + 1;
		}
		cout << endl << mn << endl;
	}
}