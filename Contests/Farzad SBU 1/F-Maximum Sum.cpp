#include<iostream>
#include<algorithm>

#define MaxSize 110
#define Inf -128;
#define For(i,a,b) for(int i = a;i<b ;i++)
#define Rof(i,b,a) for(int i = b;i>=a;i--)
using namespace std;

int dp[MaxSize][MaxSize];

int main()
{
	int n;
	cin >> n;
	int grid[MaxSize][MaxSize];
	For(i, 0, n)
	{
		For(j, 0, n)
		{
			cin >> grid[i][j];
		}
	}
	Rof(i, n - 1, 0)
	{
		Rof(j, n - 1, 0)
		{
			dp[i][j] = grid[i][j] + dp[i + 1][j] + dp[i][j + 1] - dp[i + 1][j + 1];
		}
	}
	int mx = 0;
	For(i, 0, n)
	{
		For(j, 0, n)
		{
			For(k, i, n)
			{
				For(p, j, n)
				{
					mx = max(mx, dp[i][j] - dp[i][p+1] - dp[k+1][j] + dp[k+1][p+1]);
				}
			}
		}
	}
	cout << mx << endl;
}