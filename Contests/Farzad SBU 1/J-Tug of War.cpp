#include<iostream>
#include<vector>
#include<queue>
#include<math.h>
#include<algorithm>
#include<map>
#include<unordered_map>
#include<set>
#include<unordered_set>
#include<string>
#include<string.h>

#define for(i,a,b) for(int i = a;i<b;i++)
#define rof(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define allof(x) x.begin(),x.end()

#define maxi 105
#define maxj (100*450 + 20)

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

// dp[i][j] = k         :: mishe ba astefade az k ta az i ta adade avale array, j ro sakht

int dp[maxi][maxj];
int a[maxi];

int f(int i,int j)
{
	if (j == 0) return dp[i][j] = true;
	if (j < 0) return false;

	if (dp[i][j]!=-1) return dp[i][j];
	
	int x;
	if (f(i - 1, j - a[i]))
	{
		x = true;
	}
	else if (f(i - 1, j))
		x= true;
	else x = false;
	return x;
}

int main()
{
	int t;
	cin >> t;
	bool flag = true;
	while (t--)
	{
		memset(dp, -1, sizeof dp);
		int n;
		cin >> n;
		vector<int> v(n);
		int sum = 0;
		for(i, 0, n)
		{
			cin >> a[i+1];
			sum += a[i+1];
		}
		for(i, 0, n+1)
		{
			dp[i][0] = 1;
		}
		f(n, sum);
		int mn = sum;
		for(i, 0, maxj)
		{
			if (dp[n][i] -1 == n / 2)
			{
				if (sum - 2 * i < sum - 2 * mn) mn = i;
			}
		}
		cout << mn << " " << sum - mn << endl;
	}
}