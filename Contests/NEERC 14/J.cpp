#include <iostream>
#include <vector>
#include <queue>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <iomanip>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define allof(x) x.begin(),x.end()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

vector<vector<ii>> cand;
string s;
int n;

set<int> kir;

void make_Candidates()
{
	cand.clear();
	cand.assign(n+1,vector<ii>());

	For(i,0,s.size())
	{
		if(kir.find(i) != kir.end()) continue;
		if(s[i] != '0' && s[i] - '0' <= n)
		{
			cand[s[i] - '0'].push_back(make_pair(i,i));
		}
	}
	For(i,0,s.size()-1)
	{
		if(kir.find(i) != kir.end() || kir.find(i+1) != kir.end()) 
			continue;
		if(s[i] == '0')
			continue;
		int num = stoi(s.substr(i,2));
		if(num <= n)
		{
			cand[num].push_back(make_pair(i,i+1));
		}
	}
}

int main()
{
	cin >> s;
	string reserve = s;

	if(s.size() < 10)
	{
		For(i,0,s.size())
		{
			cout<<s[i]<<" ";
		}
		cout<<endl;
		return 0;
	}
	
	n =  9 + (s.size() - 9) / 2;
	
	vector<ii> res;
	make_Candidates();
	
	For(j,0,n)
	{
		roF(i,n,1)
		{
			if(cand[i].size() == 1)
			{
				//trace(i);
				res.push_back(cand[i][0]);
				kir.insert(cand[i][0].first);
				kir.insert(cand[i][0].second);
				make_Candidates();
				break;
			}
		}
	}

	For(i, 1, n+1)
	{
		if (cand[i].size() > 0)
			res.push_back(cand[i][0]);
	}
	sort(allof(res));

	for(auto it = res.begin(); it!= res.end(); it++)
	{
		cout<<s[it->first];
		if(it->first != it->second)
			cout<< s[it->second];
		cout<<" ";
	}
	cout<<endl;
}
