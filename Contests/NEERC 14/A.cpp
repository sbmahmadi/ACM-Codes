#include <iostream>
#include <vector>
#include <queue>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <iomanip>
#include <fstream>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define allof(x) x.begin(),x.end()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int main()
{
	ifstream fin("alter.in");
	ofstream fout("alter.out");
	int n,m;
	fin >> n >> m;
	fout<< n/2 + m/2 << endl;
	for(int i = 1; i<n ;i+=2)
	{
		fout << i+1<<" "<<1<<" "<<i+1<<" "<<m << endl;	
	}
	for(int j = 1;j<m;j+=2)
	{
		fout << 1 <<" "<< j+1 <<" "<< n <<" "<< j+1 << endl;
	}
	fout.flush();
	fout.close();
}