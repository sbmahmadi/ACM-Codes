#include <iostream>
#include <vector>
#include <queue>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <iomanip>
#include <fstream>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define allof(x) x.begin(),x.end()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

bool isThere(ii car,int a, int b, int t)
{
	int len = car.second - car.first;
	int times = t/len;
	int x;
	if(times%2 == 0)
	{
		x = t%len;
	}
	else x = len - t % len;
	x += car.first;
	if(x >= a && x <= b)
		return true;
	else return false;
}

int main()
{
	ifstream fin("knockout.in");
	ofstream fout("knockout.out");

	int n,m;
	fin >> n >> m;
	vector<ii> cars;
	For(i,0,n)
	{
		int a,b;
		fin >> a >> b;
		cars.push_back(make_pair(a,b));
	}
	For(i,0,m)
	{
		int res = 0;
		int a,b,t;
		fin >> a >> b >> t;
		For(j,0,n)
		{
			if(b > cars[j].first || a < cars[j].second)
			{
				if(isThere(cars[j],a,b,t))
					res++;
			}
		}
		fout<<res<<endl;
	}
}