#include <bits/stdc++.h>

using namespace std;

struct Grade
{
	int a, b;
} p[200005];

int main()
{
	int n;
	cin >> n;
	for(int i = 0; i < n; i++)
	{
		cin >> p[i].a >> p[i].b;
	}
	int maxiA = p[0].a, maxiB = p[0].b;
	for(int i = 1; i < n; i++)
	{
		if(p[i].a > maxiA)
		{
			maxiA = p[i].a;
			maxiB = max(maxiB, p[i].b);
		}
	}
	cout << maxiB << endl;	
}