#include <bits/stdc++.h>

using namespace std;

struct Person
{
	int a[3];
	void normalize()
	{
		sort(a, a + 3);
		reverse(a, a + 3);
	}
	int idx;
	bool mark;
} P[200005];

bool operator<(const Person &p1, const Person &p2)
{
	pair <int, pair<int, int> > pp1 = make_pair(p1.a[0], make_pair(p1.a[1], p1.a[2])), pp2 = make_pair(p2.a[0], make_pair(p2.a[1], p2.a[2]));
	return pp1 > pp2;
}

int main()
{
	int n;
	cin >> n;
	for(int i = 0; i < n; i++)
	{
		cin >> P[i].a[0] >> P[i].a[1] >> P[i].a[2];
		P[i].normalize();
	}
	int maxi[3] = {-1, -1, -1};
	for(int i = 0; i < n; i++)
	{
		maxi[1] = max(P[i].a[1], maxi[1]);
		maxi[2] = max(P[i].a[2], maxi[2]);
	}
	vector <int> indices;
	for(int i = 0; i < n; i++)
	{
		if(P[i].a[0] >= maxi[1] && P[i].a[1] >= maxi[2])
			indices.push_back(i);
	}
	cout << indices.size() << endl;
	sort(indices.begin(), indices.end());
	for(int i = 0; i < indices.size(); i++)
	{
		cout << indices[i] + 1 << endl;
	}
}