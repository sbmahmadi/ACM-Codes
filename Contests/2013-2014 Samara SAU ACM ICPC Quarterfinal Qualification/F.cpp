#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)

using namespace std;

bool OK(int hit, int p, int q, vector<int>& m)
{
	int res = 0;
	For(i,0,m.size())
	{
		int tmp = m[i] - q*hit;
		if(tmp > 0)
			res += ceil((double)tmp/(p-q));
	}
	return res <= hit;
}

int main()
{
	int n,p,q;
	cin >> n >> p >> q;
	vector<int> m(n);
	for(int i =0; i < n; i++)
		cin >> m[i];
	if(p == q)
		cout << ceil(*max_element(m.begin(), m.end())/(double)p) << endl;
	else
	{
		int lo = 0, hi = 1e9 + 10;
		while(lo != hi)
		{
			int mid = (lo+hi)/2;
			if(OK(mid,p,q,m))
				hi = mid;
			else lo = mid+1;
		}
		cout << lo << endl;
	}
}