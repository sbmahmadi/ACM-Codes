#include <bits/stdc++.h>

using namespace std;

string mapping(string& s)
{
	string res = s;
	char mychar = 'a';
	map<char,char> mp;
	for(int i = 0; i < s.size(); i++)
	{
		if(!mp.count(s[i]))
		{
			mp.insert({s[i],mychar});
			mychar++;
		}
		res[i] = mp[s[i]];
	}
	return res;
}

int main()
{
	int n;
	cin >> n;
	long long res = 0;
	map<string, int> mp;
	for(int i = 0; i < n; i ++)
	{
		string s;
		cin >> s;
		string mapped = mapping(s);
		res += mp[mapped]++;
	}
	cout << res << endl;
}