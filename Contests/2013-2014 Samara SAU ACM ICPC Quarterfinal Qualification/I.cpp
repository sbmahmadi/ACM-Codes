#include <bits/stdc++.h>

using namespace std;

int main()
{
	ios_base::sync_with_stdio(0);
	cin.tie(0);
	
	int n;
	cin >> n;
	int hp = 0;
	int time = 0;
	int shot = 0;
	priority_queue<int> st;
	for(int i = 0; i < n; i++)
	{
		int t,d;
		cin >> t >> d;
		hp += t - time; time = t;
		hp -= d;
		st.push(d);
		while(hp < 0)
		{
			hp += st.top();
			st.pop();
			shot++;
		}
	}
	cout << shot << endl;
}