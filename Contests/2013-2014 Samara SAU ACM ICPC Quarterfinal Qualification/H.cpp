#include <bits/stdc++.h>

using namespace std;

int main()
{
	int n;
	cin >> n;
	string s, t;
	cin >> s >> t;
	int prefixes = 0;
	bool flag = false;
	for(int i = 0; i < n; i++)
	{
		if(s[i] == t[i] && flag)
		{
			flag = false;
			prefixes++;
		}
		else if(s[i] == t[i] && !flag)
			continue;
		else if(s[i] != t[i] && flag)
			continue;
		else
			flag = true;

	}
	if(flag)
		prefixes++;
	cout << prefixes << endl;
}