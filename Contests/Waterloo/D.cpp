#include <map>
#include <unordered_set>
#include <set>
#include <iostream>
#include <algorithm>
#include <string>

using namespace std;

int main()
{
	int n;
	cin >> n;
	string s1,s2,s3;
	
	set<pair<string,string>> reqs;
	map<string,int> mp;
	for (int i = 0; i < n; ++i)
	{
		cin >> s1 >> s2 >> s3;
		s1 += " " + s2;
		if(reqs.insert(make_pair(s1,s3)).second)
		{
			mp[s3]++;
		}
	}
	for(auto it = mp.begin();it!= mp.end();it++)
	{
		cout<<it->first<<" "<<it->second<<endl;
	}
}