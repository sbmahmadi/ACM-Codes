#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>
#include <set>

#define INF 1<<30
#define trace(x) cerr<<#x<<"  :  "<<x<<endl;
using namespace std;

typedef long long ll;

struct Edge
{
	int weight;
	int dest;
	char color;
	Edge(int d,int w,char c) : dest(d) ,weight(w), color(c){}
};

struct Node
{
	ll distanceIn;
	ll distanceOut;
	bool visited;
	vector<Edge> adj;
};

vector<Node> graph;

struct cmp
{
	bool operator() (const int n1, const int n2) const
	{
	    if(graph[n1].distanceOut == graph[n2].distanceOut) return graph[n1].distanceIn < graph[n2].distanceIn;
		return graph[n1].distanceOut < graph[n2].distanceOut;
  	}
};

void init()
{
	for(int i = 0;i<graph.size();i++)
	{
		graph[i].distanceIn = INF;
		graph[i].distanceOut = INF;
		graph[i].visited = false;
	}
}

bool djikstra(int a,int b)
{
	graph[a].distanceIn = 0;
	graph[a].distanceOut = 0;

	set<int,cmp> pq;
	
	pq.insert(a);

	while(!pq.empty())
	{
		int me = *pq.begin();
		
		if(me == b)
			return 1;
		
		pq.erase(pq.begin());
		for(int i = 0; i < graph[me].adj.size();i++)
		{
			int child = graph[me].adj[i].dest;
			bool changed = false;

			if(graph[me].adj[i].color == 'O')
			{
				if(graph[child].distanceOut > graph[me].distanceOut + graph[me].adj[i].weight)
				{
					graph[child].distanceOut = graph[me].distanceOut + graph[me].adj[i].weight;
					graph[child].distanceIn = graph[me].distanceIn;
						changed = true;
				}
				else if(graph[child].distanceOut == graph[me].distanceOut + graph[me].adj[i].weight)
				{
					if(graph[child].distanceIn > graph[me].distanceIn)
					{
						graph[child].distanceIn = graph[me].distanceIn;
						changed = true;	
					}
				}
			}
			if(graph[me].adj[i].color == 'I')
			{
				if(graph[child].distanceOut > graph[me].distanceOut)
				{
					graph[child].distanceOut = graph[me].distanceOut;
					graph[child].distanceIn = graph[me].distanceIn + graph[me].adj[i].weight;
					changed = true;
				}
				else if(graph[child].distanceOut == graph[me].distanceOut)
				{
					if(graph[child].distanceIn > graph[me].distanceIn + graph[me].adj[i].weight)
					{
						graph[child].distanceIn = graph[me].distanceIn + graph[me].adj[i].weight;
						changed = true;	
					}
					
				}
			}
			if(changed)
			{
				pq.insert(child);
			}	
		}
	}
	return false;
}

int main()
{
	int n,m,p;
	cin>> n >> m >>p;
	graph.assign(n,Node());
	for(int i = 0; i < m; i++)
	{
		int a,b,c;
		char d;
		cin>> a >> b >> c >> d;
		graph[a].adj.push_back(Edge(b,c,d));
		graph[b].adj.push_back(Edge(a,c,d));
	}
	for(int i = 0;i<p;i++)
	{
		int a,b;
		cin>> a >> b;
		init();
		bool res = djikstra(a,b);

		if(!res) cout<<"IMPOSSIBLE."<<endl;
		else cout<<graph[b].distanceOut<<" "<<graph[b].distanceOut + graph[b].distanceIn<<endl;
	}
}