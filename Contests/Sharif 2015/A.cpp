#include <iostream>
#include <vector>
#include <queue>
#include <math.h>
#include <algorithm>
#include <map>
#include <set>
#include <string>
#include <string.h>
#include <iomanip>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define allof(x) x.begin(),x.end()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int main()
{
	int t;
	cin >> t;
	while(t--)
	{
		int n;
		cin>>n;
		cout<< (n*(n+1)) /2;
		cout<<" "<< n*n<<" ";
		cout<< n * (n+1)<<endl;
	}
}