#include <iostream>
#include <vector>
#include <queue>
#include <math.h>
#include <algorithm>
#include <map>
#include <set>
#include <string>
#include <string.h>
#include <iomanip>
#include <list>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define allof(x) x.begin(),x.end()

#define MOD 1000000000

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int main()
{
	list <int> a;
	a.push_back(1);
	a.push_back(1);
	a.push_back(1);
	for(int i = 3; i <= 2^30 ;i++)
	{
		a.push_back((a.back() + *(--a.end()))%MOD);
	}
	cerr<<"KIRR"<<endl;

	int t;
	cin >> t;
	while (t--)
	{
		long long x;
		cin>>x;
		cout<<a[x]<<endl;
	}
}