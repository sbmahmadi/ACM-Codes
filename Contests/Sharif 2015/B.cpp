#include <iostream>
#include <vector>
#include <queue>
#include <math.h>
#include <algorithm>
#include <map>
#include <set>
#include <string>
#include <string.h>
#include <iomanip>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define allof(x) x.begin(),x.end()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int a[10005];
int b[10005];

void init()
{
	a[1] = 2;
	a[2] = 1;
	a[3] = 2;
	a[4] = 2;
	a[5] = 4;

	b[1] = 2;
	b[2] = 3;
	b[3] = 5;
	b[4] = 7;
	b[5] = 11;

	For(i,6,10001)
	{
		int k = sqrt(i);
		int res = i-1;
		bool flag = false;
		for(int j = 2; j<= i/2 ;j++)
		{
			if(i%j == 0)
			{
				flag = true;
				res -= a[j];
			}
			if(j>k && flag == false)
			{
				break;
			}
		}
		a[i] = res;
		b[i] = res + b[i-1];
	}
}

int main()
{
	init();

	int t;
	cin>>t;

	while(t--)
	{
		int n;
		cin >> n;
		cout<< b[n]<<endl;
	}
}