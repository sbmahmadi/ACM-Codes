#include <bits/stdc++.h>

#define fore(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0);
#define trace(x) cerr << #x << ": " << x << endl;
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl;
#define _ << " :: " <<
#define allof(x) (x).begin(),(x).end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

#define MAXN 1005
long long memo[MAXN][MAXN];
long long t[MAXN];

long long d,c;
int n;

long long solve(int index , int count){
	if(index == n) return 0;
	if(memo[index][count] != -1){
		return memo[index][count];
	}

	long long ans = 1000 * 1000 * 1000 * 1000ll;
	if(index + 1 < n) ans = solve(index + 1 , count + 1) + c * (count + 1) * (t[index + 1] - t[index]);
	ans = min(ans , solve(index + 1 , 0) + d);
	memo[index][count] = ans;
	return ans;

}

int main()
{
	cin >> n >> d >> c;
	for(int i = 0 ; i < n ; ++i){
		for(int j = 0 ; j < n ; ++j){
			memo[i][j] = -1;
		}
	}

	for(int i = 0 ; i < n ; ++i){
		cin >> t[i];
	}

	cout << solve(0 , 0) << endl;
}

