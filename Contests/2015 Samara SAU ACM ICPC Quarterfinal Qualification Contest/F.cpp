/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0);
#define trace(x) cerr << #x << ": " << x << endl;
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl;
#define _ << " :: " <<
#define allof(x) (x).begin(),(x).end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

void count(vi& v, int s, vector<ll>& kk) {
	For(i,0,v.size()) {
		For(ki,1,v[i]+1) {
			kk[ki-1] += v[i] - ki + 1;
		}
	}
}

int main()
{
	int n,m;
	cin >> n >> m;
	vector<vector<char>> grid(n, vector<char>(m));
	int k = max(n,m);
	vector<ll> KK(k);
	vi hor,ver;
	For(i,0,n) {
		For(j,0,m) {
			cin >> grid[i][j];
		}
	}
	int t = 0;
	For(i,0,n) {
		For(j,0,m) {
			if(grid[i][j] == '.')
				t++;
			else
				if(t) {
					hor.push_back(t);
					t = 0;
				}
		}
		if(t) {
			hor.push_back(t);
			t = 0;
		}
	}

	For(j,0,m) {
		For(i,0,n) {
			if(grid[i][j] == '.')
				t++;
			else
				if(t) {
					ver.push_back(t);
					t = 0;
				}
		}
		if(t) {
			ver.push_back(t);
			t = 0;
		}
	}
	
	count(hor,m,KK);
	count(ver,n,KK);
	if(KK.size())
		KK[0] /= 2;
	For(i,0,k) {
		if(i)
			cout << " ";
		cout << KK[i];
	}
	cout << endl;
}