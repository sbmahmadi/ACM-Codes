#include <bits/stdc++.h>

#define fore(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0);
#define L(a) (int)a.size()
#define trace(x) cerr << #x << ": " << x << endl;
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl;
#define _ << " :: " <<
#define allof(x) (x).begin(),(x).end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

#define MAXN 100005

struct Edge
{
	int to;
	ll weight;
	Edge(int _to, int _weight)
	{
		to = _to;
		weight = _weight;
	}
};

bool operator<(const Edge &e1, const Edge &e2)
{
	return e1.weight > e2.weight;
}

vector<Edge> adj[MAXN];
int visited[MAXN];
ll dist[MAXN];
int n, m;

ll dijkstra()
{
	priority_queue <Edge> pq;
	fore(i, 0, n)
	{
		if(visited[i] != -1)
		{
			pq.push(Edge(i, 0));
			dist[i] = 0;
		}
		else
			dist[i] = 1e15;
	}
	while(!pq.empty())
	{
		Edge e = pq.top();
		pq.pop();
		int u = e.to;
		int w = e.weight;
		fore(i, 0, L(adj[u]))
		{
			Edge out = adj[u][i];
			int v = out.to;
			if(visited[u] == visited[v])
				continue;
			if(visited[v] == -1)
			{
				if(dist[v] >= w + out.weight)
					continue;
				dist[v] = out.weight + w;
				visited[v] = visited[u];
				pq.push(Edge(v, dist[v]));
			}
			else
			{
				ll res = dist[u] + dist[v] + out.weight;
				cout << res << endl;
				cout << visited[u] + 1 << " " << visited[v] + 1 << endl;
				return res;
			}

		}
	}
	return -1;
}

int main()
{
	cin >> n >> m;
	fore(i, 0, n)
	{
		int temp;
		cin >> temp;
		if(temp == 0)
			visited[i] = -1;
		else
			visited[i] = i;
	}
	fore(i, 0, m)
	{
		int u, v, w;
		cin >> u >> v >> w;
		u--;
		v--;
		Edge e(u, w);
		adj[v].push_back(e);
		e.to = v;
		adj[u].push_back(e);
	}
	ll d = dijkstra();
	if(d == -1)
		cout << "No luck at all" << endl;
}

