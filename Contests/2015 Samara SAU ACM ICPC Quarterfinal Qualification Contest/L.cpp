/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0);
#define trace(x) cerr << #x << ": " << x << endl;
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl;
#define _ << " :: " <<
#define allof(x) (x).begin(),(x).end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a, ll b) {
	if(b == 0)
		return a;
	return gcd(b, a%b);
}

void euc(ll a, ll b, ll& x, ll& y, ll& d) {
	if (!b) {x = 1; y = 0; d = a; return; }
	euc(b, a % b, x, y, d);
	ll x1 = y, y1 = x - (a / b) * y;
	x = x1, y = y1;
}

int main()
{
	ll a,b;
	cin >> a >> b;
	ll g;
	g = gcd(a,b);
	a /= g;
	b /= g;
	if(g != 1) {
		cout << a * b *g << endl;
	} 
	else {
		ll x,y,x1,y1;
		ll k = 1;
		euc(a,b,x,y,k);
		k = 1;
		//euc(-a,-b,x1,y1,k);
		ll ans = max(abs(a*x),abs(b*y));
		//ll ans2 = max(a*x1,b*y1);
		cout << /*min(ans,ans2)*/ ans << endl;
	}
}