#include <bits/stdc++.h>

#define fore(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0);
#define trace(x) cerr << #x << ": " << x << endl;
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl;
#define _ << " :: " <<
#define allof(x) (x).begin(),(x).end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

#define MAXN 200005

map <ll, int> m;
ll a[MAXN];

int main()
{
	int n;
	cin >> n;
	ll sum = 0;
	int shortest = 1e6;
	int finish = -1;
	m[0] = -1;
	fore(i, 0, n)
	{
		cin >> a[i];
		sum += a[i];
		if(m.find(sum) == m.end())
		{
			m[sum] = i;
			continue;
		}
		int current = i - m[sum];
		if(shortest > current)
		{
			shortest = current;
			finish = i;
		}
		m[sum] = i;
	}
	if(shortest > n)
		cout << -1 << endl;
	else
		cout << finish - shortest + 2 << " " << shortest << endl;
}

