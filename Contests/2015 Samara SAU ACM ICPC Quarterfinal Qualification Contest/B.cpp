#include <bits/stdc++.h>

#define fore(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0);
#define trace(x) cerr << #x << ": " << x << endl;
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl;
#define _ << " :: " <<
#define allof(x) (x).begin(),(x).end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

#define MAXN 1005

bool is_good(string s){
	int cnt = 0;
	for(int i = 0 ; i < (int)s.size() ; ++i){
		if(s[i] == '1') ++cnt;
		if(cnt > 15) return false;
	}

	if(cnt >= 8) return true;
	return false;
}

int main()
{
	int n,m;
	cin >> n >> m;
	vector<string> v;
	for(int i = 0 ; i < n ; ++i){
		char ss[5010];
		scanf("%s",ss);
		string s(ss);
		if(is_good(s)) v.push_back(s);
	}

	sort(v.begin() , v.end());
	string smax;
	if(v.size() > 0)
		smax = v[0];
	int ans = -1;

	for(int i = 0 ; i < (int)v.size() ; ++i){
		int t = 1;
		while(i + 1 < v.size() && v[i] == v[i + 1]){
			++t;
			if(t > ans){
				smax = v[i];
				ans = t;
			}
			++i;
		}
	}

	if((int)v.size() == 0){
		for(int i = 0 ; i < 8 ; ++i) cout << '1';
		for(int i = 0 ; i < m - 8 ; ++i) cout << '0';
		cout << endl;
	}else{
		cout << smax << endl;
	}


	return 0;
}

