#include <bits/stdc++.h>

#define fore(i,a,b) for(int i = a; i < b; i++)
#define L(a) (int)a.size()
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0);
#define trace(x) cerr << #x << ": " << x << endl;
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl;
#define _ << " :: " <<
#define allof(x) (x).begin(),(x).end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

const int INF = int(1e9) + 2;
#define MAXN 100005

struct Card
{
	int r, c;
	int index;
	Card(int _r, int _c, int _index)
	{
		r = _r;
		c = _c;
		index = _index;
	}
	Card(){}
};

bool operator<(const Card &c1, const Card &c2)
{
	if(c1.c != c2.c)
		return c1.c < c2.c;
	if(c1.r != c2.r)
		return c1.r < c2.r;
	return c1.index < c2.index;
}

vector <Card> cards;
Card a[MAXN];
vi taken;
vi maxR;
int n;

void printResult()
{
	cout << L(taken) + 1 << endl;
	fore(i, 0, L(taken))
		cout << taken[i] + 1 << " ";
	cout << n << endl;
}

int main()
{
	cin >> n;
	fore(i, 0, n - 1)
	{
		cin >> a[i].c >> a[i].r;
		a[i].index = i;
		if(a[i].c >= a[i].r)
			continue;
		cards.push_back(a[i]);
	}
	sort(allof(cards));
	maxR.push_back(0);
	fore(i, 1, L(cards))
	{
		if(cards[i].r > cards[maxR[i - 1]].r)
			maxR.push_back(i);
		else
			maxR.push_back(maxR[i - 1]);
	}
	int goalc, goalr;
	cin >> goalc >> goalr;
	int currentC = 0, currentR = 1;
	while(true)
	{
		if(currentR >= goalc)
		{
			printResult();
			return 0;
		}
		Card forSearch(INF, currentR, 0);
		int index = upper_bound(allof(cards), forSearch) - cards.begin() - 1;
		if(index == -1)
		{
			cout << "No such luck" << endl;
			return 0;
		}
		int best = maxR[index];
		int idx = cards[best].index;
		currentR = a[idx].r;
		currentC = a[idx].c;
		taken.push_back(idx);
	}
}

