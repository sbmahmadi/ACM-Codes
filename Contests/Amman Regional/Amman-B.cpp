#include <iostream>
#include <vector>
#include <fstream>
#include <map>
#include <string>

typedef long long ll;

#define For(i,a,b) for (int i = a; i < b; i++)

using namespace std;

char c[] = { 'S','R','P' };
char c2[] = { 'P','S','R' };

int main()
{
	int t;
	cin >> t;
	while (t--)
	{
		vector<int> play[3];w
		int n;
		cin >> n;
		string s;
		cin >> s;

		For(j, 0, 3)
		{
			play[j].push_back(0);

			int k = 0;
			int kk = 0;
			For(i, 0, n)
			{
				if (s[i] == c[j])
					k++;
				if (s[i] == c2[j])
					k--;
				play[j].push_back(k);
			}
		}
		
		int res = 0;
		For(i, 0, n + 1)
		{
			For(j, 0, n + 1 - i)
			{
				int mywin = play[0][i] + play[1][j + i] - play[1][i] + play[2][n] - play[2][i + j];
				if (mywin > 0) 
					res++;
			}
		}
		cout << res << endl;
	}
}