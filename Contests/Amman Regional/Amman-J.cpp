#include <iostream>
#include <vector>
#include <fstream>
#include <map>

typedef long long ll;

#define For(i,a,b) for (int i = a; i < b; i++)

using namespace std;

int main()
{
	int t;
	cin >> t;
	while(t--)
	{
		map<int,int> st;
		map<int,int> pack;
		int n,m;
		cin>>n>>m;
		For(i,0,n)
		{
			int a;
			cin>>a;
			st[a]++;
		}
		For(i,0,m)
		{
			int a;
			cin>>a;
			pack[a]++;
		}
		auto itt = st.begin();
		bool found = false;
		for(auto it = pack.begin();it!=pack.end();it++)
		{
			if(it->second >= itt->second)
			{
				itt++;
				if(itt == st.end())
				{
					found = true;
					break;
				}
			}
		}
		if(found) cout<<"YES";
		else cout<<"NO";
		cout<<endl;
	}
}