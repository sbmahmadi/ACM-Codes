#include<iostream>
#include<vector>
#include<algorithm>

#define For(i,a,b) for(int i = a;i<b;i++)

using namespace std;

int* sum(int c,vector<int> &coin)
{
	//sum,min,count
	int* res = new int[3];
	res[0] = 0;
	res[1] = 0;
	res[2] = 0;
	bool flag = true;
	
	for (int i = 0; c>0; i++)
	{
		if (c & 1)
		{
			res[2]++;
			if (flag)
			{
				flag = false;
				res[1] = coin[i];
			}
			res[0] += coin[i];
		}
		c >>= 1;
	}
	return res;
}

int main()
{
	int t;
	cin >> t;
	while (t--)
	{
		int n, s;
		cin >> n >> s;
		vector<int> coins(n);
		
		For(i, 0, n)
			cin >> coins[i];
		
		sort(coins.begin(), coins.end());
		int cc = 0;
		
		For(c, 1, (1 << n))
		{
			int * p = sum(c,coins);
			if (p[0] < s) continue;
			if (p[0] - s < p[1] && p[2] > cc)
			{
				cc = p[2];
			}
		}
		cout << cc << endl;
	}
}