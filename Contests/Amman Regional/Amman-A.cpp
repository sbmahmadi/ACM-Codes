#include <iostream>
#include <vector>
#include <queue>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define allof(x) x.begin(),x.end()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

struct Cnt
{
	string name;
	int s;
	int p;
	Cnt(string s,int a, int b): name(s),s(a),p(b) {}
	bool operator<(const Cnt c)
	{
		if(s != c.s) return s < c.s;
		else return p > c.p;
	}
};
int main()
{
	int t;
	cin>>t;
	while(t--)
	{
		vector<Cnt> all;
		int n;
		cin>>n;
		For(i,0,n)
		{
			string s;
			int a,b;
			cin>>s>>a>>b;
			all.push_back(Cnt(s,a,b));
		}
		sort(all.begin(), all.end());
		cout<<all.back().name<<endl;
	}
}