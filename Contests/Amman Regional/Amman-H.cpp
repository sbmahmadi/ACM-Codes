/*
ID: behdad.1
LANG: C++11
PROB:
*/

#include <iostream>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <iomanip>
#include <fstream>
#include <limits.h>
#include <time.h>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<< 
#define allof(x) x.begin(),x.end()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int dfs[100050];
int parent[100050];
int low[100050];

bool visited[100050];
int dist[100050];

set <ii> bridges;

void findBridge(int start, vector<vi> &graph, int num)
{
	visited[start] = true;
	dfs[start] = low[start] = num;
	For(i, 0, graph[start].size())
	{
		int child = graph[start][i];
		if (!visited[child])
		{
			parent[child] = start;
			findBridge(child, graph, num + 1);
			low[start] = min(low[start], low[child]);

			if (low[child] > dfs[start])
			{
				bridges.insert(make_pair(start, child));
				bridges.insert(make_pair(child, start));
			}
		}
		else if(child != parent[start])
			low[start] = min(low[start], dfs[child]);
	}
}

int BFS(int n, vector<vi> &graph)
{
	int far = -1;
	int maxdist = -1;
	queue<int> q;
	q.push(n);
	visited[n] = true;
	dist[n] = 0;
	while(!q.empty())
	{
		n = q.front();
		q.pop();
		if(dist[n] > maxdist)
		{
			maxdist = dist[n];
			far = n;
		}
		For(i,0,graph[n].size())
		{
			int m = graph[n][i];
			if(!visited[m])
			{
				q.push(m);
				visited[m] = true;
				if(bridges.count(make_pair(n,m)) || bridges.count(make_pair(m,n)))
				{
					dist[m] = dist[n] + 1;
				}
				else 
					dist[m] = dist[n];
			}
		}
	}
	return far;
}

int main()
{
	int t;
	cin >> t;
	while (t--)
	{
		int n, m;
		cin >> n >> m;
		memset(visited, 0, sizeof visited);
		bridges.clear();
		vector<vi> graph(n);
		For(i, 0, m)
		{
			int x, y;
			cin >> x >> y;
			graph[x - 1].push_back(y - 1);
			graph[y - 1].push_back(x - 1);
		}
		
		findBridge(0, graph,0);
		
		memset(visited, 0, sizeof visited);
		int farNode = BFS(0,graph);
		
		memset(visited, 0, sizeof visited);
		int farNode2 = BFS(farNode,graph);
		
		int diam = dist[farNode2];
		
		cout << bridges.size() / 2 - diam << endl;
	}
}