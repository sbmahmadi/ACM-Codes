#include<iostream>
#include<vector>
#include<queue>

using namespace std;

#define For(i,a,b) for(int i =a;i<b;i++)
#define Prior priority_queue<Edge,vector<Edge>,less<Edge>>

struct Edge
{
	int dest;
	int weight;
	Edge(int d, int w) : dest(d), weight(w) {}
	Edge()
	{

	}
	bool operator<(const Edge e) const
	{
		return (weight > e.weight);
	}
};

struct Node
{
	vector<Edge>adj;
	bool visited;
	Node() : visited(false) {}
};

void visit(int index, vector<Node> &graph, Prior &pq)
{
	graph[index].visited = true;

	for (int i = 0; i < graph[index].adj.size();i++)
	{
		if (!graph[graph[index].adj[i].dest].visited) pq.push(graph[index].adj[i]);
	}
}

int main()
{
	int t, n, m, a, b, w;
	cin >> t;
	For(i, 0, t)
	{
		cin >> n >> m;
		vector<Node>graph;
		graph.assign(n, Node());
		Prior pq;
		For(j, 0, m)
		{
			cin >> a >> b >> w;
			graph[a-1].adj.push_back(Edge(b-1, w));
			graph[b-1].adj.push_back(Edge(a-1, w));
		}
		visit(0, graph, pq);
		int min = -1;
		while (!pq.empty())
		{
			Edge e = pq.top();
			if (!graph[e.dest].visited)
			{
				visit(e.dest, graph, pq);
				if (e.weight > min) min = e.weight;
			}
			else pq.pop();
		}
		cout << min  << endl;
	}
}