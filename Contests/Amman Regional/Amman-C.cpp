#include <iostream>
#include <vector>
#include <queue>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define allof(x) x.begin(),x.end()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int main()
{
	int t;
	cin>>t;
	while(t--)
	{
		int n;
		cin>>n;
		string s;
		cin>>s;
		int sum = 0;
		For(i,0,n-1)
		{
			if(s[i] == '*')
				while(s[i] == '*') 
					i++;
			else if(s[i] == '.') 
			{
				sum++;
				i +=2;
			}
		}
		
		if (n == 1 && s[0] == '.') sum = 1;
		trace(sum);
		//cout<<sum<<endl;
	}	
}