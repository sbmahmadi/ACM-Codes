#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define allof(x) x.begin(), x.end()

using namespace std;

int val[100005];

int dfs(int u, vector<vector<int>>&graph, int mode, int d)
{
	if(graph[u].size() == 0)
		return val[u];
	if(graph[u].size() == 1)
		return dfs(graph[u][0], graph, mode, d);
	vector<int> res;
	For(i,0,graph[u].size())
		res.push_back(dfs(graph[u][i],graph,mode,d-1));
	if(mode == 0 && d > 0 || mode == 1 && d <= 0)
		return *min_element(allof(res));
	return *max_element(allof(res));
}

int main()
{
	int n, k;
	cin >> n >> k;
	vector<vector<int>> graph(n);
	
	For(i,1,n)
	{
		int t;
		cin >> t; t--;
		graph[t].push_back(i);
	}
	int l = 0;
	For(i,0,n)
		if(graph[i].size() == 0)
			l++;
	For(i,0,n)
		cin >> val[i];
	// 0 = min 		1 = max
	cout << dfs(0, graph, 0, k) << " " << dfs(0, graph, 1, n-k-l) << endl;
}