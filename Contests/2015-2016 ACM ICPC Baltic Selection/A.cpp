#include <iostream>
#include <string.h>

#define For(i,a,b) for(int i = a; i < b; i++)

using namespace std;

int pw(int i, int j)
{
	int re = 1;
	For(k,0,j)
		re *= i;
	return re;
}

int main()
{
	int a,b;
	cin >> a >> b;
	int k = 0;
	int res = 0;
	while(a > 0)
	{
		int p = pw(10, k++);
		res += p * abs(a%10 - b%10);
		a /= 10;
		b /= 10;
	}	
	cout << res << endl;

}
