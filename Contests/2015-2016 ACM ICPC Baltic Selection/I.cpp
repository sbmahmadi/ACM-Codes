#include <iostream>
#include <math.h>
#include <iomanip>

using namespace std;

int main()
{
	int r,R,h;
	cin >> r >> R >> h;
	double a1 = sqrt((R-r)*(R-r) + h*h);
	double a2 = r*a1/(R-r);
	double L = a1 + a2;
	double p = (L + L + 2*R)/2;
	cout << fixed << setprecision(12) << min((double)h/2.0,sqrt(p*(p-L)*(p-L)*(p-2*R))/p) << endl;

}