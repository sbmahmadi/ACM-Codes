#include <iostream>
#include <queue>
#include <iomanip>
#define For(i,a,b) for(int i = a; i < b; i++)

using namespace std;

typedef pair<int,int> ii;

int grid[505][505];
int n,m;
int dx[] = {1,-1,0,0};
int dy[] = {0,0,1,-1};

void BFS()
{
	grid[0][0] = 0;
	queue<ii> q;
	q.push({0,0});
	while(!q.empty())
	{
		auto u = q.front(); q.pop();
		For(i,0,4)
		{
			int nx = u.first + dx[i];
			int ny = u.second + dy[i];
			if(nx >= 0 && nx < n && ny >= 0 && ny < m && grid[nx][ny] == -1)
			{
				grid[nx][ny] = grid[u.first][u.second] + 1;
				q.push({nx,ny});
			}
		}
	}
}

int main()
{
	cin >> m >> n;
	int a,b;
	cin >> a >> b;

	For(i,0,n)
		For(j,0,m)
		{
			char c;
			cin >> c;
			grid[i][j] = (c == '.' ? -1 : -2);
		}

	BFS();
	
	if(grid[n-1][m-1] < 0)
		cout << "IMPOSSIBLE" << endl;
	else 
		cout << (grid[n-1][m-1] / 2 ) * a + ((grid[n-1][m-1] + 1) / 2 ) * b;
}