#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
	ios_base::sync_with_stdio(0);
	cin.tie(0);
	int n;
	cin >> n;
	long long a,b;
	cout << fixed << setprecision(10);
	for(int i = 0; i < n ; i++)
	{
		cin >> a >> b;
		cout <<  (1.0/a) - (1.0/(b+1)) << endl;
	}
	return 0;
}