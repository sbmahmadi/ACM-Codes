#include <iostream>
#include <string.h>

using namespace std;

int dp[1005][10];
int w[1005][10];
int main()
{
	int t;
	cin >> t;
	while(t--)
	{
		int x;
		cin >> x; x/=100; x;
		for(int j = 9; j >=0; j--)
			for(int i = 0; i < x; i++)
				cin >> w[i][j];
		memset(dp,-1,sizeof dp);
		dp[0][0] = 0;
		for(int i = 1; i <= x; i++)
			for(int j = 0; j < 10; j++)
			{
				int tmp = 1e5;
				if(dp[i-1][j] != -1) 
					tmp = min(tmp, dp[i-1][j] + 30 - w[i-1][j]);
				if(j>0 && dp[i-1][j-1] != -1)
					tmp = min(tmp, dp[i-1][j-1] + 60 - w[i-1][j-1]);
				if(j < 9 && dp[i-1][j+1] != -1)
					tmp = min(tmp, dp[i-1][j+1] + 20 - w[i-1][j+1]);
				if(tmp != 1e5)
					dp[i][j] = tmp;
			}

		cout << dp[x][0] << endl;
	}
}