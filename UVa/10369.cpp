/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <list>
#include <vector>
#include <queue>
#include <deque>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <math.h>
#include <algorithm>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

int main()
{
	int t;
	cin >> t;
	while(t--)
	{
		int s,p;
		cin >> s >> p;
		vector<ii> cor(p);
		For(i,0,p)
			cin >> cor[i].first >> cor[i].second;
		multiset<pair<double,int>> pq;
		multiset<double> mst;
		vi visit(p,0);
		visit[0] = true;
		For(i,1,p)
			pq.insert({hypot(cor[i].first - cor[0].first, cor[i].second - cor[0].second), i});
		while(mst.size() != p-1)
		{
			while(visit[pq.begin()->second])
				pq.erase(pq.begin());
			pair<double,int> u = *pq.begin(); pq.erase(pq.begin());
			visit[u.second] = true;
			mst.insert(u.first);
			For(i,0,p)
				if(!visit[i])
					pq.insert({hypot(cor[i].first - cor[u.second].first, cor[i].second - cor[u.second].second), i});
		}
		s--;
		For(i,0,s)
			mst.erase(--mst.end());
		double res = (mst.size() ? *mst.rbegin() : 0);
		cout << fixed << setprecision(2) << res << endl;
	}
}