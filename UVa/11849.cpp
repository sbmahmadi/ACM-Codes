#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

int main()
{
	int n,m;
	while(cin >> n >> m && (n || m))
	{
		vector<int> discs(n+m);
		for(int i = 0; i < n+m; i++)
			cin >> discs[i];
		sort(discs.begin(), discs.end());
		cout << distance(unique(discs.begin(), discs.end()),discs.end()) << endl;
	}
}