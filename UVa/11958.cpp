#include<iostream>
#include<algorithm>

using namespace std;

int main()
{
	int t;
	cin >> t;
	for (int i = 0; i < t; i++)
	{
		int CurH, CurM, k, mn = 100000;
		scanf_s("%d%d:%d", k, &CurH, &CurM);

		for (int j = 0; j < k; j++)
		{
			int h, m, d;
			scanf_s("%d:%d %d", &h, &m, &d);

			if (h<CurH || (h==CurH && m<CurM)) h += 24;

			int time = (h - CurH) * 60 + (m - CurM) + d;
			
			mn = min(time, mn);
		}
		printf("Case %d: %d\n", i + 1, mn);
	}
}