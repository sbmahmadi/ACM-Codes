/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <list>
#include <vector>
#include <queue>
#include <deque>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <math.h>
#include <algorithm>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

int w[2005];
int dist[2005];

map<string,int> ID;
map<string, set<string>> mp;

struct cmp
{
	bool operator()(const int a, const int b) const
	{
		if(dist[a] != dist[b])
			return dist[a] < dist[b];
		return a < b;
	}
};

int getID(string& s)
{
	return ID.insert({s, ID.size()}).first->second;
}

int djikstra(vector<vi>& graph, string s, string d)
{
	fill(dist, dist + 2005, 1e8);
	set<int, cmp> pq;
	jor(i,0,mp[s].size())
	{
		dist[getID(mp[s][i])] = w[getID(mp[s][i])];
		pq.insert(getID(mp[s][i]));
	}
	while(!pq.empty())
	{
		int u = *pq.begin(); pq.erase(pq.begin());
		For(i,0,mp[d].size())
		{
			if(u == getID(mp[d][i]))
				return dist[u];
		}		

		For(i,0,graph[u].size())
		{
			int v = graph[u][i];
			if(dist[v] > dist[u] + w[v])
			{
				pq.erase(v);
				dist[v] = dist[u] + w[v];
				pq.insert(v);
			}
		}
	}
	return -1;
}

int main()
{
	int m;
	while(cin >> m && m)
	{
		ID.clear();
		mp.clear();

		string o,d;
		vector<vi> graph(m);

		cin >> o >> d;
		For(i,0,m)
		{
			string i1,i2,p;
			cin >> i1 >> i2 >> p;
			w[getID(p)] = p.size();
			For(j,0,mp[i1].size())
			{
				if(mp[i1][j][0] != p[0])
				{
					graph[getID(p)].push_back(getID(mp[i1][j]));
					graph[getID(mp[i1][j])].push_back(getID(p));
				}
			}
			For(j,0,mp[i2].size())
			{
				if(mp[i2][j][0] != p[0])
				{
					graph[getID(p)].push_back(getID(mp[i2][j]));
					graph[getID(mp[i2][j])].push_back(getID(p));
				}
			}
			mp[i1].push_back(p);
			mp[i2].push_back(p);
		}
		int res = djikstra(graph, o, d);
		
		if(res == -1)
			cout << "impossivel" << endl;
		else
			cout << res << endl;
	}
}