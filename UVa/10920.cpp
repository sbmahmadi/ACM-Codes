/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
//typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

ll ii,jj;
ll p;
void fill(ll x,ll y,ll v,ll s)
{
	s--;
	if(v == 1)
	{
		if(p == 1)
			ii = x, jj = y;
		return;
	}
	For(i,0,s)
	{
		if(p == v)
		{
			ii = x, jj = y; return;
		}
		x++,v--;
	}
	
	For(i,0,s)
	{
		if(p == v)
		{
			ii = x, jj = y;return;
		}
		y--, v--;	
	}

	For(i,0,s)
	{
		if(p == v)
		{
			ii = x, jj = y;return;
		}
		x--, v--;
	}
	For(i,0,s)
	{
		if(p == v)
		{
			ii = x, jj = y;return;
		}
		y++, v--;
	}
}

int main()
{
	ll s;
	while(cin >> s >> p && (s || p))
	{
		For(i,0,s/2)
		{
			if(p <= (s-i*2)*(s-i*2) && p > (s-(i+1)*2)*(s-(i+1)*2))
			{
				fill(i,s-1-i,(s-i*2)*(s-i*2), s - 2*i);
				break;
			}
		}
		if(p == 1)
			ii = jj = (s)/2;
		cout << "Line = " << s - ii << ", column = " << jj+1 <<"." << endl;
	}	
}