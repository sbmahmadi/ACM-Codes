/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int main()
{
	/*ifstream fin("file.in");
	ofstream fout("file.out");*/
	int n,m;
	int test = 1;
	while(cin >> n >> m && n)
	{
		vi ires;
		int maxres = 0;
		vi areas(n+2);
		For(i,1,n+1)
			cin >> areas[i];



		int c;
		cin >> c;
		vector<set<int>> overlaps(c);
		vi overlapsCnt(c);
		For(i,0,c)
		{
			int t;
			cin >> t;
			For(j,0,t)
			{
				int a;
				cin >> a;
				overlaps[i].insert(a);
			}
			cin >> overlapsCnt[i];
		}



		for(int bitmask = 0; bitmask < 1<<n; bitmask++)
		{
			int temp = bitmask;
			int index = 0;
			vi indexes;
			while(temp > 0)
			{
				if(temp & 1)
					indexes.push_back(n-index);
				index++;
				temp>>=1;
			}
			if(indexes.size() != m)
				continue;
			int res = 0;
			For(i,0,indexes.size())
			{
				res += areas[indexes[i]];
			}
			For(i,0,c)
			{
				int cnt = 0;
				For(j,0,indexes.size())
				{
					if(overlaps[i].count(indexes[j]))
						cnt++;
				}
				if(cnt)
					res -= (cnt-1)*overlapsCnt[i];
			}
			if(res >= maxres)
			{
				maxres = res;
				ires = indexes;
			}
		}

		cout << "Case Number  " << test++ << endl;
		cout << "Number of Customers: " << maxres << endl;
		cout << "Locations recommended:";
		roFi(it,ires)
			cout << " " << *it;
		cout << endl << endl;
	}	
}