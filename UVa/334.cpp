/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

int nc,ne,nm;
map<string,int> ID;
map<int,string> name;

bool graph[500][500];

int getID(string s)
{
	return ID.insert(make_pair(s,ID.size())).first->second;
}

int main()
{
	ios_base::sync_with_stdio(0); 
	cin.tie(0); cout << fixed;

	int tt = 0;
	while(cin >> nc && nc)
	{
		memset(graph,0,sizeof graph);
		name.clear();
		ID.clear();
		string last,s;
		For(i,0,nc)
		{
			cin >> ne;
			last = " ";
			For(i,0,ne)
			{
				cin >> s;
				if(last != " ")
					graph[getID(last)][getID(s)] = 1;
				last = s;
				name[getID(last)] = last;
				name[getID(s)] = s;
			}
		}
		
		cin >> nm;
		For(i,0,nm)
		{
			cin >> last >> s;
			graph[getID(last)][getID(s)] = 1;
		}
		int n = ID.size();
		For(k,0,n)
			For(i,0,n)
				For(j,0,n)
					graph[i][j] |= graph[i][k] & graph[k][j];

		int con = 0;
		ii p[2];
		For(i,0,n)
			For(j,i+1,n)
				if(!(graph[i][j] | graph[j][i]))
					if(con++ < 2)
						p[con-1] = make_pair(i,j);
		
		cout << "Case " << ++tt << ", "; 
		if(con == 0)
			cout << "no concurrent events." << endl;
		else
		{
			cout << con << " concurrent events:" << endl;
			if(con == 1)
				cout << "(" << name[p[0].first] << "," << name[p[0].second] << ") " << endl;
			else 
				cout << "(" << name[p[0].first] << "," << name[p[0].second] << ") (" << name[p[1].first] << "," << name[p[1].second] << ") " << endl;
		}
	}
}