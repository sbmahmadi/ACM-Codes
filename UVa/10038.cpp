#include <iostream>
#include <cmath>
#include <bitset>

using namespace std;

string res[2] = {"Not jolly","Jolly"};

bitset<3005> bit;

int main()
{
	int n,a,b;
	while (cin >> n)
	{
		bit.reset();
		cin >> a;
		for (int i = 1; i < n; i++)
		{
			cin >> b;
			bit.set(abs(a-b));
			a = b;
		}
		cout << res[bit.count() == n-1 && (bit>>=n).none()] << endl;
	}
}