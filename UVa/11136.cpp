/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

int main()
{
	ios_base::sync_with_stdio(0);
	cin.tie(0);
	int n;
	while(cin >> n && n)
	{
		multiset<int> st;
		ll sum = 0;
		For(i,0,n)
		{
			int k;
			cin >> k;
			For(j,0,k)
			{
				int a;
				cin >> a;
				st.insert(a);
			}
			sum += *st.rbegin() - *st.begin();
			st.erase(st.begin());
			st.erase(--st.end());
		}
		cout << sum << endl;
	}	
}