/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<bool> vb;

ifstream fin("file.in");
ofstream fout("file.out");

int dfs(vi &graph, vb &visited, vb &totalvisited, int u)
{
	visited[u] = true;
	totalvisited[u] = true;
	if(!visited[graph[u]])
		return 1 + dfs(graph,visited,totalvisited,graph[u]);
	else 
		return 1;
}

int main()
{
	ios_base::sync_with_stdio(0);
	cin.tie(0);

	int t;
	cin >> t;
	For(tt,0,t)
	{
		int n;
		cin >> n;
		vi graph(n);
		vi inEdge(n,0);
		For(i,0,n)
		{
			int u,v;
			cin >> u >> v; u--, v--;
			graph[u] = v;
			inEdge[v]++;
		}
		int mx = -1;
		int mxi = -1;
		int tmp;
		vb visited(n,false);
		For(i,0,n)
			if(!inEdge[i])
				if((tmp = dfs(graph,*(new vb(n,false)), visited, i)) > mx)
					mx = tmp, mxi = i;
		For(i,0,n)
			if(!visited[i])
				if((tmp = dfs(graph,*(new vb(n,false)), visited, i)) > mx)
					mx = tmp, mxi = i;

		cout << "Case "<< tt+1 <<": " << mxi + 1<< endl; 
	}
}