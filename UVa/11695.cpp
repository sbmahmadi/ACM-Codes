#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define trace(x) //cerr << #x << " : " << x << endl;
#define traceV(x,k) //cout << #x << ": "; copy(allof(x),ostream_iterator<k>(cout, " ")); cout << endl;
#define allof(x) x.begin(),x.end()
#define fastio() ios_base:: sync_with_stdio(0); cin.tie(0);
using namespace std;

typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ii dfs(int ind, vector<vi>& graph, vi& par)
{
	ii mx = {0, ind};
	For(i,0,graph[ind].size())
		if(par[graph[ind][i]] == -1)
			par[graph[ind][i]] = ind, mx = max(mx, dfs(graph[ind][i], graph, par));
	mx.first++;
	return mx;
}

vi getdiam(int ind, vector<vi>& graph)
{
	vi res,par;
	par.assign(graph.size(), -1); par[ind] = -2; ind = dfs(ind, graph, par).second;
	par.assign(graph.size(), -1); par[ind] = -2; ind = dfs(ind, graph, par).second;
	while(ind != -2)
		res.push_back(ind),
		ind = par[ind];
	return move(res);
}

int main()
{
	fastio()
	int t;
	cin >> t;
	while(t--)
	{
		int n;
		cin >> n;
		vector<vi> graph(n);
		For(i,0,n-1)
		{
			int u,v;
			cin >> u >> v; u--; v--;
			graph[u].push_back(v);
			graph[v].push_back(u);
		}
		vi d = getdiam(0, graph);
		int mn = graph.size();
		ii rm,add;
		For(i,0,d.size()-1)
		{
			graph[d[i]].erase(find(allof(graph[d[i]]),d[i+1]));
			graph[d[i+1]].erase(find(allof(graph[d[i+1]]),d[i]));
			
			vi v1 = getdiam(d[i], graph);
			vi v2 = getdiam(d[i+1], graph);
			
			int t = max({v1.size()/2 + v2.size()/2 + 1, v1.size()-1, v2.size()-1});
			
			if(mn > t)
				mn = t, rm = {d[i],d[i+1]}, add = {v1[v1.size()/2], v2[v2.size()/2]};
			graph[d[i]].push_back(d[i+1]);
			graph[d[i+1]].push_back(d[i]);
		}
		cout << mn << endl;
		cout << ++rm.first << " " << ++rm.second << endl;
		cout << ++add.first << " " << ++add.second << endl;
	}
}