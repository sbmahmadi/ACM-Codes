/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

void dfs(char c, map<char,pair<vector<char>,bool>>& graph)
{
	graph[c].second = true;
	For(i,0,graph[c].first.size())
		if(!graph[graph[c].first[i]].second)
			dfs(graph[c].first[i],graph);
}

int main()
{
	int t;
	cin >> t;
	cin.ignore();
	while(t--)
	{
		map<char,pair<vector<char>,bool>> graph;
		string s;
		while(getline(cin,s) && s[0] != '*')
		{
			graph[s[1]].first.push_back(s[3]);
			graph[s[3]].first.push_back(s[1]);
		}
		getline(cin,s);
		For(i,0,s.size())
		{
			if(i%2 == 0)
				graph[s[i]];
		}
		int tree = 0, acorn = 0;
		Fori(it,graph)
		{
			if(it->second.second)
				continue;
			dfs(it->first, graph);
			if(it->second.first.size() == 0)
				acorn++;
			else
				tree++;
		}
		cout << "There are "<< tree << " tree(s) and " << acorn << " acorn(s)." << endl;
	}	
}