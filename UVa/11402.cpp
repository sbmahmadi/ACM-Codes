/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) //cerr <<#x<<": "<<x<<endl;
#define tracevi(x) //{ copy(x.begin(), x.end(), ostream_iterator<int>(cout, " ")); cout << endl; }
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

int _getval(int lq, int rq, int l, int r, int ind, vi& seg, vi& lazy)
{
	if(lazy[ind] != -1)
	{
		seg[ind] = lazy[ind];

		if(l != r)
		{
			lazy[ind*2] = lazy[ind*2+1] = lazy[ind];
		}

		lazy[ind] = -1;
	}

	if(r < lq || l > rq)
		return 0;
	if(l >= lq && r <= rq && seg[ind] != 2)
	{
		return seg[ind] * (r-l+1);
	}
	return _getval(lq, rq, l, (l+r)/2, ind*2, seg, lazy) + _getval(lq, rq, (l+r)/2+1, r, ind*2+1, seg, lazy);
}



int getval(int lq, int rq, vi& seg, vi& lazy)
{
	return _getval(lq, rq, 0, seg.size()/2-1, 1, seg, lazy);
}

void _update(int lq, int rq, int l, int r, int ind, char c, vi& seg, vi& lazy)
{
	if(lazy[ind] != -1)
	{

		seg[ind] = lazy[ind];

		if(l != r)
		{
			lazy[ind*2] = lazy[ind*2+1] = lazy[ind];
		}

		lazy[ind] = -1;
	}
	
	if(r < lq || l > rq)
		return;
	
	if(l >= lq && r <= rq && seg[ind] != 2)
	{
		
		if(c == 'F')
		{
			seg[ind] = 1;

			if(l != r)
			{
				lazy[ind*2] = lazy[ind*2+1] = 1;
			}
		}
		if(c == 'E')
		{
			seg[ind] = 0;

			if(l != r)
			{
				lazy[ind*2] = lazy[ind*2+1] = 0;
			}
		}
		if(c == 'I')
		{
			seg[ind] ^= 1;
			if(l != r)
			{
				lazy[ind*2] = lazy[ind*2+1] = seg[ind];
			}
		}
		return;
	}
	
	_update(lq, rq, l, (l+r)/2, ind*2, c, seg, lazy);
	
	_update(lq, rq, (l+r)/2 +1, r, ind*2+1,c, seg, lazy);
	
	if(seg[ind*2] == seg[ind*2+1])
		seg[ind] = seg[ind*2];
	else
		seg[ind] = 2;
}


void update(int lq, int rq, char c, vi& seg, vi& lazy)
{
	_update(lq, rq, 0, seg.size()/2-1, 1, c, seg, lazy);
}

int main()
{
	ios_base::sync_with_stdio(0);
	cin.tie(0);
	int t;
	cin >> t;
	For(tt,0,t)
	{
		int m;
		cin >> m;
		string in;
		For(i,0,m)
		{
			int T;
			cin >> T;
			string s;
			cin >> s;
			For(j,0,T)
				in += s;
		}
		int n = in.size();
		int nn = pow(2,ceil(log2(n)));
		
		vi seg (nn*2,0);
		vi lazy (nn*2,-1);

		For(i,nn,nn + n)
			seg[i] = in[i - nn] - '0';
		tracevi(seg)
		roF(i,nn-1,1)
		{
			if(seg[i*2] == seg[i*2+1])
				seg[i] = seg[i*2];
			else
				seg[i] = 2;
		}

		int q;
		int qq = 0;
		cin >> q;
		cout << "Case " << tt+1 << ":" << endl;
		For(i,0,q)
		{
			char c;
			int a,b;
			cin >> c >> a >> b;
			if(c == 'S')
			{
				cout << "Q" << ++qq << ": " <<  getval(a,b,seg,lazy) << endl;
				
			}
			else
			{
				update(a,b,c,seg,lazy);
				tracevi(seg);
			}
		}

	}	
}