/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

#define max 22000
#define infinity 999999999
int status[max];
int dist[max];
int tst,n,m,s,t,wt;
struct st
{
	int v;
	unsigned int w;
};
class compare
{
	public:
	bool operator()(const st& v1,const st& v2)
	{
		return v1.w>v2.w;
	}
};
priority_queue<st,vector<st>,compare> pq;
vector<st> adj[max];
void dijkstra()
{
	for(int i=0;i<n;i++)
	{
		dist[i]=infinity;
	}
	dist[s]=0;
	st s1={s,dist[s]};
	pq.push(s1);
	while(!pq.empty())
	{
		st cv=pq.top();
		pq.pop();
		status[cv.v]=1;
		int size=adj[cv.v].size();
		for(int i=0;i<size;i++)
		{
			int u=adj[cv.v][i].v;
			int w=adj[cv.v][i].w;
			if(dist[u]>dist[cv.v]+w)
			{
				dist[u]=dist[cv.v]+w;
				if(status[u]==0)
				{
					st s1={u,dist[u]};
					pq.push(s1);
				}
 
			}
 
 
		}
	}
}
int main()
{ 
    cin>>tst;int k=0;
    int v1,v2;
    while(tst--)
    {
    	 cin>>n>>m>>s>>t;
    	 memset(status,0,sizeof(status));
    	 k++;
    	 for(int i=0;i<m;i++)
    	 {
    	 	cin>>v1>>v2>>wt;
    	 	st s1={v2,wt};
    	 	adj[v1].push_back(s1);
    	 	st s2={v1,wt};
    	 	adj[v2].push_back(s2);
    	 }
    	 dijkstra();
    	 int l=dist[t];
    	 if(dist[t]==infinity)
    	 {
    	 	cout<<"Case #"<<k<<": unreachable"<<"\n";
 
    	 }
    	 else
    	 {
    	    cout<<"Case #"<<k<<": "<<l<<"\n";
    	 }
    	 for(int i=0;i<n;i++)
    	 {
    	 	adj[i].clear();
    	 }
    }
    return 0;
 
}