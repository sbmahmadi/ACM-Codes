/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <list>
#include <vector>
#include <queue>
#include <deque>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <math.h>
#include <algorithm>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

void dfs(int u, int par, int d, vector<vi>& graph, vi& dist)
{
	dist[u] = d;
	For(i,0,graph[u].size())
	{
		if(graph[u][i] != par)
			dfs(graph[u][i], u, d+1, graph, dist);
	}
}

int main()
{
	int n;
	while(cin >> n)
	{
		vector<vi> graph(n);
		For(i,0,n)
		{
			int u;
			cin >> u;
			For(j,0,u)
			{
				int k;
				cin >> k;
				graph[i].push_back(k-1);
			}
		}

		vi d1(n,0),d2(n,0),d3(n,0);
		dfs(0,-1,0,graph,d1);
		int mx = d1[0];
		int mi = 0;
		For(i,0,n)
			if(d1[i] > mx)
				mx = d1[i], mi = i;
		dfs(mi,-1,0,graph,d2);
		mx = d2[0];
		mi = 0;
		For(i,0,n)
			if(d2[i] > mx)
				mx = d2[i], mi = i;
		dfs(mi,-1,0,graph,d3);

		cout << "Best Roots  :";
		//trace(mx)
		For(i,0,n)
			if(abs(d2[i] - d3[i]) == mx%2 && d2[i] + d3[i] == mx)
			{
				//trace(d2[i] _ d3[i] _ i+1)
				cout << " " << i+1;
			}
		cout << endl << "Worst Roots :";
		For(i,0,n)
			if(d2[i] == mx || d3[i] == mx)
				cout << " " << i+1;
		cout << endl;
	}
}