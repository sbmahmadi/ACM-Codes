/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

int p,n;
int coin[105];
int dp[30001];

int main()
{
	ios_base::sync_with_stdio(0); 
	cin.tie(0); cout << fixed;
	int t;
	cin >> t;
	while(t--)
	{
		cin >> p >> n;
		For(i,0,n)
			cin >> coin[i];
		memset(dp, -1, sizeof dp);
		dp[0] = 0;
		For(i,0,n)
		{
			roF(j,10000,0)
			{
				int &tmp = dp[j + coin[i]];
				if(dp[j] >= 0)
					if(tmp == -1)
						tmp = dp[j] + 1;
					else
						tmp = min(tmp, dp[j] + 1);
			}
		}

		int i;
		for(i = p; i < 30001; i++)
			if(dp[i] >= 0)
				break;
		
		cout << i << " " << dp[i] << endl;
	}
}