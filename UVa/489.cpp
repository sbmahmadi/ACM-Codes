/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <list>
#include <vector>
#include <queue>
#include <deque>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <math.h>
#include <algorithm>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

string res[] = {"You chickened out.","You lose.","You win."};

int main()
{
	int t;
	while(cin >> t && t != -1)
	{
		string s1,s2;
		cin >> s1 >> s2;
		set<char> word,guess;
		for(char c : s1)
			word.insert(c);
		
		int win = 0, g = 0;
		for(char c : s2)
		{
			if(guess.count(c))
				continue;
			guess.insert(c);
			if(word.count(c))
			{
				word.erase(c);
				if(word.empty())
				{
					win = 2; break;
				}
			}
			else
			{
				g++;
				if(g == 7)
				{
					win = 1; break;
				}
			}
		}

		fout << "Round " << t << endl << res[win] << endl;
	}
}