#include<iostream>
#include<iomanip>
#include<string>

using namespace std;

int coins[] = { 5,10,20,50,100,200,500,1000,2000,5000,10000 };

long long dp[30005][11];

int main()
{
	for (int i = 0; i < 11; i++)
		dp[0][i] = 1;

	for (int i = 5; i <= 30000; i += 5)
	{
		dp[i][0] = 1;
		for (int j = 1; j < 11; j++)
		{
			if (i < coins[j])
				dp[i][j] = dp[i][j - 1];
			else
				dp[i][j] = dp[i][j - 1] + dp[i - coins[j]][j];
		}
	}

	cout << fixed << setprecision(2);
	float f;
	while(cin >> f && f)
	{
		int n = (f * 100 + 0.5);
		cout.width(6);
		cout << f;
		cout.width(17);
		cout << dp[n][10];
		cout << endl;
	}
}