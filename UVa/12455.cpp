/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;


void solve(int n, vi& bars)
{
	For(i, 0, 1<<bars.size())
	{
		int res = 0;
		int tmp = i;
		For(j, 0, bars.size())
			if(1<<j & tmp)
				res += bars[j];
		if(res == n)
		{
			cout << "YES" << endl;
			return;
		}
	}
	cout << "NO" << endl;
}

int main()
{
	/*ifstream fin("file.in");
	ofstream fout("file.out");*/
	int t;
	cin >> t;
	while(t--)
	{
		int n;
		cin >> n;
		int p;
		cin >> p;
		vi bars(p);
		For(i,0,p)
			cin >> bars[i];
		solve(n, bars);
	}
}