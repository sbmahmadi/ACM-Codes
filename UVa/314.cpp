/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <list>
#include <vector>
#include <queue>
#include <deque>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <math.h>
#include <algorithm>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

map<string,int> direction;

int dx[] = {1,0,-1,0};
int dy[] = {0,-1,0,1};
int grid[55][55][4];


int n,m;

int bfs(int s1, int s2, string s, int e1, int e2)
{
	queue<pair<ii,int>> q;
	q.push({{s1,s2},direction[s]});
	grid[s1][s2][direction[s]] = 1;
	while(!q.empty())
	{
		auto tp = q.front(); q.pop();
		if(tp.first.first == e1 && tp.first.second == e2)
			return grid[tp.first.first][tp.first.second][tp.second] - 1;
		For(j,1,4)
		{
			int nx = tp.first.first + dx[tp.second] * j;
			int ny = tp.first.second + dy[tp.second] * j;
			if(nx < n && nx >= 1 && ny < m && ny >= 1)
			{
				if(grid[nx][ny][tp.second] == 0)
				{
					q.push({{nx,ny},tp.second});
					grid[nx][ny][tp.second] = grid[tp.first.first][tp.first.second][tp.second] + 1;
				}
				else if(grid[nx][ny][tp.second] == -1)
					break;
			}
		}
		if(grid[tp.first.first][tp.first.second][(tp.second+1)%4] == 0)
		{
			auto tt = tp;
			tt.second = (tp.second+1)%4;
			q.push(tt);
			grid[tt.first.first][tt.first.second][tt.second] = grid[tp.first.first][tp.first.second][tp.second] + 1;
		}		
		if(grid[tp.first.first][tp.first.second][(tp.second-1 + 4) % 4] == 0)
		{
			auto tt = tp;
			tt.second = (tp.second-1+4)%4;
			q.push(tt);
			grid[tt.first.first][tt.first.second][tt.second] = grid[tp.first.first][tp.first.second][tp.second] + 1;
		}
	}
	return -1;
}

int main()
{
	ios_base::sync_with_stdio(0); cin.tie(0);
	direction["south"] = 0;
	direction["west"] = 1;
	direction["north"] = 2;
	direction["east"] = 3;

	while(cin >> n >> m && n)
	{
		memset(grid,0,sizeof grid);
		For(i,0,n)
			For(j,0,m)
			{
				int a;
				cin >> a;
				if(a == 1)
				{
					grid[i][j][0] = grid[i][j][1] = grid[i][j][2] = grid[i][j][3] =
					grid[i+1][j][0] = grid[i+1][j][1] = grid[i+1][j][2] = grid[i+1][j][3] =
					grid[i][j+1][0] = grid[i][j+1][1] = grid[i][j+1][2] = grid[i][j+1][3] =
					grid[i+1][j+1][0] = grid[i+1][j+1][1] = grid[i+1][j+1][2] = grid[i+1][j+1][3] =	-1;
				}
			}
		int s1,s2,e1,e2;
		string s;
		cin >> s1 >> s2 >> e1 >> e2 >> s;
		int res = bfs(s1, s2, s, e1, e2);
		/*For(i,0,n)
		{
			For(j,0,m)
				cout << setw(3) << min({grid[i][j][0],grid[i][j][1],grid[i][j][2],grid[i][j][3]});
			cout << endl;
		}*/
		cout << res << endl;
	}	
}