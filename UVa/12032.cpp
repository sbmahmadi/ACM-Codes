/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

bool reached(int k, vi& ladder)
{
	For(i,1,ladder.size())
	{
		int d = ladder[i] - ladder[i-1];
		if(d > k)
			return false;
		if(d == k)
			k--;
	}
	return true;
}

int main()
{
	int t;
	cin >> t;
	For(tt,0,t)
	{
		int n;
		cin >> n;
		vi ladder(n+1);
		ladder[0] = 0;
		For(i,1,n+1)
			cin >> ladder[i];
		int lo = 0, hi = 1e8, mid;
		For(i, 0, 50)
		{
			mid = (hi+lo) / 2;
			if(reached(mid,ladder))
				hi = mid;
			else 
				lo = mid;
		}
		cout << "Case " << tt+1 << ": " << hi << endl;
	}	
}
