/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0);
#define trace(x) cerr << #x << ": " << x << endl;
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl;
#define _ << " :: " <<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

int dp[125*16];

int f(int u, vector<vii>& graph)
{
	int& res = dp[u];
	if(res)
		return res;
	if(graph[u].empty())
		return res = 0;
	res = INT_MAX;
	For(i,0,graph[u].size())
		res = min(res, f(graph[u][i].first, graph) + graph[u][i].second);
	return res;
}

int main()
{
	fastio();
	string s;
	while(cin >> s)
	{
		memset(dp,0,sizeof dp);
		int n,m;
		cin >> n >> m;
		vector<vii> graph(n*m);
		For(i,0,n-1)
			For(j,0,m)
				For(k,0,m)
				{
					int u;
					cin >> u;
					graph[i*m + j].push_back({(i+1)*m + k, u + 2});
				}
		int mn = INT_MAX;
		For(i,0,m)
			mn = min(mn, f(i,graph));
		cout << s << endl << mn << endl;
	}
}