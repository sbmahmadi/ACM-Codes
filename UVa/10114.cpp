/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) //cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

int main()
{
	int dur, t;
	float down, loan;
	while(cin >> dur >> down >> loan >> t && dur >= 0)
	{
		float val = loan + down;
		float dep[105];
		memset(dep, -1, sizeof dep);
		For(i,0,t)
		{
			int a;
			float f;
			cin >> a >> f;
			dep[a] = f;
		}
		float depi;
		int k = 0;
		down = loan / dur;
		For(i,0,dur)
		{
			if(dep[i] >= 0)
				depi = dep[i];
			val *= 1 - depi;
			if(val > loan)
				break;
			loan -= down;
			trace(val _ loan)
			k++;
		}
		if(k == 1)
			cout << "1 month" << endl;
		
		else 
			cout << k << " months" << endl;
	}
}