/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <list>
#include <vector>
#include <queue>
#include <deque>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <math.h>
#include <algorithm>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

map<string,int> ID;
map<int,string> name;

int getID(string s)
{
	return ID.insert({s,ID.size()}).first->second;
}

int main()
{
	ios_base::sync_with_stdio(0);
	cin.tie(0);
	int n,m;
	int t = 0;
	while(cin >> n >> m && n)
	{
		ID.clear();
		name.clear();
		vector<vector<bool>> graph(n,vector<bool>(n,false));
		For(i,0,m)
		{
			string s1,s2;
			cin >> s1 >> s2;
			graph[getID(s1)][getID(s2)] = true;
			name[getID(s1)] = s1;
			name[getID(s2)] = s2;
		}
		For(k,0,n)
			For(i,0,n)
				For(j,0,n)
					graph[i][j] = graph[i][j] | (graph[i][k] & graph[k][j]);
		vector<vi> scc;
		vi seen(n,false);
		For(i,0,n)
		{
			if(seen[i])
				continue;
			vi cc;
			cc.push_back(i);
			seen[i] = true;
			For(j,0,n)
				if(i != j && graph[i][j] & graph[j][i])
					cc.push_back(j), seen[j] = true;
			scc.push_back(cc);
		}
		if(t)
			cout << endl;
		cout << "Calling circles for data set " << ++t << ":" << endl;
		For(i,0,scc.size())
		{
			For(j,0,scc[i].size())
			{
				cout << name[scc[i][j]];
				if(j != scc[i].size()-1)
					cout << ", ";
			}
			cout << endl;
		}
	}
}