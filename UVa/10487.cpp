/*
ID: behdad.1
LANG: C++11
PROB:
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int main()
{
	/*ifstream fin("file.in");
	ofstream fout("file.out");*/
	int n;
	int cs = 0;
	while (cin >> n && n != 0)
	{
		printf("Case %d:\n", ++cs);
		vi set;
		For(i, 0, n)
		{
			int a;
			cin >> a;
			set.push_back(a);
		}
		int m;
		cin >> m;
		For(i, 0, m)
		{
			int q;
			cin >> q;
			int close = set[0] + set[1];
			For(i, 0, set.size())
			{
				For(j, 0, set.size())
				{
					if (i == j) continue;
					if (abs(set[i] + set[j] - q) < abs(close - q))
					{
						close = set[i] + set[j];
					}
				}
			}

			printf("Closest sum to %d is %d.\n", q, close);

		}
		
		
	}
}
