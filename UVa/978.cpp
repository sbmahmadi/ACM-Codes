/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

int main()
{
	int t;
	cin >> t;
	while(t--)
	{
		int b, sg, sb;
		cin >> b >> sg >> sb;
		priority_queue<int> blue, green;
		For(i,0,sg)
		{
			int a;
			cin >> a;
			green.push(a);
		}
		For(i,0,sb)
		{
			int a;
			cin >> a;
			blue.push(a);
		}
		while(!blue.empty() && !green.empty())
		{
			vi gr,bl;
			int sz = min({b, (int)blue.size(), (int)green.size()});
			For(i, 0, sz)
			{
				int nb = blue.top(); blue.pop();
				int ng = green.top(); green.pop();
				
				if(nb > ng)
					bl.push_back(nb-ng);
				else if(ng > nb)
					gr.push_back(ng-nb);
			}
			For(i,0,bl.size())
				blue.push(bl[i]);
			For(i,0,gr.size())
				green.push(gr[i]);
		}
		
		if(blue.empty() && green.empty())
			cout << "green and blue died" << endl;
		else if(blue.empty())
		{
			cout << "green wins" << endl;
			while(!green.empty())
			{
				cout << green.top() << endl;
				green.pop();
			}
		}
		else
		{
			cout << "blue wins" << endl;
			while(!blue.empty())
			{
				cout << blue.top() << endl;
				blue.pop();
			}
		}
		if(t)
			cout << endl;
	}
}
