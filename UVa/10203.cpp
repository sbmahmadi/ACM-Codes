/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0);
#define trace(x) cerr << #x << ": " << x << endl;
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl;
#define _ << " :: " <<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

int main()
{
	int t;
	cin >> t;
	while(t--)
	{
		double x,y;
		cin >> x >> y; cin.ignore();
		string line;
		double total = 0;
		while(getline(cin,line) && line != "")
		{
			stringstream ss(line);
			double a,b,c,d;
			ss >> a >> b >> c >> d;
			total += 2 * hypot(a - c, b - d);
		}
		int T = total / 20000 * 60 + 0.5;
		cout << T/60 << ":" << setw(2) << setfill('0') << T%60 << endl;
		if(t)
			cout << endl;
	}	
}