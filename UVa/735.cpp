/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a;i<b;i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int main()
{
	vi nums;
	For(i,0,21)
	{
		nums.push_back(i);
		nums.push_back(i*2);
		nums.push_back(i*3);
	}
	nums.push_back(50);
	sort(allof(nums));
	nums.erase(unique(allof(nums)),nums.end());
	int n;
	while(cin >> n && n>0)
	{
		int c, p;
		c = p = 0;
		For(i,0,nums.size())
		{
			For(j,i,nums.size())
			{
				For(k,j,nums.size())
				{
					if(nums[i] + nums[j] + nums[k] == n)
						c++;
				}
			}
		}
		For(i,0,nums.size())
		{
			For(j,0,nums.size())
			{
				For(k,0,nums.size())
				{
					if(nums[i] + nums[j] + nums[k] == n)
						p++;
				}
			}
		}

		if(c)
		{
			printf("NUMBER OF COMBINATIONS THAT SCORES %d IS %d.\nNUMBER OF PERMUTATIONS THAT SCORES %d IS %d.\n",n,c,n,p);
		}
		else 
			printf("THE SCORE OF %d CANNOT BE MADE WITH THREE DARTS.\n",n);
		printf("**********************************************************************\n");
	}
	printf("END OF OUTPUT\n");
}