/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

double compDist(vi &perm, vector<ii> &pcs)
{
	double res = 0;
	For(i,0,pcs.size()-1)
		res += hypot(pcs[perm[i]].first - pcs[perm[i+1]].first, pcs[perm[i]].second - pcs[perm[i+1]].second); 
	return res;
}

int main()
{
	ios_base::sync_with_stdio(0); 
	cin.tie(0); cout << fixed << setprecision(2);
	int n;
	int cs = 0;
	while(cin >> n && n)
	{
		cout << "**********************************************************" << endl << "Network #" << ++cs << endl;
		vector<ii> pcs(n);
		vi perm(n), minperm;
		For(i,0,n)
		{
			perm[i] = i;
			cin >> pcs[i].first >> pcs[i].second;
		}
		
		double mindist = INT_MAX;
		do
		{
			double dist = compDist(perm,pcs);
			if(dist < mindist)
				mindist = dist, minperm = perm;
		} while(next_permutation(allof(perm)));
		
		For(i,0,n-1)
			cout << "Cable requirement to connect (" << pcs[minperm[i]].first << "," << pcs[minperm[i]].second << ") to (" << pcs[minperm[i+1]].first << "," << pcs[minperm[i+1]].second << ") is " << hypot(pcs[minperm[i]].first - pcs[minperm[i+1]].first, pcs[minperm[i]].second - pcs[minperm[i+1]].second) + 16 << " feet." << endl;
		
		cout << "Number of feet of cable required is " << mindist + (n-1)*16 << "." << endl;
	}
}