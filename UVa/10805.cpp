#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)

using namespace std;

typedef vector<int> vi;

int graph[30][30];

int BFS(int ind, int ind1, vector<vi>& graph)
{
	queue<int> q; q.push(ind); if(ind1 != ind) q.push(ind1);
	vi dist(graph.size(),-1); dist[ind] = 0; dist[ind1] = 0;
	int mx1 = 0, mx2 = 0;
	while(!q.empty())
	{
		int u = q.front(); q.pop();
		int k = 0;
		For(i,0,graph[u].size())
		{
			if(dist[graph[u][i]] == -1)
			{
				k++;
				dist[graph[u][i]] = dist[u] + 1;
				q.push(graph[u][i]);
			}
		}
		if(k == 0)
		{
			if(dist[u] >= mx1)
				mx2 = mx1, mx1 = dist[u];
			else if(dist[u] >= mx2)
				mx2 = dist[u];
		}
	}
	//cerr << "as: " << ind << " " << ind1 << " : " << mx1+mx2 << endl;
	return mx1 + mx2;
}

int main()
{
	ios_base::sync_with_stdio(0);
	cin.tie(0);
	int N;
	cin >> N;
	For(t,0,N)
	{
		int n,m;
		cin >> n >> m;
		vector<vi> graph(n);
		For(i,0,m)
		{
			int u,v;
			cin >> u >> v;
			graph[u].push_back(v);
			graph[v].push_back(u);
		}
		
		int mn = INT_MAX;
		For(i,0,n)
			mn = min(mn,BFS(i,i, graph));
		For(i,0,n)
			For(j,0,graph[i].size())
				mn = min(mn, BFS(i,graph[i][j],graph) + 1);
		cout << "Case #" << t+1 << ":" << endl << mn << endl << endl; 
	}
}
