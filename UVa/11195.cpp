#include<iostream>
#include <string>
#include <vector>
#include <time.h>

#define For(i,a,b) for(int i = a;i<b;i++)
#define trace(x) cout << #x << " : " << (x) << endl;
using namespace std;

int n;
int all;
int sum;
int row;
void solve(int colbit, int diagbit, int ndiagbit, int badcell[20])
{
	if (row == n)
	{
		sum++;
		return;
	}
	int free = all & ~(colbit | diagbit | ndiagbit);
	while(free)
	{
		int next = free & -free;
		free &= free-1;
		if(badcell[row] & next)
			continue;
		row +=1;
		solve(colbit | next, (diagbit | next) >> 1, (ndiagbit | next) << 1, badcell);
		row -=1;
	}
}

int main()
{
	row = 0;
	int test = 1;
	while (cin >> n && n)
	{
		all = (1<<n) -1;
		int badcell[20] = {};
		For(i,0,n)
		{
			For(j,0,n)
			{
				char c;
				cin >> c;
				if(c == '*')
					badcell[i] |= 1<<j;
			}
		}
		sum = 0;
		solve(0, 0, 0, badcell);
		printf("Case %d: %d\n",test++,sum);
		
	}
}