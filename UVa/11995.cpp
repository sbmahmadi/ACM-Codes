/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>
#include <stack>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

int main()
{
	ios_base::sync_with_stdio(0); 
	cin.tie(0); cout << fixed;	
	int n;
	while(cin >> n)
	{
		stack<int> st;
		queue<int> q;
		priority_queue<int> pq;
		bool canStack = 1, canQ = 1, canPQ = 1;
		int cmd, val;
		For(i,0,n)
		{
			cin >> cmd >> val;
			if(cmd == 1)
			{
				st.push(val);
				q.push(val);
				pq.push(val);
			}
			else
			{
				if(st.empty() || st.top() != val) canStack = false;
				if(q.empty() || q.front() != val) canQ = false;
				if(pq.empty() || pq.top() != val) canPQ = false;
				if(!st.empty()) st.pop();
				if(!q.empty()) q.pop();
				if(!pq.empty()) pq.pop();
			}
		}
		if((canStack | canQ | canPQ) == false)
			cout << "impossible" << endl;
		else if(canStack == true && canQ == false && canPQ == false)
			cout << "stack" << endl;
		else if(canStack == false && canQ == false && canPQ == true)
			cout << "priority queue" << endl;
		else if(canStack == false && canQ == true && canPQ == false)
			cout << "queue" << endl;
		else
			cout << "not sure" << endl;
	}
}