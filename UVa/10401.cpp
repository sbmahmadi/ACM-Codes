/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <list>
#include <vector>
#include <queue>
#include <deque>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <math.h>
#include <algorithm>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

int eval(char a)
{
	if(a >= '0' && a <= '9')
		return a - '0';
	return a - 'A' + 10;
}

ll dp[20][20];
string s;
int n;
ll dfs(int last, int col)
{
	if(col == n)
		return 1;
	if(dp[last][col] >= 0)
		return dp[last][col];
	ll res = 0;
	if(s[col] == '?')
	{
		For(i,0,n)
			if(i != last && i != last + 1 && i != last - 1)
				res += dfs(i, col+1);
	}
	else
	{
		int k = eval(s[col]) - 1;
		if(k != last && k != last + 1 && k != last - 1)
			res = dfs(k, col+1);
	}
	return dp[last][col] = res;
}

int main()
{
	ios_base::sync_with_stdio(0); cin.tie(0);
	while(cin >> s)
	{
		memset(dp, -1, sizeof dp);
		n = s.size();
		cout << dfs(n+2,0) << endl;
	}	
}