/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>
#include <forward_list>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

void toposort(int u, vector<vi> &graph, vector<bool> &visited,forward_list<int> &res)
{
	visited[u] = true;
	For(i,0,graph[u].size())
		if(!visited[graph[u][i]])
			toposort(graph[u][i],graph,visited,res);
	res.push_front(u);
}

int main()
{
	int n,m;
	while(cin >> n >> m && n)
	{
		vector<vi> graph(n);
		For(i,0,m)
		{
			int u,v;
			cin >> u >> v;
			graph[u-1].push_back(v-1);
		}
		forward_list<int> res;
		vector<bool> visited(n,false);
		For(i,0,n)
		{
			if(!visited[i])
				toposort(i,graph,visited,res);
		}
		Fori(it, res)
			cout << *it+1 << " ";
		cout << endl; 
	}
}