#include<iostream>
#include<vector>
#include<queue>

using namespace std;

#define For(i,a,b) for(int i =a;i<b;i++)
#define Prior priority_queue<Edge,vector<Edge>,less<Edge>>

class Edge
{
public:

	int destIndex;
	int weight;
	Edge(int d, int w) : destIndex(d), weight(w){}

	bool operator<(const Edge e) const
	{
		return (weight < e.weight);
	}
};

class Node
{
public:
	vector<Edge>adjacent;
	bool visited;
	Node() : visited(false){}
};

void visit(int index, vector<Node> &graph, Prior &pq)
{
	graph[index].visited = true;

	for (Edge e : graph[index].adjacent)
	{
		if (!graph[e.destIndex].visited) pq.push(e);
	}
}

int main()
{
	int t, n, m, a, b, w;
	cin >> t;
	For(i, 0, t)
	{
		cin >> n >> m;
		vector<Node>graph;
		graph.assign(n, Node());
		Prior pq;
		For(j, 0, m)
		{
			cin >> a >> b >> w;
			graph[a].adjacent.push_back(Edge(b, w));
			graph[b].adjacent.push_back(Edge(a, w));
		}
		visit(0, graph, pq);
		int min = 1001;
		while (!pq.empty())
		{
			Edge e = pq.top();
			if (!graph[e.destIndex].visited)
			{
				visit(e.destIndex, graph, pq);
				if (e.weight < min) min = e.weight;
			}
			else pq.pop();
		}
		printf("Case #%d: %d\n", i + 1, min);
	}
}