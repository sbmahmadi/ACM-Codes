/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

int graph[105][105];

int main()
{
	ios_base::sync_with_stdio(0);
	cin.tie(0);
	int t;
	cin >> t;
	For(tt,0,t)
	{
		int n,r;
		cin >> n >> r;
		For(i,0,n) {For(j,0,n) graph[i][j] = 2000; graph[i][i] = 0;}
		For(i,0,r)
		{
			int u,v;
			cin >> u >> v;
			graph[u][v] = graph[v][u] = 1;
		}

		For(k,0,n)
			For(i,0,n)
				For(j,0,n)
					graph[i][j] = min(graph[i][j], graph[i][k] + graph[k][j]);

		int s,d;
		cin >> s >> d;
		int res = 0;
		For(i,0,n)
			res = max(res, graph[s][i] + graph[i][d]);
		cout << "Case " << tt+1 << ": " << res << endl;
	}	
}