/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <list>
#include <vector>
#include <queue>
#include <deque>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <math.h>
#include <algorithm>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

void dfs(int u, vector<vector<pair<int,int>>>& graph, vector<int>& visit, int mx)
{
	visit[u] = true;
	For(i,0,graph[u].size())
	{
		pair<int,int> v = graph[u][i];
		if(!visit[v.first] && v.second <= mx)
			dfs(v.first, graph, visit, mx);
	}
}

int main()
{
	int c,s,q;
	int t = 0;
	while(cin >> c >> s >> q && (c || s || q) )
	{
		vector<vector<pair<int,int>>> graph(c);
		For(i,0,s)
		{
			int u,v,w;
			cin >> u >> v >> w; u--; v--;
			graph[u].push_back({v,w});
			graph[v].push_back({u,w});
		}
		if(t)
			cout << endl;
		cout << "Case #" << ++t << endl;
		For(i,0,q)
		{
			int u,v;
			cin >> u >> v; u--; v--;
			int lo = 0, hi = 1e8;
			vector<int> visit;
			while(hi != lo)
			{
				visit.assign(c,0);
				int mid = (lo+hi)/2;
				dfs(u,graph,visit,mid);
				if(visit[v])
					hi = mid;
				else 
					lo = mid+1;
			}

			if(lo == 1e8)
				cout << "no path" << endl;
			else 
				cout << lo << endl; 
		}
	}
}