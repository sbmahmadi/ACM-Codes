/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <list>
#include <vector>
#include <queue>
#include <deque>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <math.h>
#include <algorithm>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

int crit;
int dfsc;
int rootdeg;
void dfs(int u, int p, const int root, vector<vi>& graph, vi& visit, vi& low)
{
	low[u] = visit[u] = dfsc++;
	bool iscrit = false;
	For(i,0,graph[u].size())
	{
		int v = graph[u][i];
		if(!visit[v])
		{
			if(u == root)
				rootdeg++;
			dfs(v, u,root, graph,visit,low);
			low[u] = min(low[u], low[v]);
			if(low[v] >= visit[u])
				iscrit = true;
		}
		else if(v != p)
			low[u] = min(low[u],visit[v]);
	}
	if(iscrit) 
		crit++;
}

int main()
{
	int n;
	while(cin >> n && n)
	{
		dfsc = crit = rootdeg = 0;
		dfsc++;
		cin.ignore();
		vector<vi> graph(n);
		string s;
		while(getline(cin, s) && s != "0")
		{
			stringstream ss(s);
			int u,v;
			ss >> u;
			while(ss >> v)
			{
				graph[u-1].push_back(v-1);
				graph[v-1].push_back(u-1);
			}
		}
		vi visit(n,-1), low(n,0);

		dfs(0,0,0,graph,visit,low);
		cout << crit - (rootdeg == 1) << endl;
	}
}