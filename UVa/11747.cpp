/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <list>
#include <vector>
#include <queue>
#include <deque>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <math.h>
#include <algorithm>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

int par(int u, vi& p)
{
	if(p[u] == u)
		return u;
	return par(p[u],p);
}

void unionset(int u, int v, vi& p)
{
	p[par(u,p)] = par(v,p);
}

int main()
{
	int n,m;
	while(cin >> n >> m && n){

	set<pair<int,ii>> e;
	set<int> ws;
	vi p(n);
	vi visit(n,0);
	For(i,0,n)
		p[i] = i;

	For(i,0,m)
	{
		int u,v,w;
		cin >> u >> v >> w;
		e.insert({w,{u,v}});
		ws.insert(w);
	}
	int got = 0;
	while(got < n-1)
	{
		if(e.empty())
			break;
		auto u = *e.begin(); e.erase(e.begin());
		if(par(u.second.first,p) == par(u.second.second,p))
			continue;
		unionset(u.second.first,u.second.second,p);
		got++;
		ws.erase(u.first);
	}
	if(ws.empty())
		cout << "forest" << endl;
	else
	{
		Fori(it,ws)
		{
			if(it != ws.begin())
				cout << " ";
			cout << *it;
		}
		cout << endl;
	}}
}