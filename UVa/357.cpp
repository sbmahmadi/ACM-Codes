/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

const int coins[5] = {1,5,10,25,50};

ll dp[30010][5];

ll change(int cash, int max)
{
	if(cash < 0)
		return 0;
	if(dp[cash][max] != 0)
		return dp[cash][max];
	ll sum = 0;
	For(i,0,max+1)
	{
		sum += change(cash - coins[i], i);
	}
	return dp[cash][max] = sum;
}

int main()
{
	For(i,0,5)
		dp[0][i] = 1;
	int n;
	while(cin >> n)
	{
		ll k = change(n,4);
		if(k == 1)
			cout << "There is only 1 way to produce " << n <<" cents change." << endl;
		else
			cout << "There are "<<k<<" ways to produce "<< n <<" cents change." << endl;
	}
}