#include<iostream>
#include<string.h>

#define MOD 1000000

using namespace std;

int dp[105][105];

int f(int n, int k)
{
	if(k == 0)
		return 0;
	if(n == 0 || k == 1)
		return 1;
	if(dp[n][k] >= 0)
		return dp[n][k];
	int res = 0;
	for(int i=0; i<=n; i++)
		res += f(i,k-1), res %= MOD;
	return dp[n][k] = res;
}

int main()
{
	memset(dp, -1, sizeof dp);
	int n, k;
	while (cin >> n >> k && n)
		cout << f(n,k) << endl;
}


