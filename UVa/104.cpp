/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) //cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

double graph[22][22][22];
int p[22][22][22];
int n;

void print(int i, int j, int m)
{
	if(m == 1)
	{
		cout << i+1 << " " << j+1;
		return;
	}
	print(i, p[i][j][m], m-1);
	cout <<" "<< j+1;
}

void floyd()
{
	For(m,2,n+1)
		For(i,0,n)
			For(j,0,n)
				For(k,0,n)
					if(j != k && graph[i][j][m] < graph[i][k][m-1] * graph[k][j][1])
					{
						graph[i][j][m] = graph[i][k][m-1] * graph[k][j][1];
						p[i][j][m] = k;
						
						if(i == j && graph[i][j][m] > 1.01)
						{
							print(i,j,m); 
							return;
						}
					}
	cout << "no arbitrage sequence exists";
}

int main()
{
	ios_base::sync_with_stdio(0); 
	cin.tie(0); cout << fixed;
	while(cin >> n)
	{
		memset(graph,0,sizeof graph);
		For(i,0,n)
			For(j,0,n)
				if(i == j)
					graph[i][j][1] = 1.0;
				else 
					cin >> graph[i][j][1];
		For(i,0,n)
			For(j,0,n)
				p[i][j][1] = i;
		floyd();
		cout << endl;
	}
}