/*
ID: behdad.1
LANG: C++11
PROB:
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int main()
{
	/*ifstream fin("file.in");
	ofstream fout("file.out");*/
	int f, r;
	while (cin >> f && f != 0)
	{
		cin >> r;
		vi front(f), rear(r);
		For(i, 0, f)
			cin >> front[i];
		For(i, 0, r)
			cin >> rear[i];
		vector<double> rates;
		For(i, 0, r)
		{
			For(j, 0, f)
			{
				rates.push_back(1.0 * rear[i] / front[j]);
			}
		}
		sort(allof(rates));
		double max = 0;
		For(i, 1, rates.size())
			if (rates[i] / rates[i - 1] > max)
				max = rates[i] / rates[i - 1];
		cout << setprecision(2) << fixed << max << endl;
	}
}
