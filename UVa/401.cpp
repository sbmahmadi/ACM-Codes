#include <bits/stdc++.h>

using namespace std;

map<char,char> mirror = {{'A','A'}, {'E','3'}, {'H','H'}, {'I','I'}, {'J','L'}, {'L','J'}, {'M','M'}, {'O','O'}, {'S','2'}, {'T','T'} , {'U','U'}, 
						{'V','V'}, {'W','W'}, {'X','X'}, {'Y','Y'}, {'Z','5'}, {'1','1'}, {'2','S'}, {'3','E'}, {'5','Z'}, {'8','8'}};


bool ispal(string & s)
{
	for(int i = 0; i < s.size()/2; i++)
	{
		if(s[i] != s[s.size()-1-i])
			return 0;
	}
	return 1;
}

bool ismirror(string& s)
{
	string ss = s;
	for(int i = 0 ; i < ss.size(); i++)
	{
		if(mirror.count(ss[i]))
			ss[i] = mirror[ss[i]];
		else return 0;
	}
	reverse(ss.begin(), ss.end());
	return ss == s;
}

int main()
{
	string line;
	while(cin >> line)
	{
		if(ispal(line))
		{
			if(ismirror(line))
			{
				cout << line << " -- is a mirrored palindrome." << endl;
			}
			else
				cout << line << " -- is a regular palindrome." << endl;
		}
		else if(ismirror(line))
			cout << line << " -- is a mirrored string." << endl;
		else
			cout << line << " -- is not a palindrome." << endl;
		cout << endl;
	}
}