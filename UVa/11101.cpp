/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <list>
#include <vector>
#include <queue>
#include <deque>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <math.h>
#include <algorithm>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

const int dx[] = {1,-1,0,0};
const int dy[] = {0,0,1,-1};

int main()
{
	int p1,p2;
	while(cin >> p1 && p1)
	{
		int a,b;
		queue<ii> q;
		vector<vi> visit(2005,vi(2005,-1));
		
		For(i,0,p1)
		{
			cin >> a >> b;
			ii tmp(a,b);
			q.push(tmp);
			visit[a][b] = 0;
		}
		cin >> p2;
		set<ii> m2;
		For(i,0,p2)
		{
			cin >> a >> b;
			m2.insert({a,b});
		}
		int res = INT_MAX;
		ii u;
		while(!q.empty())
		{
			u = q.front(); q.pop();
			if(m2.count(u))
				break;

			For(i,0,4)
			{
				ii tmp(u.first + dx[i], u.second + dy[i]);
				
				if(tmp.first >= 0 && tmp.second >= 0 && tmp.second < 2001 && tmp.first < 2001 && visit[tmp.first][tmp.second] == -1)
				{
					visit[tmp.first][tmp.second] = visit[u.first][u.second] + 1;
					q.push(tmp);
				}
			}
		}

		cout << visit[u.first][u.second] << endl;

	}
}