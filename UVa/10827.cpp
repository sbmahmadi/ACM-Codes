/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

int grid[160][160];


int main()
{
	//ios_base::sync_with_stdio(0); 
	//scin.tie(0); cout << fixed;

	int t;
	cin >> t;
	while(t--)
	{
		int n;
		cin >> n;
		For(i,1,n+1)
			For(j,1,n+1)
			{
				int a;
				cin >> a;
				grid[i][j] = grid[i+n][j] = grid[i][j+n] = grid[i+n][j+n] = a;
			}
		For(i,1,2*n+1)
			For(j,1,2*n+1)
				grid[i][j] += grid[i][j-1] + grid[i-1][j] - grid[i-1][j-1];
		int mx = 0;
		For(i,1,2*n+1)
			For(j,1,2*n+1)
				roF(x,i-1,0)
				{
					if(i-x > n)
						break;
					roF(y,j-1,0)
					{
						if(j-y > n)
							break;
						mx = max(mx,grid[i][j] - grid[i][y] - grid[x][j] + grid[x][y]);
					}
				}
		cout << mx << endl;
	}
}