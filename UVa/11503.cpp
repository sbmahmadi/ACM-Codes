/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0);
#define trace(x) cerr << #x << ": " << x << endl;
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl;
#define _ << " :: " <<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

vi par,size;

int getID(string s, map<string,int>& mp)
{
	auto k = mp.insert({s, mp.size()});
	if(k.second)
	{
		size.push_back(1);
		par.push_back(par.size());
	}
	return k.first->second;
}

int getpar(int u)
{
	if(par[u] == u)
		return u;
	return par[u] = getpar(par[u]);
}

void merge(int u, int v)
{
	if(getpar(v) == getpar(u))
		return;
	size[getpar(v)] += size[getpar(u)];
	par[getpar(u)] = getpar(v);
}

int main()
{
	fastio();
	int t;
	cin >> t;
	while(t--)
	{
		par.clear();
		size.clear();
		map<string,int> mp;
		int n;
		cin >> n;
		while(n--)
		{
			string s1,s2;
			cin >> s1 >> s2;
			merge(getID(s1,mp),getID(s2,mp));
			cout << size[getpar(getID(s1,mp))] << endl;
		}
	}	
}