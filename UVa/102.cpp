#include<iostream>
#include<math.h>

using namespace std;

int main()
{
	char c[3] = { 'B', 'C', 'G' };

	long long a[3][3];

	while (cin >> a[0][0] >> a[0][2] >> a[0][1])
	{
		int choose[3];
		int minres[3];
		long long min = pow(2,31);

		for (int i = 1; i < 3; i++)
			cin>>a[i][0]>>a[i][2]>>a[i][1];

		for (int i = 0; i < 3; i++)
		{
			choose[0] = i;
			for (int j = 0; j < 3; j++)
			{
				if (j == i) continue;
				else choose[1] = j;
				for (int k = 0; k < 3; k++)
				{
					if (k != j & k != i) 
					{
						choose[2] = k;
					}
				}
				
				long long res = 0;

				for (int k = 0; k < 3; k++)
				{
					for (int p = 0; p < 3; p++)
					{
						if (p != choose[k]) res += a[k][p];
					}
				}
				if (res < min)
				{
					min = res;
					copy(choose,choose+3,minres);
				}
			}
		}

		for (int i = 0; i < 3; i++)
		{
			cout << c[minres[i]];
		}
		cout << " " << min << endl;
	}
}