/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <list>
#include <vector>
#include <queue>
#include <deque>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <math.h>
#include <algorithm>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

#define MOD 1000007

int grid[105][105];
int dp[105][105];
int n;

int f(int x, int y)
{
	if(x == 0)
		return 1;
	if(dp[x][y] > 0)
		return dp[x][y];
	int res = 0;
	if(y > 0 && grid[x-1][y-1] != 1)
		res += f(x-1,y-1);
	if(y > 1 && x > 1 && grid[x-1][y-1] == 1 && grid[x-2][y-2] == 0)
		res += f(x-2,y-2);

	if(y < n-1 && grid[x-1][y+1] != 1)
		res += f(x-1,y+1);
	if(y < n-2 && x > 1 && grid[x-1][y+1] == 1 && grid[x-2][y+2] == 0)
		res += f(x-2,y+2);
	res %= MOD;
	return dp[x][y] = res;
}

int main()
{
	int t;
	cin >> t;
	For(tt,0,t)
	{
		memset(dp,-1,sizeof dp);
		cin >> n;
		int mx,my;
		For(i,0,n)
			For(j,0,n)
			{
				char c;
				cin >> c;
				if(c == 'W')
				{
					mx = i, my = j;
				}
				else if(c == 'B')
					grid[i][j] = 1;
				else
					grid[i][j] = 0;
			}
		cout << "Case " << tt+1 << ": " << f(mx,my) << endl;
	}
}