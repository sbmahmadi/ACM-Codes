/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

/*ifstream fin("file.in");
ofstream fout("file.out");*/

bool found;
vector<ii> dom;
vector<bool> used;
int last;
int n;

void solve(int toFill, int face)
{
	if(toFill == n+1)
	{
		if(face == last)
			found = true;
		return;
	}
	For(i,0,used.size())
	{
		if(!used[i])
		{
			int nface = -1;
			if(dom[i].first == face)
				nface = dom[i].second;
			else if(dom[i].second == face)
				nface = dom[i].first;
			if(nface != -1)
			{
				used[i] = true;
				solve(toFill+1, nface);
				used[i] = false;	
			}
		}
	}
}

int main()
{
	int m;
	while(cin >> n && n)
	{
		cin >> m;
		
		used.assign(m, false);
		found = false;
		dom.resize(m);

		int a,b,d;
		cin >> a >> b >> last >> d;
		For(i,0,m)
			cin >> dom[i].first >> dom[i].second;
		
		solve(1,b);
		
		if(found)
			cout << "YES" << endl;
		else 
			cout << "NO" << endl;
	}
}
