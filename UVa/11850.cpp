#include<iostream>
#include<vector>
#include<algorithm>

using namespace std;

int main()
{
	int n;
	cin >> n;
	while (n != 0)
	{
		vector<int> sts;
		sts.push_back(0);
		sts.push_back(1422);
		for (int i = 0; i < n; i++)
		{
			int temp;
			cin >> temp;
			sts.push_back(temp);
		}
		
		sort(sts.begin(), sts.end());
		bool flag = false;
		for (vector<int>::iterator it = sts.begin(); it != sts.end()-1; it++)
		{
			if (*(it + 1) - *it > 200)
			{
				flag = true;
				break;
			}
		}

		if (1422 - *(sts.end() - 2) > 100) flag = true;

		if (!flag) cout << "POSSIBLE" << endl;
		else cout << "IMPOSSIBLE" << endl;
		cin >> n;
	}
}