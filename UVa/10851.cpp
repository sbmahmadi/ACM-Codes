/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0);
#define trace(x) cerr << #x << ": " << x << endl;
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl;
#define _ << " :: " <<
#define allof(x) (x).begin(),(x).end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

int main()
{
	int t;
	cin >> t;
	while(t--){
		vector<vi> m(10);
		string line;
		For(i,0,10){
			cin >> line;
			For(j,1,line.size()-1){
				m[i].push_back(line[j] == '/' ? 0 : 1);
			}
		}

		string res;
		For(i,0,line.size()-2){
			int c = 0;
			For(j,1,9){
				c += m[j][i] << (j-1);
			}
			res += char(c);
		}
		cout << res << endl;
	}	
}
