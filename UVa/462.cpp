/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <sstream>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <iomanip>
#include <fstream>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int eval(char c)
{
	switch(c)
	{
		case 'A' : return 1;
		case 'T' : return 10;
		case 'J' : return 11;
		case 'Q' : return 12;
		case 'K' : return 13;
		
		case 'S' : return 0;
		case 'H' : return 1;
		case 'D' : return 2;
		case 'C' : return 3;
	}
	return c - '0';
}

int main()
{
	char rank[4] = {'S','H','D','C'};
	/*ifstream fin("file.in");
	ofstream fout("file.out");*/
	string s;
	while(getline(cin,s))
	{
		stringstream ss(s);
		int suits[4][15] = {0};

		For(i,0,13)
		{
			string str;
			ss >> str;
			suits[eval(str[1])][eval(str[0])]++;
			suits[eval(str[1])][0]++;
		}

		int pnt1 = 0;
		int pnt2 = 0;
		For(i,0,4)
		{
			pnt1 += suits[i][1] * 4 + suits[i][13] * 3 + suits[i][12] * 2 + suits[i][11];
			
			if(suits[i][0] == suits[i][13] && suits[i][13])
				pnt1--;
			if(suits[i][0] - suits[i][12] < 2 && suits[i][12])
				pnt1--;
			if(suits[i][0] - suits[i][11] < 3 && suits[i][11])
				pnt1--;

		}
		pnt2 = pnt1;
		For(i,0,4)
		{
			if(suits[i][0] == 2)
				pnt1++;
			if(suits[i][0] < 2)
				pnt1+=2;
			suits[i][14] = suits[i][1] | (suits[i][13] & suits[i][0] - suits[i][13] > 0) | (suits[i][12] & suits[i][0] - suits[i][12] > 1);
		}
		
		string res;
		if(pnt1 < 14)
			res = "PASS";
		else
		{
			char c;
			int cnt = 0;
			For(i,0,4)
				if(suits[i][0] > cnt)
				{
					cnt = suits[i][0];
					c = rank[i];
				}
			res = "BID ";
			if(pnt2 >= 16 && (suits[0][14] & suits[1][14] & suits[2][14] & suits[3][14]))
				res += "NO-TRUMP";
			else res += c;
		}
		cout << res << endl;
	}
}