import java.util.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

class Main{
	public static void main(String[] args) {
		Scanner sc  = new Scanner(System.in);
		while(true){
			int n,d,m,y;
			n = sc.nextInt();
			d = sc.nextInt();
			m = sc.nextInt();
			y = sc.nextInt();
			if(n == 0 && d == 0 && m == 0 && y == 0)
				break;
			GregorianCalendar date = new GregorianCalendar(y,m-1,d);
			date.add(Calendar.DATE, n);
			System.out.println(date.get(date.DATE) + " " + (date.get(date.MONTH) + 1) + " " + date.get(date.YEAR));
		}
	}
}