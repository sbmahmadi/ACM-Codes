/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int dp[1000000];

int f(int a)
{
	if(a >= 1000000)
	{
		if(a%2)
			return f(3*a + 1) + 1;
		return f(a/2) + 1;	
	}
	if(dp[a])
		return dp[a];
	if(a%2)
		return dp[a] = f(3*a + 1) + 1;
	return dp[a] = f(a/2) + 1;
}

int main()
{
	/*ifstream fin("file.in");
	ofstream fout("file.out");*/
	dp[0] = 0;
	dp[1] = 1;
	int a,b;
	while(cin >> a >> b)
	{
		int max = 0;
		For(i,a,b+1)
		{
			if(f(i) > max)
				max = f(i);
		}
		cout << a << ' ' << b << ' ' << max << endl;
	}
}