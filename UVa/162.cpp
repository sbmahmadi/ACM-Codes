/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <iomanip>
#include <fstream>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int isFace(string s)
{
	int k = 0;
	switch (s[1])
	{
		case 'A': k = 4;        break;         
		case 'K': k = 3;        break;
		case 'Q': k = 2;        break;
		case 'J': k = 1;        break;
	}
	return k;
}

int main()
{
	string s;
	while(true)
	{
		deque<string> p1, p2, heap;
		cin >> s;
		if(s == "#") break;
		p1.push_back(s);
		cin >> s;
		p2.push_back(s);
		For(i,0,25)
		{
			cin >> s;
			p1.push_back(s);
			cin >> s;
			p2.push_back(s);
		}
		bool turn = 0;
		int n = 1;
		bool inSeq = false;
		while(true)
		{
			if(!turn)
			{
				if(p1.size() == 0)
					break;
				heap.push_front(p1.back());
				p1.pop_back();
				n--;
				if(isFace(heap.front()))
				{
					turn = 1;
					n = isFace(heap.front());
					inSeq = true;
				}
				else
				{
					if(inSeq && n == 0)
					{
						while(heap.size())
						{
							p2.push_front(heap.back());
							heap.pop_back();
						}
						inSeq = false;
						turn = 1;
						n = 1;
					}
					else if(!inSeq)
					{
						turn = 1;
						n = 1;
					}
				}
			}
			if(turn)
			{
				if(p2.size() == 0)
					break;
				heap.push_front(p2.back());
				p2.pop_back();
				n--;
				if(isFace(heap.front()))
				{
					turn = 0;
					n = isFace(heap.front());
					inSeq = true;
				}
				else
				{
					if(inSeq && n == 0)
					{
						while(heap.size())
						{
							p1.push_front(heap.back());
							heap.pop_back();
						}
						inSeq = false;
						turn = 0;
						n = 1;
					}
					else if(!inSeq)
					{
						turn = 0;
						n = 1;
					}
				}
			}
		}
		if(p1.size())
			cout << 2 << setw(3) << p1.size() << endl;
		else 
			cout << 1 << setw(3) << p2.size() << endl;
	}
}
