/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int n;
vi circle;
vector<bool> used;

bool isPrime(int n)
{
	For(i,2,n)
	{
		if(!(n%i))
			return false;
	}
	return true;
}

void solve(int l)
{
	if(l == n)
	{
		if(isPrime(circle.back() + 1))
		{
			cout << 1;
			For(i,1,n)
				cout << " " <<circle[i];
			cout << endl;
		}
		return;
	}
	For(i,1,n+1)
	{
		if(!used[i] && isPrime(circle[l-1] + i))
		{
			used[i] = true;
			circle.push_back(i);
			solve(l+1);
			used[i] = false;
			circle.pop_back();
		}
	}

}

int main()
{
	/*ifstream fin("file.in");
	ofstream fout("file.out");*/
	circle.push_back(1);
	int test = 1;
	while(cin >> n)
	{
		used.assign(n+1,false);
		used[1] = true;
		if(test != 1)
			cout << endl;
		printf("Case %d:\n", test++);
		solve(1);
	}
}