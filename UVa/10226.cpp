/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

int main()
{	
	cout << fixed << setprecision(4);
	int n;
	cin >> n;
	cin.ignore(2);
	For(i,0,n)
	{
		string line;
		map<string,int> mp;
		int total = 0;
		while(getline(cin,line) && line != "")
		{
			cout << "line : " <<  line << endl;
			mp[line]++;
			total++;
		}
		Fori(it,mp)
			cout << it-> first << " " << 100.0*it->second/total << endl;
		if(i != n-1)
			cout << endl;
	}
}