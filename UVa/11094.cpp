/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

int m,n;

string grid[25];
const int dx[] = {1,-1,0,0};
const int dy[] = {0,0,1,-1};
char l,w;
int flood(int x, int y)
{
	grid[x][y] = w;
	int res = 1;
	For(i,0,4)
	{
		int nx = x + dx[i];
		int ny = (y + dy[i] + n) % n;
		if(nx >= 0 && nx < m && grid[nx][ny] == l)
		{
			res += flood(nx,ny);
		}
	}
	return res;
}

int main()
{
	while(cin >> m >> n)
	{
		int res = 0;
		For(i,0,m)
			cin >> grid[i];
		int x,y;
		cin >> x >> y;
		l = grid[x][y];
		bool flag = false;
		For(i,0,m) { For(j,0,n) if(grid[i][j] != l) {w = grid[i][j]; flag = true; break;} if(flag) break;}
		if(!flag)
			w = l+1;
		flood(x,y);
		For(i,0,m)
			For(j,0,n)
				if(grid[i][j] == l)
					res = max(res,flood(i,j));
		cout << res << endl;
	}
}