#include<iostream>
#include<vector>
#include<queue>
#include<math.h>
#include<algorithm>
#include<map>
#include<unordered_map>
#include<set>
#include<unordered_set>
#include<string>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

struct Node
{
	bool visited;
	vector<int> adj;
	int distance;
};

void BFS(int start, vector<Node> &graph)
{
	queue<int> q;
	q.push(start);
	while (!q.empty())
	{
		int top = q.front();
		q.pop();
		graph[top].visited = true;
		For(i, 0, graph[top].adj.size())
		{
			if (!graph[graph[top].adj[i]].visited)
			{
				q.push(graph[top].adj[i]);
				graph[graph[top].adj[i]].visited = true;
				graph[graph[top].adj[i]].distance = graph[top].distance + 1;
			}
		}
	}
}

ii findBOOM(vector<Node> &graph,int n)
{
	int* a = new int[n+5];
	For(i, 0, graph.size())
		a[i] = 0;
	For(i, 0, graph.size())
	{
		a[graph[i].distance]++;
	}
	int maxval = 0, maxindex;
	For(i, 1, n + 5)
	{
		if (a[i] > maxval)
		{
			maxval = a[i];
			maxindex = i;
		}
	}
	return make_pair(maxval, maxindex);
}

int main()
{
	int n;
	cin >> n;
	vector<Node> graph(n);

	For(i, 0, n)
	{
		int m;
		cin >> m;
		graph[i].adj.assign(m, 0);
		For(j, 0, m)
		{
			cin >> graph[i].adj[j];
		}
	}

	int t;
	cin >> t;
	For(i, 0, t)
	{
		For(j, 0, graph.size())
		{
			graph[j].distance = 0;
			graph[j].visited = false;
		}
		int src;
		cin >> src;
		if (graph[src].adj.size() == 0)
		{
			cout << 0 << endl;
			continue;
		}
		BFS(src,graph);
		ii p = findBOOM(graph,n);
		cout << p.first << " " << p.second << endl;
	}
}