/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <list>
#include <vector>
#include <queue>
#include <deque>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <math.h>
#include <algorithm>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

char grid[105][105];

int di[] = {-1,-1,-1,0,0,1,1,1};
int dj[] = {-1,0,1,1,-1,1,0,-1};

int main()
{
	int n,m;
	int t = 0;
	while(cin >> n >> m && n)
	{
		if(t)
			cout << endl;
		For(i,0,n)
			For(j,0,m)
				grid[i][j] = '0';
		For(i,0,n)
			For(j,0,m)
			{
				char c;
				cin >> c;
				if(c == '*')
				{
					grid[i][j] = c;
					For(k,0,8)
					{
						int ni = i + di[k];
						int nj = j + dj[k];
						if(ni >= 0 && ni < n && nj >= 0 && nj < m && grid[ni][nj] != '*')
							grid[ni][nj]++;
					}
				}
			}
		cout << "Field #" << ++t << ":" << endl;
		For(i,0,n)
		{
			For(j,0,m)
				cout << grid[i][j];
			cout << endl;
		}
	}
}