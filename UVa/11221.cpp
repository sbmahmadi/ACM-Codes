#include <bits/stdc++.h>

using namespace std;

bool ispal(const string& s)
{
	for(int i = 0; i < s.size(); i++)
	{
		if(s[i] != s[s.size() - 1 - i])
			return false;
	}
	return true;
}

int main()
{
	int t;
	cin >> t;
	cin.ignore();
	for(int tt = 0; tt < t; tt++)
	{
		cout << "Case #" << tt+1 << ":" << endl;
		string line;
		getline(cin,line);
		string s;
		for(int i = 0 ; i< line.size(); i++)
			if(isalpha(line[i]))
				s.push_back(line[i]);
		int sz; 
		if(sqrt(s.size()) == (sz = int(sqrt(s.size()))))
		{
			string s1 = "";
			for(int i = 0; i < sz; i++)
			{
				for(int j = 0; j < s.size(); j += sz)
				{
					s1 += s[i+j];
				}
			}
			if(ispal(s1) && ispal(s))
			{
				cout << sz << endl;
				continue;
			}
		}
		cout << "No magic :(" << endl;
	}
}