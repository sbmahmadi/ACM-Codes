#include<iostream>

using namespace std;

int main()
{
	int t, n;
	cin >> t;

	for (int i = 0; i < t; i++)
	{
		cin >> n;
		int High = 0, Low = 0, now, next;
		cin >> now;
		for (int i = 1; i < n; i++)
		{
			cin >> next;
			if (next > now) High++;
			else if (next < now) Low++;
			now = next;
		}
		printf("Case %d: %d %d\n", i+1, High, Low);
	}
}