/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

const int INF = 1e7;

int graph[2][26][26];

int main()
{
	int n;
	while(cin >> n && n)
	{
		For(i,0,26)
			For(j,0,26)
				graph[0][i][j] = graph[1][i][j] = INF;

		For(i,0,n)
		{
			char t,d,x,y;
			int w;
			cin >> t >> d >> x >> y >> w; if(t == 'M') t = 'Z';
			graph[t - 'Y'][x - 'A'][y - 'A'] = w;
			if(d == 'B')
				graph[t - 'Y'][y - 'A'][x - 'A'] = w;
		}
		For(j,0,26) graph[0][j][j] = graph[1][j][j] = 0;
		For(k,0,26)
			For(i,0,26)
				For(j,0,26)
					graph[0][i][j] = min(graph[0][i][j], graph[0][i][k] + graph[0][k][j]), 
					graph[1][i][j] = min(graph[1][i][j], graph[1][i][k] + graph[1][k][j]);

		char s1,s2;
		cin >> s1 >> s2;
		int res = INF;
		vi v;
		For(i,0,26)
			if(graph[0][s1-'A'][i] + graph[1][s2-'A'][i] < res)
			{
				res = graph[0][s1-'A'][i] + graph[1][s2-'A'][i];
				v.clear();
				v.push_back(i);
			}
			else if(graph[0][s1-'A'][i] + graph[1][s2-'A'][i] == res)
				v.push_back(i);

		if(res == INF)
			cout << "You will never meet." << endl;
		else 
		{
			cout << res;
			sort(allof(v));
			Fori(it,v)
			{
				cout << " " << char(*it + 'A');
			}
			cout << endl;
		}
	}
}