#include<iostream>
#include<vector>

using namespace std;

class Node
{
public:
	bool visited;
	vector<int> adjacent;
	Node() :visited(false){}
};

void tour(int index, vector<Node> &graph)
{
	for (int a : graph[index].adjacent)
	{
		if (!graph[a].visited)
		{
			graph[a].visited = true;
			tour(a, graph);
		}
	}
}

int main()
{
	int n;
	while (cin >> n && n)
	{
		vector<Node> graph;
		graph.assign(n, Node());

		//input edges
		int a, b;

		while (cin >> a && a)
			while (cin >> b && b)
				graph[a - 1].adjacent.push_back(b - 1);
			
		int r,start;
		cin >> r;
		for (int i = 0; i < r; i++)
		{
			cin >> start;
			tour(start - 1, graph);
			vector<int> unvisited;
			for (int j = 0; j < n; j++)
				if (!graph[j].visited) unvisited.push_back(j + 1);
			
			cout <<unvisited.size();
			for (auto it = begin(unvisited); it != end(unvisited); it++)
				cout << " " << *it;
			
			cout << endl;
			for (int j = 0; j < n; j++)
				graph[j].visited = false;
		}
	}
}