/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

int dp[210][30][210];
int ns[210];
int nn[210];
int d,n;

int knapsack(int id, int rem, int m)
{
	if (m == 0 && rem == 0) return 1;
	if (id == n) return 0;
	int &res = dp[id][rem][m];
	if(res >=0) return res;
	return res = knapsack(id+1, (rem + ns[id]) % d, m-1) + knapsack(id+1, rem, m);
}

int main()
{
	ios_base::sync_with_stdio(0);
	cin.tie(0); cout << fixed;
	int q;
	int t = 0;
	while(cin >> n >> q && q)
	{
		cout << "SET " << ++t << ":" << endl;
		For(i,0,n)
			cin >> nn[i];

		For(qq,0,q)
		{
			int m;
			cin >> d >> m;
			For(i,0,n)
			{
				ns[i] = nn[i];
				ns[i] = (ns[i] % d + d) % d;
			}
			memset(dp, -1, sizeof dp);
			cout << "QUERY " << qq+1 << ": " << knapsack(0,0,m) << endl;
		}
	}
}