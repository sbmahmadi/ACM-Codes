/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

ll grid[110][110];

ll calc(int a, int b, int c, int d)
{
	return grid[a][b] - grid[a][d] - grid[c][b] + grid[c][d]; 
}

int main()
{
	ios_base::sync_with_stdio(0);
	cin.tie(0);
	int t;
	cin >> t;
	For(tt,0,t)
	{
		int n,m,k;
		cin >> n >> m >> k;
		For(i,1,n+1)
			For(j,1,m+1)
				cin >> grid[i][j] , grid[i][j] += grid[i][j-1] + grid[i-1][j] - grid[i-1][j-1];
		int mxsize = 0, mncost = 0;
		For(i,1,n+1)
			For(j,1,m+1)
				roF(x,i-1,0)
				{
					if(calc(i,j,x,j-1) > k)
						break;
					roF(y,j-1,0)
					{
						if(calc(i,j,x,y) > k)
							break;
						if((i-x)*(j-y) > mxsize || (i-x)*(j-y) == mxsize && calc(i,j,x,y) < mncost)
							mncost = calc(i,j,x,y), mxsize = (i-x)*(j-y);
					}
				}
		cout << "Case #" << tt+1 << ": " << mxsize << " " << mncost << endl;
	}
}