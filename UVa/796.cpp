/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <list>
#include <vector>
#include <queue>
#include <deque>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <math.h>
#include <algorithm>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

vector<ii> edges;
int dfscount;
void dfs(int u, int p, vector<vi>& graph, vi& visit, vi& low)
{
	visit[u] = low[u] = dfscount++;
	For(i,0,graph[u].size())
	{
		int v = graph[u][i];
		if(!visit[v])
		{
			dfs(v,u,graph,visit,low);
			low[u] = min(low[u], low[v]);
			if(low[v] == visit[v])
				edges.push_back({min(u,v), max(u,v)});
		}
		else if(v != p)
			low[u] = min(low[u], visit[v]);
	}
}

int main()
{
	int n;
	while(cin >> n)
	{
		dfscount = 1;
		vector<vi> graph(n);
		For(i,0,n)
		{
			int u,v,m;
			cin >> u;
			cin.ignore(2);
			cin >> m;
			cin.ignore();
			For(j,0,m)
			{
				cin >> v;
				graph[u].push_back(v);
				graph[v].push_back(u);
			}
		}
		vi visit(n,0), low(n,0);
		edges.clear();
		For(i,0,n)
			if(!visit[i])
				dfs(i,i,graph,visit,low);
		sort(allof(edges));
		cout << edges.size() << " critical links" << endl;
		For(i,0,edges.size())
			cout << edges[i].first << " - " << edges[i].second << endl;
		cout << endl;
	}
}