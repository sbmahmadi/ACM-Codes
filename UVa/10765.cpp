/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <list>
#include <vector>
#include <queue>
#include <deque>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <math.h>
#include <algorithm>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

struct res
{
	int val,n;
	res(int valval, int nn): val(valval), n(nn){}
	bool operator<(res r) const
	{
		if(r.val != val)
			return val > r.val;
		return n < r.n;
	}
};

int dfscount, rootchild;
vector<res> result;

void dfs(int u, int p, vector<vi>& graph, vi& visit, vi& low)
{
	visit[u] = low[u] = dfscount++;
	int comp = 0;
	For(i,0,graph[u].size())
	{
		int v = graph[u][i];
		if(!visit[v])
		{
			if(u == 0)
				rootchild++;
			dfs(v,u,graph,visit,low);
			low[u] = min(low[u],low[v]);
			if(low[v] >= visit[u])
				comp++;
		}
		else if(v != p)
			low[u] = min(low[u], visit[v]);
	}
	if(u != 0)
		result.push_back({comp+1,u});
}

int main()
{
	ios_base::sync_with_stdio(0);
	cin.tie(0);
	int n,m;
	while(cin >> n >> m && n)
	{
		int u,v;
		vector<vi> graph(n);
		result.clear();
		while(cin >> u >> v && u != -1)
		{
			graph[u].push_back(v);
			graph[v].push_back(u);
		}
		
		vi visit(n,0), low(n,0);
		dfscount = 1, rootchild = 0;
		dfs(0,0,graph,visit,low);
		
		result.push_back({rootchild,0});
		
		sort(allof(result));
		For(i,0,m)
			cout << result[i].n << " " << result[i].val << endl;
		cout << endl;
	}
}