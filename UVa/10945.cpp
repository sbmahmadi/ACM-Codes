#include <bits/stdc++.h>

using namespace std;

string res[2] = {"You won't be eaten!", "Uh oh.."};

int ispal(string& s)
{
	for(int i = 0; i < s.size()/2; i++)
	{
		if(s[i] != s[s.size()-1-i])
			return 1;
	}
	return 0;
}

int main()
{
	string line;
	while(getline(cin,line) && line != "DONE")
	{
		string s = "";
		for(int i = 0; i < line.size(); i++)
		{
			if(isalpha(line[i]))
				s.push_back(tolower(line[i]));
		}
		cout << res[ispal(s)] << endl;
	}
}