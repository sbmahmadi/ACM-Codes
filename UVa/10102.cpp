/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int main()
{
	/*ifstream fin("file.in");
	ofstream fout("file.out");*/
	int m;
	while(cin >> m)
	{
		int** grid = new int*[m];
		For(i,0,m)
			grid[i] = new int[m];
		For(i,0,m)
			For(j,0,m)
			{
				char c;
				cin >> c;
				grid[i][j] = c - '0';
			}
			int mx = 0;
		For(i,0,m)
			For(j,0,m)
			{
				if(grid[i][j] != 1) continue;
				int mn = 3*m;
				For(k,0,m)
					For(p,0,m)
					{
						if(grid[k][p] != 3) continue;
						int d = abs(i-k) + abs(j-p);
						mn = min(mn,d);
					}
				mx = max(mx,mn);
			}
		cout << mx << endl;
	}
}
