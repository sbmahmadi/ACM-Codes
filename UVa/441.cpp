/*
ID: behdad.1
LANG: C++11
PROB:
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int main()
{
	/*ifstream fin("file.in");
	ofstream fout("file.out");*/
	int k;
	int count = 0;
	while (cin >> k && k)
	{
		if (count++ > 0)
			cout << endl;
		vi st(k);
		For(i, 0, k)
			cin >> st[i];
		For(a, 0, st.size())
		{
			For(b, a + 1, st.size())
			{
				For(c, b + 1, st.size())
				{
					For(d, c + 1, st.size())
					{
						For(e, d + 1, st.size())
						{
							For(f, e + 1, st.size())
							{
								printf("%d %d %d %d %d %d\n", st[a], st[b], st[c], st[d], st[e], st[f]);
							}
						}
					}
				}
			}
		}
		
	}
}
