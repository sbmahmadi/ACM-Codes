/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

vector<vi> all;

bool valid(int row,int col,vi& queens)
{
	For(i,0, queens.size())
	{
		if(queens[i] == col || abs(row - i) == abs(col - queens[i]))
			return false;
	}
	return true;
}

void solve(int row, vi& queens)
{
	if(row == 8)
	{
		all.push_back(queens);
		return;
	}	
	For(i, 0, 8)
	{
		if(valid(row, i, queens))
		{
			queens.push_back(i);
			solve(row+1,queens);
			queens.pop_back();
		}
	}
}

int main()
{
	vi queens;
	solve(0, queens);	
	int a;
	int test = 1;
	while(cin >> a)
	{

		vi input(8);
		input[0] = a;
		For(i,1,8)
			cin >> input[i];
		For(i,0,8)
			input[i]--;
		int mn = 10;
		For(i,0,all.size())
		{
			int dif = 0;
			For(j,0,8)
				if(all[i][j] != input[j])
					dif++;
			mn = min(mn, dif);
		}
		cout << "Case " << test++ << ": " << mn << endl;
	}
}
