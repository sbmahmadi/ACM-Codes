/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

set<int> st;
vector<vi> graph;

bool dfs(int u, int v, vector<bool> &visited)
{
	if(u == v) return true;
	visited[u] = true;
	For(i,0,graph[u].size())
		if(!visited[graph[u][i]] && dfs(graph[u][i], v, visited))
			return true;
	return false;
}

bool c(int a)
{
	Fori(it, st)
		if(dfs(*it, a, *(new vector<bool>(250,0))))
			return true;
	return false;
}

int main()
{
	int n;
	int t = 0;
	while(cin >> n)
	{
		graph.clear();
		graph.resize(n);
		vi w(n);
		For(i,0,n)
			cin >> w[i];
		int r;
		cin >> r;
		For(i,0,r)
		{
			int a,b;
			cin >> a >> b;
			graph[a-1].push_back(b-1);
		}
		
		vi dist(n,1e7);
		if(n>0) 
			dist[0] = 0;

		For(i,0,n-1)
			For(j,0,n)
				if(dist[j] < 1e7)
				For(k,0,graph[j].size())
					dist[graph[j][k]] = min(dist[graph[j][k]], dist[j] + (int)pow(w[graph[j][k]] - w[j], 3));

		st.clear();
		For(i,0,n)
			For(j,0,graph[i].size())
			{
				if(dist[graph[i][j]] > dist[i] + (int)pow(w[graph[i][j]] - w[i],3))
				{
					//trace("Cycle" _ i _ graph[i][j])
					st.insert(i);
				}
			}

		fout << "Set #" << ++t << endl;
		int q;
		cin >> q;
		For(i,0,q)
		{
			int a;
			cin >> a;
			fout << (dist[a-1] < 3 || c(a-1) || dist[a-1] == 1e7 ? "?" : to_string(dist[a-1])) << endl;
		}
	}
}
