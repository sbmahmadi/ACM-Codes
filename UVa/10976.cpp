/*
ID: behdad.1
LANG: C++11
PROB:
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int main()
{
	/*ifstream fin("file.in");
	ofstream fout("file.out");*/
	int z;
	while (cin >> z)
	{
		vector<string> res;
		For(y, z + 1, 2 * z + 1)
		{
			double x = 1.0 * y*z / (y - z);
			if (x == (int)x)
			{
				string s = "";
				s += "1/" + to_string(z) + " = 1/" + to_string((int)x) + " + 1/" + to_string(y);
				res.push_back(s);
			}
		}
		cout << res.size() << endl;
		For(i, 0, res.size())
			cout << res[i] << endl;
	}
}
