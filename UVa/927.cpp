/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int main()
{
	/*ifstream fin("file.in");
	ofstream fout("file.out");*/
	int t;
	cin >> t;

	while(t--)
	{
		
		vi coef;
		int a;
		int cnt;
		cin >> cnt; cnt++;
		while (cnt--)
		{
			cin >> a;
			coef.push_back(a);
		}
			
		int d,k;
		cin >> d >> k;

		ll aa = 0;
		int i = 1;
		while(aa<k)
		{
			aa += i*d;
			i++;
		}
		i--;
		ll res = 0;
		int n = i;
		For(i,0,coef.size())
		{
			res += (ll)pow(n,i) * (ll)coef[i];
		}
		cout << res << endl;

	}
}
