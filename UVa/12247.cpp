#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a ; i < b ; i++)

using namespace std;

int p1[3], p2[2];

int f(int a)
{
	for(int i = a; i <= 52; i++)
	{
		if(find(p1,p1+3,i) == p1+3 && find(p2,p2+2,i) == p2+2)
			return i;
	}
	return -1;
}

int main()
{
	while(cin >> p1[0] >> p1[1] >> p1[2] >> p2[0] >> p2[1] && p1[0])
	{
		sort(p1, p1 + 3);
		sort(p2, p2 + 2);
		int ind = -1;
		if(p2[0] > p1[2])
			cout << f(1) << endl;
		else if(p2[0] > p1[1])
			cout << f(p1[1]) << endl;
		else if(p2[1] > p1[2])
			cout << f(p1[2]) << endl;
		else
			cout << -1 << endl;
	}
}