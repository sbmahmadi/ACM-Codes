/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <list>
#include <vector>
#include <queue>
#include <deque>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <math.h>
#include <algorithm>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<double, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

int main()
{
	int t;
	cin >> t;
	For(tt,0,t)
	{
		int n,r;
		cin >> n >> r;
		set<ii> pq;
		vector<ii> points(n);
		vi visit(n,0);
		For(i,0,n)
			cin >> points[i].first >> points[i].second;
		int states = 1;
		long double trail = 0,troad = 0;
		
		visit[0] = true;
		For(i,1,n)
			pq.insert({hypot(points[0].first - points[i].first, points[0].second - points[i].second), i});
		For(i,0,n-1)
		{
			while(visit[pq.begin()->second])
				pq.erase(pq.begin());
			ii u = *pq.begin();
			if(u.first >= r)
			{
				states++;
				trail += u.first;
			}
			else 
				troad += u.first;
			visit[u.second] = true;
			For(j,0,n)
				if(!visit[j])
					pq.insert({hypot(points[u.second].first - points[j].first, points[u.second].second - points[j].second), j});
		}
		fout << "Case #" << tt+1 << ": " << states << " " << int(troad+.5) << " " <<  int(trail+.5) << endl;
	}
}