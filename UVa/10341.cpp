/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

int p,q,r,s,t,u;

double calc(double x)
{
	return p * exp(-x) + q * sin(x) + r * cos(x) + s * tan(x) + t * x * x + u; 
}

int main()
{
	
	cout << fixed << setprecision(4);
	while(cin >> p >> q >> r >> s >> t >> u)
	{
		if(calc(0) * calc(1) > 0)
		{
			cout << "No solution" << endl;
			continue;
		}
		double low = 0,up = 1,mid;
		For(i,0,25)
		{
			mid = (low + up)/2;
			double res = calc(mid);
			if(res >= 0)
				low = mid;
			else 
				up = mid;
		}
		cout << mid << endl;
	}
}