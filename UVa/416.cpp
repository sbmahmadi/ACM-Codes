/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

/*ifstream fin("file.in");
ofstream fout("file.out");*/

const bool seg[10][8] = {{1,1,1,1,1,1,0}, {0,1,1,0,0,0,0}, {1,1,0,1,1,0,1}, {1,1,1,1,0,0,1}, {0,1,1,0,0,1,1}, {1,0,1,1,0,1,1}, {1,0,1,1,1,1,1}, {1,1,1,0,0,0,0}, {1,1,1,1,1,1,1}, {1,1,1,1,0,1,1}};

bool isSeq(int i, int j, bool burn[8], vector<bool[8]>& p)
{
	if(j == -1)
		return true;
	//compare seg[i] & p[j]
	For(k,0,8)
	{
		if(p[j][k] == 1 && seg[i][k] == 0)
			return false;
		if(p[j][k] == 1 && burn[k] == 1)
			return false;
	}
	For(k,0,8)
	{
		if(seg[i][k] == 1 && p[j][k] == 0)
			burn[k] = 1;
	}
	return isSeq(i-1,j-1,burn,p);
}

bool solve(vector<bool[8]>& p)
{
	For(i,p.size()-1,10)
	{
		bool burn[8] = {};
		if(isSeq(i,p.size() - 1,burn,p))
			return true;
	}
	return false;
} 

int main()
{	
	int n;
	while(cin >> n && n)
	{
		vector<bool[8]> p(n);
		For(i,0,n)
		{
			string s;
			cin >> s;
			For(j,0,8)
			{
				p[n-1-i][j] = (s[j] == 'Y');
			}
		}
		cout << (solve(p) ? "MATCH" : "MISMATCH") << endl; 
	}
}