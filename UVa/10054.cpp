/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0);
#define trace(x) cerr << #x << ": " << x << endl;
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl;
#define _ << " :: " <<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

vector<vi> graph;
list<ii> res;

void euler(list<ii>::iterator it, int u)
{
	For(i,0,graph[u].size())
	{
		int v = graph[u][i];
		graph[u].erase(find(allof(graph[u]),v));
		graph[v].erase(find(allof(graph[v]),u));
		euler(res.insert(it,{v,u}),v);
	}
}

int main()
{
	fastio();
	int t;
	cin >> t;
	For(tt,0,t)
	{
		int n;
		cin >> n;
		graph.assign(50,vi());
		res.clear();
		int c;
		For(i,0,n)
		{
			int a,b;
			cin >> a >> b; a--; b--;
			graph[a].push_back(b);
			graph[b].push_back(a);
			c = a;
		}
		bool ok = true;
		For(i,0,50)
			ok &= 1 - graph[i].size() % 2;
		cout << "Case #" << tt+1 << endl;
		if(!ok)
			cout << "some beads may be lost" << endl;
		else
		{
			list<ii> res;
			euler(res.begin(), c);
			for(auto& it : res)
				cout << it.first+1 << " " << it.second+1 << endl;
		}
		if(tt != t-1)
			cout << endl;
	}
}