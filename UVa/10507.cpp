/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0);
#define trace(x) cerr << #x << ": " << x << endl;
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl;
#define _ << " :: " <<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

int main()
{
	int n,m;
	while(cin >> n >> m)
	{
		vi v(26,-1);
		vector<vi> graph(26);
		char c1,c2,c3;
		cin >> c1 >> c2 >> c3;
		For(i,0,m)
		{
			char c1,c2;
			cin >> c1 >> c2;
			v[c1 - 'A'] = v[c2 - 'A'] = 0;
			graph[c1-'A'].push_back(c2-'A');
			graph[c2-'A'].push_back(c1-'A');
		}
		v[c1 - 'A'] = v[c2 - 'A'] = v[c3 - 'A'] = 1;
		n -= 3;
		int res = 0;
		while(n)
		{
			vi nw;
			For(i,0,graph.size())
			{
				if(v[i] != 0)
					continue;
				int cnt = 0;
				For(j,0,graph[i].size())
					if(v[graph[i][j]] == 1)
						cnt++;
				if(cnt >= 3)
					nw.push_back(i);
			}
			if(nw.size() == 0)
			{
				res = -1;
				break;
			}
			For(i,0,nw.size())
				nw[i] = 1;
			n -= nw.size();
			res++;
		}
		if(res == -1)
			cout << "THIS BRAIN NEVER WAKES UP" << endl;
		else
			cout << "WAKE UP IN, " << res << ", YEARS"  << endl;
	}	
}