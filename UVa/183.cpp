/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

int n,m;

string to_D(int x1, int y1, int x2, int y2, vector<vector<char>>& grid)
{
	if(x1 == x2 || y1 == y2) 
		return "";
	bool isD = false;
	For(i,x1,x2)
		For(j,y1,y2)
			if(grid[i][j] != grid[x1][y1])
				isD = true;

	int temp = 0;
	if(isD)
	{
		int nx = ceil((x1+x2)/2.0) , ny = ceil((y1+y2)/2.0);
		return ("D" + to_D(x1,y1,nx,ny,grid) + to_D(x1,ny, nx, y2,grid) + to_D(nx,y1, x2, ny,grid) + to_D(nx,ny, x2, y2,grid));
	}
	else return to_string(grid[x1][y1]-'0');
}

void to_B(string s,int& index, int x1, int y1, int x2, int y2, vector<vector<char>>& grid)
{
	if (x1 == x2 || y1 == y2) 
		return;
	if (s[index] != 'D')
	{
		For(i, x1, x2)
			For(j, y1, y2)
				grid[i][j] = s[index];
		index++;
	}
	else
	{
		index++;
		int nx = ceil((x1 + x2) / 2.0), ny = ceil((y1 + y2) / 2.0);
		to_B(s,index, x1, y1, nx, ny, grid);
		to_B(s,index, x1, ny, nx, y2, grid);
		to_B(s,index, nx, y1, x2, ny, grid);
		to_B(s,index, nx, ny, x2, y2, grid);
	}
}

int main()
{
	char type;
	
	while(cin >> type && type != '#')
	{
		cin >> n >> m;
		if(type == 'B')
		{
			vector<vector<char>> grid(n);
			For(i,0,n)
			{
				grid[i].resize(m);
				For(j,0,m)
					cin >> grid[i][j];
			}
			cout << 'D' << setw(4) << right << n << setw(4) << m << endl;
			string res = to_D(0,0,n,m,grid);
			For(i,0,res.size())
			{
				if(i%50 == 0 && i >0)
				{
					cout << endl;
				}
				cout << res[i];
			}
			cout << endl;
		}
		else
		{
			string s;
			cin >> s;
			vector<vector<char>> grid(n);
			For(i,0,n)
				grid[i].resize(m);
			int ind = 0;
			to_B(s,ind,0,0,n,m,grid);
			cout << 'B' << setw(4) << right << n << setw(4) << m << endl;
			int ctr = 0;
			For(i,0,n)
				For(j,0,m)
				{
					if ((i*m + j) % 50 == 0 && i+j > 0)
					{
						cout << endl;
					}
					cout << grid[i][j];
				}
			cout << endl;
		}
	}
}