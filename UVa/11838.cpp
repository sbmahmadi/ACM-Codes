/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <list>
#include <vector>
#include <queue>
#include <deque>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <math.h>
#include <algorithm>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

vector<vi> graph1,graph2;

void dfs1(int u, vi& visited, vi& stack)
{
	visited[u] = true;
	For(i,0,graph1[u].size())
	{
		if(!visited[graph1[u][i]])
			dfs1(graph1[u][i], visited, stack);
	}
	stack.push_back(u);
}

void dfs2(int u, vi& visited)
{
	visited[u] = true;
	For(i,0,graph2[u].size())
		if(!visited[graph2[u][i]])
			dfs2(graph2[u][i],visited);
}

bool isSCC()
{
	int n = graph1.size();
	vi stack,visited(n,0);
	For(i,0,n)
		if(!visited[i])
			dfs1(i,visited,stack);
	int res = 0;
	visited = vi(n,0);
	while(!stack.empty())
	{
		int u = stack.back(); stack.pop_back();
		if(!visited[u])
		{
			res++;
			dfs2(u,visited);
		}
	}
	return (res == 1);
}

int main()
{
	int n,m;
	while(cin >> n >> m && n)
	{
		graph1 = vector<vi>(n);
		graph2 = vector<vi>(n);
		For(i,0,m)
		{
			int u,v,p;
			cin >> u >> v >> p;
			graph1[u-1].push_back(v-1);
			graph2[v-1].push_back(u-1);
			if(p == 2)
				graph1[v-1].push_back(u-1),
				graph2[u-1].push_back(v-1);
		}
		cout << (isSCC()) << endl;
	}
}