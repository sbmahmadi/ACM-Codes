/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0);
#define trace(x) cerr << #x << ": " << x << endl;
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl;
#define _ << " :: " <<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

int dp[26];

int f(int u, vector<vi>& graph, vi& time)
{
	int& res = dp[u];
	if(res)
		return res;
	For(i,0,graph[u].size())
	{
		res = max(res,f(graph[u][i],graph,time));
	}
	return res += time[u];
}

int main()
{
	fastio()
	int t;
	cin >> t;
	cin.ignore(2);
	while(t--)
	{
		vector<vi> graph(26);
		memset(dp,0,sizeof dp);
		vi time(26,0);
		string line;

		while(getline(cin,line) && line != "")
		{
			stringstream ss(line);
			char c;
			ss >> c;
			ss >> time[c-'A'];	
			char u;
			while(ss >> u)
				graph[u-'A'].push_back(c-'A');
		}
		int mx = 0;
		For(i,0,26)
			mx = max(mx,f(i,graph,time));
		cout << mx << endl;
		if(t)
			cout << endl;
	}
}