/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

vector<string> rot(vector<string> v, int i)
{
	For(ii,0,i)
	{
		vector<string> v1(v);
		For(j,0,v.size())
			For(k,0,v.size())
				v1[j][k] = v[v.size()-1-k][j];
		v = v1;
	}
	return v;
}


int check(const vector<string> &v1, const vector<string> &v2)
{
	int res = 0;
	For(i,0,v1.size() - v2.size() + 1)
	{
		For(j,0,v1.size() - v2.size() + 1)
		{
			bool is = true;
			For(ii,0,v2.size())
				For(jj,0,v2.size())
					if(v2[ii][jj] != v1[i + ii][j + jj])
						is = false;
			if(is)
				res++;
		}	
	}
	return res;
}

int main()
{
	int N,n;
	while(cin >> N >> n && N)
	{
		vector<string> VS(N),vs(n);
		For(i,0,N)
			cin >> VS[i];
		For(i,0,n)
			cin >> vs[i];
		
		cout << check(VS,rot(vs,0)) << " " << check(VS,rot(vs,1)) << " " << check(VS,rot(vs,2)) << " " << check(VS,rot(vs,3)) << endl;
	}
}