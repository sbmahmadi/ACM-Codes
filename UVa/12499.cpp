#include <iostream>

using namespace std;

int main()
{
	int t;
	cin >> t;
	for(int i = 1; i <= t; i++)
	{
		int n,l;
		cin >> n >> l;
		int a = 0,b = 0,c;
		cin >> a;
		for(int j = 0; j < n-1;j++)
		{
			if(j == 0)
				cin >> b;
			else 
				cin >> c;
		}
		cout << "Case " << i << ": ";
		if((n%2 == 0 && a == b || n%2 == 1 && a != b) && n > 1)
			cout << "Second Player" << endl;
		else cout << "First Player" << endl;

	}
}