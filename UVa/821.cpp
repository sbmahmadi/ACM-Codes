/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

int n = 100;
int graph[100][100];

int main()
{
	cout << fixed << setprecision(3);
	int t = 0;
	while(++t)
	{
		int a,b;
		For(i,0,n)
			For(j,0,n)
				graph[i][j] = 1000, graph[j][j] = 0;
		bool l = true;
		while(cin >> a >> b && a)
		{
			l = false;
			graph[a-1][b-1] = 1;
		}
		if(l) 
			break;
		For(k,0,n)
			For(i,0,n)
				For(j,0,n)
					graph[i][j] = min(graph[i][j], graph[i][k] + graph[k][j]);
		double res = 0;
		int c = 0;
		For(i,0,n)
			For(j,0,n)
				if(graph[i][j] != 0 && graph[i][j] != 1000)
					res += graph[i][j], c++;
		cout << "Case " << t << ": average length between pages = " << res/c << " clicks" << endl;
	}
}