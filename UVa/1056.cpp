#include<iostream>
#include<vector>
#include<map>
#include<string>
#include<queue>
#include<algorithm>

#define For(i,a,b) for(int i = a;i<b;i++)

using namespace std;

map<string,int> ID;

int getID(string s)
{
	return ID.insert(make_pair(s,ID.size())).first->second;
}

int graph[50][50];

int main()
{
	int n, r;
	int k = 0;
	
	while (cin >> n >> r && n)
	{
		ID.clear();
		For(i,0,n) For(j,0,n) graph[i][j] = 1000, graph[j][j] = 0;
		string s1, s2;
		For(i, 0, r)
		{
			cin >> s1 >> s2;
			graph[getID(s1)][getID(s2)] = graph[getID(s2)][getID(s1)] = 1;
		}

		For(k,0,n)
			For(i,0,n)
				For(j,0,n)
					if(graph[i][j] > graph[i][k] + graph[k][j])
						graph[i][j] = graph[i][k] + graph[k][j];
		int mx = 0;
		For(i,0,n)
			For(j,0,n)
				mx = max(mx,graph[i][j]);

		cout << "Network " << ++k << ": ";
		if(mx == 1000) cout << "DISCONNECTED" << endl << endl;
		else cout << mx << endl << endl;

	}
}