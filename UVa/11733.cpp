#include<iostream>
#include<queue>

#define priorityQ priority_queue<MSTEdge,vector<MSTEdge>,less<MSTEdge>>

using namespace std;

class MSTEdge
{
public:
	int destIndex;
	int weight;
	bool operator<(const MSTEdge e) const
	{
		return (e.weight < weight);
	}
	MSTEdge(int d, int w) : destIndex(d), weight(w){}
};

class MSTNode
{
public:
	bool visited;
	vector<MSTEdge> Adjacent;
	MSTNode() : visited(false){}
};

void Visit(int index, priorityQ &pq, vector<MSTNode> &graph)
{
	graph[index].visited = true;
	for (MSTEdge e : graph[index].Adjacent)
	{
		if (!graph[e.destIndex].visited) pq.push(e);
	}
}


int main()
{
	int t;
	cin >> t;
	for (int i = 0; i < t; i++)
	{
		priorityQ pq;
		vector<MSTNode> graph;
		int n, m, AirportCost, AirportNum = 0, visited = 0;
		cin >> n >> m >> AirportCost;

		long long costs = 0;
		graph.assign(n, MSTNode());
		for (int j = 0; j < m; j++)
		{
			int a, b, w;
			cin >> a >> b >> w;
			graph[a-1].Adjacent.push_back(MSTEdge(b-1, w));
			graph[b-1].Adjacent.push_back(MSTEdge(a-1, w));
		}
		
		while (visited < n)
		{
			AirportNum++;
			for (int i = 0; i < n; i++) 
				if (!graph[i].visited)
				{
					Visit(i, pq, graph);
					visited++;
					break;
				}

			while (!pq.empty())
			{
				MSTEdge e = pq.top();
				pq.pop();
				if (!graph[e.destIndex].visited)
				{
					Visit(e.destIndex, pq, graph);
					visited++;

					if (e.weight >= AirportCost)
						AirportNum++;
					else
					{
						costs += e.weight;
					}
				}
			}
		}
		costs += AirportNum*AirportCost;

		printf("Case #%d: %lld %d\n", i + 1, costs, AirportNum);
	}
}