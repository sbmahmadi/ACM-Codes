/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

int main()
{
	string s;
	priority_queue<ii,vector<ii>,greater<ii>> pq;
	while(getline(cin,s) && s != "#")
	{
		stringstream ss(s);
		string t;
		int a,b;
		ss >> t >> a >> b;
		trace(a _ b)
		For(i,0,10000)
			pq.push({b*(i+1),a});
	}	
	int k;
	cin >> k;
	For(i,0,k)
	{
		ii tmp = pq.top();
		pq.pop();
		cout << tmp.second << endl;
	}
}