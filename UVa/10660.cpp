/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int main()
{
	/*ifstream fin("file.in");
	ofstream fout("file.out");*/
	int t;
	cin >> t;
	while(t--)
	{
		int grid[5][5] = {};
		int n;
		cin >> n;
		For(i,0,n)
		{
			int a,b,c;
			cin >> a >> b >> c;
			grid[a][b] = c;
		}
		int mndist = 1<<28;
		int res[5];
		For(i1,0,25)
		{
			For(i2,i1 + 1,25)
			{
				For(i3,i2 +1,25)
				{
					For(i4,i3+1,25)
					{
						For(i5,i4+1,25)
						{
							int sum = 0;
							For(x,0,25)
							{
								int pop = grid[x/5][x%5];
								int mn = min({(abs(i1/5 - x/5) + abs(i1%5 - x%5)) * pop,
									(abs(i2/5 - x/5) + abs(i2%5 - x%5)) * pop,
									(abs(i3/5 - x/5) + abs(i3%5 - x%5)) * pop,
									(abs(i4/5 - x/5) + abs(i4%5 - x%5)) * pop,
									(abs(i5/5 - x/5) + abs(i5%5 - x%5)) * pop});
								sum += mn;
							}
							if(mndist > sum)
							{
								mndist = sum;
								res[0] = i1; res[1] = i2; res[2] = i3; res[3] = i4; res[4] = i5;
							}
						}
					}
				}
			}
		}
		printf("%d %d %d %d %d\n",res[0],res[1],res[2],res[3],res[4]);
	}	
}
