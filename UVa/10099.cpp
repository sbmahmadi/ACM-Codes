#include<iostream>
#include<vector>
#include<algorithm>
#include<math.h>

using namespace std;

class Edge
{
public:
	int destIndex;
	int weight;
	Edge(int des, int w) : destIndex(des), weight(w) {}
};

class Node
{
public:
	bool visited;
	int distance;
	vector<Edge> edges;
	Node() : visited(false), distance(-1)
	{}
};

void step(int node, vector<Node> &g)
{
	g[node].visited = true;
	for (Edge e : g[node].edges)
	{
		if (!g[e.destIndex].visited)
		{
			g[e.destIndex].distance = max(g[e.destIndex].distance, min(g[node].distance, e.weight));
		}
	}
}

int main()
{
	int n, r, s, d, t,T=0;
	while (true)
	{
		cin >> n >> r;
		if (n == r && r == 0) return 0;

		vector<Node> graph;
		graph.assign(n, Node());

		for (int i = 0; i < r; i++)
		{
			int a, b, c;
			cin >> a >> b >> c;
			graph[a - 1].edges.push_back(Edge(b - 1, c));
			graph[b - 1].edges.push_back(Edge(a - 1, c));
		}
		cin >> s >> d >> t;
		graph[s - 1].distance = 100000000;

		for (int j = 0; j < n; j++)
		{
			int dmax = -1, indmax = 0;
			for (int i = 0; i < n; i++)
			{
				if (!graph[i].visited && graph[i].distance > dmax)
				{
					dmax = graph[i].distance;
					indmax = i;
				}
			}
			step(indmax, graph);
		}
		printf("Scenario #%d\nMinimum Number of Trips = %d\n\n", ++T, (int)ceil(t*1.0 / (graph[d - 1].distance - 1)));
	}
}