/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0);
#define trace(x) cerr << #x << ": " << x << endl;
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl;
#define _ << " :: " <<
#define allof(x) (x).begin(),(x).end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

int main()
{
	int t;
	cin >> t;
	For(tt,0,t) {
		int res[26] = {};
		string line;
		cin >> line;
		string ss;
		For(i,0,line.size()) {
			if(ss.empty() || ss.back() != line[i]) {
				ss.push_back(line[i]);
				res[ss.back() - 'A']++;
			}
			else if(ss.back() == line[i]) {
				ss.pop_back();
				if(!ss.empty())
					res[ss.back() - 'A']++;
			}
		}
		res[line[0] - 'A']--;
		cout << "Case " << tt+1 << endl;
		For(i,0,26) {
			if(res[i]) {
				cout << char(i + 'A') << " = " << res[i] << endl;
			}
		}
	}
}

