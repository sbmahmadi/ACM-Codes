/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <list>
#include <vector>
#include <queue>
#include <deque>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <math.h>
#include <algorithm>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

int main()
{
	ios_base::sync_with_stdio(0);
	cin.tie(0);
	int l,U,r;
	int tt = 0;
	while(cin >> l >> U >> r && r)
	{
		vi b(r);
		For(i,0,r)
			cin >> b[i];

		vi dist(10000,-1);
		queue<int> q;

		q.push(l);
		dist[l] = 0;
		bool find = false;
		while(!q.empty())
		{
			int u = q.front(); q.pop();
			if(u == U)
			{
				find = true;
				break;
			}

			For(i,0,r)
			{
				int v = (u + b[i]) % 10000;
				if(dist[v] < 0)
					dist[v] = dist[u] + 1,
					q.push(v);
			}
		}
		cout << "Case " << ++tt << ": " << (dist[U] == -1 ? "Permanently Locked" : to_string(dist[U])) << endl;
	}	
}