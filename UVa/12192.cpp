/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int calc(vector<vi>& grid, int i, int j, int h)
{
	int res = 0;
	while(i < grid.size() && j < grid[i].size() && grid[i++][j++] <= h)
		res++;
	return res;
}

int main()
{
	ifstream fin("file.in");
	ofstream fout("file.out");

	int n,m;
	while(cin >> n >> m && n)
	{
		vector<vi> grid(n);
		For(i,0,n)
		{
			grid[i].resize(m);
			For(j,0,m)
				cin >> grid[i][j];
		}
		int q;
		cin >> q;
		For(i,0,q)
		{
			int l,u;
			cin >> l >> u;
			int mx = 0;
			For(j,0,n)
			{
				auto it = lower_bound(allof(grid[j]), l);
				if(*it == l)
					mx = max (mx,calc(grid,j,distance(grid[j].begin(),it),u));
				else
				{
					auto it2 = upper_bound(allof(grid[j]), l);
					if(it2 != grid[j].end())
						mx = max (mx, calc(grid,j,distance(grid[j].begin(),it2),u));
				}
			}
			cout << mx << endl;
		}
		cout << '-' << endl;
	}
}
