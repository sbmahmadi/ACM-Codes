#include <iostream>
#include <vector>

using namespace std;

int k;

void dfs(int u, vector<vector<int>>& graph, vector<int>& v)
{
	v[u] = true;
	k += graph[u].size();
	for(int i = 0; i < graph[u].size(); i++)
		if(!v[graph[u][i]])
			dfs(graph[u][i],graph,v);
}

int main()
{
	int n,m;
	while(cin >> n >> m)
	{
		k = 0;
		int o = 0;
		vector<vector<int>> graph(n);
		for(int i = 0; i < m; i++)
		{
			int a,b;
			cin >> a >> b;
			graph[a].push_back(b);
			graph[b].push_back(a);
			o = a;
		}
		int t = 0;
		for(int i = 0; i < n; i++)
		{
			t += graph[i].size()%2;
		}
		vector<int> v(n,0);
		dfs(o,graph,v);
		k /= 2;

		if(k != m || m == 0)
			t = 1;
		cout << ((t == 0) ? "Possible" : "Not Possible") << endl;
	}
}