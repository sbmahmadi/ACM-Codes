/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0);
#define trace(x) cerr << #x << ": " << x << endl;
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl;
#define _ << " :: " <<
#define allof(x) (x).begin(),(x).end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

void dfs(int u, vector<vi>& adjmat, vector<vi>& adjlist, vi& par)
{
	For(i,0,adjlist[u].size())
		if(par[adjlist[u][i]] == -1 && adjmat[u][adjlist[u][i]] > 0)
			par[adjlist[u][i]] = u, dfs(adjlist[u][i], adjmat, adjlist, par);
}

int maxflow(vector<vi>& adjmat, vector<vi>& adjlist)
{
	int flow = 0;
	while(1)
	{
		vi par(38, -1);
		par[0] = 0;
		dfs(0, adjmat, adjlist, par);
		
		if(par[37] == -1)
			break;
		int cost = INT_MAX;
		int me = 37;
		while(me)
		{
			cost = min(cost, adjmat[par[me]][me]);
			me = par[me];
		}
		me = 37;
		while(me)
		{
			adjmat[par[me]][me] -= cost;
			adjmat[me][par[me]] += cost;
			me = par[me];
		}
		flow += cost;
	}
	return flow;
}

int main()
{
	string line;
	while(getline(cin, line))
	{
		// src = 0; task = 1-26; cmp = 27-36; drain = 37
		vector<vi> adjmat(38,vi(38,0));
		vector<vi> adjlist(38);
		For(i,27,37)
			adjmat[i][37] = 1, adjlist[i].push_back(37);
		//read
		stringstream ss(line);
		char c; int n; char c1;
		ss >> c >> n;
		int tot = n;
		
		adjmat[0][c-'A' + 1] = n; adjlist[0].push_back(c-'A' + 1);
		while(ss >> c1 && isdigit(c1))
		{
			
			adjmat[c - 'A' + 1][27 + c1 - '0'] = 1; 
			adjlist[c - 'A' + 1].push_back(27 + c1 -'0'); 
			adjlist[27 + c1 - '0'].push_back(c - 'A' + 1);
			
		}
		while(getline(cin, line) && line != "")
		{
			
			stringstream ss(line);
			ss >> c >> n;
			tot += n;
			adjmat[0][c-'A' + 1] = n; adjlist[0].push_back(c-'A' + 1);
			while(ss >> c1 && isdigit(c1))
			{
			
				adjmat[c - 'A' + 1][27 + c1 - '0'] = 1; 
				adjlist[c - 'A' + 1].push_back(27 + c1 -'0');
				adjlist[27 + c1 - '0'].push_back(c - 'A' + 1);
			}	
		}
	
		int t = maxflow(adjmat, adjlist);
		if(t != tot)
		{
			cout << "!" << endl;
			continue;
		}
		For(i,27,37)
		{
			bool t = false;
			For(j,1,27)
				if(adjmat[i][j])
					t = true, cout << char(j - 1 + 'A');
			if(!t)
				cout << "_"; 
		}
		cout << endl;
	}
}