/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

map<string,int> id;

int getID(string s)
{
	return id.insert(make_pair(s,id.size())).first->second;
}

bool check(string s1, string s2)
{
	if(s1.size() != s2.size())
		return false;
	int d = 0;
	For(i,0,s1.size())
		if(s1[i] != s2[i]) 
			d++;
	return (d == 1);
}

int graph[200][200];

int main()
{
	int n;
	cin >> n;
	while(n--)
	{
		string s;
		vector<string> v;
		id.clear();
		For(i,0,200)
			For(j,0,200)
				graph[i][j] = 1e7, graph[j][j] = 0;

		while(cin >> s && s != "*")
		{
			Fori(it,v)
			{
				if(check(*it,s))
					graph[getID(s)][getID(*it)] = graph[getID(*it)][getID(s)] = 1;
			}
			v.push_back(s);
		}

		For(k,0,200)
			For(j,0,200)
				For(i,0,200)
					graph[i][j] = min(graph[i][j], graph[i][k] + graph[k][j]);
		cin.ignore();
		while(getline(cin,s) && s != "")
		{
			stringstream ss(s);
			string s1,s2;
			ss >> s1 >> s2;
			cout << s1 << " " << s2 << " " << graph[getID(s1)][getID(s2)] << endl;
		}
		if(n != 0)
			cout << endl;
	}
}
