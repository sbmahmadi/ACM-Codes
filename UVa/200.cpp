/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

void dfs(int ind, vector<vi>& graph, vi& visited, vi& stack)
{
	visited[ind] = true;
	for(int v : graph[ind])
		if(!visited[v])
			dfs(v,graph,visited,stack);

	stack.push_back(ind);		
}

int main()
{
	string s;
	vector<vi> graph(26);
	vi visited(26,1);
	vector<string> ss;

	while(getline(cin,s) && s != "#")
	{
		ss.push_back(s);
		For(i,0,ss.back().size())
			visited[ss.back()[i] - 'A'] = false;
	}
	For(j,0,ss.size())
		For(i,j+1,ss.size())
		{
			int ind = 0;
			while(ind < ss[j].size() && ind< ss[i].size() && ss[j][ind] == ss[i][ind])
				ind++;
			if(ind < ss[j].size() && ind < ss[i].size())
				graph[ss[j][ind]-'A'].push_back(ss[i][ind] - 'A');
		}
	
	vi stack;
	For(i,0,26)
		if(!visited[i])
			dfs(i,graph, visited,stack);
	roF(i,stack.size()-1,0)
		cout << char(stack[i] + 'A');
	cout << endl;
}