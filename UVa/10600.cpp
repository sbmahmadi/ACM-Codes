/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <list>
#include <vector>
#include <queue>
#include <deque>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <math.h>
#include <algorithm>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) //cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

int n,m;
struct Edge
{
	int w,u,v;
	bool operator<(const Edge e) const
	{
		return w < e.w;
	}
};

int mst(vector<vector<Edge>>& graph, ii nt)
{
	multiset<Edge> pq;
	vi visit(n,0);
	visit[0] = true;
	For(i,0,graph[0].size())
		if(ii(graph[0][i].u, graph[0][i].v) != nt && ii(graph[0][i].v, graph[0][i].u) != nt)
			pq.insert(graph[0][i]);
	int res = 0;
	int k = 0;
	while(!pq.empty())
	{
		auto tmp = *pq.begin(); pq.erase(pq.begin());
	
		visit[tmp.v] = true;
		res += tmp.w; k++;
		For(i,0,graph[tmp.v].size())
			if(ii(graph[tmp.v][i].u, graph[tmp.v][i].v) != nt && ii(graph[tmp.v][i].v, graph[tmp.v][i].u) != nt)
				pq.insert(graph[tmp.v][i]);
		while(!pq.empty() && visit[pq.begin()->v])
			pq.erase(pq.begin());
	}
	if(k == n-1)
		return res;
	return INT_MAX;
}

int main()
{
	int t;
	cin >> t;
	while(t--)
	{
		cin >> n >> m;
		// w, u, v
		vector<vector<Edge>> graph(n);
		vector<vi> taken(n,vi(n,0));
		
		For(i,0,m)
		{
			int a,b,c;
			cin >> a >> b >> c; a--; b--;
			graph[a].push_back({c,a,b});
			graph[b].push_back({c,b,a});
		}
		vector<ii> vii;
		multiset<Edge> pq;
		vi visit(n,0);
		visit[0] = true;
		For(i,0,graph[0].size())
			pq.insert(graph[0][i]);
		int res = 0;

		while(!pq.empty())
		{
			trace(1)
			auto tmp = *pq.begin(); pq.erase(pq.begin());
			trace(2)
			visit[tmp.v] = true;
			res += tmp.w;
			vii.push_back({tmp.u,tmp.v});
			For(i,0,graph[tmp.v].size())
				pq.insert(graph[tmp.v][i]);
			trace(3)
			while(!pq.empty() && visit[pq.begin()->v])
				pq.erase(pq.begin());
			trace(4)
		}
		trace(5)
		int res2 = INT_MAX;
		for(auto i : vii)
		{
			res2 = min(res2, mst(graph,i));
		}
		cout << res << " " << res2 << endl;
	}
}