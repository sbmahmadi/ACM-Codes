/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

int grid[1005][1005];

int dx[4] = {1,-1,0,0};
int dy[4] = {0,0,-1,1};
int r,c;

void BFS(int x, int y)
{
	queue<int> qx,qy;
	qx.push(x);
	qy.push(y);
	grid[x][y] = 0;
	while(!qx.empty())
	{
		x = qx.front();
		qx.pop();
		y = qy.front();
		qy.pop();
		For(i,0,4)
		{
			int nx = x + dx[i];
			int ny = y + dy[i];
			if(nx >=0 && nx < r && ny >= 0 && ny < c && grid[nx][ny] == -1)
			{
				grid[nx][ny] = grid[x][y] + 1;
				qx.push(nx);
				qy.push(ny);
			}
		}
	}
}

int main()
{
	
	while(cin >> r >> c && r)
	{
		memset(grid, -1, sizeof grid);
		int rr; cin >> rr;
		For(i,0,rr)
		{
			int row, cc;
			cin >> row >> cc;
			For(i,0,cc)
			{
				int col;
				cin >> col;
				grid[row][col] = -2;
			}
		}
		int s1,d1,s2,d2;
		cin >> s1 >> d1 >> s2 >> d2;
		BFS(s1,d1);
		cout << grid[s2][d2] << endl;
	}	
}