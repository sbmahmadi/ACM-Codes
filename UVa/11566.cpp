/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

#define EPC 1e-6

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

int p[205];
int f[205];
int n,x,t,k;
int dp[205][1200][24];

int knapsack(int id, int cost, int dish)
{
	if(id == k || dish == 2*n)
		return 0;
	if((int)ceil((cost + t*n) * 1.1 - EPC) == n*x)
		return 0;
	int &res = dp[id][cost][dish];
	if(res >= 0)
		return res;
	if(ceil((cost + p[id] + t*n) * 1.1 - EPC) > n*x)
		return res = knapsack(id+1, cost, dish);
	return res = max(knapsack(id+1, cost, dish), knapsack(id+1, cost + p[id], dish+1) + f[id]);
}

int main()
{
	ios_base::sync_with_stdio(0); 
	cin.tie(0); cout << fixed << setprecision(2);
	while(cin >> n >> x >> t >> k && n)
	{
		n++;
		For(i,0,k)
		{
			cin >> p[i*2];
			p[i*2+1] = p[i*2];
			f[i*2] = 0;
			int tmp;
			For(j,0,n)
				cin >> tmp, f[i*2] += tmp;
			f[i*2+1] = f[i*2];
		}
		k*=2;
		memset(dp, -1, sizeof dp);
		cout << knapsack(0,0,0) * 1.0 / n << endl; 
	}
}