/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int eval(int bitmask, vi& durs)
{
	int res = 0;
	For(i, 0, durs.size())
		if(1 << i & bitmask)
			res += durs[i];
	return res;
}

int main()
{
	/*ifstream fin("file.in");
	ofstream fout("file.out");*/
	int n;
	while(cin >> n)
	{
		int cnt;
		cin >> cnt;
		vi durs(cnt);
		For(i,0,cnt)
			cin >> durs[i];
		vi res;
		int max = -1;
		int maxbit = 0;
		For(bitmak, 0, 1<<cnt)
		{
			int ev = eval(bitmak, durs); 
			if(ev > max && ev <= n )
			{
				max = ev;
				maxbit = bitmak;
			}
		}

		For(i,0,cnt)
		{
			if(maxbit & 1<<i)
				cout << durs[i] << " ";
		}
		cout << "sum:" << max << endl;
	}
}