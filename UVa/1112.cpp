/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <list>
#include <vector>
#include <queue>
#include <deque>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <math.h>
#include <algorithm>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) fout <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

int main()
{
	int t;
	cin >> t;
	
	while(t--)
	{
		int n,e,time,m;
		cin >> n >> e >> time >> m;
	
		vector<vi> graph(n,vi(n,1e8));
		For(i,0,n) 
			graph[i][i] = 0;		
		For(i,0,m)
		{
			int a,b,tt;
			cin >> a >> b >> tt; a--; b--;
			graph[a][b] = tt;
		}

		For(k,0,n)
			For(i,0,n)
				For(j,0,n)
					if(graph[i][j] > graph[i][k] + graph[k][j])
						graph[i][j] = graph[i][k] + graph[k][j];
		int res = 0;
		For(i,0,n)
			if(graph[i][e-1] <= time)
				res++;
		cout << res << endl;
		if(t)
			cout << endl;
	}
}