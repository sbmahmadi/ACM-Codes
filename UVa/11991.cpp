#include<iostream>
#include<vector>
#include<unordered_map>

using namespace std;

#define For(i,a,b) for(int i = a; i<b;i++)

int main()
{
	long n, m;

	while (cin >> n >> m)
	{
		unordered_map<int, vector<int>> mp;
		For(i, 0, n)
		{
			long temp;
			cin >> temp;
			mp[temp].push_back(i+1);
		}
		For(i, 0, m)
		{
			long k, v;
			cin >> k >> v;
			
			if (mp[v].size() < k) cout << '0' << endl;
			else cout << mp[v][k-1] << endl;
		}
	}
}