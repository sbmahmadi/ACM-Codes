import java.util.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

class Main{

	static String get(GregorianCalendar date)
	{
		int m = date.get(date.MONTH);
		int d = date.get(date.DATE); 
		if((m == 0 && d >= 21) || (m == 1 && d<= 19))
			return "aquarius";
		if((m == 1 && d >= 20) || (m == 2 && d<= 20))
			return "pisces";
		if((m == 2 && d >= 21) || (m == 3 && d<= 20))
			return "aries";
		if((m == 3 && d >= 21) || (m == 4 && d<= 21))
			return "taurus";
		if((m == 4 && d >= 22) || (m == 5 && d<= 21))
			return "gemini";
		if((m == 5 && d >= 22) || (m == 6 && d<= 22))
			return "cancer";
		if((m == 6 && d >= 23) || (m == 7 && d<= 21))
			return "leo";
		if((m == 7 && d >= 22) || (m == 8 && d<= 23))
			return "virgo";
		if((m == 8 && d >= 24) || (m == 9 && d<= 23))
			return "libra";
		if((m == 9 && d >= 24) || (m == 10 && d<= 22))
			return "scorpio";
		if((m == 10 && d >= 23) || (m == 11 && d<= 22))
			return "sagittarius";
		return "capricorn";
		
	}

	public static void main(String[] args) {
		Scanner sc  = new Scanner(System.in);
		int n = sc.nextInt();
		for(int i = 0; i < n; i++){
			int k,d,m,y;
			
			k = sc.nextInt();
		
			GregorianCalendar date = new GregorianCalendar(k%10000, k/1000000 - 1, (k/10000)%100);
			date.add(Calendar.WEEK_OF_MONTH, 40);
			System.out.println((i+1) + " " + ((date.get(date.MONTH) + 1) < 10 ? "0" : "") + (date.get(date.MONTH) + 1) + "/" + (date.get(date.DATE) < 10 ? "0" : "") + date.get(date.DATE) + "/" + date.get(date.YEAR) + " " + get(date));
		}
	}
}