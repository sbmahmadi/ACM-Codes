/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

bool visited[105];
int e[105];

bool dfs(int u, int v, vector<vi> &graph)
{
	if(u == v)
		return true;
	visited[u] = true;
	For(i,0,graph[u].size())
		if(!visited[graph[u][i]] && dfs(graph[u][i], v, graph))
			return true;
	return false;
			
}

int main()
{
	int n;

	while(cin >> n && n+1)
	{
		vector<vi> graph(n);

		For(i,0,n)
		{
			int m;
			cin >> e[i] >> m;
			For(j, 0, m)
			{
				int a;
				cin >> a;
				graph[i].push_back(a-1);
			}
		}

		vi dist(n,-1e7);
		dist[0] = 0;
		
		For(i,0,n-1)
			For(j,0,n)
				if(dist[j] > -100)
					For(k,0,graph[j].size())
						if(dist[graph[j][k]] < dist[j] + e[graph[j][k]])
							dist[graph[j][k]] = dist[j] + e[graph[j][k]];

		memset(visited, 0, sizeof visited);
		bool c = false;
		For(i,0,n)
			if(dist[i] > -100)
				For(j,0,graph[i].size())
					if(dist[graph[i][j]] < dist[i] + e[graph[i][j]] && dfs(i, n-1, graph))
						c = true;
		
		if(c || dist[n-1] > -100)
			cout << "winnable" << endl;
		else 
			cout << "hopeless" << endl;
	}
}
