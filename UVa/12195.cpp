#include<iostream>

using namespace std;

int main()
{
	char c;
	int count;
	while ((c = cin.get()) != '*')
	{
		count = 0;
		while (cin.peek() != '\n')
		{
			int m = 0;
			while ((c = cin.get()) != '/')
			{
				switch (c)
				{
				case 'W': m += 64;  break;
				case 'H': m += 32;	break;
				case 'Q': m += 16;	break;
				case 'E': m += 8;	break;
				case 'S': m += 4;	break;
				case 'T': m += 2;	break;
				case 'X': m += 1;	break;
				}
			}
			if (m == 64) count++;
		}
		cout << count << endl;
		cin.ignore(1);
	}
}