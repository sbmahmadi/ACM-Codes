#include<iostream>
#include<vector>
#include<algorithm>

using namespace std;

#define For(i,a,b) for(int i =a;i<b;i++)

int wayX[4] = { 1, 0, 0, -1 };
int wayY[4] = { 0, -1, 1, 0 };


int main()
{
	int n, r, c, k;
	while (cin >> n >> r >> c >> k)
	{
		if (r == 0) break;
		int ** map = new int*[r];

		For(i, 0, r)
			map[i] = new int[c];
		
		For(i, 0, r)
			For(j, 0, c)
				cin >> map[i][j];
			
		For(i, 0, k)
		{
			vector<pair<int, int>> nums;
			For(i, 0, r)
				For(j, 0, c)
					For(p, 0, 4)
					{
						int newx = i + wayX[p];
						int newy = j + wayY[p];
						if (newx < 0 || newy < 0 || newx > r - 1 || newy > c - 1) continue;
						if (map[i][j] == map[newx][newy] - 1 || (map[i][j] == n - 1 && map[newx][newy] == 0))  nums.push_back(make_pair(newx, newy));
					}
				
			sort(nums.begin(), nums.end());
			nums.erase(unique(nums.begin(), nums.end()), nums.end());
			For(i, 0, nums.size())
			{
				int x = nums[i].first;
				int y = nums[i].second;
				if (map[x][y] == 0) map[x][y] = n - 1;
				else map[x][y]--;
			}
		}

		For(i, 0, r)
		{
			For(j, 0, c-1)
				cout<< map[i][j]<<" ";
			
			cout <<map[i][c-1] << endl;
		}
	}
}