#include <iostream>
#include <vector>
#include <string.h>
#include <sstream>

using namespace std;

int main() {
	int n;
	cin >> n;
	cin.ignore();
	for(int i = 0; i < n; i++) {
		cin.ignore();
		string line1, line2;
		getline(cin, line1);
		getline(cin, line2);
		stringstream ss1(line1);
		stringstream ss2(line2);
		vector<string> ii;
		vector<string> res;
		vector<int> pos;
		int ind = 0;
		while(ss1 >> ind) {
			pos.push_back(ind);
			res.push_back("");

			string me;
			ss2 >> me;
			ii.push_back(me);
		}
		for(int j = 0; j < pos.size(); j++) {
			res[pos[j]-1] = ii[j];
		}
		if(i > 0)
			cout << endl;
		for(int j = 0; j < res.size(); j++) {
			cout << res[j] << endl;
		}
	}
}