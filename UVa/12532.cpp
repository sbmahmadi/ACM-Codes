#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
using namespace std;

void makeSeg(vector<int>& seg)
{
	roF(i, seg.size()/2-1, 1)
		seg[i] = seg[i*2] * seg[i*2+1];
}

void update(int ind, int val, vector<int>& seg)
{
	if(val != 0)
		val /= abs(val);
	seg[seg.size()/2 + ind] = val;
	int i = (seg.size()/2 + ind)/2;
	while(i != 0)
	{
		seg[i] = seg[i*2] * seg[i*2+1];
		i/= 2;
	}
}

int getval(int lq, int rq, int l, int r,int ind, vector<int>& seg)
{
	trace(l _ r _ ind);
	if(l >= lq && r <= rq)
	{
		trace("found");
		return seg[ind];
	}
	if(lq > r || rq < l)
	{
		trace("Not found");
		return 1;
	}
	trace ("split");
	return getval(lq,rq,l,(r+l)/2,ind*2,seg) * getval(lq,rq,(l+r)/2 +1, r, ind*2+1, seg);
}

int main()
{
	int n,k;
	while(cin >> n >> k)
	{
		int nn = pow(2,ceil(log2(n)));
		vector<int> seg(nn*2,1);
		For(i,0,n)
		{
			cin >> seg[nn+i];
			if(seg[nn+i] != 0)
				seg[nn+i] /= abs(seg[nn+i]);
		}
		makeSeg(seg);
		/*for(int i : seg)
			cout << setw(3) << i;
		cout << endl;*/
		For(i,0,k)
		{
			char c;
			int a,b;
			cin >> c >> a >> b;

			if(c == 'C')
				update(a-1,b,seg);
			else
			{
				int res = getval(a-1,b-1,0,nn-1,1,seg);
				if(res > 0)
					cout << "+";
				if(res < 0)
					cout << "-";
				if(res == 0)
					cout << "0";
			}
		}
		cout << endl;
	}
}