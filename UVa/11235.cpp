/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

int getmax(int lq, int rq, int l, int r, int ind, vi& nums)
{
	if(lq >= l && rq <= r)
		return nums[ind];
	if(lq > r || rq < l)
		return 0;
	return max(getmax(lq, rq, l, (l+r)/2, ind*2, nums), getmax(lq, rq, (l+r)/2+1,r, ind*2+1, nums));
}

int main()
{
	int n,q;
	while(cin >> n && n)
	{
		cin >> q;
		
		int nn = pow(2,ceil(log2(nn)));
		
		vector<multiset<int>> seg(nn*2);
		For(i,0,n)
		{
			int a;
			cin >> a;
			seg[nn+i].insert(a);
		}

		roF(i, nn-1, 1)
		{
			seg[i].insert(allof(seg[i*2]));
			seg[i].insert(allof(seg[i*2+1]));
		}

		For(i,0,q)
		{
			int a,b;
			cin >> a >> b;
			multiset<int> res = getmax(a-1, b-1, 0, nn-1, 1, seg);
			for (auto it : res)
				if(res.count(*it) > max)
			cout << getmax(a-1, b-1, 0, nn-1, 1, seg) << endl;
		}
	}
}