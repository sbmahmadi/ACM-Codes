/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

int dx[] = {1,0,-1,0};
int dy[] = {0,1,0,-1};
char grid[105][105];
int d,n,m,s;
string ins;

int main()
{
	ios_base::sync_with_stdio(0);
	cin.tie(0);
	while(cin >> n >> m >> s && n)
	{
		int sx,sy;
		d = 0;
		For(i,0,n)
			For(j,0,m)
			{
				cin >> grid[i][j];
				switch(grid[i][j])
				{
					case 'N':
					{
						d = 2;
						sx = i, sy = j;
						break;
					}
					case 'S':
					{
						d = 0;
						sx = i, sy = j;
						break;
					}
					case 'L':
					{
						d = 1;
						sx = i, sy = j;
						break;
					}
					case 'O':
					{
						d = 3;
						sx = i, sy = j;
						break;
					}
				}
			}
		
		cin >> ins;
		int res = 0;
		For(i,0,ins.size())
		{
			if(grid[sx][sy] == '*')
				res++,
				grid[sx][sy] = '.';
			if(ins[i] == 'F')
			{
				int nx = sx + dx[d];
				int ny = sy + dy[d];
				if(nx >= 0 && nx < n && ny >= 0 && ny < m && grid[nx][ny] != '#')
					sx = nx, sy = ny;
			}
			else
			{
				d += (ins[i] == 'D') ? 3 : 5;
				d%=4;
			}
		}
		if(grid[sx][sy] == '*')
			res++;
		cout << res << endl;
	}
}