#include<iostream>
#include<string.h>
#include<math.h>

using namespace std;

long long dp[1000010];

long long f(int i)
{
	if (i == 0) return 1;
	if (dp[i] != -1) return dp[i];
	return dp[i] = (f(floor(i - sqrt(i))) + f(floor(log(i))) + f(i*pow(sin(i), 2))) % 1000000;
}

int main()
{
	memset(dp, -1, sizeof dp);
	int n;
	while (cin >> n && n != -1)
	{
		cout << f(n) << endl;
	}
}