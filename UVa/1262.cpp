/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

vector<string> res;

void solve(string str, set<char> cols[2][5], int col)
{
	if(col == 5)
	{
		res.push_back(str);
		return;
	}
	Fori(it, cols[0][col])
	{
		if(cols[1][col].count(*it))
		{
			solve(str + *it,cols,col+1);
		}
	}
}

int main()
{
	int t;
	cin >> t;
	while(t--)
	{
		int k;
		cin >> k;
		
		set<char> cols[2][5];
		res.clear();

		For(j,0,2)
		{
			For(i,0,6)
			{
				string s;
				cin >> s;
				For(p,0,5)
				{
					cols[j][p].insert(s[p]);
				}
			}
		}

		solve("",cols,0);
		if(res.size() < k)
			cout << "NO" << endl;
		else cout << res[k-1] << endl;
	}
}