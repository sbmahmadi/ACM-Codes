#include<iostream>
#include<vector>
#include<queue>
using namespace std;

class MSTEdge
{
public:
	int destination;
	int weight;

	MSTEdge(int x, int w) : destination(x), weight(w){}

	bool operator<(const MSTEdge &e) const
	{
		return (weight > e.weight);
	}
};

class MSTNode
{
public:
	bool visited = false;
	vector<MSTEdge> Adjacent;
};


void Visit(int index, vector<MSTNode> &graph, priority_queue<MSTEdge, vector<MSTEdge>, less<MSTEdge>> &pq)
{
	graph[index].visited = true;
	for (MSTEdge e : graph[index].Adjacent)
	{
		if (!graph[e.destination].visited) pq.push(e);
	}
}


int main()
{
	int m, n;
	while (cin >> m >> n)
	{
		if (n == m && n == 0) return 0;

		vector<MSTNode> graph;
		priority_queue<MSTEdge, vector<MSTEdge>, less<MSTEdge>> pq;

		long long totalWeight = 0;

		graph.assign(m,MSTNode());
		
		for (int i = 0; i < n; i++)
		{
			int x, y, w;
			cin >> x >> y >> w;
			graph[x].Adjacent.push_back(MSTEdge(y, w));
			graph[y].Adjacent.push_back(MSTEdge(x, w));
			totalWeight += w;
		}

		Visit(0,graph,pq);
		
		long long res = 0;

		while (!pq.empty())
		{
			MSTEdge e = pq.top();
			pq.pop();
			if (!graph[e.destination].visited)
			{
				res += e.weight;
				Visit(e.destination, graph, pq);
			}
		}
		cout << totalWeight - res<< endl;
	}
}