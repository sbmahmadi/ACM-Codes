/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <list>
#include <vector>
#include <queue>
#include <deque>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <math.h>
#include <algorithm>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

int eval(string s)
{
	if(isdigit(s[0]))
		return s[0] - '0';
	else return 10;
}

int main()
{
	int t;
	cin >> t;
	For(tt,0,t)
	{
		deque<string> hand(25),pile(27);
		For(i,0,27)
			cin >> pile[i];
		For(i,0,25)
			cin >> hand[i];
		int y = 0;
		For(i,0,3)
		{
			int a = eval(pile.back());;
			y += a;
			pile.erase(pile.end() - (11-a),pile.end());
		}
		For(i,0,25)
		{
			pile.push_back(hand.front());
			hand.pop_front();
		}
		cout << "Case " << tt+1 << ": " << pile[y-1] << endl;
	}
}