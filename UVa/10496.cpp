/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

struct Pair
{
	int x,y;
};

int main()
{
	ios_base::sync_with_stdio(0); 
	cin.tie(0); cout << fixed;	
	int t;
	cin >> t;
	while(t--)
	{
		int x,y;
		cin >> x >> y;
		Pair beg;
		cin >> beg.x >> beg.y;
		int n;
		cin >> n;
		vector<Pair> v(n);
		vi perm(n);
		For(i,0,n)
			cin >> v[i].x >> v[i].y, perm[i] = i;
		int mn = INT_MAX;
		do
		{
			int res = 0;
			For(i,0,n-1)
				res += abs(v[perm[i]].x - v[perm[i+1]].x) + abs(v[perm[i]].y - v[perm[i+1]].y);
			res += abs(beg.x - v[perm[0]].x) + abs(beg.y - v[perm[0]].y) + abs(beg.x - v[perm[n-1]].x) + abs(beg.y - v[perm[n-1]].y);
			mn = min(mn, res);
		} while(next_permutation(allof(perm)));
		cout << "The shortest path has length " << mn << endl;
	}
}