/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

int m,s;
int convs[50], infs[5];
int dp[305][305];

int change(int conv, int inf)
{
	if(conv < 0 || inf < 0) return INT_MAX;
	if(conv == 0 && inf == 0) return 0;
	
	int &res = dp[conv][inf];
	if(res >= 0) return res;
	res = INT_MAX;
	For(i,0,m)
		res = min(res, change(conv - convs[i], inf - infs[i]));
	
	if(res != INT_MAX) res++;
	return res;
}

int main()
{
	ios_base::sync_with_stdio(0); 
	cin.tie(0); cout << fixed;
	int t;
	cin >> t;
	while(t--)
	{
		cin >> m >> s;
		For(i,0,m)
			cin >> convs[i] >> infs[i];
		memset(dp,-1,sizeof dp);
		int res = INT_MAX;
		For(i,0,s+1)
			For(j,0,i+1)
				if(i*i + j*j == s*s)
					res = min({res, change(i,j), change(j,i)});
		
		cout << ((res == INT_MAX) ? "not possible" : to_string(res)) << endl;
	}
}