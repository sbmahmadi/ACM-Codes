/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0);
#define trace(x) cerr << #x << ": " << x << endl;
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl;
#define _ << " :: " <<
#define allof(x) (x).begin(),(x).end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

int main()
{
	map<int, int> f;
	int lo = 1, hi = 1;
	int k = 0;
	while(hi > 0){
		f[hi] = k;
		k++;
		lo += hi;
		swap(hi,lo);
	}

	int t;
	cin >> t;
	while(t--){
		int n;
		cin >> n;
		vi fib(n);
		For(i,0,n)
			cin >> fib[i];
		int mx = *max_element(allof(fib));
		mx = f[mx];
		cin.ignore();
		string line;
		getline(cin,line);
		string me;
		for(char c : line)
			if(c <= 'Z' && c >= 'A')
				me.push_back(c);
		string res;
		For(i,0,mx+1) res += " ";
		For(i,0,fib.size())
			res[f[fib[i]]] = me[i];
		cout << res << endl;
	}	
}
