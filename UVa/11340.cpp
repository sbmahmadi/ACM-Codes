/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

int main()
{
	cout << fixed << setprecision(2);
	int n;
	cin >> n;
	while(n--)
	{
		int k;
		cin >> k;
		map<char,int> p;
		For(i,0,k)
		{
			int a; char c;
			cin >> c >> a;
			p[c] = a;
		}
		int m;
		int res = 0;
		cin >> m;
		cin.ignore();
		For(i,0,m)
		{
			string s;
			getline(cin,s);
			For(j,0,s.size())
				res += p[s[j]];
		}
		cout << (res/100.0) << "$" << endl;
	}

}