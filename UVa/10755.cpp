/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

ll kadane(vector<vector<vector<ll>>> &grid, int i1, int j1, int i2, int j2)
{
	int k = grid[0][0].size();
	ll res = grid[i1][j1][0] - grid[i1][j2][0] - grid[i2][j1][0] + grid[i2][j2][0];
	ll res2 = res;
	For(i,1,k)
	{
		ll temp = grid[i1][j1][i] - grid[i1][j2][i] - grid[i2][j1][i] + grid[i2][j2][i];
		res = max(temp, res + temp);
		res2 = max(res2, res);
	}
	return res2;
}

int main()
{
	int t;
	cin >> t;
	while(t--)
	{
		int a,b,c;
		cin >> a >> b >> c;
		vector<vector<vector<ll>>> grid(a+1, vector<vector<ll>>(b+1, vector<ll>(c, 0)));
		
		For(i,1,a+1)
			For(j,1,b+1)
				For(k,0,c)
				{
					cin >> grid[i][j][k];
					grid[i][j][k] += grid[i][j-1][k] + grid[i-1][j][k] - grid[i-1][j-1][k];
				}

		long long mx = grid[1][1][1];
		For(i1,1,a+1)
			For(j1,1,b+1)
				For(i2,0,i1)
					For(j2,0,j1)
						mx = max(mx,kadane(grid,i1,j1,i2,j2));
					
		cout << mx << endl;
		if(t)
			cout << endl;
	}
}