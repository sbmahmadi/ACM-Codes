/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

void print(vi &par, vi &nums, int i)
{
	if(i == -1) return;
	print(par,nums,par[i]);
	cout << nums[i] << endl;
}

int main()
{
	ios_base::sync_with_stdio(0); 
	cin.tie(0); cout << fixed;
	
	vi nums;
	int a;
	while(cin >> a)
		nums.push_back(a);
	
	int n = nums.size();

	vi par(n,0), dp(n,0), v(n,0);
	int mx = 0, mxi = 0;
	
	For(i,0,n)
	{
		int q = lower_bound(dp.begin(), dp.begin() + mx, nums[i]) - dp.begin();
		dp[q] = nums[i];
		v[q] = i;
		par[i] = (q == 0) ? -1 : v[q-1];
		if(q == mx) 
			mx++;
	}
	cout << mx << endl << '-' << endl;
	print(par,nums,v[mx-1]);
}