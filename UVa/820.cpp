/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

vi BFS(vector<vi> &graph, int s, int d)
{
	int n = graph.size();
	queue<int> q;
	vector<bool> visited(n,false);

	q.push(s);
	visited[s] = true;
	
	vi par(n,-1);
	par[s] = s;

	while(!q.empty())
	{
		int h = q.front();
		q.pop();
		
		if(h == d) break;
		
		For(i,0,graph[h].size())
		{
			if(!visited[i] && graph[h][i] > 0)
			{
				q.push(i);
				visited[i] = true;
				par[i] = h;
			}
		} 
	}
	return par;
}

int findMin(vector<vi> &graph, vi &par,int t)
{
	int me = t;
	int res = 2000;
	while(me != par[me])
	{
		res = min(res,graph[par[me]][me]);
		me = par[me];
	}
	return res;
}

int main()
{
	int n;
	int count = 1;

	while (cin >> n && n)
	{
		vector<vi> graph(n,vi(n,0));

		int s,t,c;
		cin >> s >> t >> c; s--; t--;

		For(i,0,c)
		{
			int a,b,w;
			cin >> a >> b >> w; a--; b--;
			graph[a][b] += w;
			graph[b][a] += w;
		}
		
		vi par;
		int res = 0;
		while((par = BFS(graph,s,t))[t] != -1)
		{
			int d = findMin(graph,par,t);
			res += d;

			int me = t;
			while(me != par[me])
			{
				graph[me][par[me]] += d;
				graph[par[me]][me] -= d;
				me = par[me];
			}
		}
		cout << "Network " << count++ << endl; 
		cout << "The bandwidth is " << res << "." << endl << endl;

	}
}