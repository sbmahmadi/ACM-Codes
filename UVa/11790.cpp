/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

int lis(vi &h, vi &w)
{
	int res = 0;
	vi dp(h.size(), 0);
	For(i,0,h.size())
	{
		For(j,0,i)
		{
			if(h[j] < h[i] && dp[j] > dp[i])
				dp[i] = dp[j];
		}
		dp[i] += w[i];
		res = max(res, dp[i]);
	}
	return res;
}

int main()
{
	ios_base::sync_with_stdio(0); 
	cin.tie(0); cout << fixed;
	int t;
	cin >> t;
	For(tt,0,t)
	{
		string res = "Case " + to_string(tt + 1) + ".";
		int n;
		cin >> n;
		vi h(n,0),w(n,0);
		For(i,0,n)
			cin >> h[i];
		For(i,0,n)
			cin >> w[i];

		int is = lis(h, w);
		reverse(allof(h)); reverse(allof(w));
		int ds = lis(h, w);

		res += (is >= ds) ?  " Increasing (" + to_string(is) + "). Decreasing (" + to_string(ds) + ")." : " Decreasing (" + to_string(ds) + "). Increasing (" + to_string(is) + ").";
		cout << res << endl;
	}
}