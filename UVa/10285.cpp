/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0);
#define trace(x) cerr << #x << ": " << x << endl;
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl;
#define _ << " :: " <<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

int dp[110][110];
int grid[110][110];
int dx[] = {1,-1,0,0};
int dy[] = {0,0,1,-1};
int r, c;

int f(int i, int j)
{
	int& res = dp[i][j];
	if(res)
		return res;
	For(k,0,4)
	{
		int nx = dx[k] + i;
		int ny = dy[k] + j;
		if(nx >= 0 && nx < r && ny >= 0 && ny < c && grid[nx][ny] < grid[i][j])
			res = max(res,f(nx,ny));
	}
	return ++res;
}

int main()
{
	int t;
	cin >> t;
	while(t--)
	{
		memset(dp,0,sizeof dp);
		string s;
		cin >> s >> r >> c;
		For(i,0,r)
			For(j,0,c)
				cin >> grid[i][j];
		int mx = 0;
		For(i,0,r)
			For(j,0,c)
				mx = max(mx,f(i,j));
		cout << s << ": " << mx << endl;
	}
}