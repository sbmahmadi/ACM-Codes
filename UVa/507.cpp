/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

int road[20010];

int main()
{
	ios_base::sync_with_stdio(0);
	cin.tie(0);
	
	int b;
	cin >> b;
	
	For(r,1,b+1)
	{
		int s;
		cin >> s;
		
		int* d = road;
		For(i,0,s-1)
			cin >> *(d++);
		
		int curmax, mx, beg, beg1, end1;
		mx = curmax = road[0];
		beg = beg1 = end1 = 0;
		
		d = road;
		
		For(i,1,s-1)
		{
			beg = (curmax < 0) * i + beg * (curmax >=0);
			curmax = curmax * (curmax>=0) + *(++d);
			
			if(curmax > mx || curmax == mx && i - beg > end1 - beg1)
			{
				mx = curmax;
				beg1 = beg;
				end1 = i;
			}
		}

		if(mx > 0)
			cout << "The nicest part of route "<< r <<" is between stops " << beg1 + 1 << " and " << end1 + 2 << endl;
		else 
			cout << "Route "<< r <<" has no nice parts" << endl;
	}
}