#include <bits/stdc++.h>

#define For(i,a,b) for(int i=a;i<b;i++)

using namespace std;

int main()
{
	int n,p;
	int cnt = 0;
	while(cin >> n >> p && ( n || p ))
	{
		cin.ignore();
		string s;
		For(i,0,n)
			getline(cin,s);
		string ws;
		double ps = 0;
		int rs = 0;
		For(i,0,p)
		{
			getline(cin,s);
			float d;
			int r;
			cin >> d >> r;
			cin.ignore();
			if(r > rs || r == rs && d < ps)
				ws = s, ps = d, rs = r;
			For(j,0,r)
				getline(cin,s); 
		}
		if(cnt != 0)
			cout << endl;
		cout << "RFP #" << ++cnt << endl << ws << endl;
	}
}