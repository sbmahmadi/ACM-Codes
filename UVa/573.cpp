#include <bits/stdc++.h>

using namespace std;

int main()
{
	//6 3 1 0.3
	float h,u,d,f;
	while(cin >> h >> u >> d >> f && h)
	{
	
		int day = 1;
		float e = 0;
		f = (f/100) * u;
		while(1)
		{
			e += u;
			if(e > h)
			{
				cout << "success on day " << day << endl; 
				break;
			}
			e -= d;
			if(e < 0)
			{
				cout << "failure on day " << day << endl;
				break;
			}
			u -= f;
			u = max(u, float(0));
			day++;
		}
	}
}