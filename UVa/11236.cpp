/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int main()
{
	int mx = 2000000000;
	int c = 1000000;
	ios_base::sync_with_stdio(0);
	cout << fixed << setprecision(2);
	/*ifstream fin("file.in");
	ofstream fout("file.out");*/
	int cnt = 0;
	For(i,1,2001)
	{
		if(i*i*i*i > mx)
			break;
		For(j,i,2001 - i)
		{
			if(i*j*j*j > mx)
				break;
			For(k,j,2001 - i - j)
			{
				if(i*j*k*k > mx)
					break;
				int a = i+j+k;
				int b = i*j*k;
				double dp = 1.0*c*a/(b-c);
				if(dp == (int)dp)
				{
					int p = (int)dp;
					if(p >= k && a + p <= 2000 && b*p <= mx && (a+p)*c == b*p)
						{
							cout <<  (float)i/100 << " " << (float)j/100 << " " << (float)k/100 << " " << (float)p/100 << endl;
							cnt++;
						}
					
				}
			}
		}
	}
	//trace(cnt)
}
