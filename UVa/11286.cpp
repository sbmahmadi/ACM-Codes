/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

int main()
{
	int n;
	while(cin >> n && n)
	{
		map<string,int> mp;
		For(i,0,n)
		{
			vector<string> vs(5);
			string s;
			For(j,0,5)
				cin >> vs[j];
			sort(allof(vs));
			For(j,0,5)
				s += vs[j];

			mp[s]++;
		}
		int mx = 0; int cnt = 0;
		Fori(it,mp)
		{
			if(mx < it->second)
				cnt = 0, mx = it->second;
			if(mx == it->second)
				cnt+= it->second;
		}
		cout << cnt << endl;
	}	
}