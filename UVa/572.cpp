#include<iostream>
#include<string>

#define For(i,a,b) for(int i = a; i < b;i++)

using namespace std;


int di[] = { 1, 1, 1, -1, -1, -1, 0, 0 };
int dj[] = { 1, 0, -1, 1, 0, -1, 1, -1 };
int n, m;

void DFS(int i, int j, int**map)
{
	map[i][j] = -1;
	int I , J ;
	For(k, 0, 8)
	{
		I = i + di[k];
		J = j + dj[k];

		if (I >= 0 && J >= 0 && I < m && J < n && map[I][J] == 1)
		{
			DFS(I, J, map);
		}
	}
}



int main()
{
	
	string s;

	while (1)
	{
		cin >> m >> n;
		if (0 == m) break;
		int** map = new int*[m];
		For(i, 0, m)
		{
			map[i] = new int[n];
			cin >> s;
			For(j, 0, n) 
				map[i][j] = (s[j] == '@');
		}
		int count = 0;
		For(i, 0, m)
		{
			For(j, 0, n)
			{
				if (map[i][j] == 1)
				{
					DFS(i, j, map);
					count++;
				}
			}
		}
		cout << count << endl;
	}
}
