/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

bool valid(int bitmask, int s, int d)
{
	For(i,0,8)
	{
		int res = 0;
		For(j, i, i+5)
		{
			if(1<<j & bitmask)
				res += s;
			else res -= d;
		}
		if(res >= 0) return false;
	}
	return true;
}

int main()
{
	/*ifstream fin("file.in");
	ofstream fout("file.out");*/
	int s,d;
	while(cin >> s >> d)
	{
		int res = -15*d;
		For(bitmask, 0, 1 << 12)
		{
			if(!valid(bitmask,s,d))
				continue;

			int val = 0;
			For(i, 0, 12)
			{
				if(1<<i & bitmask)
					val += s;
				else val -= d;
			}
			res = max(res, val);
		}
		if(res >= 0)
			cout << res << endl; 
		else 
			cout << "Deficit" << endl;
	}
}
