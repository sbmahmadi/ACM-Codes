#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)

using namespace std;

void f(string s, int& ind, int ind2, int size, bool ar[])
{
	if(s[ind] == 'p')
	{
		For(j,0,4)
		{
			f(s, ++ind, ind2 + j*(size*size/4), size/2,ar);
		}
	}
	else if(s[ind] == 'f')
	{
		For(i,0,size*size)
			ar[ind2+i] |= 1;
	}
}

int main()
{
	int n;
	cin >> n;
	for(int i = 0; i < n; i++)
	{
		string s1,s2;
		cin >> s1 >> s2;
		bool ar[1024] = {};
		int a = 0, b = 0;
		f(s1,a,b,32,ar);
		a = 0, b = 0;
		f(s2,a,b,32,ar);
		a = 0;
		For(i,0,1024)
			a += ar[i];
		cout << "There are " << a << " black pixels."<< endl;
	}
}