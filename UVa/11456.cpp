/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

int main()
{
	ios_base::sync_with_stdio(0); 
	cin.tie(0); cout << fixed;

	int t;
	cin >> t;
	while(t--)
	{
		int n;
		cin >> n;
		vi v(n);
		For(i,0,n)
			cin >> v[i];
		reverse(allof(v));
		vi lis(n,0), lds(n,0);
		int res = 0;
		For(i,0,n)
		{
			For(j,0,i)
			{
				if(v[j] < v[i] && lis[j] > lis[i])
					lis[i] = lis[j];
				if(v[j] > v[i] && lds[j] > lds[i])
					lds[i] = lds[j];
			}
				lds[i]++; lis[i]++;
				res = max(res,lds[i] + lis[i] - 1);
		}
		cout << res << endl;
	}
}
