/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <list>
#include <vector>
#include <queue>
#include <deque>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <math.h>
#include <algorithm>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

bool isBipartite(int u, vector<vi> &graph, vi &visit, vi& color)
{
	visit[u] = true;
	bool res = true;
	For(i,0,graph[u].size())
		if(visit[graph[u][i]] && color[u] == color[graph[u][i]])
			return false;
		else if(!visit[graph[u][i]])
		{
			color[graph[u][i]] = color[u] ^ 1;
			res &= isBipartite(graph[u][i], graph, visit, color);
		}
	return res;
}

int main()
{
	int n;
	while(cin >> n && n)
	{
		vector<vi> graph(n);
		int m;
		cin >> m;
		For(i,0,m)
		{
			int u,v;
			cin >> u >> v;
			graph[u].push_back(v);
			graph[v].push_back(u);
		}
		vi color(n,0);
		vi visit(n,0);
		if(isBipartite(0,graph,visit,color))
			cout << "BICOLORABLE." << endl;
		else 
			cout << "NOT BICOLORABLE." << endl;
	}
}