/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

int solve(vi& milk, int mid)
{
	int res = 1;
	int sum = 0;
	For(i, 0, milk.size())
	{
		if(sum + milk[i] > mid)
		{
			res++;
			sum = 0;
		}
		sum += milk[i];
	}
	return res;
}

int main()
{
	int n,m;
	while(cin >> n >> m)
	{
		m = min(m,n);
		vi milk(n);
		For(i,0,n)
			cin >> milk[i];
		int lo = *max_element(allof(milk)), hi = 1e9 + 5, mid;
		For(i,0,50)
		{
			mid = (lo+hi)/2;
			int res = solve(milk,mid);
			if(res > m)
				lo = mid;
			else
				hi = mid;
		}
		cout << hi << endl;
	}
}
