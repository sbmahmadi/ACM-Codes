/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>
#include <list>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

map<string,int> ID;
map<int,string> name;

int getID(string s)
{
	return ID.insert({s,ID.size()}).first->second;
}

int main()
{
	int cs = 1;
	int n;
	while(cin >> n)
	{
		ID.clear();
		name.clear();
		For(i,0,n)
		{
			string s;
			cin >> s;
			name[getID(s)] = s;
		}
		vector<vi> graph(n);
		vi in(n,0);
		int m;
		cin >> m;
		For(i,0,m)
		{
			string s1,s2;
			cin >> s1 >> s2;
			graph[getID(s1)].push_back(getID(s2));
			in[getID(s2)]++;
		}
		vector<bool> visit(n,0);
		list<int> lst;
		For(j,0,n)
		{
			set<int> st;
			For(i,0,n)
				if(in[i] == 0 && !visit[i])
					st.insert(i);
			int me = *st.begin();
			visit[me] = true;
			For(i,0,graph[me].size())
				in[graph[me][i]]--;
			lst.push_back(me);
		}
		cout << "Case #" << cs++ << ": Dilbert should drink beverages in this order:";
		Fori(it,lst)
			cout << " " << name[*it];
		cout << ".\n\n";
	}
}