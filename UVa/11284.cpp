/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) //cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<double> vd;

ifstream fin("file.in");
ofstream fout("file.out");

map<ii,int> dp; 

int graph1[55][55];
int graph2[15][15];
int diff[55];
int op[15];
int n,p;

int TSP(int last, int bitmask)
{
	ii pp(last,bitmask);
	if(dp.count(pp))
		return dp[pp];

	int res = INT_MAX;
	For(bit,0,p)
	{
		if((bitmask >> bit & 1) == 0)
			res = min(res, TSP(bit, bitmask | (1<<bit)) + graph2[last][bit] - diff[op[bit]]);
	}

	res = min(res, graph2[last][0]);
	trace(last _ bitmask _ res)
	return dp[pp] = res;
}

int main()
{
	//ios_base::sync_with_stdio(0); 
	cin.tie(0); cout << fixed << setprecision(2);
	fout << fixed << setprecision(2);
	int t;
	cin >> t;
	while(t--)
	{
		int m; 
		cin >> n >> m; n++;
		For(i,0,n)
			For(j,0,n)
				graph1[i][j] = INT_MAX;
		For(i,0,n)
			graph1[i][i] = 0;
		For(i,0,m)
		{
			int u,v;
			int w1,w2,w;
			cin >> u >> v;
			if(scanf("%d.%d", &w1,&w2) == 1)
				w2 = 0;
			w = w1*100+w2;
			graph1[u][v] = graph1[v][u] = min(graph1[v][u], w);
		}

		cin >> p; p++;
		For(i,0,n)
			diff[i] = 0;
		For(i,0,p)
			op[i] = 0;
		For(i,0,p-1)
		{
			int a; cin >> a;
			int d,d1,d2;
			scanf("%d.%d",&d1,&d2);
			d = d1*100 + d2;
			diff[a] = d;
			op[i+1] = a;
		}
		
		dp.clear();

		///////////// Floyd
		For(k,0,n)
			For(i,0,n)
				For(j,0,n)
					if(graph1[i][k] != INT_MAX && graph1[k][j] != INT_MAX && graph1[i][j] > graph1[i][k] + graph1[k][j])
						graph1[i][j] = graph1[i][k] + graph1[k][j];

		For(i,0,p)
			For(j,0,p)
				graph2[i][j] = 0;

		//////////// Fill
		For(i,0,p)
			For(j,0,p)
				graph2[i][j] = graph1[op[i]][op[j]];


		int res = TSP(0,1);
		
		if(res < 0)
			fout << "Daniel can save $" << (double)-res / 100.0 << endl;
		else
			fout << "Don't leave the house" << endl;
	}
	return 0;
}
