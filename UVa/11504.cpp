/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <list>
#include <vector>
#include <queue>
#include <deque>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <math.h>
#include <algorithm>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

void dfs(int u, vector<vi>& graph, vi& visited)
{
	visited[u] = true;
	For(i,0,graph[u].size())
		if(!visited[graph[u][i]])
			dfs(graph[u][i],graph,visited);
}

void toposort(int u, vector<vi>& graph, vi& visited, vi& topo)
{
	visited[u] = true;
	For(i,0,graph[u].size())
		if(!visited[graph[u][i]])
			toposort(graph[u][i], graph, visited,topo);
	topo.push_back(u);	
}

int main()
{
	ios_base::sync_with_stdio(0);
	cin.tie(0);
	int t;
	cin >> t;
	while(t--)
	{
		int n,m;
		cin >> n >> m;
		vector<vi> graph(n);
		vi visited(n,0);
		For(i,0,m)
		{
			int u,v;
			cin >> u >> v; u--; v--;
			graph[u].push_back(v);
		}
		int res = 0;
		vi topo;
		For(i,0,n)
			if(!visited[i])
				toposort(i,graph,visited,topo);
		visited = vi(n,0);
		roF(i,n-1,0)
			if(!visited[topo[i]])
				dfs(topo[i],graph,visited), res++;
		cout << res << endl;
	}
}