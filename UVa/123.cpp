/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

int main()
{
	string igs;
	string line;
	multimap<string,string> st;

	while(getline(cin,line) && line != "::")
		igs += " " + line;
	
	while(getline(cin,line))
	{
		transform(allof(line),line.begin(),::tolower);
		For(i,0,line.size())
		{
			string word;
			if(!isalpha(line[i]))
				continue;
			int j = i;
			while(j < line.size() && isalpha(line[j]))
				word += line[j], j++;
			
			if(igs.find(word) == -1)
			{
				string linecp = line;
				string upperword = word;
				transform(allof(upperword),upperword.begin(), ::toupper);
				linecp.replace(i,word.size(),upperword);
				st.insert({word,linecp});
			}
			i = j;
		}
	}
	for(auto it : st)
		cout << it.second << endl;
}