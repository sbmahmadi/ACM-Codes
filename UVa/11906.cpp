/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>
#include <array>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

int grid[105][105];

int r, c, m, n, w, x, y;
int res[2];
array<int,8> dx;
array<int,8> dy;

void dfs(int x, int y)
{
	grid[x][y] = -2;
	int cnt = 0;
	For(i,0,((n > 0 && m > 0 && n != m) ? 8 : 4))
	{
		int nx = x + dx[i];
		int ny = y + dy[i];
		if(nx >= 0 && ny >= 0 && nx < r && ny < c && grid[nx][ny] != -1)
		{
			cnt++;
			if(grid[nx][ny] == 0)
				dfs(nx,ny);
		}
	}
	res[cnt % 2]++;
}

int main()
{
	ios_base::sync_with_stdio(0);
	cin.tie(0);
	int t;
	cin >> t;
	For(cs,1,t+1)
	{
		res[0] = res[1] = 0;
		memset(grid,0,sizeof grid);
		cin >> r >> c >> m >> n >> w;
		if(n == 0)
			swap(n,m);
		if(n > 0 && m > 0)
			if(n != m)
				dx = {n,n, -n,-n,m,m,-m,-m},
				dy = {m,-m,m,-m,n,-n,n,-n};
			else
				dx = {n,-n,n,-n},
				dy = {n,n,-n,-n};
		else
			dx = {n,-n,0,0},
			dy = {0,0,n,-n};

		For(i,0,w)
		{
			cin >> x >> y;
			grid[x][y] = -1;
		}
		dfs(0,0);
		cout << "Case " << cs << ": " << res[0] << " " << res[1] << endl;
	}
}