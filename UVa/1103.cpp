#include <iostream>
#include <iomanip>
#include <algorithm>
#include <set>
#include <cstring>
#include <fstream>
#include <queue>

#define For(i,a,b) for(int i = a; i < b; i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)

using namespace std;

typedef pair<int,int> ii;

const string sym = "WAKJSD";
set<int> st;
const int dx[4] = {1,-1,0,0};
const int dy[4] = {0,0,1,-1};
int h,w;
int grid[205][205];

void flood(int x,int y, int m1, int m2)
{
	queue<ii> q;
	q.push({x,y});
	grid[x][y] = m2;
	while(!q.empty())
	{
		ii p = q.front(); q.pop();
		x = p.first; y = p.second;
		For(i,0,4)
		{
			int nx = x + dx[i];
			int ny = y + dy[i];
			if(nx >= 0 && nx < h + 2 && ny >= 0 && ny < w+2)
			{
				if(grid[nx][ny] < 0)
					st.insert(grid[nx][ny]);
				if(grid[nx][ny] == m1)
				{
					grid[nx][ny] = m2;
					q.push({nx,ny});
				}
			}
		}	
	}
}

int main()
{
	ios_base::sync_with_stdio(0);
	cin.tie(0);
	int cs = 0;
	while(cin >> h >> w && h)
	{
		memset(grid,0,sizeof grid);
		For(i,0,h)
			For(j,0,w)
			{
				char c;
				cin >> c;
				string t;
				t+=c;
				int a = stoi(t,nullptr,16);
				roF(k,3,0)
				{
					grid[i+1][j*4+k+1] = a&1;
					a >>=1;
				}
			}
		w*=4;
		int cmp = -1;
		For(i,0,h+2)
			For(j,0,w+2)
				if(grid[i][j] == 0)
					flood(i,j,0,cmp--);
		string res;
		For(i,0,h+2)
			For(j,0,w+2)
				if(grid[i][j] == 1)
				{
					st.clear();
					flood(i,j,1,2);
					res += sym[st.size()-1];
				}
		sort(res.begin(), res.end());
		cout << "Case " << ++cs << ": " << res << endl;
	}
}