/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0);
#define trace(x) cerr << #x << ": " << x << endl;
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl;
#define _ << " :: " <<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

void solve(vi& p, vi& b, vi& d)
{
	int j = 0;
	For(i,0,d.size())
	{
		p[j] = min(100, p[j] + d[i]);
		p[j] += b[p[j]];
		if(p[j] == 100)
			return;
		j++; j%= p.size();
	}
}

int main()
{
	int t;
	cin >> t;
	while(t--)
	{
		int p, s, d;
		cin >> p >> s >> d;
		vi player(p,1);
		vi board(101,0);
		vi die(d);
		For(i,0,s)
		{
			int u,v;
			cin >> u >>v;
			board[u] = v-u;
		}
		For(i,0,d)
			cin >> die[i];
		solve(player,board,die);
		For(i,0,p)
			cout << "Position of player " << i+1 << " is " << player[i] << "." << endl;
	}	
}