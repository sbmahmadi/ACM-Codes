/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int main()
{
	/*ifstream fin("file.in");
	ofstream fout("file.out");*/
	int t;
	cin >> t;
	while(t--)
	{
		int n;
		cin >> n;
		vector<vi> grid(n);
		vi vals;
		For(i,0,n)
		{
			grid[i].assign(n,0);
			For(j,0,n)
			{
				cin >> grid[i][j];
			}
		}
		vi perm;
		For(i,0,n)
		{	
			perm.push_back(i);
		}

		do
		{
			int a = 0;
			For(i,0,n)
			{
				a += grid[i][perm[i]];
			}
			vals.push_back(a);
		} while(next_permutation(allof(perm)));

		cout << *min_element(allof(vals)) << endl;
	}	
}
