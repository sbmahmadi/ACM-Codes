/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0);
#define trace(x) cerr << #x << ": " << x << endl;
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl;
#define _ << " :: " <<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

vi par;

int root(int u)
{
	if(par[u] == u)
		return u;
	return par[u] = root(par[u]);
}

void merge(int u, int v)
{
	if(root(u) == root(v))
		return;
	par[root(u)] = root(v);
}

int main()
{
	int t;
	cin >> t;
	while(t--)
	{
		int n;
		cin >> n;
		par.assign(n,0);
		For(i,0,n)
			par[i] = i;
		cin.ignore();
		char c;
		int u,v;
		string s;
		int to = 0, ok = 0;
		while(getline(cin,s) && s != "")
		{
			stringstream ss(s);
			ss >> c >> u >> v; u--; v--;
			
			if(c == 'c')
				merge(u,v);
			else
				to++, ok += root(u) == root(v); 	
		}
		cout << ok << "," << to-ok << endl;
		if(t)
			cout << endl;
	}	
}