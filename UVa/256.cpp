#include<iostream>
#include<vector>
#include<math.h>
#include<string>
using namespace std;

int main()
{
	vector<long int> squares;
	for (int i = 0; i <= 10000; i++) squares.push_back(i*i);

	int k;
	while (cin >> k)
	{
		long ten = pow(10, k / 2);
		for (int i = 0; squares[i] < pow(10, k); i++)
		{
			if (pow(squares[i] % ten + squares[i] / ten, 2) == squares[i])
			{
				switch (k)
				{
				case 2: printf("%02d\n", squares[i]); break;
				case 4: printf("%04d\n", squares[i]); break;
				case 6: printf("%06d\n", squares[i]); break;
				case 8: printf("%08d\n", squares[i]); break;
				}
			}
		}
	}
}