/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0);
#define trace(x) cerr << #x << ": " << x << endl;
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl;
#define _ << " :: " <<
#define allof(x) (x).begin(),(x).end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

int main()
{
	int n;
	cout << "PERFECTION OUTPUT" << endl;
	while(cin >> n && n != 0) {
		int sum = 0;
		For(i,1,n/2+1) {
			if(i == n)
				continue;
			if(n%i == 0)
				sum += i;
		}
		cout << setw(5) << right << n << "  ";
		if(sum < n)
			cout << "DEFICIENT" << endl;
		if(sum > n)
			cout << "ABUNDANT" << endl;
		if(sum == n) 
			cout << "PERFECT" << endl;
	}	
	cout << "END OF OUTPUT" << endl;
}