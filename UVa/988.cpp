/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <list>
#include <vector>
#include <queue>
#include <deque>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <math.h>
#include <algorithm>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

int res;

int  dfs(int u, vector<vi>& graph, vi& visited, vi& dp)
{
	if(visited[u])
		return dp[u];
	if(graph[u].size() == 0)
		return 1 ;
	int res = 0;
	For(i,0,graph[u].size())
	{
		int v = graph[u][i];
			res += dfs(v, graph, visited, dp);
	}
	visited[u] = true;
	return dp[u] = res;
}

int main()
{
	int n;
	int t = 0;
	while(cin >> n)
	{
		if(t++)
			cout << endl;
		res = 0;
		vector<vi> graph(n);
		For(i,0,n)
		{
			int m;
			cin >> m;
			For(j,0,m)
			{
				int u;
				cin >> u;
				graph[i].push_back(u);
			}
		}
		
		cout << dfs(0,graph, *(new vi(n,0)), *(new vi(n,0))) << endl;
	}
}