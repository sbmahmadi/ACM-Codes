#include<iostream>
#include<vector>
#include<algorithm>
#include<string.h>

using namespace std;

int dp[1005][35];

int main()
{
	int t;
	cin >> t;
	while (t--)
	{
		memset(dp, 0, sizeof dp);
		int n;
		cin >> n;
		vector<int> w(n), p(n);
		for (int i = 0; i < n; i++)
			cin >> p[i] >> w[i];
		
		int g;
		cin >> g;
		vector<int> mw(g);
		for (int i = 0; i < g; i++)
			cin >> mw[i];
		int maxw = *max_element(mw.begin(), mw.end());
		
		for (int i = 1; i <= n; i++)
		{
			for (int ww = 0; ww <= maxw; ww++)
			{
				if (ww >= w[i - 1])
					dp[i][ww] = max(dp[i - 1][ww], p[i - 1] + dp[i - 1][ww - w[i - 1]]);
				else
					dp[i][ww] = dp[i - 1][ww];
			}
		}
		int sum = 0;
		for (int i = 0; i < mw.size();i++)
		{
			sum += dp[n][mw[i]];
		}
		cout << sum << endl;
	}
}