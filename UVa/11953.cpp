/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

int n;
string grid[105];

const int dx[] = {1,-1,0,0};
const int dy[] = {0,0,1,-1};

void flood(int x, int y)
{
	grid[x][y] = 'K';
	For(i,0,4)
	{
		int nx = x + dx[i];
		int ny = y + dy[i];
		if(nx >= 0 && nx < n && ny >= 0 && ny < n && (grid[nx][ny] == 'x' || grid[nx][ny] == '@'))
			flood(nx,ny);
	}
}

int main()
{
	int t;
	cin >> t;
	For(tt,1,t+1)
	{
		cin >> n;
		For(i,0,n)
			cin >> grid[i];
		int res = 0;
		For(i,0,n)
			For(j,0,n)
				if(grid[i][j] == 'x')
					flood(i,j), res++;
		cout << "Case " << tt << ": " << res << endl;
	}
}