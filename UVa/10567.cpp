/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

int eval(char c)
{
	if(c>='A' && c <= 'Z')
		return c - 'A';
	else return c-'a' + 26;
}

vi data[52];

void solve(const string& ss)
{
	int ptr = -1;
	int first,last;
	For(i,0,ss.size())
	{
		auto res = upper_bound(allof(data[eval(ss[i])]),ptr);
		if(res == data[eval(ss[i])].end())
		{
			cout << "Not matched" << endl;
			return;
		}
		ptr = *res;
		if(i == 0)
			first = *res;
		if(i == ss.size()-1)
			last = *res;
	}
	cout << "Matched " << first << " " << last << endl;
}

int main()
{
	string s;
	cin >> s;
	int q;
	cin >> q;
	
	For(i,0,s.size())
		data[eval(s[i])].push_back(i);
	
	For(i,0,q)
	{
		string ss;
		cin >> ss;
		solve(ss);
	}	
}
