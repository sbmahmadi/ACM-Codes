/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

int fav[110];
int price[110];
int n, m;
int dp[110][10010];

int knapsack(int id, int cost)
{
	if(cost > 2000 && cost > m + 200) 
		return -5000;
	if(id == n)
	{
		if(cost <= m && cost <= 2000)
			return 0;
		if(cost <= m + 200 && cost > 2000)
			return 0;
		return -5000;
	}
	
	int &res = dp[id][cost];
	if(res >= 0) return res;
	return res = max(knapsack(id+1, cost), knapsack(id+1, cost + price[id]) + fav[id]);
}

int main()
{
	ios_base::sync_with_stdio(0); 
	cin.tie(0); cout << fixed;
	while(cin >> m >> n)
	{
		memset(dp, -1, sizeof dp);
		For(i,0,n)
			cin >> price[i] >> fav[i];

		cout << knapsack(0,0) << endl;
	}
}