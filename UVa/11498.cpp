/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

int sgn(int x)
{
	return !(x < 0); 
}

string s[2][2] = {"SO", "NO" , "SE", "NE"};

int main()
{
	int k;
	while(cin >> k && k)
	{
		int x,y;
		cin >> x >> y;
		For(i,0,k)
		{
			int x1,y1;
			cin >> x1 >> y1;
			if(x1 == x || y1 == y)
				cout << "divisa" << endl;
			else
				cout << s[sgn(x1-x)][sgn(y1-y)] << endl;
		}
	}
}