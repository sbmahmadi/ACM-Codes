/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <list>
#include <vector>
#include <queue>
#include <deque>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <math.h>
#include <algorithm>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

map<char,int> ID;
map<int,char> name; 

int getID(char c)
{
	return ID.insert({c,ID.size()}).first->second;
}
bool hasansw;

void printAll(vector<vi> &graph, vi& in, vector<bool> &visit,vi &res)
{
	bool flag = false;
	For(i,0,graph.size())
	{
		if(!visit[i] && in[i] == 0)
		{
			flag = true;
			
			visit[i] = true;
			For(j,0,graph[i].size())
				in[graph[i][j]]--;
			res.push_back(i);
			
			printAll(graph,in,visit,res);
			
			res.pop_back();
			For(j,0,graph[i].size())
				in[graph[i][j]]++;
			visit[i] = false;
		}
	}
	if(!flag && res.size() == graph.size())
	{
		hasansw = true;
		cout << name[res[0]];
		For(i,1,res.size())
			cout << " " << name[res[i]];
		cout << endl;
	}
}

int main()
{
	int t; cin >> t;
	cin.ignore();
	while(t--)
	{
		cin.ignore();
		hasansw = false;
		ID.clear();
		name.clear();
		string s;
		getline(cin,s);
		stringstream ss;
		
		sort(allof(s));
		s.erase(unique(allof(s)),s.end());

		int n = 0;
		For(i,0,s.size())
		{
			if(s[i] == ' ') continue;
			n++;
			name[getID(s[i])] = s[i];
		}
		vector<vi> graph(n);
		vi in(n);
		getline(cin,s);
		ss = stringstream(s);
		while(ss >> s)
		{
			graph[getID(s[0])].push_back(getID(s[2]));
			in[getID(s[2])]++;
		}
		vi res;
		vector<bool> visit(n,0);
		printAll(graph,in,visit,res);
		if(!hasansw)
			cout << "NO" << endl;
		if(t!=0)
			cout << endl;
	}
}