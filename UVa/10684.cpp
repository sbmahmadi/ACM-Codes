#include<iostream>

#define For(i,a,b) for(int i = a;i<b;i++)

using namespace std;

int arr[10005];

int main()
{
	int n;
	while (cin >> n && n)
	{
		arr[0] = 0;
		For(i, 1, n+1)
		{
			cin >> arr[i];
			arr[i] += arr[i - 1];
		}
		int max = -1;
		For(i, 1, n + 1)
		{
			For(j, 0, i)
			{
				if (arr[i] - arr[j] > max) max = arr[i] - arr[j];
			}
		}
		if (max <= 0) cout<<"Losing streak.";
		else cout<<"The maximum winning streak is "<< max <<".";
		cout << endl;
	}
}