/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

int main()
{
	int c;
	cin >> c;
	while(c--)
	{
		int n,m;
		cin >> n >> m; 
		vector<vector<ii>> graph(n);
		For(i,0,m)
		{
			int a,b,t;
			cin >> a >> b >> t;
			graph[a].push_back(make_pair(b,t));
		}
		vi dist;
		dist.assign(n,-1);
		dist[0] = 0;
		For(k,0,n-1)
			For(i,0,n)
			{
				if(dist[i] == -1)
					continue;
				For(j,0,graph[i].size())
				{
					int child = graph[i][j].first;
					int w = graph[i][j].second;
					if(dist[child] == -1 || dist[child] > dist[i] + w)
					dist[child] = dist[i] + w; 
				}
			}
		bool canRelax = false;
		For(i,0,n)
		{
			For(j,0,graph[i].size())
			{
				int child = graph[i][j].first;
				int w = graph[i][j].second;
				if(dist[child] > dist[i] + w)
					canRelax = true;
			}
		}
		cout << (canRelax ? "possible" : "not possible") << endl;
	}
}