/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <list>
#include <vector>
#include <queue>
#include <deque>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <math.h>
#include <algorithm>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

vector<vi> grid,dist;
int dx[] = {1,-1,0,0};
int dy[] = {0,0,-1,1};
struct cmp
{
	bool operator()(const ii a, const ii b) const
	{return dist[a.first][a.second] < dist[b.first][b.second];}
};

int main()
{
	int t;
	cin >> t;
	while(t--)
	{	
		int n,m;
		cin >> n >> m;
		
		grid = vector<vi>(n,vi(m)), dist = vector<vi>(n,vi(m,1e8));
		For(i,0,n)
			For(j,0,m)
				cin >> grid[i][j];
		dist[0][0] = grid[0][0];
		multiset<ii,cmp> pq;
		pq.insert({0,0});
		
		while(!pq.empty())
		{
			ii u = *pq.begin(); pq.erase(pq.begin());
			For(i,0,4)
			{	
				int nx = u.first + dx[i];
				int ny = u.second + dy[i];
				if(nx>=0 && nx < n && ny >= 0 && ny < m && dist[nx][ny] > dist[u.first][u.second] + grid[nx][ny])
				{
					if(pq.count({nx,ny}))
						pq.erase({nx,ny});
					dist[nx][ny] = dist[u.first][u.second] + grid[nx][ny];
					pq.insert({nx,ny});
				}
			}
		}
		cout << dist[n-1][m-1] << endl;
	}
}