import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Scanner;

public class Main
{
   public static void main(String args[])
   {
       Scanner scanner = new Scanner(System.in);
       while (scanner.hasNext())
       {
           BigInteger max = BigInteger.valueOf(-999999);
           BigInteger[] array = new BigInteger[110];
           ArrayList<Integer> zeros = new ArrayList<Integer>(110);
           array[0] = BigInteger.valueOf(1);
           int n = 1;
           while(true)
           {
               int a = scanner.nextInt();
               if(a == -999999)
                   break;
               max = max.max(BigInteger.valueOf(a));
               if(a == 0) {
                   zeros.add(n);
                   a = 1;
               }
               array[n] = BigInteger.valueOf(a);
               n++;
           }

           for(int i = 1; i < n; i++)
           {
                array[i] = array[i].multiply(array[i-1]);
           }
           for(int i = 1; i < n; i++)
           {
               for(int j = 0; j < i-1; j++)
               {
                   BigInteger val = array[i].divide(array[j]);
                   for(int z :zeros)
                   {
                       if(z<=i && z >j)
                           val = BigInteger.ZERO;
                   }
                   max = max.max(val);
               }
           }
           System.out.println(max);
       }
   }
}