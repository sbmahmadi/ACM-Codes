/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

int N,M,K;

ll dp[55][55][55];

ll f(int n, int l, int k)
{
	if(n == N+1)
	{
		if(k == K)
			return 1;
		return 0;
	}
	ll &res = dp[n][l][k];
	if(res >= 0)
		return res;
	if(l == M)
		return res = f(n+1,1,k+1);
	else 
		return res = f(n+1,l+1,k) + f(n+1,1,k+1);
}

int main()
{
	ios_base::sync_with_stdio(0); 
	cin.tie(0); cout << fixed;
	while(cin >> N >> K >> M)
	{
		memset(dp,-1,sizeof dp);
		fout << f(2,1,1) << endl;
	}
}