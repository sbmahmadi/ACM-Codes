/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0);
#define trace(x) cerr << #x << ": " << x << endl;
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl;
#define _ << " :: " <<
#define allof(x) (x).begin(),(x).end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

		map<string,pair<string,bool>> mp;
bool isnum(string& s){
	for(char c : s)
		if(isalpha(c))
			return false;
	return true;
}

int main()
{
	int t;
	cin >> t;
	For(tt,0,t){
		mp.clear();
		int n;
		cin >> n;
		string var, n1,op,n2;
		For(i,0,n){
			cin >> var;
			cin >> n1;
			cin >> n1 >> op >> n2;
			if(op == "*")
				mp[var] = (need(n1))
			else
				mp[var] = (isnum(n1) ? n1 : mp[n1]) + op + (isnum(n2) ? n2 : "(" + mp[n2] + ")");
			trace(var _ mp[var]);
		}
		cout << "Expression #" << tt+1 << ": " << mp[var] << endl;
	}	
}
