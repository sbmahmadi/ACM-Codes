/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

bool visited[105];
bool color[105];

int res;
int maxres;
vi myres;

void solve(int node, vector<vi>& graph)
{
	if(node == graph.size())
		return;
	visited[node] = true;
	
	bool hasBlack = false;
	For(i,0,graph[node].size())
		if(color[graph[node][i]])
			hasBlack = true;

	if(!hasBlack)
	{
		color[node] = 1;
		res += 1;
		if(res > maxres)
		{
			maxres = res;
			myres.resize(0);
			For(i,0,graph.size())
				if(color[i])
					myres.push_back(i);
		}
		solve(node+1,graph);
		res -=1;
	}
	color[node] = 0;
	solve(node+1,graph);
	
	visited[node] = false;
}

int main()
{
	int m;
	cin >> m;
	while(m--)
	{
		int n,k;
		cin >> n >> k;
		memset(color,0,sizeof color);
		memset(visited,0,sizeof visited);
		vector<vi> graph(n);
		For(i,0,k)
		{
			int a,b;
			cin >> a >> b;
			graph[a-1].push_back(b-1);
			graph[b-1].push_back(a-1);
		}
		res = maxres = 0;

		solve(0,graph);
		cout << maxres << endl;
		cout << myres[0] +1;
		For(i,1,myres.size())
		{
			cout << " "<< myres[i] +1 ;
		}
		cout << endl;
	}
}
