/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <list>
#include <vector>
#include <queue>
#include <deque>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <math.h>
#include <algorithm>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

int T[5];
int dist[110];

struct cmp
{
	bool operator()(const int a, const int b) const
	{
		if(dist[a] != dist[b])
			return dist[a] < dist[b];
		return a < b;
	}
};

int main()
{
	int n,k;
	while(cin >> n >> k)
	{
		For(i,0,n)
			cin >> T[i];
		cin.ignore();
		vector<vi> elevs(n);
		For(i,0,n)
		{
			string s;
			getline(cin,s);
			stringstream ss(s);
			int a;
			while(ss >> a)
				elevs[i].push_back(a);
		}

		set<int,cmp> pq;
		fill(dist, dist + 100, 1e8);
		dist[0] = 0;
		pq.insert(0);
		
		while(!pq.empty())
		{
			int u = *pq.begin(); pq.erase(pq.begin());
			if(u == k)
				break;
			For(i,0,n)
			{
				if(count(allof(elevs[i]), u))
					For(j,0,elevs[i].size())
					{
						int v = elevs[i][j];
						if(dist[v] > dist[u] + 60 + abs(u - v) * T[i])
						{
							if(pq.count(v))
								pq.erase(v);
							
							dist[v] = dist[u] + 60 + abs(u - v) * T[i];
							pq.insert(v);
							
						}
					}
			}
		}
		if(dist[k] == 1e8)
			cout << "IMPOSSIBLE" << endl;
		else 
			cout << max(dist[k] - 60, 0) << endl;
	}
}