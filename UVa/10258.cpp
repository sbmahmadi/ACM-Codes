/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <list>
#include <vector>
#include <queue>
#include <deque>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <math.h>
#include <algorithm>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

struct cont
{
	bool solved[10];
	int id;
	int tried[10];
	int solve;
	int pen;
	cont()
	{
		memset(solved,0,sizeof solved);
		memset(tried,0,sizeof tried);
		id = 0;
		solve = 0;
		pen = 0;
	}

	bool operator<(const cont& c) const
	{
		if(solve != c.solve)
			return solve > c.solve;
		if(pen != c.pen)
			return pen < c.pen;
		return id < c.id;
	}
};

int main()
{
	int t;
	cin >> t;
	cin.ignore(2);
	while(t--)
	{
		string s;
		map<int,cont> v;
		while(getline(cin,s) && s != "")
		{
			stringstream ss(s);
			int c,p,t;
			char l;
			ss >> c >> p >> t >> l;
			
			if(l=='C')
			{
				if(!v[c].solved[p])
				{
					v[c].solved[p] = true;
					v[c].solve++;
					v[c].pen += v[c].tried[p] * 20 + t;
				}
			}
			else if(l == 'I')
				v[c].tried[p]++;
			v[c].id = c;
		}
		set<cont> st;

		for(pair<const int,cont>& a : v)
			st.insert(a.second);
		for(const cont& a : st)
		{
			cout << a.id << " " << a.solve << " " << a.pen << endl;
		}
		if(t)
			cout << endl;
	}	
}
