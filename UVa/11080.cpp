/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <list>
#include <vector>
#include <queue>
#include <deque>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <math.h>
#include <algorithm>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

bool bip;

int Bipartite(int u, vector<vi> &graph, vi &visit)
{
	vi color(graph.size(),0);
	int c[2] = {};
	queue<int> q;

	visit[u] = true;
	q.push(u);
	c[0]++;
	while(!q.empty())
	{
		u = q.front(); q.pop();
		For(i,0,graph[u].size())
		{
			if(visit[graph[u][i]] && color[u] == color[graph[u][i]])
				{bip = false;
				return 0;}
			else if(!visit[graph[u][i]])
			{
				color[graph[u][i]] = color[u] ^ 1;
				visit[graph[u][i]] = true;
				c[color[u] ^ 1]++;
				q.push(graph[u][i]);				
			}
		}
	}
	return max(min(c[0],c[1]),1);
}

int main()
{
	int t;
	cin >> t;
	while(t--)
	{
		int n,m;
		cin >> n >> m;
		vector<vi> graph(n);
		
		For(i,0,m)
		{
			int u,v;
			cin >> u >> v;
			graph[u].push_back(v);
			graph[v].push_back(u);
		}
		vi visit(n,0);
		bip = true;
		int res = 0;
		For(i,0,n)
			if(!visit[i])
				res += Bipartite(i,graph,visit);
				
		cout << (bip ? res : -1) << endl;
	}
}