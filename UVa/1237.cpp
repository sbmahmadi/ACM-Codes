/*
ID: behdad.1
LANG: C++11
PROB:
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int main()
{
	/*ifstream fin("file.in");
	ofstream fout("file.out");*/
	int t;
	cin >> t;
	while (t--)
	{
		int d;
		cin >> d;
		vector<string> names;
		vi lo, hi;
		For(i, 0, d)
		{
			string m;
			int l, h;
			cin >> m >> l >> h;
			names.push_back(m);
			lo.push_back(l);
			hi.push_back(h);
		}

		int q;
		cin >> q;
		while (q--)
		{
			int p;
			cin >> p;
			int maxi = 0;
			int cnt = 0;
			For(i, 0, d)
			{
				if (p >= lo[i] && p <= hi[i])
				{
					maxi = i;
					cnt++;
				}
			}
			if (cnt == 1)
				cout << names[maxi] << endl;
			else
				cout << "UNDETERMINED" << endl;
		}
		if(t!=0)
			cout << endl;
	}
}
