/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

#define interval 100000

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

int getval(int lq, int rq, int h, int l, int r,int ind, vi& seg, vi& lazy)
{
	if(lazy[ind] != 0)
	{
		if(seg[ind] != -1)
			seg[ind] = max(seg[ind], lazy[ind]);
		if(r != l)
		{
			lazy[ind*2] = max(lazy[ind*2], lazy[ind]);
			lazy[ind*2+1] = max(lazy[ind*2+1], lazy[ind]);
		}
		lazy[ind] = 0;
	}

	if(l >= lq && r <= rq && seg[ind] != -1)
	{
		if(seg[ind] <= h)
			return r - l + 1;
		else return 0;
	}
	if(r < lq || l > rq)
		return 0;
	return getval(lq,rq,h,l,(r+l) / 2, ind * 2, seg, lazy) + getval(lq, rq, h, (r+l)/2+1, r, ind*2 + 1, seg, lazy); 
}

void update(int lq, int rq, int h, int l, int r, int ind, vi& seg, vi& lazy)
{
	if(lazy[ind] != 0)
	{
		if(seg[ind] != -1)
			seg[ind] = max(seg[ind], lazy[ind]);
	
		if(r != l)
		{
			lazy[ind*2] = max(lazy[ind*2], lazy[ind]);
			lazy[ind*2+1] = max(lazy[ind*2+1], lazy[ind]);
		}
	
		lazy[ind] = 0;
	}
	if(l >= lq && r <= rq && seg[ind] != -1)
	{
		seg[ind] = max(seg[ind],h);
		if(r != l)
		{
			lazy[ind*2+1] = max(lazy[ind*2+1],h);
			lazy[ind*2] = max(lazy[ind*2],h);
		}
		return;
	}
	if(r < lq || l > rq)
		return;
	update(lq,rq,h,l,(l+r)/2,ind*2,seg,lazy);
	update(lq,rq,h,(l+r)/2+1,r,ind*2+1,seg,lazy);
	if(seg[ind*2] == seg[ind*2+1])
		seg[ind] = seg[ind*2];
	else seg[ind] = -1;
}

int main()
{
	int t;
	cin >> t;
	while(t--)
	{
		int nn = pow(2,ceil(log2(interval)));
		vi seg(nn*2,0);
		vi lazy(nn*2,0);
		int n;
		cin >> n;
		int sum = 0;
		For(i,0,n)
		{
			int l,r,h;
			cin >> l >> r >> h;
			sum += getval(l-1,r-2,h,0,nn-1,1,seg,lazy);
			update(l-1,r-2,h,0,nn-1,1,seg,lazy);
		}
		cout << sum << endl;
	}	
}