/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

vi nums;
vi sol;
int t,n;

ifstream fin("file.in");
ofstream fout("file.out");

bool resFound;

bool solve(int index, int sum)
{
	bool res = false;
	int i = index;
	while(i<n)
	{
		if(sum + nums[i] == t)
		{
			resFound = true;
			sol.push_back(nums[i]);
			
			cout << sol[0];
			For(j,1,sol.size())
				cout << "+" << sol[j];
			cout << endl;

			sol.pop_back();
		}
		if(sum + nums[i] >= t)
		{
			res = true;
			while(i < n-1 && nums[i] == nums[i+1])
			{
				i++;
			}
		}
		else
		{
			sol.push_back(nums[i]);
			if(solve(i+1, sum + nums[i]))
			{
				res = true;
				while(i < n-1 && nums[i] == nums[i+1])
				{
					i++;
				}
			}
			sol.pop_back();
		}
		i++;
	}
	return res;
}

int main()
{
	while(cin >> t >> n && n)
	{
		resFound = false;
		nums.resize(n);
		For(i,0,n)
			cin >> nums[i];
		cout << "Sums of " << t << ":" << endl;
		solve(0, 0);
		if(!resFound)
			cout << "NONE" << endl;
	}	
}