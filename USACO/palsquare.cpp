/*
ID: behdad.1
PROB: palsquare
LANG: C++11
*/


#include<iostream>
#include<fstream>
#include<vector>
#include<string>
#include<algorithm>

using namespace std;

string baseB(int n, int base)
{
	string s;
	while (n > 0)
	{
		int t = n%base;
		if (t < 10)
		{
			s += '0' + t;
		}
		else s += 'A' + t-10;
		n /= base;
	}
	reverse(s.begin(), s.end());
	return s;
}

bool isPal(string s)
{
	for (int i = 0; i < s.size() / 2; i++)
	{
		if (s[i] != s[s.size() - 1 - i])
			return false;
	}
	return true;
}

int main()
{
	ifstream fin("palsquare.in");
	ofstream fout("palsquare.out");
	int base;
	fin >> base;
	for (int i = 1; i <= 300; i++)
	{
		if (isPal(baseB(i*i, base)))
		{
			fout << baseB(i, base) << " " << baseB(i*i, base) << endl;
		}
	}
}