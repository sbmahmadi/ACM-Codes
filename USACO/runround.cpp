/*
ID: behdad.1
LANG: C++11
PROB: runround 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0);
#define trace(x) cerr << #x << ": " << x << endl;
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl;
#define _ << " :: " <<
#define allof(x) (x).begin(),(x).end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef long double ld;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<double> vd;

bool isRunaround(const string &s) {
	bool visit[10] = {};
	int ind = 0;
	For(i,0,s.size()) {
		if(visit[ind])
			return false;
		visit[ind] = true;
		ind = (ind + s[ind] - '0') % s.size();
	}
	return ind == 0;
}

void prep(vector<int>& v, vector<bool>& visit, int cnt, string res) {
	if(!cnt) {
		if(isRunaround(res))
			v.push_back(stoi(res)); 
		return;
	}
	For(i,1,10)
		if(!visit[i]) {
			visit[i] = true;
			res += '0' + i;
			prep(v, visit, cnt-1, res);
			res.pop_back();
			visit[i] = false;
		}
}

int main()
{
	ifstream fin("runround.in");
	ofstream fout("runround.out");
	int n;
	fin >> n;
	vector<int> v;
	vector<bool> visit(10);
	string res;
	For(i,1,10)
		prep(v,visit,i,res);
	fout << *upper_bound(allof(v), n) << endl;
}