"""
ID: behdad.1
LANG: PYTHON3
PROB: zerosum 
"""
res = []
res2 = []

def solve(s, ind):
	if(ind == n+1):
		if(eval(s) == 0):
			res.append(s)
		return
	solve(s+str(ind), ind+1)
	solve(s+'+'+str(ind),ind+1)
	solve(s+'-'+str(ind),ind+1)


fin = open('zerosum.in','r')
fout = open('zerosum.out','w')
n = int(fin.readline())
solve('1',2)
for s in res:
	k = ''
	for i in range(len(s)-1):
		k += s[i]
		if((s[i].isdigit()) and (s[i+1].isdigit())):
			k += ' '
	k += s[-1]
	res2.append(k)
for s in sorted(res2):
	fout.write(s + '\n')