/*
ID: behdad.1
PROG: ride
LANG: C++11
*/

#include<iostream>
#include<string>
#include<fstream>

using namespace std;

#define For(i,a,b) for(int i = a; i <b ; i++)

int main()
{
	ifstream fin("ride.in");
	ofstream fout("ride.out");
	string comet, group;
	long cometVal = 1, groupVal = 1;
	fin >> comet >> group;

	For(i, 0, comet.size())
	{
		cometVal *= comet[i] - 'A' + 1;
	}
	For(i, 0, group.size())
	{
		groupVal *= group[i] - 'A' + 1;
	}
	if (groupVal % 47 == cometVal % 47) fout << "GO"<<endl;
	else fout << "STAY"<<endl;

}