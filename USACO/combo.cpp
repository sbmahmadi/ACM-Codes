/*
ID: behdad.1
PROB: combo
LANG: C++11
*/

#include<iostream>
#include<vector>
#include<algorithm>
#include<set>
#include<fstream>

#define For(i,a,b) for(int i = a;i<b;i++)

using namespace std;

bool diff(int a,int b,int n)
{
	return (abs(a - b) <= 2 || n - max(a, b) + min(a, b) <= 2);
}

bool f(int a[3], vector<int> v,int n)
{
	For(i, 0, 3)
	{
		if (!diff(a[i], v[i], n)) return false;
	}
	return true;
}

int main()
{
	ifstream fin("combo.in");
	ofstream fout("combo.out");

	int n;
	fin >> n;
	vector<int> FJ(3);
	vector<int> master(3);
	int res = 0;
	
	For(i, 0, 3)
	{
		fin >> FJ[i];
	}
	
	For(i, 0, 3)
	{
		fin >> master[i];
	}
	
	For(i, 1, n + 1)
		For(j, 1, n + 1)
			For(k, 1, n + 1)
			{
				int a[3] = { i,j,k };
				res += f(a, FJ, n) || f(a, master, n);
			}
		}
	}

	fout << res << endl;
}