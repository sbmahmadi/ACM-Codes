/*
ID: behdad.1
LANG: C++11
PROB: contact
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0)
#define trace(x) cerr << #x << ": " << x << endl
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl
#define _ << " :: " <<
#define allof(x) (x).begin(),(x).end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef long double ld;
typedef pair<int, int> ii;
typedef pair<double, double> dd;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<double> vd;
typedef vector<string> vs;

int main()
{
	ifstream fin("contact.in");
	ofstream fout("contact.out");
	int a,b,n;
	fin >> a >> b >> n;
	fin.ignore();
	string line, s;
	while(getline(fin,line))
		s += line;
	map<string,int> mp;
	For(i,a,min((int)b+1,(int)s.size()+1)) {
		For(j,0,s.size() - i + 1){
			mp[s.substr(j,i)]++;
		}
	}
	map<int, vector<string>> st;
	for(auto& it : mp)
		st[-it.second].push_back(it.first);
	int i = 0;
	for(auto it = st.begin(); it != st.end() && i < n; it++, i++) {
		sort(allof(it->second),[](string& s1, string& s2) -> bool {
			if(s1.size() != s2.size()) return s1.size() < s2.size();
			return s1 < s2;
		});
		fout << -it->first << endl;
		for(int j = 0; j < it->second.size(); j++) {
			if(j % 6) fout << " ";
			fout << it->second[j];
			if(j % 6 == 5 && j != it->second.size() -1) fout << endl;
		}
		fout << endl;
	}
}
