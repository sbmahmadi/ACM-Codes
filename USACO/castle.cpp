/*
ID: behdad.1
LANG: C++11
PROB: castle 
*/

#include <iostream>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <iomanip>
#include <fstream>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int grid[105][105];
int visited[105][105];

int c = 0;

int n, m;

int dx[4] = {0,-1,0,1};
int dy[4] = {-1,0,1,0};

void DFS(int i, int j, int p)
{
	visited[i][j] = p;
	c++;
	For(k,0,4)
	{
		if(!(grid[i][j] >> k & 1))
			if(visited[i + dx[k]][j + dy[k]] < 0)
				DFS(i + dx[k], j + dy[k], p);
	}
}

int main()
{
	ifstream fin("castle.in");
	ofstream fout("castle.out");
	vector<int> comps; 
	memset(visited, -1, sizeof visited);
	fin >> m >> n;
	For(i,0,n)
		For(j,0,m)
			fin >> grid[i][j];
	For(i, 0, n)
		For(j, 0, m)
			if (visited[i][j] < 0)
			{
				c = 0;
				DFS(i, j,comps.size());
				comps.push_back(c);
			}
	int mx = 0;
	int mxi = 0, mxj = 0;
	char mxc = 'a';
	
	For(j, 0, m)
		roF(i, n-1, 0)
		{
			if(i !=0 && visited[i][j] != visited[i-1][j] && comps[visited[i][j]] + comps[visited[i-1][j]] > mx)
			{

				mx = comps[visited[i][j]] + comps[visited[i-1][j]];
				mxi = i;
				mxj = j;
				mxc = 'N';
			}
			if(j != m-1 && visited[i][j] != visited[i][j+1] && comps[visited[i][j]] + comps[visited[i][j+1]] > mx)
			{
				mx = comps[visited[i][j]] + comps[visited[i][j+1]];
				mxi = i;
				mxj = j;
				mxc = 'E';
			}
		}
	fout << comps.size() << endl << *max_element(allof(comps)) << endl << mx << endl << mxi + 1 << " " << mxj + 1  << " " << mxc << endl;
}