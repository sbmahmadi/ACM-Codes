"""
ID: behdad.1
LANG: PYTHON3
PROB: lamps
"""

def f1():
	for i in range(len(lamps)):
		lamps[i] ^= 1

def f2():
	for i in range(0,len(lamps),2):
		lamps[i] ^= 1

def f3():
	for i in range(1,len(lamps),2):
		lamps[i] ^= 1

def f4():
	for i in range(0,len(lamps),3):
		lamps[i] ^= 1

def search(lvl):
	if(lvl == 5):
		return
	res[lvl].append(list(lamps))
	f1(); search(lvl+1); f1();
	f2(); search(lvl+1); f2();
	f3(); search(lvl+1); f3();
	f4(); search(lvl+1); f4();

def ok(lst):
	for i in onL:
		if(lst[i-1] == 0):
			return False
	for i in offL:
		if(lst[i-1] == 1):
			return False
	return True

fin = open("lamps.in",'r')
fout = open("lamps.out",'w')

n = int(fin.readline())
c = int(fin.readline())

onL = [int(x) for x in fin.readline().split(' ')]
onL.pop()

offL = [int(x) for x in fin.readline().split(' ')]
offL.pop()

lamps = [1] * n
res = []
for i in range(5):
	res.append([])
finalres = []

search(0)

for lst in res[min(c,4)]:
	if(ok(lst)):
		finalres.append("".join(str(x) for x in lst))
finalres = list(set(finalres))
if(len(finalres) == 0):
	fout.write("IMPOSSIBLE\n")
for s in sorted(finalres):
	fout.write(s + '\n')