/*
ID: behdad.1
LANG: C++11
PROB: preface
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0);
#define trace(x) cerr << #x << ": " << x << endl;
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl;
#define _ << " :: " <<
#define allof(x) (x).begin(),(x).end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef long double ld;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<double> vd;

string chars = "IVXLCDMK";

map<char,int> mp;

void f(int n) {
	int ind = 0;
	while(n) {
		int d = n % 10; n/=10;
		if(d == 0);
		else if(d < 4)
			For(i,0,d)
				mp[chars[ind]]++;
		else if(d == 4){
			mp[chars[ind+1]]++; mp[chars[ind]]++;
		} else if(d < 9) {
			For(i,0,d-5)
				mp[chars[ind]]++;
			mp[chars[ind+1]]++;
		} else {
			mp[chars[ind+2]]++; mp[chars[ind]]++;
		}
		ind+=2;
	}
}

int main()
{
	ifstream fin("preface.in");
	ofstream fout("preface.out");
	int n;
	fin >> n;
	For(i,1,n+1)
		f(i);
	For(i,0,chars.size()){
		if(mp[chars[i]] == 0) break;
		fout << chars[i] << " " << mp[chars[i]] << endl;
	}
}