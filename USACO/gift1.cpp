/*
ID: behdad.1
PROG: gift1
LANG: C++11
*/

#include<iostream>
#include<fstream>
#include<map>
#include<string>
#include<vector>

using namespace std;

#define For(i,a,b) for(int i = a;i<b;i++)

int main()
{
	ifstream fin("gift1.in");
	ofstream fout("gift1.out");

	map<string, int> group;
	vector<string> names;
	int n;
	fin >> n;
	For(i, 0, n)
	{
		string name;
		fin >> name;
		group.insert(group.end() ,make_pair(name, 0));
		names.push_back(name);
	}

	For(i, 0, n)
	{
		string giver;
		int giftAmount, givenNum;
		fin >> giver >> giftAmount >> givenNum;
		For(j, 0, givenNum)
		{
			string receiver;
			fin >> receiver;
			group[receiver] += giftAmount / givenNum;
			group[giver] -= giftAmount / givenNum;
		}
	}
	For(i, 0, n) fout << names[i] << " " << group[names[i]] << endl;
}