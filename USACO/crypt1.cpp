/*
ID: behdad.1
PROB: crypt1
LANG: C++11
*/

#include<iostream>
#include<vector>
#include<fstream>

#define For(i,a,b) for(int i = a;i<b;i++)

using namespace std;

typedef vector<int> vi;

vi tupl(3,0);

void fillFirst(int i, int n, vi &res, vi &digits)
{
	For(j, 0, n)
	{
		tupl[i] = digits[j];
		if (i == 2)
		{
			res.push_back(tupl[0] * 100 + tupl[1] * 10 + tupl[2]);
		}
		else fillFirst(i + 1, n, res, digits);
	}
}

int dignum(int n)
{
	int sum = 0;
	while (n)
	{
		sum++;
		n /= 10;
	}
	return sum;
}

bool f(int n, vi &digits)
{
	while (n)
	{
		int temp = n % 10;
		bool flag = false;
		For(i, 0, digits.size())
		{
			if (digits[i] == temp) flag = true;
		}
		if (!flag) return false;
		n /= 10;
	}
	return true;
}

int main()
{
	ifstream fin("crypt1.in");
	ofstream fout("crypt1.out");

	int n;
	fin >> n;
	vector<int> digits(n);
	For(i, 0, n)
	{
		fin >> digits[i];
	}
	vi first;
	vi second;
	vi third;
	fillFirst(0, n, first, digits);
	tupl = { 0,0,0 };
	fillFirst(1, n, second, digits);
	
	int sum = 0;
	
	For(i, 0, first.size())
	{
		For(j, 0, second.size())
		{
			if (f(first[i] * (second[j] % 10), digits) &&
				f(first[i] * (second[j] / 10), digits) &&
				f(first[i] * second[j], digits) &&
				dignum(first[i] * (second[j] % 10)) == 3 &&
				dignum(first[i] * (second[j] / 10)) == 3 &&
				dignum(first[i] * second[j]) == 4)
				sum++;
		}
	}
	fout << sum << endl;
	

}