/*
ID: behdad.1
LANG: C++11
PROB: pprime 
*/

#include <iostream>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <iomanip>
#include <fstream>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

string makePal(string s, int q)
{
	string t = s.substr(0, s.size()-q);
	reverse(allof(t));
	return s + t;

}

bool isPrime(int n)
{
	if(n<2) return 0;
	if(n<4) return true;
	if(n%2 == 0 || n%3 == 0) return false;
	for(int i = 5;i*i<=n;i+=6)
	{
		if(n%i == 0 || n%(i+2) == 0) return false;
	}
	return true;
}

int main()
{
	ifstream fin("pprime.in");
	ofstream fout("pprime.out");
	int a,b;
	fin >> a >> b;
	vector<int> v;
	
	for(int i = 1; i < 1000000; i++)
	{
		string s = to_string(i);
		int k = stoi(makePal(s,0));
		if(k<a) continue;
		if(k <= b && isPrime(k) ) 
			v.push_back(k);
		k = stoi(makePal(s,1));
		if(k>b) break;
		if(k >= a && isPrime(k))
			v.push_back(k);
	}

	sort(allof(v));
	For(i,0,v.size())
		fout << v[i] << endl;
}
