/*
ID: behdad.1
LANG: C++11
PROB: sprime
*/

#include <iostream>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <iomanip>
#include <fstream>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

bool isPrime(int n)
{
	if(n < 2) 
		return false;
	if(n < 4) 
		return true;
	if(n%2 == 0 || n%3 == 0) 
		return false;
	for(int i = 5; i*i <= n; i+=6)
		if(n%i == 0 || n%(i+2) == 0) 
			return false;
	return true;
}

int n;

ifstream fin("sprime.in");
ofstream fout("sprime.out");


void makeSuper(string s)
{
	if(s.size() == n)
	{
		fout << s << endl;
		return;
	}
	for(char c : {'1','3','7','9'})
	{
		string temp = s;
	 	temp += c;
		if(isPrime(stoi(temp)))
			makeSuper(temp);
	}
}	

int main()
{
	
	fin >> n;
	
	for(char cc : {'2','3','5','7'})
	{
		string temp = "";
		temp += cc;
		makeSuper(temp);
	}
}