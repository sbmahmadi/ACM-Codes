/*
ID: behdad.1
TASK: transform
LANG: C++11
*/

#include<fstream>
#include<iostream>
#include<vector>

#define For(i,a,b) for(int i = a;i<b;i++)

using namespace std;

typedef vector<vector<bool>> vvb;

bool eql(const vvb &map1, const vvb &map2, int size)
{
	For(i, 0, size)
	{
		For(j, 0, size)
		{
			if (map1[i][j] != map2[i][j]) return false;
		}
	}
	return true;
}

vvb rotate(const vvb &map1, int size)
{
	vvb temp;
	temp.assign(size, vector<bool>());
	For(i, 0, size)
	{
		temp[i].assign(size, 0);
		For(j, 0, size)
		{
			temp[i][j] = map1[size - 1 - j][i];
		}
	}
	return temp;
}

int isRotated(vvb map1, const vvb &map2, int size)
{
	For(i, 0, 3)
	{
		vvb a = rotate(map1, size);
		if (eql(a, map2, size)) return i+1;
		map1 = a;
	}
	return 0;
}

vvb reflect(const vvb &map1, int size)
{
	vvb temp;
	temp.assign(size, vector<bool>());
	For(i, 0, size)
	{
		For(j, 0, size)
		{
			temp[i].push_back(map1[i][size - j - 1]);
		}
	}
	return temp;
}

bool isComb(const vvb &map1,const vvb &map2, int size)
{
	if (isRotated(reflect(map1, size), map2, size) != 0)
	{
		return true;
	}
	return false;
}

int main()
{
	ifstream fin("transform.in");
	ofstream fout("transform.out");

	int n;
	fin >> n;
	fin.ignore();
	vvb map1, map2;
	map1.assign(n, vector<bool>());
	map2.assign(n, vector<bool>());

	For(i, 0, n)
	{
		For(j, 0, n)
		{
			map1[i].push_back((fin.get() == '@' ? 1 : 0));
		}
		fin.ignore();
	}
	For(i, 0, n)
	{
		For(j, 0, n)
		{
			map2[i].push_back(fin.get() == '@' ? 1 : 0);
		}
		fin.ignore();
	}

	int a;
	if ((a = isRotated(map1, map2, n)) != 0)
	{
		fout << a << endl;
	}
	else if (eql(reflect(map1, n), map2, n))
	{
		fout << 4 << endl;
	}
	else if (isComb(map1, map2, n))
	{
		fout << 5 << endl;
	}
	else if (eql(map1, map2, n))
	{
		fout << 6 << endl;
	}
	else fout << 7 << endl;
}

