/*
ID: behdad.1
LANG: C++11
PROB: ttwo
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0)
#define trace(x) cerr << #x << ": " << x << endl
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl
#define _ << " :: " <<
#define allof(x) (x).begin(),(x).end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef long double ld;
typedef pair<int, int> ii;
typedef pair<double, double> dd;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<double> vd;
typedef vector<string> vs;

int dx[] = {-1,0,1,0};
int dy[] = {0,1,0,-1};
char grid[10][10];
bool visit[10][10][4][10][10][4] = {};

void advance(int& x, int& y, int& face) {
	int nx = x + dx[face];
	int ny = y + dy[face];
	if(nx < 0 || nx == 10 || ny < 0 || ny == 10 || grid[nx][ny] == '*') {
		face = (face+1)%4;
		return;
	}
	x = nx;
	y = ny;
}

int main()
{
	ifstream fin("ttwo.in");
	ofstream fout("ttwo.out");
	int cx,cy,fx,fy;
	int faceC = 0, faceF = 0;
	For(i,0,10)
		For(j,0,10){
			fin >> grid[i][j];
			if(grid[i][j] == 'C')
				cx = i, cy = j;
			else if(grid[i][j] == 'F')
				fx = i, fy = j;	
		}
	int step = 0;
	for(; !visit[fx][fy][faceF][cx][cy][faceC] && (cx != fx || cy != fy); step++) {
		visit[fx][fy][faceF][cx][cy][faceC] = true;
		advance(fx,fy,faceF);
		advance(cx,cy,faceC);
	}
	fout << step * (visit[fx][fy][faceF][cx][cy][faceC]^1) << endl;
}
