/*
ID: behdad.1
LANG: C++11
PROB: prefix 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0)
#define trace(x) cerr << #x << ": " << x << endl
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl
#define _ << " :: " <<
#define allof(x) (x).begin(),(x).end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef long double ld;
typedef pair<int, int> ii;
typedef pair<double, double> dd;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<double> vd;

set<string> st;

int main()
{
	ifstream fin("prefix.in");
	ofstream fout("prefix.out");

	string token;
	while(fin >> token && token != ".") {
		//reverse(allof(token));
		st.insert(token);
	}
	fin.ignore();
	string seq = " ", line;
	while(getline(fin, line)) {
		seq += line;
	}
	vector<bool> ok(seq.size());
	ok[0] = true;
	int mxok = 0;
	For(i,0,seq.size()) {
		if(ok[i]) {
			string token;
			For(j,0,10) {
				token += seq[i + j + 1];
				if(st.count(token)) {
					ok[i + j + 1] = true;
					mxok = max(mxok, i + j + 1);
				}
			}
		}
	}
	
	fout << mxok << endl;
}
