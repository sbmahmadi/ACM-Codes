/*
ID: behdad.1
PROB: dualpal
LANG: C++11
*/

#include<iostream>
#include<fstream>
#include<algorithm>

using namespace std;

string baseB(int n, int base)
{
	string s;
	while (n > 0)
	{
		int t = n%base;
		s += '0' + t;
		n /= base;
	}
	reverse(s.begin(), s.end());
	return s;
}

bool isPal(string s)
{
	for (int i = 0; i < s.size() / 2; i++)
	{
		if (s[i] != s[s.size() - 1 - i])return false;
	}
	return true;
}

bool f(int n)
{
	bool flag = false;
	for (int i = 2; i < 11; i++)
	{
		if (isPal(baseB(n, i)))
		{
			if (flag) return true;
			else flag = true;
		}
	}
	return false;
}

int main()
{
	ifstream fin("dualpal.in");
	ofstream fout("dualpal.out");

	int n, s;
	fin >> n >> s;
	while (n)
	{
		if (f(++s))
		{
			fout << s << endl;
			n--;
		}
	}
}