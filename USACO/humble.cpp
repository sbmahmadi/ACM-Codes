/*
ID: behdad.1
LANG: C++11
PROB: humble
*/

#include <bits/stdc++.h>

using namespace std;

typedef long long ll;

int main() {
	ios_base::sync_with_stdio(0); cin.tie(0);
	ifstream fin("humble.in");
	ofstream fout("humble.out");
	ll k,n;
	fin >> k >> n;
	set<ll> pq;
	vector<ll> pr(k);
	for(int i = 0; i < k; i++) {
		fin >> pr[i];
		pq.insert(pr[i]);
	}
	sort(pr.begin(), pr.end());
	for(ll p : pr) {
		for(auto it = pq.begin(); it != pq.end() ; it++) {
			ll me = *it * p;
			if(pq.size() == n && me > *--pq.end())
				break;
			if(me > 0)
				pq.insert(me);
			while(pq.size() > n)
				pq.erase(--pq.end());			
		}
	}
	fout << *--pq.end() << endl;
}