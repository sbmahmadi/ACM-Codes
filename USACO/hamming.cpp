/*
ID: behdad.1
LANG: C++11
PROB:  
*/

#include <iostream>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <iomanip>
#include <fstream>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("hamming.in");
ofstream fout("hamming.out");

int grid[256][256];
int n,b,d;
vi res;
bool found = false;

int hamDist(int a, int b)
{
	int k = max(a,b), res = 0;
	for(int i = 1; i <= k; i<<=1)
		res += ((a & i) > 0) ^ ((b & i) > 0);
	return res;
}

int main()
{
	fin >> n >> b >> d;
	res.push_back(0);
	For(i,1,1<<b)
	{
		bool found = true;
		for(int j : res)
			if(hamDist(i,j) < d)
				found = false;
		if(found)
			res.push_back(i);
		if(res.size() == n)
			break;
	}
	For(i,0,res.size())
	{
		if(i % 10 != 0)
			fout << " ";
		fout << res[i];
		if(i % 10 == 9 && i != res.size() -1)
			fout << endl;
	}
	fout << endl;
}
