/*
ID: behdad.1
LANG: C++11
PROB: money
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0)
#define trace(x) cerr << #x << ": " << x << endl
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl
#define _ << " :: " <<
#define allof(x) (x).begin(),(x).end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef long double ld;
typedef pair<int, int> ii;
typedef pair<double, double> dd;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<double> vd;
typedef vector<string> vs;

vi coins;
ll dp[10001][26];


ll solve(int n, int ind) {
	if(n <= 0)
		return n == 0;
	if(dp[n][ind] >= 0)
		return dp[n][ind];

	ll& res = dp[n][ind] = 0;
	for(int c = ind; c < coins.size(); c++) {
		res += solve(n-coins[c], c);
	}
	return res;
}

int main()
{
	ifstream fin("money.in");
	ofstream fout("money.out");
	memset(dp, -1, sizeof dp);
	int v,n;
	fin >> v >> n;
	coins = vi(v);
	For(i,0,v)
		fin >> coins[i];
	fout << solve(n,0) << endl;
}