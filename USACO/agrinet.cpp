/*
ID: behdad.1
LANG: C++11
PROB: agrinet
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0)
#define trace(x) cerr << #x << ": " << x << endl
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl
#define _ << " :: " <<
#define allof(x) (x).begin(),(x).end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef long double ld;
typedef pair<int, int> ii;
typedef pair<double, double> dd;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<double> vd;
typedef vector<string> vs;

vector<vi> graph;
vi dist;

vi visit;

int main()
{
	ifstream fin("agrinet.in");
	ofstream fout("agrinet.out");
	int n;
	fin >> n;
	graph = vector<vi>(n,vi(n));
	visit = vi(n,false);
	dist = vi(n,1e8);
	For(i,0,n) 
		For(j,0,n)
			fin >> graph[i][j];
	
	visit[0] = true;
	For(i,0,n) {
		dist[i] = min(dist[i], graph[0][i]);
	}
	int sum = 0;
	For(i,0,n-1) {
		int minidx = 1e8;
		int mincost = 1e8;
		For(j,0,n)
			if(!visit[j] && dist[j] < mincost)
				minidx = j, mincost = dist[j];
		visit[minidx] = true;
		sum += dist[minidx];
		For(j,0,n)
			dist[j] = min(dist[j], graph[minidx][j]);
	}
	fout << sum << endl;
}
