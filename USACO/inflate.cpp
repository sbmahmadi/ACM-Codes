/*
ID: behdad.1
LANG: C++11
PROB: inflate
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0)
#define trace(x) cerr << #x << ": " << x << endl
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl
#define _ << " :: " <<
#define allof(x) (x).begin(),(x).end()
#define X real()
#define Y imag()

#define INF (int(1e8))

using namespace std;

typedef long long ll;
typedef long double ld;
typedef pair<int, int> ii;
typedef pair<double, double> dd;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<double> vd;
typedef vector<string> vs;

int m,n;
int dp[10005];
vii cls;

int main()
{
	fastio();
	ifstream fin("inflate.in");
	ofstream fout("inflate.out");
	fin >> m >> n;
	memset(dp,0,sizeof dp);
	cls = vii(n);
	For(i,0,n)
		fin >> cls[i].first >> cls[i].second;
	For(i,0,m+1)
		For(j,0,n)
			if(i >= cls[j].second)
				dp[i] = max(dp[i], cls[j].first + dp[i - cls[j].second]);
	fout << dp[m] << endl;
}