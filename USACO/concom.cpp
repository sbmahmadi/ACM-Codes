/*
ID: behdad.1
LANG: C++11
PROB: concom
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0)
#define trace(x) cerr << #x << ": " << x << endl
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl
#define _ << " :: " <<
#define allof(x) (x).begin(),(x).end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef long double ld;
typedef pair<int, int> ii;
typedef pair<double, double> dd;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<double> vd;
typedef vector<string> vs;

int graph[105][105], con[105][105];

int main()
{
	ifstream fin("concom.in");
	ofstream fout("concom.out");

	int n;
	fin >> n;
	memset(graph, 0, sizeof graph);
	memset(con, 0, sizeof con);
	For(i,0,n) {
		int u,v,w;
		fin >> u >> v >> w; u--;v--;
		graph[u][v] = w;
		if(w > 50)
			con[u][v] = true;
	}
	For(k,0,100) {
		For(i,0,100) {
			For(j,0,100) {
				if(graph[i][j] > 50)
					con[i][j] = true;
				else {
					int sum = graph[i][j];
					For(p,0,100) {
						if(con[i][p])
							sum += graph[p][j];
					}
					if(sum > 50)
						con[i][j] = true;
				}
			}
		}
	}
	For(i,0,100)
		For(j,0,100)
			if(con[i][j] && i != j)
				fout << i+1 << " " << j+1 << endl;
}
