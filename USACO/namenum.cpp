/*
ID: behdad.1
PROB: namenum
LANG: C++11
*/


#include<iostream>
#include<fstream>
#include<vector>
#include<map>
#include<string>
#include<unordered_set>
#include<set>

using namespace std;

map<int, vector<char>> keypad;
unordered_set<string> dict;

ifstream ffin("dict.txt");
ifstream fin("namenum.in");
ofstream fout("namenum.out");
bool flag = false;

void f(string src, string dst, int index)
{
	if (index == src.size())
	{
		if (dict.count(dst))
		{
			fout << dst << endl;
			flag = true;
		}
		return;
	}
	for (int i = 0; i < 3; i++)
		f(src, dst + keypad[src[index] - '0'][i], index + 1);
}

int main()
{
	bool fl = 0;
	for (int i = 2; i < 10; i++)
		for (int j = 0; j < 3; j++)
		{
			keypad[i].push_back('A' + j + (i - 2) * 3 + fl);
			if (keypad[i].back() == 'Q')
			{
				keypad[i].pop_back();
				j--;
				fl = true;
			}
		}

	string s;

	while (ffin >> s)
		dict.insert(s);

	fin >> s;
	f(s, "", 0);
	if (!flag) fout << "NONE" << endl;
}