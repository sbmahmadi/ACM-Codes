/*
ID: behdad.1
LANG: C++11
PROB: holstein
*/

#include <iostream>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <iomanip>
#include <fstream>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int feeds[16][26];
int need[26];

int v;
int minN;
int minc;
void add(int a[], int b[])
{
	For(i,0,v)
		a[i] += b[i];
}

bool valid(int food[], int feed[])
{
	For(i,0,v)
	{
		if(food[i]<feed[i])
			return false;
	}
	return true;
}

void solve(int n)
{
	int tmp = n;
	int food[26] = {};
	int c = 0;
	int i = 0;
	while(n > 0)
	{
		if(n%2)
		{
			add(food,feeds[i]);
			c++;
		}
		n>>=1;
		i++;
	}
	if(c < minc && valid(food,need))
	{
		minc = c;
		minN = tmp;
	}
}

int main()
{
	ifstream fin("holstein.in");
	ofstream fout("holstein.out");
	minc = 2000;
	minN = 500000;
	fin >> v;
	For(i,0,v)
		fin >> need[i];
	int g;
	fin >> g;
	For(i,0,g)
		For(j,0,v)
			fin >> feeds[i][j];
	For(i, 0, 1<<15)
		solve(i);
	fout << minc;
	int  k = 1;
	while(minN > 0)
	{
		if(minN%2)
			fout << " " << k;
		minN >>=1;
		k++;
	}
	fout << endl;
}