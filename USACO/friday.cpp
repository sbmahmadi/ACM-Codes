/*
ID: behdad.1
PROG: friday
LANG: C++11
*/

#include<iostream>
#include<fstream>

using namespace std;

int months[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
int week[7] = {};

int main()
{
	ifstream fin("friday.in");
	ofstream fout("friday.out");

	bool isLeap;
	int n, day = 2;
	fin >> n;

	for (int year = 1900; n > 0; n--, year++)
	{
		isLeap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
			
		for (int month = 0; month < 12; month++)
		{	
			int m = months[month];
			if (month == 1 && isLeap) m++;
			for (int j = 0; j < m; j++)
			{
				if (j == 12) week[day]++;
				day++;
				day %= 7;
			}
		}
	}

	for (int i = 0; i < 6;i++) fout << week[i] << " ";
	fout <<week[6] << endl;
}
