/*
ID: behdad.1
LANG: C++11
PROB: frac1 
*/

#include <iostream>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <iomanip>
#include <fstream>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int gcd(int a, int b)
{
	if(a == 0) return b;v 
	return gcd(b%a,a);
}

struct cmp
{
	bool operator() (const ii &a,const ii &b)
	{
		return a.first*b.second < b.first*a.second;
	}
};

int main()
{
	ifstream fin("frac1.in");
	ofstream fout("frac1.out");
	set<ii,cmp> fracs;
	int n;
	fin >> n;
	For(i,1,n+1) // denominator
		For(j,0,i+1) // numerator
		{
			int i1 = i, j1 = j;
			int g = gcd(j,i);
			i1 /= g;
			j1 /= g;
			
			fracs.insert(make_pair(j,i));
		}

	for(ii p : fracs)
	{
		fout << p.first << "/" << p.second << endl;		
	}
}
