/*
ID: behdad.1
PROB: milk
LANG: C++11
*/

#include<iostream>
#include<vector>
#include<fstream>
#include<algorithm>
#include<utility>

#define For(i,a,b) for(int i = a;i<b;i++)

using namespace std;

int main()
{
	ifstream fin("milk.in");
	ofstream fout("milk.out");

	int n, m;
	fin >> n >> m;
	vector<pair<int,int>> costs;
	For(i, 0, m)
	{
		int p,a;
		fin >> p>>a;
		costs.push_back(make_pair(p,a));
	}
	long long sum = 0;
	sort(costs.begin(), costs.end());
	for (int i = 0; n != 0; i++)
	{
		int temp = costs[i].second;
		if (temp > n)
		{
			sum += n*costs[i].first;
			n = 0;
		}
		else
		{
			sum += costs[i].first * costs[i].second;
			n -= temp;
		}
	}
	fout << sum << endl;
}