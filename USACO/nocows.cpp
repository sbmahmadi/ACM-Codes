/*
ID: behdad.1
LANG: C++11
PROB: nocows
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0)
#define trace(x) //cerr << #x << ": " << x << endl
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl
#define _ << " :: " <<
#define allof(x) (x).begin(),(x).end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef long double ld;
typedef pair<int, int> ii;
typedef pair<double, double> dd;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<double> vd;

#define MOD int(9901)

int dp[205][105];

int main()
{
	ifstream fin("nocows.in");
	ofstream fout("nocows.out");

	int n,k;
	fin >> n >> k;
	memset(dp, 0, sizeof dp);
	For(i,1,201)
		For(j,1,101) {
			if(i % 2 == 0)
				continue;
			if(i == j && i == 1){
				dp[i][j] = 1;
				continue;
			}
			For(p,1,i-1) {
				For(q,1,j-1)
					dp[i][j] += (dp[p][j-1] * dp[i - p - 1][q] * 2) % MOD;
				dp[i][j] += dp[p][j-1] * dp[i-p-1][j-1] % MOD;
			}
			dp[i][j] %= MOD;
		}
	

	fout << dp[n][k] << endl;
}