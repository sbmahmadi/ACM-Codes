/*
ID: behdad.1
LANG: C++11
PROB: skidesign
*/

#include<iostream>
#include<vector>
#include<algorithm> 
#include<fstream>

#define allof(x) x.begin(),x.end()

using namespace std;

int main()
{
	ifstream fin("skidesign.in");
	ofstream fout("skidesign.out");
	
	int n;
	cin >> n;
	
	vector<int> hills(n);
	
	for (int i = 0; i < n; i++)
	{
		cin >> hills[i];
	}

	sort(allof(hills));

	int neededDif = (hills.back() - hills.front()) - 17;
	int totalCost = 0;
	while (neededDif > 0)
	{
		int begCost, endCost;
		int i,j;
		for (i = 1; i < n && hills[i] == hills[i - 1]; i++);
		begCost = i * min(neededDif, hills[i] - hills[i - 1])*min(neededDif, hills[i] - hills[i - 1]);

		for (j = n - 2; j >= 0 && hills[j] == hills[j + 1]; j--);
		endCost = (n - j - 1) * min(neededDif, hills[j + 1] - hills[j])*min(neededDif, hills[j + 1] - hills[j]);

		if (begCost > endCost)
		{
			int diff = min(neededDif, hills[j + 1] - hills[j]);
			totalCost += endCost;
			for (; j < n - 1; j++)
			{
				hills[j+1] -= diff;
			}
			neededDif -= diff;
		}
		else
		{
			int diff = min(neededDif, hills[i] - hills[i - 1]);
			totalCost += begCost;
			for (; i > 0; i--)
			{
				hills[i - 1] += diff;
			}
			neededDif -= diff;
		}
	}
	cout << totalCost << endl;
}