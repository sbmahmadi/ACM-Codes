/*
ID: behdad.1
LANG: C++11
PROB: comehome
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0)
#define trace(x) cerr << #x << ": " << x << endl
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl
#define _ << " :: " <<
#define allof(x) (x).begin(),(x).end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef long double ld;
typedef pair<int, int> ii;
typedef pair<double, double> dd;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<double> vd;
typedef vector<string> vs;

vector<vii> graph;

vi dist;

map<char,int> ids;
int getid(char c) {
	return ids.insert({c,ids.size()}).first->second;
}

int main()
{
	ifstream fin("comehome.in");
	ofstream fout("comehome.out");
	graph = vector<vii>(60);
	int p;
	fin >> p;
	For(i,0,p) {
		char u,v;
		int w;
		fin >> u >> v >> w;
		graph[getid(u)].push_back({getid(v),w});
		graph[getid(v)].push_back({getid(u),w});
	}
	set<ii> pq; pq.insert({0, getid('Z')});
	dist = vi(60,1e8); dist[getid('Z')] = 0;
	while(!pq.empty()) {
		ii p = *pq.begin(); pq.erase(pq.begin());
		if(p.first > dist[p.second])
			continue;
		For(i,0,graph[p.second].size()) {
			ii v = graph[p.second][i];
			if(dist[v.first] > dist[p.second] + v.second) {
				dist[v.first] = dist[p.second] + v.second;
				pq.insert({dist[v.first], v.first});
			}
		}
	}
	int mx = 1e8;
	char mxc;
	for(auto it : ids) {
		if(dist[it.second] < mx && it.first >= 'A' && it.first <= 'Y')
			mx = dist[it.second], mxc = it.first;
	}
	fout << mxc << " " << mx << endl;
}
