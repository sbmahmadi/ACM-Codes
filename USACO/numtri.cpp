/*
ID: behdad.1
LANG: C++11
PROB: numtri
*/

#include <iostream>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <iomanip>
#include <fstream>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int tri[1005][1005];

int main()
{
	ifstream fin("numtri.in");
	ofstream fout("numtri.out");
	int r;
	fin >> r;
	For(i,0,r)
		For(j,0,i+1)
			fin >> tri[i][j];
	roF(i,r-2,0)
		For(j,0,i+1)
			tri[i][j] += max(tri[i+1][j],tri[i+1][j+1]);
	fout << tri[0][0] << endl;
}
