/*
ID: behdad.1
LANG: C++11
PROB: skidesign
*/

#include <iostream>
#include <vector>
#include <algorithm> 
#include <fstream>

#define allof(x) x.begin(),x.end()
#define For(i,a,b) for(int i = a ; i<b;i++)
#define NUM 8.5
#define trace(x) cout<<#x<<" : "<<x<<endl;

using namespace std;
vector<int> hills;

int f(int a,int b)
{
	int res = 0;
	For(i,0,hills.size())
	{
		if(hills[i] < a) res += (a - hills[i])*(a - hills[i]);
		if(hills[i] > b) res += (b - hills[i])*(b - hills[i]);
	}
	return res;
}

int main()
{
	ifstream fin("skidesign.in");
	ofstream fout("skidesign.out");
	int n;
	fin >> n;
	hills.assign(n,0);
	float ave = 0;
	for (int i = 0; i < n; i++)
	{
		fin >> hills[i];
		ave += hills[i];
	}
	ave /= n;
	sort(allof(hills));
	fout << min( {f((int)(ave - NUM),(int)(ave - NUM) + 17) , f((int)(ave - NUM) +1,(int)(ave - NUM) +18),f((int)(ave - NUM) - 1,(int)(ave - NUM) +16)}) << endl;
}