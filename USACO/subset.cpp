/*
ID: behdad.1
LANG: C++11
PROB: subset
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0);
#define trace(x) cerr << #x << ": " << x << endl;
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl;
#define _ << " :: " <<
#define allof(x) (x).begin(),(x).end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef long double ld;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<double> vd;

int n;

ll dp[40][800];

ll f(int num, int sum) {
	if(sum <= 0)
		return sum == 0 ? 1 : 0;
	if(num > n)
		return 0;
	ll& res = dp[num][sum];
	if(res != -1)
		return res;
	res = 0;
	For(i,num + 1,n+1) {
		res += f(i, sum - i);
	} 
	return res;
}

int main()
{
	ifstream fin("subset.in");
	ofstream fout("subset.out");

	memset(dp, -1, sizeof dp);
	fin >> n;
	int sum = 0;
	For(i,0,n)
		sum += i+1;
	if(sum & 1)
		fout << 0 << endl;
	else
		fout << f(0, sum/2) / 2 << endl;
}