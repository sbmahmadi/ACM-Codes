#include<iostream>
#include<vector>
#include<algorithm>

using namespace std;

int f(int n, int l, int r)
{
	if (l > r) return 0;
	if (l == r) return l;
	if (n == 1)
	{
		int sum = 0;
		for (int i = l; i <= r; i++)
		{
			sum += i;
		}
		return sum;
	}

	vector<int> res;
	
	for (int i = l; i <= r; i++)
	{
		res.push_back(max(f(n - 1, l, i - 1), f(n, i + 1, r) + i));
	}
	return *min_element(res.begin(), res.end());
}

int main()
{
	int n,l;
	while (cin >> n>>l)
	{
		cout << f(n, 1, l) << endl;
	}
}