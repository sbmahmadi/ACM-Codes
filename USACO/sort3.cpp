/*
ID: behdad.1
LANG: C++11
PROB: sort3
*/

#include <iostream>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <iomanip>
#include <fstream>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int main()
{
	ifstream fin("sort3.in");
	ofstream fout("sort3.out");
	int n;
	int a[4][4] = {};
	fin >> n;
	vi v(n);
	For(i,0,n)
	{
		fin >> v[i];
	}
	vi o(allof(v));
	sort(allof(v));
	int res = 0;
	
	For(i,0,n)
		a[v[i]][o[i]]++;

	For(i,1,4)
		For(j,i+1,4)
		{
			int k = min(a[i][j],a[j][i]);
			a[i][j] -= k;
			a[j][i] -= k;
			res += k;
		}
		res += (a[1][2] + a[1][3])*2;
	fout << res << endl;
}
