/*
ID: behdad.1
LANG: C++11
PROB: maze1
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0)
#define trace(x) cerr << #x << ": " << x << endl
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl
#define _ << " :: " <<
#define allof(x) (x).begin(),(x).end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef long double ld;
typedef pair<int, int> ii;
typedef pair<double, double> dd;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<double> vd;
typedef vector<string> vs;

vector<vi> maze;

int main()
{
	ifstream fin("maze1.in");
	ofstream fout("maze1.out");
	int w,h;
	fin >> w >> h;
	fin.ignore();
	maze = vector<vi>(2*h+1, vi(2*w+1));
	int x1 = -1, x2 = -1, y1 = -1, y2 = -1;
	
	For(i,0,2*h+1) {
		string s;
		getline(fin,s);
		if(i == 0 || i == 2*h) {
			For(j,0,s.size()) {
				if(s[j] == ' ')
					if(x1 == -1)
						x1 = i, y1 = j;
					else
						x2 = i, y2 = j;
			}
		}
		if(s[0] == ' ')
			if(x1 == -1)
				x1 = i, y1 = 0;
			else
				x2 = i, y2 = 0;
		if(s.back() == ' ')
			if(x1 == -1)
				x1 = i, y1 = s.size() - 1;
			else
				x2 = i, y2 = s.size() - 1;

		For(j,0,s.size())
			maze[i][j] = (s[j] == ' ') ? -1 : -2;
	}

	trace(x1 _ y1 _ x2 _ y2);
	int dx[] = {0,0,1,-1};
	int dy[] = {1,-1,0,0};
	queue<ii> q;
	q.push({x1,y1}); q.push({x2,y2});
	maze[x1][y1] = maze[x2][y2] = 0;
	int mx;
	while(!q.empty()) {
		ii p = q.front(); q.pop();
		int x = p.first;
		int y = p.second;
		mx = maze[x][y];
		For(i,0,4) {
			int nx = x + dx[i];
			int ny = y + dy[i];
			if(nx >= 0 && ny >= 0 && nx < 2*h+1 && ny < 2*w+1 && maze[nx][ny] == -1) {
				maze[nx][ny] = maze[x][y] + (nx&ny&1);
				q.push({nx,ny});
			}
		}
	}
	fout << mx << endl;
}
