/*
ID: behdad.1
LANG: C++11
PROB: stamps
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0)
#define trace(x) cerr << #x << ": " << x << endl
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl
#define _ << " :: " <<
#define allof(x) (x).begin(),(x).end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef long double ld;
typedef pair<int, int> ii;
typedef pair<double, double> dd;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<double> vd;
typedef vector<string> vs;

short dp[201*10000];

int main()
{
	ifstream fin("stamps.in");
	ofstream fout("stamps.out");
	int k,n;
	fin >> k >> n;
	vi vals(n);
	For(i,0,n)
		(fin >> vals[i]),
		dp[vals[i]] = 1;
	int i = 1;
	for(; i < 201*10000; i++) {
		if(dp[i])
			continue;
		for(int v : vals) 
			if(i > v && dp[i-v] && dp[i-v] + 1 <= k)
				if(dp[i] == 0 || dp[i-v] < dp[i] - 1)
					dp[i] = dp[i-v] + 1;
		if(!dp[i])
			break;
	}
	fout << i-1 << endl;
}