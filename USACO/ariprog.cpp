/*
ID: behdad.1
LANG: C++11
PROB: ariprog
*/

#include <iostream>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <iomanip>
#include <fstream>
#include <limits.h>
#include <time.h>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int n,m;
int arr[130000];

bool f(int beg,int diff,int lvl)
{
	if(lvl == n)
		return true;
	if(!arr[beg + diff])
		return false;
	else 
		return f(beg + diff,diff,lvl+1);
}

int main()
{
	ios_base::sync_with_stdio(0);

	ifstream fin("ariprog.in");
	ofstream fout("ariprog.out");
	
	fin >> n >> m;
	
	vector<ii> res;
	For(i,0,m+1)
		For(j,0,m+1)
			arr[i*i + j*j] = 1;
		
	for(int i = 0; i < 130000; i++)
	{
		if(!arr[i]) 
			continue;
		for(int j = i+1; j < 130000; j++)
		{
			if(!arr[j]) 
				continue;
			int diff = j-i;
			if(diff >= (130000-i) / (n-1))
				break;
			if(f(i,diff , 1))
				res.push_back(make_pair(diff, i));
		}
	}

	sort(allof(res));
	
	if(res.size())
		for(auto it = res.begin(); it != res.end(); it++)
			fout << it->second << " "<< it->first << endl;
	else 
		fout<<"NONE"<<endl;
}