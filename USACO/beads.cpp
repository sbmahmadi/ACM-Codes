/*
ID: behdad.1
PROG: beads
LANG: C++11
*/

#include<string>
#include<algorithm>
#include<fstream>

#define For(i,a,b) for(int i = a;i<b;i++)

using namespace std;

int count(int index, string beads)
{
	char endmark = 'k';
	int p = 0, c = 0;
	while (beads[index + p] != endmark)
	{
		if (beads[index + p] == 'b') endmark = 'r';
		else if (beads[index + p] == 'r') endmark = 'b';
		p++; c++;
		if (p + index == beads.size()) p = -index;
		if (c == beads.size()) break;
	}

	endmark = 'k';
	p = -1; int c2 = 0;
	if (index == 0) p = beads.size() - 1;
	while (beads[index + p] != endmark)
	{
		if (beads[index + p] == 'b') endmark = 'r';
		else if (beads[index + p] == 'r') endmark = 'b';
		p--; c2++;
		if (p + index == -1) p = beads.size() - index - 1;
		if (c2 == beads.size()) break;
	}
	return c + c2;
}

int main()
{
	ifstream fin("beads.in");
	ofstream fout("beads.out");
	int n;
	string s;
	fin >> n >> s;
	int mx = 0;
	For(i, 0, n)
	{
		if (count(i, s) > mx) mx = count(i, s);
	}
	mx = min(mx, n);
	fout << mx << endl;
}