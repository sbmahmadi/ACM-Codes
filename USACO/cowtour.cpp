/*
ID: behdad.1
LANG: C++11
PROB: cowtour
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0)
#define trace(x) cerr << #x << ": " << x << endl
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl
#define _ << " :: " <<
#define allof(x) (x).begin(),(x).end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef long double ld;
typedef pair<int, int> ii;
typedef pair<double, double> dd;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<double> vd;
typedef vector<string> vs;

vii points;
vector<vd> grid;

double getDist(int i, int j) {
	return hypot(points[i].first - points[j].first, points[i].second - points[j].second);
}

int main()
{
	ifstream fin("cowtour.in");
	ofstream fout("cowtour.out");
	
	int n;
	fin >> n;
	points = vii(n);
	For(i,0,n) {
		fin >> points[i].first >> points[i].second;
	}
	grid = vector<vd>(n, vd(n,1e8));
	For(i,0,n)
		For(j,0,n) {
			char f; fin >> f;
			if(f == '1') grid[i][j] = getDist(i,j);
		}

	For(i,0,n) grid[i][i] = 0;
	For(k,0,n) {
		For(i,0,n) {
			For(j,0,n) {
				if(grid[i][j] > grid[i][k] + grid[k][j])
					grid[i][j] = grid[i][k] + grid[k][j];
			}
		}
	}

	double d1;
	For(i,0,n)
		For(j,0,n) {
			if(grid[i][j] != 1e8)
				d1 = max(d1, grid[i][j]);
		}
	
	double mnDiam = 1e8;
	For(i,0,n) {
		double mx1 = 0;
		For(j,0,n) {
			if(grid[i][j] != 1e8)
				mx1 = max(mx1, grid[i][j]);
		}
		For(j,0,n) {
			if(grid[i][j] != 1e8) continue;
			double mx2 = 0;
			For(k,0,n) {
				if(grid[j][k] != 1e8)
					mx2 = max(mx2, grid[j][k]);
			}
			mnDiam = min(mnDiam, mx2 + mx1 + getDist(i,j));
		}
	}
	fout << fixed << setprecision(6) << max(d1,mnDiam) << endl;
}