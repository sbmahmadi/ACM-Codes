/*
ID: behdad.1
LANG: C++11
PROB: milk3 
*/

#include <iostream>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <iomanip>
#include <fstream>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int cube[21][21][21];

int A,B,C;
	
void f(int a, int b, int c)
{
	if(cube[a][b][c]) 
		return;
	cube[a][b][c] = 1;
	
	int k = min(a,B-b);
	f(a - k, b + k, c);
	
	k = min(A-a, b);
	f(a + k, b - k, c);
	
	k = min(A-a, c);
	f(a + k, b, c - k);
	
	k = min(a, C-c);
	f(a - k, b, c + k);
	
	k = min(B-b, c);
	f(a, b + k, c - k);
	
	k = min(C-c, b);
	f(a, b - k, c + k);
}

int main()
{
	ifstream fin("milk3.in");
	ofstream fout("milk3.out");
	
	fin >> A >> B >> C;
	
	f(0,0,C);
	set<int> res;
	For(i,0,B+1)
		For(j,0,C+1)
			if(cube[0][i][j])
				res.insert(j);
	fout << *res.begin();
	for(auto it = ++res.begin(); it != res.end(); it++)
		fout <<" "<< *it;
	fout << endl;
}