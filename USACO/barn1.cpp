/*
ID: behdad.1
PROB: barn1
LANG: C++11
*/

#include<iostream>
#include<fstream>
#include<vector>
#include<algorithm>

#define For(i,a,b) for(int i = a;i<b;i++)

using namespace std;

int main()
{
	ifstream fin("barn1.in");
	ofstream fout("barn1.out");
	int m, s, c;
	fin >> m >> s >> c;
	vector<int> barn(c,0);
	vector<int> gaps;
	For(i, 0, c)
	{
		fin >> barn[i];
	}
	sort(barn.begin(), barn.end());
	int sum = barn.back() - barn.front() + 1;
	For(i, 0, c-1)
	{
		if (barn[i] + 1 != barn[i + 1])
		{
			gaps.push_back(barn[i + 1] - barn[i] - 1);
		}
	}
	sort(gaps.begin(), gaps.end());
	For(i, 0, m - 1)
	{
		if (gaps.empty()) break;
		sum -= gaps.back();
		gaps.pop_back();
	}
	fout << sum << endl;
}