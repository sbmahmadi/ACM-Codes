/*
ID: behdad.1
LANG: C++11
PROB: fracdec
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)

using namespace std;

int main()
{
	ifstream fin("fracdec.in");
	ofstream fout("fracdec.out");
	int a,b;
	cin >> a >> b;
	string res;
	int r = a/b; a%=b;
	map<int,int> mp;
	res += to_string(r) + '.';
	do {
		a *= 10;
		if(mp.count(a)) {
			res.insert(mp[a],"(");
			res += ")";
			break;
		}
		mp.insert({a, res.size()});
		r = a/b; a%=b;
		res += to_string(r);
	} while(a != 0);
	For(i,0,res.size()){
		if(i%76 == 0 && i) 
			fout << endl;
		cout << res[i];
	}
	cout << endl;
}
