/*
ID: behdad.1
TASK: milk2
LANG: C++11
*/

#include<fstream>
#include<algorithm>
#include<vector>
#include<iostream>

#define For(i,a,b) for(int i = a;i<b;i++)

using namespace std;

bool t[1000010] = {};

int main()
{
	ifstream fin("milk2.in");
	ofstream fout("milk2.out");

	int n;
	fin >> n;

	int maxmilk = 0, maxidle = 0, milke = 0,milks=1000020;
	For(i, 0, n)
	{
		int a, b;
		fin >> a >> b;
		milke = max(b, milke);
		milks = min(a, milks);
		For(j, a, b)
		{
			t[j] = true;
		}
	}

	For(i, milks, milke)
	{
		int c = 1;
		while (i != milke-1 && t[i] == t[i + 1])
		{
			c++;
			i++;
		}
		if (t[i]) maxmilk = max(maxmilk, c);
		else maxidle = max(maxidle, c);
	}
	fout << maxmilk << " " << maxidle << endl;
}