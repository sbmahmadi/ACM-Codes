#include <iostream>
#include <vector>
#include <queue>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define allof(x) x.begin(),x.end()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

unordered_map <string, int> mp;

int getIndex(string s)
{
	return mp.insert(make_pair(s,mp.size())).first->second;
}

struct Node
{
	int count;
	Node* parent;
	Node()
	{
		count = 1;
		parent = this;
	}
};

struct DisjointSet
{
	vector<Node> v;
	void Union(int i1,int i2)
	{
		Node* r1 = findRoot(&v[i1]);
		Node* r2 =  findRoot(&v[i2]);
		if(r1 == r2)
			return;
		
		r1->count += r2->count;
		r2->parent = r1;
	}

	Node* findRoot(Node* n)
	{
		if(n->parent == n) return n;
		else return n->parent = findRoot(n->parent);
	}
};

int main()
{
	int t;
	cin>>t;
	while(t--)
	{
		mp.clear();
		DisjointSet djs;
		int n;
		cin>>n;
		while(n--)
		{
			string s1,s2;
			cin>>s1>>s2;
			int i1 = getIndex(s1),i2 = getIndex(s2);
			if(i1 >= djs.v.size())
				djs.v.push_back(*(new Node()));
			if(i2 >= djs.v.size())
				djs.v.push_back(*(new Node()));

			djs.Union(i1,i2);
			cout<< djs.findRoot(&djs.v[i1])->count<<endl;
		}
	}
}