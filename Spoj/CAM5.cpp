#include <iostream>
#include <vector>
#include <queue>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <iomanip>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define allof(x) x.begin(),x.end()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

bool visited[100005]; 

void DFS(vector<vi> &graph,int s)
{
	visited[s] = true;
	For(i,0,graph[s].size())
	{
		if(!visited[graph[s][i]])
			DFS(graph,graph[s][i]);
	}
}

int main()
{
	int t;
	cin >> t;
	while(t--)
	{
		memset(visited,0,sizeof visited);
		int n;
		cin >> n;
		vector<vi> graph(n);
		int e;
		cin >> e;
		For(i,0,e)
		{
			int a,b;
			cin >> a >> b;
			graph[a].push_back(b);
			graph[b].push_back(a);
		}
		int res = 0;
		For(i,0,n)
		{
			if(!visited[i])
			{
				res++;
				DFS(graph,i);
			}
		}
		cout << res << endl;
	}	
}