#include <iostream>
#include <vector>
#include <queue>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define allof(x) x.begin(),x.end()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

bool visited[105][105];
char grid[105][105];
const string lit = "ALLIZZWELL";
int XDif[] = { 0,0,1,1,1,-1,-1,-1 };
int YDif[] = { 1,-1,-1,0,1,1,0,-1 };
int n, m;

bool DFS(int i, int j, int step)
{
	if (grid[i][j] != lit[step]) return false;
	if (step == 9) return true;
	visited[i][j] = true;
	For(k, 0, 8)
	{
		int ni = i + XDif[k];
		int nj = j + YDif[k];
		if (ni >= 0 && nj >= 0 && ni < n && nj < m && !visited[ni][nj])
		{
			if (DFS(ni, nj, step + 1))
			{
				visited[i][j] = false;
				return true;
			}
		}
	}
	visited[i][j] = false;
	return false;
}

string f()
{
	For(i, 0, n)
	{
		For(j, 0, m)
		{
			if (DFS(i, j, 0)) return "YES";
		}
	}
	return "NO";
}

int main()
{
	int t;
	cin >> t;
	while (t--)
	{
		memset(visited, 0, sizeof visited);
		memset(grid, '.', sizeof grid);
		
		cin >> n >> m;
		For(i, 0, n)
		{
			For(j, 0, m)
			{
				cin >> grid[i][j];
			}
		}
		cout << f() << endl;
	}
}