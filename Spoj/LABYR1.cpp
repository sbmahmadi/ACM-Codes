#include <iostream>
#include <vector>
#include <queue>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <iomanip>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define allof(x) x.begin(),x.end()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int grid[1005][1005];
int farY,farX;
int maxLenght;

int dx[4] = {0,0,1,-1};
int dy[4] = {-1,1,0,0};

int c,r;

void DFS(int x,int y, int step)
{
	grid[x][y] = step;
	if(step > maxLenght)
	{
		farX = x;
		farY = y;
		maxLenght = step;
	}
	For(i,0,4)
	{
		int nx = x + dx[i];
		int ny = y + dy[i];
		if(nx < r && nx >=0 && ny < c && ny >=0 && grid[nx][ny] == -1)
		{
			DFS(nx,ny,step +1);
		}
	}
}

int main()
{
	int t;
	cin >> t;
	while(t--)
	{
		int x,y;
		cin >> c >> r;
		For(i,0,r)
			For(j,0,c)
			{
				char ch;
				cin >> ch;
				grid[i][j] = (ch == '.' ? -1 : -2);
				if(grid[i][j] == -1)
				{
					x = i;
					y = j;
				}
			}			
		DFS(x,y,0);
		For(i,0,r)
			For(j,0,c)
				if(grid[i][j]>=0)
					grid[i][j] = -1;
		maxLenght = 0;
		DFS(farX,farY,0);
		cout << "Maximum rope length is " << maxLenght << "." << endl;
	}
}