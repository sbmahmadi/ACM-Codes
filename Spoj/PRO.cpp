#include <iostream>
#include <set>

using namespace std;

int main()
{
	int n;
	cin >> n;
	multiset<int> mset;
	long long sum = 0;
	while(n--)
	{
		int k;
		cin >> k;
		while(k--)
		{
			int temp;
			cin >> temp;
			mset.insert(temp);
		}
		auto itend = --mset.end();
		auto itbeg = mset.begin();
		sum += *(itend) - *(itbeg);
		mset.erase(itend);
		mset.erase(itbeg);
	}
	cout << sum << endl;
}