#include <iostream>
#include <vector>
#include <queue>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <iomanip>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define allof(x) x.begin(),x.end()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

///////////////////// DFS Solution /////////////////////
/*

bool visited[10005];

void DFS(int start, vector<vi> &graph)
{
	visited[start] = true;
	For(i,0,graph[start].size())
		if(!visited[graph[start][i]])
			DFS(graph[start][i],graph);
}

int main()
{
	int n,m;
	cin >> n >> m;
	vector<vi> graph(n);
	For(i,0,m)
	{
		int u,v;
		cin >> u >> v;
		graph[u-1].push_back(v-1);
		graph[v-1].push_back(u-1);
	}
	bool res = 1;
	DFS(0,graph);
	For(i,0,n)
	{
		res &= visited[i];
	}
	res &= (m==n-1);
	if(res) cout << "YES" << endl;
	else cout << "NO" << endl;
}*/

////////////////////// Disjoint-Set Solution ////////////////////

int parent[10005];

int getParent(int i)
{
	while(parent[i] != i)
		i = parent[i];
	
	return i;
}

int main()
{
	int n,m;
	cin >> n >> m;
	For(i,0,n)
	{
		parent[i] = i;
	}
	For(i,0,m)
	{
		int u,v;
		cin >> u >> v;
		u--;v--;
		if(getParent(u) == getParent(v))
		{
			cout<<"NO"<<endl;
			return 0;
		}
		else 
		{
			parent[getParent(u)] = getParent(v);
		}
	}
	cout <<"YES" << endl;
}