#include <iostream>
#include <vector>
#include <string.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Trace(x) cout<<#x<<"   :   "<<x<<endl
using namespace std;

typedef vector<int> vi;

int clr[2005];

bool DFS(int s, vector<vi> &graph)
{
	For(i,0,graph[s].size())
	{
		int child = graph[s][i];
		if(clr[child] == clr[s])
			return true;
			
		else if(clr[child] == -1)
		{
			clr[child] = !clr[s];
			if(DFS(child,graph))
				return true;
		}
	}
	return false;
}

int main()
{
	ios_base::sync_with_stdio(0);
	cin.tie();
	int t;
	cin >> t;
	For(i,0,t)
	{
		memset(clr, -1, sizeof clr);
		int n, m;
		cin >> n >> m;
		vector<vi> graph(n);
		For(j,0,m)
		{
			int u,v;
			cin >> u >> v;
			graph[u-1].push_back(v-1);
			graph[v-1].push_back(u-1);
		}
		cout<<"Scenario #"<<i+1<<":"<<endl;
		bool res = 0;
		For(j,0,n)
		{
			if(clr[j] == -1)
			{
				clr[j] = 0;
				res |= DFS(j,graph);
			}
		}
		if(res)	
			cout << "Suspicious bugs found!" << endl;
		else
			cout << "No suspicious bugs found!" << endl;
	}
}