#include<iostream>
#include<vector>

#define ll long long

using namespace std;

bool isPrime(ll n)
{
	if (n < 4) return true;
	if (n % 2 == 0 || n % 3 == 0) return false;
	
	for (ll i = 5; i <= sqrt(n); i += 6)
		if (n%i == 0 || n % (i + 2) == 0) return false;
	
	return true;
}

int main()
{
	ll totalcount = 1;
	ll primecount = 0;
	for (ll i = 2;; i++)
	{
		totalcount += 4;
		ll temp = 2 * i - 1;

		for (int j = 1; j < 4; j++)
			if (isPrime((temp*temp) - j*(temp - 1))) 
				primecount++;
		
		if (primecount * 10 < totalcount)
		{
			cout << temp << endl;
			return 0;
		}
	}
}