#include<iostream>
#include<vector>
#include<algorithm>

using namespace std;

bool sameDigits(int a, int b)
{
	int checked[10] = {};
	
	while (a>0)
	{
		checked[a%10]++;
		a /= 10;
	}
	while (b>0)
	{
		checked[b % 10]--;
		b /= 10;
	}
	for (int i = 0; i < 10; i++)
	{
		if (checked[i] != 0) return 0;
	}
	return 1;
}

int main()
{
	int digit = 2;
	int limit = _Pow_int(10, digit) / 6;
	for (long int i = 10;;i++)
	{
		if (i > limit)
		{
			i = _Pow_int(10, digit++);
			limit = i * 10 / 6;
		}
		
		bool flag = false;

		for (int j = 2; j < 7; j++)
		{
			if (!sameDigits(i,i*j))
			{
				flag = true;
				break;
			}
		}
		if (flag) continue;
		cout << i << endl;
		return 0;
	}
}