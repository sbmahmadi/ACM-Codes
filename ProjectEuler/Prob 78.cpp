#include<iostream>
#include<vector>

using namespace std;

int p[100000];

int pentagon(int n)
{
	return n*(3 * n - 1) / 2;
}

int euler(int n)
{
	if (n < 0) return 0;
	int t = 1;
	int sum = 0;
	
	if (p[n] != -1)
	{
		return p[n];
	}
	else
	{
		while (1)
		{
			int k = n - pentagon(t);
			
			if (k < 0) break;
			
			if(abs(t)%2==1) sum += euler(k);
			else sum -= euler(k);

			if (t > 0) t *= -1;
			else t = t*-1 + 1;
		}
		return p[n] = sum%1000000;
	}
}

int main()
{
	memset(p, -1, sizeof p);
	p[0] = 1;
	
	int t = 0;
	for (; euler(t) % 1000000 != 0; t++);
	cout << t<<endl;
}