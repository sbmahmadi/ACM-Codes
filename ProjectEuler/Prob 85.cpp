#include<iostream>

using namespace std;

typedef long long ll;

ll twoOfN(int n)
{
	return (n * (n - 1)) / 2;
}

ll rectCount(int x, int y)
{
	return twoOfN(x) * twoOfN(y);
}

int main()
{
	ll min = 500000;
	pair<int, int> res;

	for (int i = 2; i <= 2000; i++)
		for (int j = 2; j <= i; j++)
		{
			ll temp = rectCount(i, j);
			if (abs(temp - 2000000) < min)
			{
				min = abs(temp - 2000000);
				res.first = i;
				res.second = j;
			}
		}
	cout << (res.first - 1)* (res.second - 1);
}