#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;

vector<int> sum(vector<int>a, vector<int>b)
{
	vector<int> c;
	int carry = 0,buff=0;

	for (int i = 0; i<a.size(); i++)
	{
		buff = b[i] + a[i] + carry;
		b[i] = buff % 10;
		carry = buff / 10;
	}
	
	for (int i = a.size(); i < b.size(); i++)
	{
		buff = b[i] + carry;
		b[i] = buff % 10;
		carry = buff / 10;
	}

	if (carry != 0)
	{
		b.push_back(carry);
	}

	return b;
}

int main()
{
	vector<vector<int>> a;
	vector<int> f = { 1 };
	a.push_back(f);
	a.push_back(f);

	while (a[a.size() - 1].size() < 1000)
	{
		a.push_back(sum(a[a.size() - 2], a[a.size() - 1]));
	}
		cout << a.size();
}