#include<iostream>
#include<string>
#include<fstream>
#include<algorithm>

#define For(i,a,b) for(int i = a; i < b; i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)

using namespace std;

class Card
{
public:
	int value;
	char suit;
	string hand;
	bool operator<(const Card c)
	{
		return value < c.value;
	}
};

bool isSameSuit(const Card a[])
{
	For(i, 1, 5)
		if (a[i].suit != a[i - 1].suit) 
			return false;
	
	return true;
}

int sameValue(const Card a[])
{
	int sum = 0;
	For(i, 1, 5)
		if (a[i].value == a[i - 1].value) sum++;
	
	return sum;
}

bool isConsecutive(const Card a[])
{
	For(i, 1, 5)
		if (a[i].value != a[i - 1].value + 1) return false;
	
	return true;
}

int scoreHand(const Card a[])
{	
	int sv = sameValue(a);
	
	if (sv == 3)
			return 7;
	
	if (isSameSuit(a))
		return 6;
	
	if (isConsecutive(a))
		return 5;
	
	else if (sv == 2)
	{
		int table[15] = {};
		For(i, 0, 5)
			table[a[i].value]++;
		
		int k = *max_element(table, table + 15) + 1;
		return k;
			
	}
	else if (sv == 1) 
		return 2;
	
	return 1;
}
  
bool biggerWins(const Card p1[], const Card p2[])
{
	roF(i, 4, 0)
	{
		if (p1[i].value > p2[i].value) return true;
		else if (p1[i].value < p2[i].value) return false;
	}
}

bool checkTie(const Card p1[],const Card p2[],const int rank)
{
	if (rank == 1)
		return biggerWins(p1, p2);
	
	else if (rank == 2)
	{
		int pair1, pair2;
		For(i, 1, 5)
		{
			if (p1[i].value == p1[i - 1].value) pair1 = p1[i - 1].value;
			if (p2[i].value == p2[i - 1].value) pair2 = p2[i - 1].value;
		}
		if (pair1 == pair2) return biggerWins(p1, p2);
		else return pair1 > pair2;
	}
	return false;
}

int main()
{
	ifstream input("p054_poker.txt");
	
	int win = 0;
	int tie = 0;
	
	For(i, 0, 1000)
	{
		Card p1[5], p2[5];
		For(j, 0, 5)
		{
			string s;
			input >> s;
			p1[j].hand = s;
		
			switch (s[0])
			{
			case 'A': p1[j].value = 14;				  break;
			case 'Q': p1[j].value = 12;				  break;
			case 'K': p1[j].value = 13;				  break;
			case 'J': p1[j].value = 11;				  break;
			case 'T': p1[j].value = 10;				  break;
			default: p1[j].value = s[0] - '0';        break;
			}

			p1[j].suit = s.back();
		}

		For(j, 0, 5)
		{
			string s;
			input >> s;

			p2[j].hand = s;
			switch (s[0])
			{
			case 'A': p2[j].value = 14;				  break;
			case 'Q': p2[j].value = 12;				  break;
			case 'K': p2[j].value = 13;				  break;
			case 'J': p2[j].value = 11;				  break;
			case 'T': p2[j].value = 10;				  break;
			default : p2[j].value = s[0] - '0';       break;
			}
			p2[j].suit = s.back();
		}

		sort(p1, p1 + 5);
		sort(p2, p2 + 5);
	
		if (scoreHand(p1) > scoreHand(p2) ||
			(scoreHand(p1) == scoreHand(p2) && 
			checkTie(p1, p2, scoreHand(p2))))
			win++;
	}
	printf("Win: %d\n", win);
}