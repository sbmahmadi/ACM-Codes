#include <iostream>
#include <vector>
#include <string>

using namespace std;

int digitCount(int n)
{
	int sum = 0;
	while (n > 0)
	{
		sum++;
		n /= 10;
	}
	return sum;
}

void turnNumber(int& n, int digit)
{
	string s = to_string(n);

	for (int i = s.size(); i < digit; i++)
	{
		s.insert(s.begin(), '0');
	}

	char temp = s[0];
	for (int i = 0; i < s.size()-1; i++)
	{
		s[i] = s[i + 1];
	}
	s[s.size() - 1] = temp;


	n = stoi(s);
}

int main()
{
	int nums[1000000] = {};
	vector<int>circle;

	for (int i = 2; i <= 1000; i++)
	{
		if (nums[i] == 0)
		{
				for (int j = i * 2; j < 1000000; j+=i)
					nums[j] = -1;
		}
	}
	
	for (int i = 2; i < 1000000; i++)
	{
		if (nums[i] != 0) continue;

		int dc = digitCount(i);
		int temp = 0;
		
		for (int j = 0; j < dc; j++)
		{
			if (nums[i] == 0) temp++;
			circle.push_back(i);
			turnNumber(i,dc);
		}

		if (temp == dc)
		{
			for (int j = 0; j < circle.size(); j++)
			{
				nums[circle[j]] = 1;
				//cout << circle[j]<<endl;
			}
		}
		circle.clear();
	}

	long int sum = 0;
	for (int i = 2; i < 1000000; i++)
	{
		if (nums[i] == 1)
		{
			sum++;
			
		}
	}

	cout << sum << endl;
}