#include <iostream>
#include <math.h>
#include <vector>

using namespace std;

bool cprime (long long int a)
{
    int sn,i;
    sn = trunc(sqrt(a));
    for (i=2; i<=sn;i++)
    {
        if(a%i ==0)
        {
            return 0;
        }
    }
    return 1;
}
int main ()
{
    int counter=0;
    int n=1;
    while (counter != 10001)
    {
        n++;
        if(cprime(n)==1) counter++;
    }

    cout<<n;

}
