#include<iostream>
#include<vector>
using namespace std;

//Check For Mode of Number

bool Check (long int a)
{
	int sum=0;
	for (int i = 1; i <= a / 2; i++)
	{
		if (a%i == 0)
		{
			sum += i;
		}
	}

	if (sum > a) { return true; }
	return false;
}



int main()
{
	int a[28124] = {};
	vector<int> abundant;

	//Saving Abundant Numbers in Vector

	for (int i = 1; i < 28124; i++)
	{
		if (Check(i) == true)
		{
			abundant.push_back(i);
		}
	}

	//Finding And Flagging All Possible Combinations of Sum of Abundant Numbers

	long int k;
	for (int i = 0; i < abundant.size(); i++)
	{
		for (int j = 0; j < abundant.size(); j++)
		{
			k = abundant[i] + abundant[j];
			if (k <= 28123)
			{
				a[k] = 1;
			}
		}
	}

	//Finding Sum of Non-Flaged Numbers

	long long int sum=0;
	for (int i = 1; i < 28124; i++)
	{
		if (a[i] != 1)
		{
			sum += i;
		}
	}

	cout << sum;
}