#include<iostream>
#include<vector>
#include<unordered_set>
#include<set>
using namespace std;


void primeSieve(int* num)
{
	for (int i = 2; i < 1000; i++)
		if (num[i] == 0)
			for (int j = i * 2; j < 1000000; j += i)
				num[j] = -1;
}

int main()
{
		vector<int> primes;
		vector<long long> sumPrimes;
		unordered_set<long long> primeset;

		int num[1000000] = {};
		
		primeSieve(num);
			
		//////////////////Building Prime Set and PrimeSum Array////////////////

		primeset.insert(2);
		sumPrimes.push_back(2);

		for (int i = 3; i < 1000000; i++) 
			if (num[i] == 0) 
			{

				sumPrimes.push_back(i + sumPrimes.back());
				primeset.insert(i);
			}

		////////////////Process/////////////////////////
		
			cout << "Pre-Process Done" << endl << "# of Buckets: " << primeset.bucket_count() << endl;

		//Sum of first 546 primes goes above one million
		int sequenceSize = 546;
		
		while (true)
		{
			int begin = 0;	
			while (begin + sequenceSize != primeset.size())
			{
				if (begin != 0 && sequenceSize % 2 == 0) break;
				if (primeset.count((sumPrimes[sequenceSize + begin] - sumPrimes[begin])))
				{
					cout << "Result : "<<sumPrimes[sequenceSize + begin] - sumPrimes[begin] << endl;
					return 0;
				}
				begin++;
			}
			sequenceSize--;
		}
}
