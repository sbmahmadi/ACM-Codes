#include <iostream>
#include "VispparLib.h"
#include <vector>
using namespace std;

int main()
{
	int maxA, MaxB, MaxN = 0;
	for (int b = 2; b < 1000; b++)
	{
		if (!CheckPrime(b)) continue;
		for (int a = -999; a < 1000; a++)
		{
			int n = 1;
			while (CheckPrime(n*n + a*(n++) + b));

			if (n>MaxN)
			{
				MaxN = n;
				MaxB = b;
				maxA = a;
			}
		}
	}

	cout << maxA*MaxB;
}