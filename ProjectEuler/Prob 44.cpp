#include<iostream>

using namespace std;

bool isPentagonal(int x)
{
	double temp = (sqrt(24.0*x +1.0) + 1.0) / 6.0;
	return (temp == (int)temp);
}

long int pentagonal(int n)
{
	return n*(3 * n - 1.0) / 2.0;
}


int main()
{
	for (int i = 1;; i++)
	{
		for (int j = i - 1; j >= 1; j--)
		{
			if (isPentagonal(pentagonal(i) + pentagonal(j)) && isPentagonal(pentagonal(i) - pentagonal(j)))
			{
				cout << pentagonal(i) - pentagonal(j) << endl;
				return 0;
			}	
		}
	}
}