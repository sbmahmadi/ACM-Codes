#include<iostream>

using namespace std;

int Combination(int n, int r)
{
	long long int temp = 1;
	for (int i = (n - r + 1); i <= n; i++)
	{
		temp *= i;
	}
	for (int i = 2; i <= r; i++)
	{
		temp /= i;
	}
	return temp;
}



int main()
{
	long int res = 0;
	for (int i = 23; i <= 100; i++)
	{
		int r = 4;
		while (Combination(i, r) <= 1000000) r++;
		if (i % 2 == 1)
		{
			res += ((i + 1) / 2 - r) * 2;
		}
		else
		{
			res += (((i / 2) - r) * 2) + 1;
		}
	}
	cout << res;
}