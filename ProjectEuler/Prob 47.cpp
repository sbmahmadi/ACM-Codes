#include<iostream>
#include<vector>

using namespace std;

void fill(vector<int> &a,int b)
{
	a.clear();
	int temp = b;
	for (int i = 2; i <= temp / 2; i++)
	{
		if (b%i == 0)
		{
			a.push_back(i);
			while (b%i == 0) b /= i;
		}
	}
}

int main()
{
	// 2 * 3 * 5 * 7 = 210
	int n = 209;
	vector<int> a[4];

	while (true, n++)
	{
		bool invalid = false;
		for (int i = 0; i < 4; i++)
		{
			fill(a[i], n + i);
			if (a[i].size() != 4) 
			{
				invalid = true;
				n += i;
				break;
			}
		}
		if (invalid) continue;

		cout << n;
		return 0;
	}
}