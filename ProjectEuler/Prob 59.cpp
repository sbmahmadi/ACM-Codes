#include<iostream>
#include<vector>
#include<fstream>
#include<string>
#include<algorithm>

#define For(i,a,b) for(int i = a;i<b;i++)
#define allof(a) a.begin(),a.end() 

using namespace std;

int main()
{
	ifstream fin("p059_cipher.txt");
	string a;
	vector<int> text[3];
	
	int temp = 0;
	while (getline(fin,a,','))
	{
		text[temp%3].push_back(stoi(a));
		temp++;
	}
	
	sort(allof(text[0]));
	sort(allof(text[1]));
	sort(allof(text[2]));

	int c[3] = {};
	For(j, 0, 3)
	{
		int mx = 0;
		For(i, 1, text[j].size())
		{
			int count = 0;
			while (text[j][i] == text[j][i - 1])
			{
				i++;
				count++;
			}
			if (count > mx)
			{
				mx = count; c[j] = text[j][i - 1];
			}
		}
	}

	int space = ' ';
	char key[3];

	For(i, 0, 3)
		key[i] = c[i] ^ space;
	
	int sum = 0;

	For(i, 0, 401)
		For(j, 0, 3)
			if (i != text[j].size())
				sum += (text[j][i] ^ key[j]);
		
	cout << sum << endl;
}

