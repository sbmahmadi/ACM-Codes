#include<iostream>
#include<math.h>
#include<fstream>
#include<string>

using namespace std;

int main()
{
	ifstream fin("p099_base_exp.txt");
	
	int base = 0, ex = 0,mxline = 0;
	long double mx = 0;
	
	string s1,s2;
	int i = 0;
	for (int i = 1; !fin.eof();i++)
	{
		getline(fin, s1, ',');
		getline(fin, s2);
		base = stoi(s1);
		ex = stoi(s2);
		long double num = ex*log10(base);
		if (num > mx)
		{
			mx = num;
			mxline = i;
		}
	}
	cout << mxline;
}