#include<iostream>
#include<fstream>
#include<string>
#include<algorithm>

#define For(i,a,b) for(int i = a; i<b; i++)
#define roF(i,b,a) for(int i = b; i >=a ; i--)

using namespace std;

int main()
{
	int map[80][80];
	ifstream in("p081_matrix.txt");
	
	For (i, 0, 80)
	{
		For(j, 0, 80)
		{
			string s;
			while (in.peek() != ',' && in.peek() != '\n')
			{
				char c = in.get();
				s += c;
			}
			in.ignore(1);
			map[i][j] = stoi(s);
		}
	}
	
	cout << "File Read Done" << endl;


	int dynamic[80][80];
	roF(i, 79, 0)
	{
		roF(j, 79, 0)
		{
			if (i == j && i == 79) dynamic[i][j] = map[i][j];
			else if (i == 79) dynamic[i][j] = dynamic[i][j + 1] + map[i][j];
			else if (j == 79) dynamic[i][j] = dynamic[i + 1][j] + map[i][j];
			else dynamic[i][j] = min(dynamic[i + 1][j], dynamic[i][j + 1]) + map[i][j];
		}
	}
	cout << dynamic[0][0] << endl;

}