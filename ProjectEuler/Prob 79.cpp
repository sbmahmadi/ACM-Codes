#include<iostream>
#include<fstream>
#include<vector>
#include<string>
#include<deque>
#include<list>

using namespace std;

typedef vector<int> vi;

void dfs(int i, vector<pair<vi, bool>> &graph,list<int> &list)
{
	if (!graph[i].second)
	{
		for (int j = 0; j < graph[i].first.size(); j++)
		{
			dfs(graph[i].first[j], graph, list);
		}
		graph[i].second = true;
		list.push_front(i);
	}
}

int main()
{
	ifstream fin("p079_keylog.txt");
	
	vector<pair<vi,bool>> graph;
	string s;
	
	graph.assign(10, pair<vi,bool>());
	for (int i = 0; i < 10; i++)
	{
		graph[i].second = true;
	}
	
	while (fin >> s)
	{
		graph[s[0] - '0'].first.push_back(s[1] - '0');
		graph[s[1] - '0'].first.push_back(s[2] - '0');
		graph[s[0] - '0'].second = false; graph[s[2] - '0'].second = false; graph[s[1] - '0'].second = false;
	}
	list<int> list;
	
	for (int i = 1; i < graph.size(); i++)
	{
		if (!graph[i].second) dfs(i, graph,list);
	}

	for (auto it = list.begin(); it != list.end(); it++)
	{
		cout << *it;
	}
}