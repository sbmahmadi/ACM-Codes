#include<iostream>
#include<set>
#include<string>
#include<fstream>

using namespace std;

int main()
{
	
	ifstream myfile;
	set <string> b;

	myfile.open("names.txt");
	char k;
	string name;
	
	//Dividing Names And putting Them in Set

	while (myfile.good())
	{

		k = myfile.get();
		if ( k == '"')
		{
			k = myfile.get();
			name = "";
			while (k!='"')
			{
				name += k;
				k = myfile.get();
			}
			b.insert(name);
		}
	}

	//Scorring Names

	string str;
	unsigned long long int sum=0;
	for (set<string>::iterator it = b.begin(); it != b.end(); it++)
	{
		str = *it;
		int buff=0;
		for (int i = 0; i < str.size(); i++)
		{
			buff += str[i] - 'A' +1;
		}
		int d = distance(b.begin(), it);
		d++;
		sum += d*buff;
	}

	//Printing The Score

	cout << sum;

}
