#include<iostream>

using namespace std;

bool isTriangle(int a, int b, int c)
{
	if (_Pow_int(a, 2) + _Pow_int(b, 2) == _Pow_int(c, 2)) return true;
	return false;
}

int main()
{
	int maxSum = 0;
	int maxSumP = 0;
	for (int p = 1000; p > 3; p--)
	{
		int sum = 0;
		for (int c = p - 2; c >= ceil(p / 3); c--)
		{
			for (int b = p - c - 1; b >= ceil((p - c) / 2); b--)
			{
				int a = p - c - b;
				if (isTriangle(a, b, c)) sum++;
			}
		}
		if (sum > maxSum)
		{
			maxSum = sum;
			maxSumP = p;
		}
	}

	cout << maxSumP;
}