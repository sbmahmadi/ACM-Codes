#include <iostream>
#include <math.h>

using namespace std;

int Factorial(int n)
{
	return (n < 2) ? 1 : n*Factorial(n - 1);
}

int main()
{
	int facts[10];

	for (int i = 0; i < 10; i++)
	{
		facts[i] = Factorial(i);
	}

	long int sum = 0;
	
	for (long int i = 10; i < 2540200; i++)
	{
		long int temp = i;
		long int tempsum = 0;
		while (temp>0)
		{
			tempsum += facts[temp % 10];
			temp /= 10;
		}
		if (tempsum == i) sum += i;
	}

	cout << sum;

}