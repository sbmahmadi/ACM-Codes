#include<iostream>
#include<vector>
#include<unordered_map>

#define For(i,a,b) for(int i = a;i<b;i++)

using namespace std;

void bigSum(vector<int> &a, const vector<int> &b)
{
	int temp = 0;
	For(i, 0, a.size())
	{
		int t = (a[i] + b[i] + temp);
		a[i] = (t % 10);
		temp = t / 10;
	}
	while (temp)
	{ 
		a.push_back(temp % 10); temp /= 10;
	}
}

bool isPalindrome(const vector<int> &a)
{
	For(i, 0, a.size() / 2)
	{
		if (a[i] != a[a.size() - 1 - i]) return false;
	}
	return true;
}

bool mapToPalindrome(vector<int> &number, int level)
{
	if (level == 50) return 0;
	else
	{
		vector<int> mirror = number;
		reverse(mirror.begin(), mirror.end());
		bigSum(number, mirror);
		if (isPalindrome(number)) return 1;
		else return mapToPalindrome(number, level + 1);
	}
}

int main()
{
	int sum = 0;
	For(i, 1, 10000)
	{
		int temp = i;
		vector<int> a;
		while (temp)
		{
			a.push_back(temp % 10);
			temp /= 10;
		}
		if (!mapToPalindrome(a, 1)) sum++;
	}
	cout << sum << endl;
}