#include<iostream>
#include<forward_list>

using namespace std;

bool isPrime(int a)
{
	for (int i = 5; i <= sqrt(a); i += 6)
		if (a%i == 0 || a % (i + 2) == 0) return false;
	
	return true;
}

bool sameDigits(int a, int b)
{
	int checked[10] = {};

	while (a>0){
		checked[a % 10]++;
		a /= 10;
	}
	while (b>0){
		checked[b % 10]--;
		b /= 10;
	}
	for (int i = 0; i < 10; i++)
		if (checked[i] != 0) return 0;
	return true;
}

int main()
{
	forward_list<int> primes;
	for (int i = 1001; i < 10000; i += 6)
	{
		if (isPrime(i) && isPrime(i + 3330)) 
			primes.push_front(i);
		if (isPrime(i + 2) && isPrime(i + 3332)) 
			primes.push_front(i + 2);
		if (!primes.empty())
			if (primes.front() == 1487 || primes.front() == 4817 || primes.front() == 8147) 
				primes.pop_front();
	}

	for (auto it = primes.begin(); it != primes.end(); it++)
		for (auto it2 = next(it); it2 != primes.end(); it2++)
			if (isPrime(*it2 - (*it - *it2)))
				if (sameDigits(*it, *it2) && sameDigits(*it2 - (*it - *it2), *it))
				{
					printf("%d | %d | %d\n", *it2 - (*it - *it2), *it2, *it);
					return 0;
				}
}