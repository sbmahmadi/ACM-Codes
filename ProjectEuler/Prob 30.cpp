#include <iostream>
#include<math.h>
using namespace std;

void main()
{
	int Powers[10];

	for (int i = 0; i < 10; i++)
	{
		Powers[i] = pow(i, 5);
	}

	long int sum = 0;

	for (int i = 2; i < 360000; i++)
	{
		int temp = i;
		long int DigitSum = 0;

		while (temp>0)
		{
			DigitSum += Powers[temp % 10];
			temp /= 10;
		}

		if (DigitSum == i) sum += DigitSum;
	}

	cout << sum;
}
