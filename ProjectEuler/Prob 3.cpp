#include<iostream>

using namespace std;

int main()
{
	long long num = 600851475143;

	int divisor;

	while (num > 1)
		for (long long i = 2; i <= num; i++)
			if (num%i == 0)
			{
				num /= i;
				divisor = i;
				break;
			}
	
	cout << divisor;
}