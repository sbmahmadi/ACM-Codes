#include<iostream>

using namespace std;

bool isPrime(int a)
{
	for (int i = 2; i <= sqrt(a); i++)
	{
		if (a%i ==0) return 0;
	}
	return 1;
}

bool isPandigital(int a)
{
	int b[8] = {};
	while (a > 0)
	{
		int temp = a % 10;
		a /= 10;
		if (temp > 7 || temp == 0) return 0;
		if (b[temp] != 0) return 0;
		else b[temp]++;
	}
	return 1;
}

int main()
{
	for (int i = 7654321; i >= 1234567; i--)
	{
		if (isPandigital(i))
		{
			if (isPrime(i))
			{
				cout << i<<endl;
				return 0;
			}
		}
	}
}