#include <iostream>
#include <string>

using namespace std;

bool isPandigital(int n)
{
	int k[10] = {1};
	while (n > 0)
	{
		if (k[n % 10] == 0) k[n % 10] = 1;
		else return false;
		n /= 10;
	}
	return true;
}

int main()
{
	for (int i = 987654321; i >= 930000000; i--)
	{
		if (isPandigital(i))
		{
			string s = to_string(i);
			
			for (int j = 2; j < 5; j++)
			{
				string temp = s.substr(0, j);
				int ntemp = stoi(temp);
				for (int k = 2; temp.size() < 9; k++)
				{
					temp += to_string(ntemp * k);
				}
				if (temp == s)
				{
					cout << s <<"   " <<j<< endl;
					return 0;
				}
			}
		}
	}
}