 /*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("p096_sudoku.txt");
ofstream fout("file.out");

int grid[9][9];
bool foundSolution;

ii next(int i, int j)
{
	if(j == 8)
		return make_pair(i+1,0);
	return make_pair(i,j+1);
}

bool OK(int i, int j)
{
	int me = grid[i][j];
	For(k,0,9)
	{
		if(grid[i][k] == me && k != j)
			return false;
		if(grid[k][j] == me && k != i)
			return false;
	}
	int i2 = i - i%3, j2 = j - j%3;
	For(p,0,3)
		For(q,0,3)
		{
			if(p + i2 == i && q + j2 == j)
				continue;
			if(grid[p+i2][q+j2] == me)
				return false;
		}
	return true;
}

void solve(int i,int j)
{
	if(i == 9)
	{
		foundSolution = true;
		return;
	}
	ii nxt = next(i,j);
	if(grid[i][j] > 0)
	{
		solve(nxt.first,nxt.second);
		if(foundSolution)
			return;
	}
	else
	{
		For(k,1,10)
		{
			grid[i][j] = k;
			if(OK(i,j))
			{
				solve(nxt.first,nxt.second);
				if(foundSolution)
					return;
			}
		}
		grid[i][j] = 0;
	}
}

int main()
{
	int res = 0;
	char c;
	For(t,0,50)
	{
		fin.ignore(255, '\n');
		For(i,0,9)
			For(j,0,9)
			{
				fin >> c;
				grid[i][j] = c - '0';
			}
		fin.ignore();
		foundSolution = false;
		solve(0,0);
		cout << t+1 << " Done." << endl;
		res += grid[0][0] * 100 + grid[0][1] * 10 + grid[0][2];
	}
	cout << res << endl;
}
