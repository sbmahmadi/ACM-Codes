#include <iostream>
#include <string>

using namespace std;

bool isPalindrome(string s)
{
	for (int i = 0; i < s.size() / 2; i++)
	{
		if (s[i] != s[s.size() - i - 1]) return false;
	}
	return true;
	
}


int main()
{
	long int sum = 0;
	for (int i = 0; i < 1000000; i++)
	{
		if (isPalindrome(to_string(i)))
		{
			string sTemp = "";
			int nTemp = i;
			while (nTemp>0)
			{
				sTemp += nTemp % 2;
				nTemp /= 2;
			}

			if (isPalindrome(sTemp)) sum += i;
		}
	}

	cout << sum;
}