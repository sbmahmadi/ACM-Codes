#include<iostream>
#include<fstream>
#include<vector>

using namespace std;

int main()
{
	vector<int> TriangleNums;
	TriangleNums.push_back(1);
	for (int i = 2; i < 1000; i++)
	{
		TriangleNums.push_back(i + *(TriangleNums.end()-1));
	}

	ifstream input;
	input.open("p042_words.txt");

	int sum = 0;

	while (!input.eof())
	{
		int wordScore = 0;

		if (input.get() == '"')
		{
			while (input.peek() != '"')
			{
				wordScore += input.get() - 'A' + 1;
			}

			if (find(TriangleNums.begin(), TriangleNums.end(), wordScore) != TriangleNums.end()) sum++;
			input.ignore(2);
		}
	}
	cout << sum;
	
}