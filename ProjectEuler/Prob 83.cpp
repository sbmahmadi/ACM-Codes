#include<iostream>
#include<fstream>
#include<string>
#include<algorithm>

#define For(i,a,b) for(int i = a;i<b;i++)

#define SIZE 80

using namespace std;

int xDif[4] = { 1,-1,0,0 };
int yDif[4] = { 0,0,1,-1 };

struct Node
{
	int val;
	int state = 0;
	int distance = INT_MAX;
};

void dijkstra(Node graph[SIZE][SIZE])
{
	int x;
	int y;
	while (1)
	{
		int mn = INT_MAX;
		For(i, 0, SIZE)
		{
			For(j, 0, SIZE)
			{
				if (graph[i][j].state == 1 && graph[i][j].distance < mn)
				{
					mn = graph[i][j].distance;
					x = i;
					y = j;
				}
			}
		}
		if (mn == INT_MAX) return;
		graph[x][y].state = 2;
		For(i, 0, 4)
		{
			int newx = x + xDif[i];
			int newy = y + yDif[i];
			if (newx < SIZE && newx >= 0 && newy < SIZE && newy >= 0)
			{
				if (graph[newx][newy].state != 2)
				{
					graph[newx][newy].state = 1;
					graph[newx][newy].distance = min(graph[newx][newy].distance, graph[x][y].distance + graph[newx][newy].val);
				}
			}
		}

	}
}

int main()
{
	ifstream fin("p083_matrix.txt");
	string s;

	Node graph[SIZE][SIZE];

	For(i, 0, SIZE)
	{
		For(j, 0, SIZE-1)
		{
			getline(fin, s, ',');
			graph[i][j].val = stoi(s);
		}
		getline(fin, s);
		graph[i][SIZE-1].val = stoi(s);
	}
	
	graph[0][0].distance = graph[0][0].val;
	graph[0][0].state = 1;
	dijkstra(graph);

	cout << graph[SIZE-1][SIZE-1].distance;
}

