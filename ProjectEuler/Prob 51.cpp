/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <list>
#include <vector>
#include <queue>
#include <deque>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <math.h>
#include <algorithm>
#include <string>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <complex>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

ifstream fin("file.in");
ofstream fout("file.out");

bool vals[10][6] = {{1,1,1,0,0,0} ,{1,1,0,1,0,0} ,{1,1,0,0,1,0} ,{1,0,1,1,0,0} ,{1,0,1,0,1,0} ,{1,0,0,1,1,0} ,{0,1,1,1,0,0} ,{0,1,1,0,1,0} ,{0,1,0,1,1,0} ,{0,0,1,1,1,0}};


bool isPrime(int a)
{
	if (a%2 == 0 || a%3 == 0)
		return false;
	for(int i =5; i <= sqrt(a); i+= 6)
	{
		if(a%i == 0 || a%(i+2) == 0)
			return false;
	}
	return true;
}

//
bool valid(int a)
{
	For(i,0,10)
	{
		if(same(a,i) && othersum(a,i) % 3 != 0)
	}
}

int main()
{
	For(i,1e5,1e6)
	{
		if(isPrime(i) && valid(i))
		{
			cout << i << endl;
			break;
		}
	}
}