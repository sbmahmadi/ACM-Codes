#include <iostream>

using namespace std;

int main()
{

	long long int a[10000] = {};
	a[1] = 1;
	long long int buffer,counter;
	long long int max = 0;
	int maxi = 0;

	for (long long int i = 2; i < 1000000; i++)
	{
		buffer = i;
		counter = 1;
		while (buffer != 1)
		{
			if (buffer % 2 == 0)
			{
				buffer /= 2;
			}
			else
			{
				buffer = (buffer * 3) + 1;
			}
			counter++;
			if (buffer < 10000)
			{

				if (a[buffer] != 0)
				{
					counter += a[buffer];
					break;
				}
			}
		}

		if (i < 10000) { a[i] = counter; }
		if (counter > maxi)
		{
			maxi = counter;
			max = i;
		}
	}

	cout << max;

}