#include<iostream>
#include<vector>
#include<math.h>

using namespace std;

bool isPrime(long long a)
{
	if (a <= 1)
		return false;
	else if (a <= 3)
		return true;
	else if (a % 2 == 0 || a % 3 == 0)
		return false;

	long long k = sqrt(a);
	for (int i = 5; i <= k; i += 6)
		if (a % i == 0 || a % (i + 2) == 0) return false;
	
	return true;
}

int main()
{
	
	long long squareRoot = 1;
	vector<long long> squares = { 1, 4 };

	for (long long oddNum = 5;; oddNum += 2)
	{
		if (isPrime(oddNum)) continue;
		if (oddNum > squares.back())
		{
			squareRoot++;
			squares.push_back(squareRoot * squareRoot);
		}
		bool found = true;
		for (long long j = 0; j < squares.size(); j++)
			if (isPrime(oddNum - 2 * squares[j])) 
			{
				found = false;
				break;
			}
		
		if (found)
		{
			cout << "Result: " << oddNum << endl;
			return 0;
		}
	}
}