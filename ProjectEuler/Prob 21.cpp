#include<iostream>

using namespace std;

long long int DivSum(long long int a)
{
	long long int sum=0;
	for (int i = 1; i <= a / 2; i++)
		if (a%i == 0) 
			sum += i;
	return sum;
}

int main()
{
	long long int sum=0;

	for (int i = 3; i < 10000; i++)
		if (DivSum(DivSum(i)) == i && DivSum(i)!=i)
			sum += i;

	cout << sum;
}