#include<iostream>

using namespace std;

int nums[1000000] = {};


int digitCount(long int n)
{
	int temp = 0;
	while (n > 0)
	{
		temp++;
		n /= 10;	
	}
	return temp;
}

bool leftTrunc(int i)
{
	while (i > 0)
	{
		if (nums[i] == -1) return false;
		i %= _Pow_int(10, digitCount(i) - 1);
	}
	return true;
}

bool rightTrunc(int i)
{
	while (i > 0)
	{
		if (nums[i] == -1) return false;
		i /= 10;
	}
	return true;
}

int main()
{
	for (int i = 2; i <= 1000; i++)
	{
		if (nums[i]==0)
		for (int j = i * 2; j < 1000000; j+=i)
		{
			nums[j] = -1;
		}
	}
	nums[0] = nums[1] = -1;

	
	int counter = 0,sum = 0;

	for (int i = 10; i < 1000000; i++)
	{
		if (nums[i] == 0)
		{
			if (leftTrunc(i) & rightTrunc(i))
			{
				counter++;
				sum += i;
				cout << i << endl;
				if (counter == 11) break;
			}
		}
	}
	cout << sum << endl;
}