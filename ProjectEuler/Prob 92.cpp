#include<iostream>
#include<time.h>

using namespace std;

int step(int n)
{
	int sum = 0;
	while (n)
	{
		sum += (n % 10) * (n % 10);
		n /= 10;
	}
	return sum;
}

int d[10000000];

bool is89(int n)
{
	if (d[n] != -1) return d[n];
	int &ret = d[n];
	return ret = is89(step(n));
}

int main()
{
	long t = clock();
	memset(d, -1, sizeof(d));
	d[1] = 0;
	d[89] = 1;

	int sum = 0;
	for (int i = 1; i < 10000000; i++)
	{
		sum += is89(i);
	}
	long t2 = clock();
	cout << sum << endl;
	cout << "Run Time: " << t2 - t<<endl;
}


