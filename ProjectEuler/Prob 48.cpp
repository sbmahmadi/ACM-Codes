#include<iostream>
#include<vector>
#include<algorithm>
#include<time.h>

using namespace std;

void multiplication(vector<int> &a, int b)
{
	int t=0;

	for (int j = 0; j < a.size(); j++)
	{
		int buffer = a[j] * b + t;
		a[j] = buffer % 10;
		t = buffer / 10;
	}
	while (t>0)
	{
		a.push_back(t % 10);
		t /= 10;
	}
}

vector<int> selfPow(int a)
{
	vector<int> temp = { 1 };
	
	for (int i = 0; i < a; i++)
	{
		multiplication(temp, a);
		temp.resize(10);
	}
	return temp;
}

void sumation(vector<int> &a,vector<int> &b)
{
	int t = 0;
	vector<int> r;
	int minSize = min(a.size(), b.size());
	int maxSize = max(a.size(), b.size());

	for (int i = 0; i < minSize; i++)
	{
		int buffer = (a[i] + b[i] + t);
		a[i] = buffer % 10;
		t = buffer / 10;
	}

	if (maxSize == a.size())
	{
		for (int i = minSize; i < maxSize; i++)
		{
			int buffer = (a[i] + t);
			a[i] = buffer % 10;
			t = (a[i] + t) / 10;
		}
	}
	else
	{
		for (int i = minSize; i < maxSize; i++)
		{
			a.push_back((b[i] + t) % 10);
			t = (b[i] + t) / 10;
		}
	}

	while (t>0)
	{
		a.push_back(t % 10);
		t /= 10;
	}
}

int main()
{
	vector<int> num;

	clock_t s1 = clock();

	for (int i = 1; i < 1001; i++){
		sumation(num, selfPow(i));
		//cout << i<<endl;
	}

	clock_t s2 = clock();

	for (int i = 0; i < 10; i++) 
		cout << num[9 - i];



	cout <<"Time = "<<s2-s1<< endl;
}