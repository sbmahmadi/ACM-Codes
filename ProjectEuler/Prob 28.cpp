#include<iostream>

using namespace std;

struct Point
{
	int x;
	int y;
};

int main()
{
	long int table[1001][1001] = {};
	Point p;
	p.x = 500; 
	p.y = 500;
	table[p.x][p.y] = 1;
	int i = 2;
	p.x++;

	while (p.x<=1000 || p.y>0)
	{
		do
		{
			table[p.x][p.y] = i;
			p.y++;
			i++;
		} while (table[p.x-1][p.y]!=0);

		do
		{
			table[p.x][p.y] = i;
			p.x--;
			i++;
		} while (table[p.x][p.y-1] != 0);

		do
		{
			table[p.x][p.y] = i;
			p.y--;
			i++;
		} while (table[p.x + 1][p.y] !=0);

		do
		{
			table[p.x][p.y] = i;
			p.x++;
			i++;
			if (p.x == 1001)
			{
				break;
			}
		} while (table[p.x][p.y + 1] != 0);
	}

	long long int sum = 0;

	for (int i = 0; i < 1001; i++)
	{
		sum += table[i][i] + table[1000 - i][i];
	}

	cout << sum - 1;

}