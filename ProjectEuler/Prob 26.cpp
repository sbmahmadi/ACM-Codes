#include<iostream>
#include "D:\Project Euler\BehdadCPPLib.h"
#include <vector>
#include <algorithm>
using namespace std;

int main()
{
	vector<int> fraction;
	int MaxLength = 0, MaxDenom = 0;
	for (int denom = 7; denom < 1000; denom++)
	{
		if (!CheckPrime(denom)) continue;
		int num = 1;

		//Calculate 2xDenominator number of digits

		for (int j = 0; j < (denom-1)*2; j++)
		{
			num *= 10;
			fraction.push_back(num / denom);
			num %= denom;
		}
		
		// Check for Recursion
		
		for (int length = 1; length < denom; length++)
		{
			bool flag = true;
			
			//Check for a Specific-Lengthed Recursion
			
			for (int i = 0; i < length; i++)
			{
			
				// Check for Recursion of each digit

				int p = length + i;
				while (p<fraction.size())
				{
					if (fraction[i] != fraction[p])
					{
						flag = 0;
						break;
					}
					p += length;
				}
				if (!flag) break;
			}

			if (flag)
			{
				if (length > MaxLength)
				{
					MaxLength = length;
					MaxDenom = denom;
				}
				//cout << denom << " : " << length<<endl;
				break;
			}

		}

		fraction.clear();
	}
	cout << MaxDenom;
}


