#include <iostream>
#include<set>
#include<vector>
#include <algorithm>
#include <string>

using namespace std;

bool isPandigital(string s)
{
	int ns[10] = {};

	for (int i = 0; i < s.size(); i++)
	{		
		if (s[i] == '0') return 0;
		ns[s[i] - '0'] = 1;
	}

	for (int i = 1; i <= s.size(); i++)
	{
		if (ns[i] == 0) return 0;
	}
	return 1;
}

int main()
{
	set<int> AllUnusualNums;

	for (int i = 1; i < 3; i++)
	{
		int less = _Pow_int(10, i - 1);
		int more = _Pow_int(10, 4 - i);

		for (int j = less; j < less * 10; j++)
		{
			for (int k = more; k < more * 10; k++)
			{
				string res = to_string(j*k);
				if (res.size() != 4) continue;
				string temp = to_string(j) + to_string(k) + res;
				if (isPandigital(temp)) AllUnusualNums.insert(j*k);
			}
		}
	}

	long long int result = 0;
	set<int>::iterator it;
	for (it = AllUnusualNums.begin(); it != AllUnusualNums.end(); it++)
	{
		result += *it;
	}
	cout << result;
}

/*

1 * 4 = 4
3 * 2 = 4

*/