#include<iostream>
#include<vector>
#include<math.h>

using namespace std;

bool isPrime(int n)
{
	if (n < 4) return true;
	if (n % 2 == 0 || n % 3 == 0) return false;
	for (int i = 5; i <= sqrt(n); i+=6)
		if (n%i == 0 || n % (i + 2) == 0) return false;
	
	return true;
}

int main()
{	
	int sum = 1; int e;

	for (int i = 2; sum < 1000000; i++)
		if (isPrime(i))
		{
			sum *= i;
			e = i;
		}
	
	cout << sum/e << endl;
}