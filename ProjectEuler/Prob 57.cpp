#include<iostream>
#include<vector>
#include<D:/VispaarACMLib.h>
using namespace std;

int digitCount(int n)
{
	int r = 0;
	while (n > 0)
	{
		r++;
		n /= 10;
	}
	return r;
}

int main()
{
	vector<int>a = { 2 };
	vector<int> b = { 1 };
	pair<vector<int>*, vector<int>*> num(&a,&b);
	int res = 0;
	for (int i = 0; i < 1000; i++)
	{
		swap(num.first, num.second);
		*num.first = bigSum(*num.first,*num.second);
		res += num.first->size() > num.second->size();
		*num.first = bigSum(*num.first, *num.second);
	}
	cout << res << endl;
}