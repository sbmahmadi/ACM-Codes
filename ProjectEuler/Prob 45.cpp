#include<iostream>

using namespace std;

long int Triangle(int n)
{
	return (n*(n + 1)) / 2;
}

long int Hexagon(int n)
{
	return n*(2*n - 1);
}

bool isPentagonal(long long int x)
{
	long double temp = (sqrt(24.0 * x + 1.0) + 1.0) / 6.0;
	return (temp == (int)temp);
}

bool isHexagonal(long long int x)
{
	long double temp = (1.0 + sqrt(1.0 + 8.0*x)) / 4.0;
	return (temp == (int)temp);
}

int main()
{
	for (int i = 286;; i++)
	{
		long long int x = /*Triangle*/Hexagon(i);
		if (/*isHexagonal(x) & */isPentagonal(x))
		{
			cout << x<<endl;
			return 0;
		}
	}
}