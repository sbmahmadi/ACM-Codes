#include <iostream>
#include <vector>
#include <time.h>

#define MAX (int)1e8*5

using namespace std;

int main()
{
	int* arr = new int[MAX];
	double t = clock();
	for(int i = 0; i < MAX; i++)
	{
		arr[i] = i;
	}
	t-= clock();
	t*= -1;	
	cout << "Array Manip Time for " << MAX << " Elements: " << t/CLOCKS_PER_SEC << endl;
	
	vector<int> v;
	double t1 = clock();
	for(int i = 0; i < MAX; i++)
	{
		v.push_back(i);
	}
	t1-= clock();
	t1*= -1;
	cout << "Vector Manip Time for " << MAX << " Elements: " << t1/CLOCKS_PER_SEC << endl;
	
}