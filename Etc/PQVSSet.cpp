#include <iostream>
#include <set>
#include <time.h>
#include <queue>

#define MAX (int)1e7*5

using namespace std;


int main()
{
	cout << fixed;
	priority_queue<int> pq;
	set<int> s;
	int k;
	double t1;
	double t = clock();
	
	for(int i = 0; i < MAX; i++)
	{
		s.insert(MAX - i - 1);
	}
	t -= clock();
	t*= -1;
	cout << "Set Push Time for " << MAX << " values: " <<  t / CLOCKS_PER_SEC << endl;
	
	t1 = clock();
	for(int i = 0; i < MAX; i++)
	{
		pq.push(i);
	}
	t1 -= clock();
	t1 *= -1;
	cout << "PQ Push Time for " << MAX << " values: " <<  t1 / CLOCKS_PER_SEC << endl << endl;
	cout << "PQ/Set Push Ratio: " << (1 - t1/t)*100 << endl << endl << endl;
	
	t = clock();
	for(int i = 0; i < MAX; i++)
	{
		*s.begin();
	}
	t -= clock();
	t*= -1;
	cout << "Set GetMin Time for " << MAX << " values: " <<  t / CLOCKS_PER_SEC << endl;
	
	t1 = clock();
	for(int i = 0; i < MAX; i++)
	{
		pq.top();
	}
	t1 -= clock();
	t1 *= -1;
	cout << "PQ GetMin Time for " << MAX << " values: " <<  t1 / CLOCKS_PER_SEC << endl << endl;
	cout << "PQ/Set GetMin Ratio: " << (1 - t1/t)*100 << endl << endl << endl;
	
	t = clock();
	for(int i = 0; i < MAX; i++)
	{
		s.erase(s.begin());
	}
	t -= clock();
	t*= -1;
	cout << "Set ExtractMin Time for " << MAX << " values: " <<  t / CLOCKS_PER_SEC << endl;
		
	t1 = clock();
	for(int i = 0; i < MAX; i++)
	{
		pq.pop();
	}
	t1 -= clock();
	t1 *= -1;
	cout << "PQ ExtractMin Time for " << MAX << " values: " <<  t1 / CLOCKS_PER_SEC << endl << endl;
	cout << "PQ/Set ExtractMin Ratio: " << (1 - t1/t) * 100 << endl << endl << endl;
	
}