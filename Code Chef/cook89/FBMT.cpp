/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0);
#define trace(x) cerr << #x << ": " << x << endl;
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl;
#define _ << " :: " <<
#define allof(x) (x).begin(),(x).end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

int main()
{
	int t;
	cin >> t;
	int n;
	while(cin >> n) {
		string s;
		map<string,int> mp;
		For(i,0,n) {
			cin >> s;
			mp[s]++;
		}
		if(mp.size() == 0) {
			cout << "Draw" << endl;
			continue;
		}
		if(mp.size() == 1) {
			cout << mp.begin()->first << endl;
			continue;
		}

		auto it = mp.begin();
		auto itt = it; itt++;

		if(it->second == itt->second)
			cout << "Draw" << endl;
		else
			cout << (it->second > itt->second ? it->first : itt->first) << endl;
	}	
}