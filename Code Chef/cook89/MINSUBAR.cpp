/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0);
#define trace(x) cerr << #x << ": " << x << endl;
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl;
#define _ << " :: " <<
#define allof(x) (x).begin(),(x).end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

int main()
{
	fastio();
	int t;
	cin >> t;
	int n,d;
	while(t--) {
		cin >> n >> d;
		vi v(n);

		For(i,0,n) {
			cin >> v[i];
		}

		if(d <= 0) {
			bool found = false;
			For(i,0,n) {
				if(v[i] >= d)
					found = true;
			}
			cout << (found ? 1 : -1) << endl;
			continue;
		}

		int res = 0;
		int mx = INT_MIN;
		For(i,0,n) {
			if(res < 0)
				res = 0;
			res += v[i];
			mx = max(mx, res);
		}
		if(mx < d) {
			cout << -1 << endl;
			continue;
		}


		int minsize = n + 1;

		For(i,0,n) {
			if(v[i] <= 0)
				continue;
			int res = 0;

			For(j,i,n) {
				res += v[j];
				if(res >= d) {
					minsize = min(minsize, j - i + 1);
					break;
				}
				if(res <= 0)
					break;
			}
		}

		cout << (minsize == n+1 ? -1 : minsize) << endl;
	}	
}