/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define fastio() ios_base::sync_with_stdio(0); cin.tie(0);
#define trace(x) cerr << #x << ": " << x << endl;
#define traceV(x,t) cerr << #x << ": "; copy(allof(x),ostream_iterator<t>(cerr, " ")); cerr << endl;
#define _ << " :: " <<
#define allof(x) (x).begin(),(x).end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

int main()
{
	int t;
	cin >> t;
	int n;
	while(cin >> n) {
		int v[4] = {};
		For(i,0,n) {
			int a;
			cin >> a;
			v[a%4]++;
		}
		int res = min(v[1], v[3]);
		v[1] -= res;
		v[3] -= res;
		int rem = max(v[1],v[3]);
		v[2] += rem / 2;
		res += rem / 2;
		rem %= 2;
		if(rem || (v[2] & 1))
			cout << -1 << endl;
		else 
			cout << res + v[2] / 2 << endl;
	}	
}