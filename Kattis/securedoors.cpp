#include<iostream>
#include<map>
#include<string>

using namespace std;

int main()
{
	int n;
	cin >> n;
	map<string, bool> mp;
	for (int i = 0; i < n; i++)
	{
		string s1, s2,res;
		cin >> s1 >> s2;
		if (s1 == "entry")
		{
			res = " entered";
			if (mp[s2]) res += " (ANOMALY)";
			mp[s2] = true;
		}
		else
		{
			res = " exited";
			if (!mp[s2]) res += " (ANOMALY)";
			mp[s2] = false;
		}
		cout << s2 << res << endl;
	}
}