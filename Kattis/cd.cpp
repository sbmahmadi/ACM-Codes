#include<iostream>
#include<unordered_set>

using namespace std;

int main()
{
	int n, m;
	while (cin >> n >> m && n)
	{
		unordered_set<int> list;
		for (int i = 0; i < n + m; i++)
		{
			int a;
			cin >> a;
			list.insert(a);
		}
		cout << n + m - list.size() << endl;
	}
}