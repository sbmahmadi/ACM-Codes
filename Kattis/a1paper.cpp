/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <iomanip>
#include <fstream>
#include <limits.h>
#include <time.h>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int main()
{
	/*ifstream fin("file.in");
	ofstream fout("file.out");*/
	cout << fixed << setprecision(12);
	int n;
	cin >> n;
	vector<int> papers(n-1);
	For(i,0,n-1)
	{
		cin >> papers[i];
	}
	int t = 2;
	bool f = false;
	For(i,0,n-1)
	{
		if(papers[i]>=t)
		{
			papers[i] = t;
			papers.resize(i+1);
			f = true;
			break;
		}
		else 
			t = (t-papers[i]) * 2;
	}
	if(!f)
		cout<<"impossible"<<endl;
	else
	{
		double a = pow(2,-5.0/4.0),b = pow(2,-3.0/4.0), sum = -(a+a+b);
		For(i,0,papers.size())
		{
			sum+= papers[i] * (a+b);
			double t = a;
			a = b/2.0;
			b = t;
		}
		cout << sum << endl;
	}
	
}
