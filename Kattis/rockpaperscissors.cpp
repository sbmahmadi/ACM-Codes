#include<iostream>
#include<string>
#include<string.h>

using namespace std;
// 1 = p1   -1 = p2
int play(string p1, string p2)
{
	if (p1 == p2) return 0;
	if (p1 == "paper" && p2 == "scissors" || p2 == "paper" && p1 == "scissors")
	{
		return p1 > p2 ? 1 : -1;
	}
	return p1 < p2 ? 1 : -1;
}
//0 = lose	1 = win
int players[110][2];

int main()
{
	int n, k;
	while (cin >> n && n)
	{
		cin >> k;
		memset(players, 0, sizeof players);
		int pi1, pi2;
		string p1, p2;
		for (int i = 0; i < n * (n-1) * k / 2; i++)
		{
			cin >> pi1 >> p1 >> pi2 >> p2;
			int res = play(p1, p2);
			if (res != 0)
			{
				players[pi1][res>0 ? 1 : 0]++;
				players[pi2][res<0 ? 1 : 0]++;
			}
		}
		for (int i = 1; i <= n; i++)
		{
			if (players[i][0] + players[i][1] == 0)
			{
				cout << "-" << endl;
			}
			else printf("%.3f\n", 1.0 * players[i][1] / (players[i][0] + players[i][1]));
		}
		cout << endl;
	}
}