#include<iostream>
#include<string>
#include<vector>
#include<math.h>

using namespace std;

vector<pair<int, int>> queens;

int main()
{
	for (int i = 0; i < 8; i++)
	{
		string s;
		cin >> s;
		for (int j = 0; j < 8; j++)
		{
			if (s[j] == '*')
			{
				for (int k = 0; k < queens.size(); k++)
				{
					if (queens[k].second == j || queens[k].first == i || abs(queens[k].second - j) == abs(queens[k].first - i))
					{
						cout << "invalid";
						return 0;
					}
				}
				queens.push_back(make_pair(i, j));
			}
		}
	}
	if (queens.size() == 8)
	{
		cout << "invalid";
		return 0;
	}
	cout << "valid";
}