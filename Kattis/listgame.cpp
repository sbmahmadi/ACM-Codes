#include<iostream>

using namespace std;

typedef long long ll;

int main()
{
	ll n;
	cin >> n;
	ll temp = n;


	ll i = 5, res = 0;
	
	while (true)
	{
		if (n % 2 == 0)
		{
			n /= 2;
			res++;
		}
		else if (n % 3 == 0)
		{
			n /= 3;
			res++;
		}
		else break;
	}
	bool two = true;
	temp = sqrt(temp);
	while (n != 1 && (i <= temp || res != 0))
	{
		if (n%i == 0)
		{
			n /= i;
			res++;
		}
		else
		{
			i += two ? 2 : 4;
			two = !two;
		}
	}
	cout << (res > 0 ? res : 1) << endl;
}