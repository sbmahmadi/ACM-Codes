#include<iostream>
#include<algorithm>
#include<vector>

using namespace std;

int pw(int a,int b)
{
	int res = 1;
	while(b--)
	{
		res*=a;
		res%=10;
	}
	return res;
}

int fact[5] = {1,1,2,6,24};
int dp[1000050];

int f(int n)
{
	if(dp[n] != 0) 
		return dp[n];
	return dp[n] = (f(n/5) * pw(2,n/5) * fact[n%5]) % 10;
}


int main()
{
	ios_base::sync_with_stdio(0);
	cin.tie(0);
	dp[0] = 1;
	int n;
	while(cin >> n && n)
	{
		cout<<f(n)<<endl;
	}
}     		