#include<iostream>
#include<algorithm>
#include<vector>
#include<string>

#define allof(n) n.begin(),n.end()

using namespace std;

const bool cmp(const int a,const int b)
{
	return a > b;
}

int main()
{
	int n;
	cin >> n;
	for (int i = 1; i <= n; i++)
	{
		int n1;
		cin >> n1;
		vector<int> red, blue;

		for (int j = 0; j < n1; j++)
		{
			string s;
			cin >> s;
			if (s.back() == 'B')
			{
				s.pop_back();
				blue.push_back(stoi(s));
			}
			if (s.back() == 'R')
			{
				s.pop_back();
				red.push_back(stoi(s));
			}
		}
		sort(allof(red), cmp);
		sort(allof(blue), cmp);

		int sum = 0;

		for (int j = 0; j < min(red.size(), blue.size()); j++)
		{
			sum += red[j] + blue[j] - 2;
		}
		printf("Case #%d: %d\n", i, sum);
	}
}