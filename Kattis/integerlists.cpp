#include<iostream>
#include<vector>
#include<string>
#include<sstream>

using namespace std;

void run(string cmd, vector<int> &v)
{
	auto it1 = v.begin(), it2 = v.end();

	bool flag = 0;

	for (int i = 0; i < cmd.size(); i++)
	{
		if (cmd[i] == 'D')
		{
			if (it1 == it2)
			{
				cout << "error" << endl;
				return;
			}
			if (!flag)
			{
				it1++;
			}
			else
			{
				it2--;
			}
		}
		else
		{
			flag = !flag;
		}
	}

	cout << '[';
	int a = 0;

	while (it1 != it2)
	{
		if (a++ != 0) cout << ',';
		if (!flag)
		{
			cout << *(it1++);
		}
		else
		{
			cout << *(--it2);
		}
	}
	cout << ']' << endl;
}

int main()
{
	int t;
	cin >> t;
	while (t--)
	{
		string commands;
		cin >> commands;
		
		int n;
		cin >> n;
		
		vector<int> v;
		string list;
		cin >> list;
		for (int i = 0; i < list.size(); i++)
		{
			if (list[i] == ',' || list[i] == '[' || list[i] == ']')
			{
				list[i] = ' ';
			}
		}
		stringstream ss(list);
		int a;
		while (ss >> a)
		{
			v.push_back(a);
		}
		run(commands, v);
	}
}