#include<iostream>

using namespace std;

int main()
{
	int max = 0, index = 0;
	for (int i = 0; i < 5; i++)
	{
		int mx = 0;
		for (int j = 0; j < 4; j++)
		{
			int k;
			cin >> k;
			mx += k;
		}
		if (mx > max)
		{
			max = mx;
			index = i+1;
		}
	}
	printf("%d %d\n", index, max);
}