#include<iostream>

typedef unsigned long long ull;

using namespace std;

int main()
{
	ull a, b;
	while (cin >> a >> b)
	{
		cout << (a < b ? b - a : a - b) << endl;
	}
}