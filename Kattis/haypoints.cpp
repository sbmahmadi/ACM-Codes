#include<iostream>
#include<map>
#include<string>

using namespace std;

int main()
{
	int n, m;
	cin >> m >> n;
	map<string, int> dic;
	for (int i = 0; i < m; i++)
	{
		string s;
		int d;
		cin >> s >> d;
		dic[s] = d;
	}
	string word;
	for (int i = 0; i < n; i++)
	{
		long val = 0;
		while (cin >> word && word != ".")
		{
			if (dic.find(word) != dic.end())
			{
				val += dic[word];
			}
		}
		cout << val << endl;
	}
}