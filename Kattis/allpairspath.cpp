#include<iostream>
#include<vector>
#include<queue>
#include<algorithm>

#define For(i,a,b) for(int i = a;i<b;i++)

using namespace std;

typedef vector<int> vi;

int main()
{
	int n, m, q, a, b, w, u, v;

	while (cin >> n >> m >> q && n)
	{
		vector<vi> graph(n,vi(n, 1e8));
		For(i, 0, m)
		{
			cin >> a>> b>> w;
			graph[a][b] = w;
		}
		For(i,0,n)
			graph[i][i] = 0;
		For(k,0,n)
			For(i,0,n)
				For(j,0,n)
					graph[i][j] = min(graph[i][j], graph[i][k] + graph[k][j]);

		For(i, 0, q)
		{
			cin >> u >> v;
			if(u == v)
				cout << 0 << endl;
			else if(graph[u][v] == 1e8)
				cout << "Impossible" << endl;
			else if(graph[u][u] < 0 || graph[v][v] < 0)
				cout << "-Infinity" << endl;
			else cout << graph[u][v] << endl;	
		}
		cout << endl;
	}
}