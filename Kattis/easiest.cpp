#include<iostream>

using namespace std;

int digitSum(int a)
{
	int res = 0;
	while (a > 0)
	{
		res += a % 10;
		a /= 10;
	}
	return res;
}


int main()
{
	int n;
	while (cin >> n && n)
	{
		int m = digitSum(n);
		int i = 11;
		while (digitSum(i++*n) != m);
		cout << i - 1 << endl;
	}
}