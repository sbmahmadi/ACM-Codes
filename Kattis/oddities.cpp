#include<iostream>
#include<math.h>
using namespace std;

int main()
{
	int n;
	cin >> n;
	for (int i = 0; i < n; i++)
	{
		int k;
		cin >> k;
		printf("%d is %s\n", k, abs(k) % 2 == 0 ? "even" : "odd");
	}
}