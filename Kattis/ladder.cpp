#include<iostream>
#include<math.h>

#define PI 3.14159265358979323846

using namespace std;

int main()
{
	int h, v;
	cin >> h >> v;
	double rad = v * PI * 2.0 / 360.0;
	int l = ceil((1.0 * h / sin(rad)) + (1e-4));
	

	cout << l << endl;
	
}