/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

vector<vi> g;
vi dp;

int dfs(int ind){
	if(dp[ind] >= 0)
		return dp[ind];
	int mx = 0;
	For(i, 0, g[ind].size()){
		mx = max(mx, dfs(g[ind][i]));
	}
	return dp[ind] = mx +1;
}

int main()
{
	int n,m;
	cin >> n >> m;
	int mx = 0;
	g.resize(n);
	dp.assign(n,-1);
	For(i,0,m){
		int a,b;
		cin >> a >> b; a--; b--;
		g[a].push_back(b);
	}
	
	For(i,0,n){
		mx = max(mx, dfs(i));
	}
	cout << mx << endl;
}