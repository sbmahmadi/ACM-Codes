#include<iostream>

using namespace std;

int main()
{
	int n,res = 0;
	cin >> n;
	while (n)
	{
		if (n & 1)
			res |= 1;
		res <<= 1;
		n >>= 1;
	}
	cout << (res >> 1) << endl;
}