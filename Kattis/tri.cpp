#include<iostream>

using namespace std;

char process(int a, int b, int c)
{
	if (a / b == c) return '/';
	if (a + b == c) return '+';
	if (a - b == c) return '-';
	if (a * b == c) return '*';
	return 'n';
}

int main()
{
	int a, b, c;
	cin >> a >> b >> c;
	char ch;
	if ((ch = process(a, b, c)) != 'n')
	{
		printf("%d%c%d=%d\n", a, ch, b, c);
	}
	else printf("%d=%d%c%d\n", a, b, process(b, c, a), c);
}