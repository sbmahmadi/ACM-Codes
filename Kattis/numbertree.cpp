#include<iostream>
#include<queue>
#include<string>
#include<math.h>

using namespace std;

typedef long long ll;

int main()
{
	int h;
	string s = "";
	cin >> h;
	if (!(cin >> s))
	{
		cout << pow(2, h + 1) - 1 << endl;
		return 0;
	}
	
	ll v = 0;
	for (int i = 0; i < s.size(); i++)
	{
		v |= s[i] == 'R' ? 1 : 0;
		v <<= 1;
	}
	v >>= 1;
	v += pow(2, s.size());
	cout << pow(2,h+1) - v << endl;
}