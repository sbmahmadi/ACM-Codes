import sys
for line in sys.stdin:
	n = int(line) - 1
	if n == -1:
		break

	res = "{"
	k = 0
	while(n > 0):
		if n%2 == 1:
			res += " " + str(3**k) + ","
		k+=1
		n//=2
	if res == "{":
		res = "{ }"
	else:
		res = res[0:-1] + " }"
	print(res)