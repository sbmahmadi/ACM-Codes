#include<iostream>
#include<vector>
#include<algorithm>

using namespace std;

int main()
{
	int t;
	cin >> t;
	while (t--)
	{
		int l, n;
		cin >> l >> n;
		vector<int>ants;
		for (int i = 0; i < n; i++)
		{
			int t;
			cin >> t;
			ants.push_back(t);
		}

		int mx = l - min(ants.front(), l - ants.back());
		double a = n / 2.0;
		int mn = 1000100;
		for (int i = 0; i < n; i++)
		{
			if (abs(a - ants[i]) < mn) mn = abs(a - ants[i]);
		}
		mn = a - mn;
		printf("%d %d\n", mn, mx);
	}
}