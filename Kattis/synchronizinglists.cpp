#include<iostream>
#include<unordered_map>
#include<vector>
#include<algorithm>

#define For(i,a,b) for(int i = a;i<b;i++)

using namespace std;

int main()
{
	int n;
	int c = 0;
	while (cin >> n && n)
	{
		if (c++ != 0)
		{
			cout << endl;
		}
		vector <int> a,b,temp_a;
		int t;
		For(i, 0, n)
		{
			cin >> t;
			a.push_back(t);
		}
		For(i, 0, n)
		{
			cin >> t;
			b.push_back(t);
		}
		temp_a.assign(a.begin(),a.end());
		sort(temp_a.begin(), temp_a.end());
		sort(b.begin(), b.end());
		unordered_map<int, int> m;
		For(i, 0, n)
		{
			m[temp_a[i]] = b[i];
		}
		For(i, 0, n)
		{
			printf("Res: %d\n", m[a[i]]);
		}
	}
}