#include<iostream>
#include<vector>
#include<stack>
#include<math.h>

using namespace std;

typedef vector<vector<int>> vvi;

bool dfs(int start, int end, vvi &graph, bool* visited)
{
	stack<int> st;
	st.push(start);
	while (!st.empty())
	{
		start = st.top();
		if (start == end) return true;
		st.pop();
		if (!visited[start])
		{
			visited[start] = true;
			for (int i = 0; i < graph[start].size(); i++)
			{
				st.push(graph[start][i]);
			}
		}
	}
	return false;
}

int main()
{
	int t;
	cin >> t;
	while (t--)
	{
		int n;
		cin >> n;
		vvi graph;
		bool visited[110];
		vector<pair<int, int>> poses;
		for (int i = 0; i < n + 2; i++)
		{
			visited[i] = false;
			vector<int> node;
			int x, y;
			cin >> x >> y;
			poses.push_back(make_pair(x, y));
			for (int j = 0; j < poses.size() - 1; j++)
			{
				if (abs(poses[j].first - poses.back().first) + abs(poses[j].second - poses.back().second) <= 1000)
				{
					graph[j].push_back(i);
					node.push_back(j);
				}
			}
			graph.push_back(node);
		}
		if (dfs(0, n + 1, graph, visited)) cout << "happy" << endl;
		else cout << "sad" << endl;
	}
}