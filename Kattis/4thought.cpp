/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <iostream>
#include <vector>
#include <queue>
#include <deque>
#include <math.h>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <string.h>
#include <iomanip>
#include <fstream>
#include <limits.h>
#include <time.h>

#define For(i,a,b) for(int i = a;i<b;i++)
#define roF(i,b,a) for(int i = b;i>=a;i--)
#define trace(x) cout<<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int n;
vector<char> v;
bool found;
void f(int t,int s)
{
	if(s==3)
	{
		if(t==n)
			found = true;
		return;
	}
	
	v.push_back('*');
	f(t*4,s+1);
	if(found) return;
	v.pop_back();

	v.push_back('/');
	f(t/4,s+1);
	if(found) return;
	v.pop_back();

	v.push_back('+');
	f(t+4,s+1);
	if(found) return;
	v.pop_back();

	v.push_back('-');
	f(t-4,s+1);
	if(found) return;
	v.pop_back();

	
}

int main()
{
	/*ifstream fin("file.in");
	ofstream fout("file.out");*/
	int t;
	cin >> t;
	while(t--)
	{
		v.clear();
		found = false;
		cin >> n;
		f(4,0);
		if(v.size() > 0)
		{
			cout<<4;
			For(i,0,3)
				cout << " "<<v[i]<<" "<<4;
			cout << " = " << n << endl;
		}
		else cout << "no solution" << endl;
	}
}