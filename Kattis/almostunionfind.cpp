#include<iostream>
#include<vector>
#include<map>
#include<unordered_set>
#include<set>
#include<algorithm>
#include<list>

typedef long long ll;

using namespace std;

struct Node
{
	Node * parent;
	int elemCount;
	int sum;
	Node(int n)
	{
		elemCount = 1;
		sum = n;
		parent = this;
	}
};

struct DS
{
	vector<Node> nodes;
	Node* findRoot(Node* n)
	{
		if (n->parent == n) return n;
		else return n->parent = findRoot(n->parent);
	}

	void f1(int p, int q)
	{
		Node* pRoot = findRoot(&nodes[p]);
		Node* qRoot = findRoot(&nodes[q]);
		if (qRoot != pRoot)
		{
			pRoot->parent = qRoot;
			qRoot->sum += pRoot->sum;
			qRoot->elemCount += pRoot->elemCount;
		}
	}
	void f2(int p, int q)
	{
		Node* pRoot = findRoot(&nodes[p]);
		Node* qRoot = findRoot(&nodes[q]);
		if (qRoot != pRoot)
		{
			nodes[p].parent = qRoot;
			qRoot->sum += p;
			pRoot->sum -= p;
			qRoot->elemCount++;
			pRoot->elemCount--;
		}
	}

	void f3(int p)
	{
		Node* pRoot = findRoot(&nodes[p]);
		printf("%d %d\n", pRoot->elemCount, pRoot->sum);
	}
};

int main()
{
	int n, m;
	while (cin >> n >> m)
	{
		DS union_find;
		for (int i = 0; i <= n; i++)
		{
			union_find.nodes.push_back(*(new Node(i)));
		}

		int command, p, q;
		
		for (int i = 0; i < m; i++)
		{
			cin >> command;
			if (command == 3)
			{
				cin >> p;
				union_find.f3(p);
			}
			else if (command == 2)
			{
				cin >> p >> q;
				union_find.f2(p, q);
			}
			else if (command == 1)
			{
				cin >> p >> q;
				union_find.f1(p, q);
			}
		}
	}
}