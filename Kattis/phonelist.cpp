#include<iostream>
#include<string>
#include<algorithm>
#include<vector>

using namespace std;

struct Node
{
	Node(char c) : value(c){}
	char value;
	vector<Node> childs;
};

bool add(string s, Node* temp)
{
	bool flag = false;
	while (!s.empty())
	{
		bool f2 = false;
		for (int j = 0; j < temp->childs.size(); j++)
		{
			if (temp->childs[j].value == s.back())
			{
				temp = &temp->childs[j];
				s.pop_back();
				f2 = true;
				break;
			}
		}
		if (f2) continue;
		if (temp->childs.size() == 0 && !flag) return false;
		else
		{
			temp->childs.push_back(Node(s.back()));
			s.pop_back();
			temp = &temp->childs.back();
			flag = true;
		}
	}
	return flag;
}

int main()
{
	int t;
	cin >> t;
	for (int i = 0; i < t; i++)
	{
		Node trie(NULL);
		int n;
		cin >> n;
		string s;
		bool m = 0;
		cin >> s;
		Node* p = &trie;
		for (int i = 0; i < s.size(); i++)
		{
			p->childs.push_back(s[i]);
			p = &p->childs[0];
		}
		for (int j = 1; j < n; j++)
		{

			cin >> s;
			reverse(s.begin(), s.end());

			if (!m && !add(s, &trie))
			{
				cout << "NO" << endl;
				m = 1;
			}
		}
		if (!m) cout << "YES" << endl;
	}
}