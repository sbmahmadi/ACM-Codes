#include<iostream>
#include<vector>
#include<algorithm>
#include<string>

using namespace std;

int main()
{
	int n;
	cin >> n;
	vector<string> vs(n);
	cin >> vs[0];
	bool inc = true;
	bool dec = true;
	for (int i = 1; i < n; i++)
	{
		cin >> vs[i];
		if (vs[i] > vs[i - 1]) dec = false;
		if (vs[i] < vs[i - 1]) inc = false;
	}
	if (dec)
		cout << "DECREASING" << endl;
	else if (inc)
		cout << "INCREASING" << endl;
	else cout << "NEITHER" << endl;
}