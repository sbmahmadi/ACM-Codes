#include<iostream>
#include<vector>
#include<algorithm>

#define allof(a) a.begin(),a.end()

using namespace std;

int main()
{
	int n;
	cin >> n;
	for (int j = 0; j < n;j++)
	{
		vector<long long> gs;
		int g;
		cin >> g;
		for (int i = 0; i < g; i++)
		{
			long long k;
			cin >> k;
			gs.push_back(k);
		}
		sort(allof(gs));
		bool f = false;
		for (int i = 0; i < g-1; i++)
		{
			if (gs[i] == gs[i + 1]) i++;
			else
			{
				printf("Case #%d: %d\n", j + 1, gs[i]);
				f = true;
				break;
			}
		}
		if (!f) printf("Case #%d: %d\n", j + 1, gs.back());
	}
}