#include<iostream>
#include<vector>

using namespace std;

typedef vector<vector<int>> vvi;
typedef vector<int> vi;

#define For(i,a,b) for(int i = a; i<b; i++)

char f(int i,vvi &graph,char* vals)
{
	For(j, 0, graph[i].size())
	{
		if (vals[graph[i][j]] == 'p') return 'h';
		if (vals[graph[i][j]] == 'h') return 'p';
	}
	return 'p';
}


int main()
{
	int n, m;
	cin >> n >> m;
	vvi graph;
	graph.assign(n, vi());
	For(i, 0, m)
	{
		int a, b;
		cin >> a >> b;
		graph[a - 1].push_back(b - 1);
		graph[b - 1].push_back(a - 1);
	}
	char* vals = new char[n];
	For(i, 0, n)
	{
		if (graph[i].size() == 0)
		{
			cout << "Impossible" << endl;
			return 0;
		}
		vals[i] = 'n';
	}
	vals[0] = 'p';
	For(i, 1, n)
	{
		vals[i] = f(i, graph, vals);
	}

	For(i, 0, n)
	{
		cout << (vals[i] == 'p' ? "pub" : "house");
		if (i != n - 1) cout << " ";
	}
	cout << endl;
}