#include<iostream>
#include<vector>
#include<algorithm>

using namespace std;

typedef long long ll;

int main()
{
	int k;
	k = 50;

	ll l, d, n;
	cin >> l >> d >> n;
	if (n == 0)
	{
		cout << (l - 12) / d + 1 << endl;
		return 0;
	}
	vector<long long> birds;
	birds.assign(n, 0);

	for (int i = 0; i < n; i++)
	{
		cin >> birds[i];
	}

	sort(birds.begin(), birds.end());

	ll sum = 0;
	for (int i = 0; i < n - 1; i++)
	{
		sum += (birds[i + 1] - birds[i]) / d - 1;
	}

	sum += (birds[0] - 6) / d;
	sum += ((l - 6) - birds.back()) / d;

	cout << sum << endl;
}