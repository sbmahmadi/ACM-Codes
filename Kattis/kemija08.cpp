#include<iostream>
#include<string>
#include<algorithm>

using namespace std;

int main()
{
	string s;
	getline(cin,s);
	char a[] = { 'a', 'e', 'o', 'i', 'u' };

	for (int i = 0; i < s.size(); i++)
	{
		if (find(a, a + 5, s[i]) != a + 5)
		{
			s.erase(i+1, 2);
		}
	}
	cout << s << endl;
}