#include<iostream>
#include<vector>
#include<string.h>
#include<algorithm>
#include<stack>

#define For(i,a,b) for(int i = a;i<b;i++)

using namespace std;

typedef vector<vector<int>> vvi;

int dp;
bool visited[10100] = {};

void dfs(int start, vvi &graph,int depth)
{
	int child = 0;
	visited[start] = true;

	For(i, 0, graph[start].size())
	{
		if (!visited[graph[start][i]])
		{
			child++;
			dfs(graph[start][i], graph, depth + 1);
		}
	}
	if (child == 0 && depth > dp) dp = depth;
	visited[start] = false;
}

void dfs1(int start, vvi &graph, int depth)
{
	int child = 0;
	visited[start] = true;

	For(i, 0, graph[start].size())
	{
		if (!visited[graph[start][i]])
		{
			child++;
			dfs(graph[start][i], graph, depth + 1);
		}
	}
	if (child == 0 && depth > dp) dp = depth;
	visited[start] = false;
}

int main()
{
	int n, d, m;
	cin >> n >> d >> m;
	int arr[10100];
	For (i,0,n)
	{
		int a;
		cin >> a;
		arr[i] = a;
	}
	vvi graph;
	graph.assign(n, {});
	For (i,0,n)
	{
		For(j, i+1, i + d + 1)
		{
			if (j>= n) break;
			if (abs(arr[j] - arr[i]) <= m)
			{
				graph[i].push_back(j);
				graph[j].push_back(i);
			}
		}
	}	
	int maxele = 0;
	For(i, 0, n)
	{
		dp = 0;
		dfs(i, graph, 1);
		if (dp > maxele) maxele = dp;
	}
	cout << maxele << endl;
}