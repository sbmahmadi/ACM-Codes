/*
ID: behdad.1
LANG: C++11
PROB: 
*/

#include <bits/stdc++.h>

#define For(i,a,b) for(int i = a; i < b; i++)
#define Fori(i,s) for(auto i = s.begin(); i != s.end(); i++)
#define roF(i,b,a) for(int i = b; i >= a; i--)
#define roFi(i,s) for(auto i = s.rbegin(); i != s.rend(); i++)
#define trace(x) cerr <<#x<<": "<<x<<endl;
#define _ <<" :: "<<
#define allof(x) x.begin(),x.end()
#define X real()
#define Y imag()

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

int main()
{
	int m;
	while(cin >> m && m)
	{
		int a,b;
		int h = 0, w = 0;
		int mxw = 0;
		int th = 0;
		while(cin >> a >> b && a != -1)
		{
			if(w+a > m)
				mxw = max(mxw, w), w = 0, th += h, h = 0;
			w += a;
			h = max(h,b);
		}
		mxw = max(mxw, w), th += h;
		cout << mxw << " x " << th << endl;
	}	
}
