#include<iostream>
#include<string>

using namespace std;

int main()
{
	string s;
	int t;
	cin >> t;
	cin.ignore();
	while (t--)
	{
		getline(cin, s);
		if (s.size() > 12 && s.substr(0, 10) == "simon says")
		{
			s.erase(0, 11);
			cout << s;
		}
		cout << endl;
	}
}